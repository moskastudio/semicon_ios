//
//  ExhibitionNewsDetailViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitionNewsDetailViewController.h"

@interface ExhibitionNewsDetailViewController ()

@end

@implementation ExhibitionNewsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([_navBartitle isEqualToString:@"MediaPartners"]) {
        self.title = NSLocalizedString(@"Media Partners", "");
    }
    else if ([_navBartitle isEqualToString:@"ExhibitorPRs"]){
        self.title = NSLocalizedString(@"Exhibitor PRs", "");
    }
    else if ([_navBartitle isEqualToString:@"PressRelease"]){
        self.title = NSLocalizedString(@"Press Release", "");
    }
    
    NSLog(@"%@",_newsTitle);
    
    if ([_navBartitle isEqualToString:@"MediaPartners"]) {
        UILabel *mediaPartnerLabel=[[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 300)];
        [mediaPartnerLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        mediaPartnerLabel.textColor = [UIColor blackColor];
        mediaPartnerLabel.lineBreakMode = NSLineBreakByWordWrapping;
        mediaPartnerLabel.numberOfLines = 0;
        mediaPartnerLabel.textAlignment = NSTextAlignmentLeft;
        NSString *mediaPartnerString = _newsTitle;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:mediaPartnerString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [mediaPartnerString length])];
        mediaPartnerLabel.attributedText = attributedString ;
        mediaPartnerLabel.adjustsFontSizeToFitWidth = NO;
        mediaPartnerLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [mediaPartnerLabel sizeToFit];
        [mediaPartnerLabel setFrame:CGRectMake(mediaPartnerLabel.frame.origin.x, mediaPartnerLabel.frame.origin.y, mediaPartnerLabel.frame.size.width, mediaPartnerLabel.frame.size.height+5)];
        [self.view addSubview:mediaPartnerLabel];
        
        UILabel *websiteLabel= [[UILabel alloc] initWithFrame:CGRectMake(27, mediaPartnerLabel.frame.origin.y+mediaPartnerLabel.frame.size.height+24, self.view.frame.size.width-54, 30)];
        [websiteLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        websiteLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        websiteLabel.textAlignment = NSTextAlignmentLeft;
        websiteLabel.text = _partnerWebsite;
        websiteLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(didTapLabelWithGesture:)];
        [websiteLabel addGestureRecognizer:tapGesture];
        [self.view addSubview:websiteLabel];
        
        _webView.hidden = YES;
    }
    else{
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:self.partnerWebsite]];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.autoresizesSubviews = YES;
        [_webView.scrollView setScrollEnabled:YES];
        [_webView loadRequest:request];
        _webView.hidden = NO;
        [self.view addSubview:_webView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    NSLog(@"tapGesture,%@",self.partnerWebsite);
    NSString *str = _partnerWebsite;
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *result = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSURL *url = [NSURL URLWithString:result];
    [[UIApplication sharedApplication] openURL: url];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
