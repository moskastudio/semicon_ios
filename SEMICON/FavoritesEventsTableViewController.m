//
//  FavoritesEventsTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FavoritesEventsTableViewController.h"
#import "EvevtActiviteIntroTableViewController.h"

@interface FavoritesEventsTableViewController ()
@property NSArray *titleArray;
@property NSArray *timeArray;
@property NSMutableArray *eventFarvorites;
@end

@implementation FavoritesEventsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _eventFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"TechXPOTFarvorites"]];
    
//    self.titleArray = [[NSArray alloc] initWithObjects:@"Test test 123 321",@"Laser Seminar",@"Sustainable Manufacturing Forum", nil];
//    self.timeArray = [[NSArray alloc] initWithObjects:@"Sep. 7 12:00~13:00",@"Sep. 8 12:00~17:00",@"Sep. 9 10:00~12:00", nil];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _eventFarvorites.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FavoritesEventsCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }

    UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(35,25,self.view.frame.size.width-139,16)];
    [titleLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    titleLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    titleLabel.text = [[_eventFarvorites objectAtIndex:indexPath.row] objectForKey:@"topic"];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    [titleLabel setNumberOfLines:1];
    titleLabel.adjustsFontSizeToFitWidth = NO;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:titleLabel];
    
    UILabel *timeLabel=[[UILabel alloc]initWithFrame:CGRectMake(35,49,self.view.frame.size.width-139,14)];
    [timeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
    timeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
    timeLabel.text = [NSString stringWithFormat:@"%@", [[_eventFarvorites objectAtIndex:indexPath.row] objectForKey:@"date"]];
    timeLabel.textAlignment = NSTextAlignmentLeft;
    [timeLabel setNumberOfLines:1];
    timeLabel.adjustsFontSizeToFitWidth = NO;
    timeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:timeLabel];
    
    UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    moreBtn.frame = CGRectMake(cell.frame.size.width-55, 0, 40, 88);
    UIImage *btnImage = [UIImage imageNamed:@"btn_myfavorite_more_grey.png"];
    [moreBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:moreBtn];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *activity = [_eventFarvorites objectAtIndex:indexPath.row];
    
    [_delegate showEvent:activity dataSource:@"TechXPOT"];
}

- (void)moreBtnAction:(id)sender {
    NSLog(@"moreBtnAction");
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* shareBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Share", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSLog(@"shareBtn");
                                  [self share];
                              }];
    
    UIAlertAction* deleteBtn = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Delete", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    NSLog(@"Delete");
                                    [_eventFarvorites removeObjectAtIndex:indexPath.row];
                                    [[NSUserDefaults standardUserDefaults] setObject:_eventFarvorites forKey:@"TechXPOTFarvorites"];
                                    
                                    [self.tableView beginUpdates];
                                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                    [self.tableView endUpdates];
                                }];
    
    [alert addAction:Cancel];
//    [alert addAction:shareBtn];
    [alert addAction:deleteBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) share{
    NSLog(@"SHARE");
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* copyLinkBtn = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Copy Link", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                  }];
    
    UIAlertAction* fbBtn = [UIAlertAction
                            actionWithTitle:NSLocalizedString(@"Facebook", "")
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                            }];
    UIAlertAction* twitterBtn = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Twitter", "")
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action)
                                 {
                                 }];
    
    UIAlertAction* googlePlusBtn = [UIAlertAction
                                    actionWithTitle:NSLocalizedString(@"Google+", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                    }];
    UIAlertAction* mailBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Mail", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                              }];
    
    UIAlertAction* evernoteBtn = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Evernote", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                  }];
    
    [copyLinkBtn setValue:[[UIImage imageNamed:@"img_copy_link.png"]
                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [fbBtn setValue:[[UIImage imageNamed:@"img_fb_share.png"]
                     imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [twitterBtn setValue:[[UIImage imageNamed:@"img_twitter_share.png"]
                          imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [googlePlusBtn setValue:[[UIImage imageNamed:@"img_google_plus_share.png"]
                             imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [mailBtn setValue:[[UIImage imageNamed:@"img_mail_share.png"]
                       imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];
    [evernoteBtn setValue:[[UIImage imageNamed:@"img_evernote_share.png"]
                           imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] forKey:@"image"];

    [alert addAction:Cancel];
    [alert addAction:copyLinkBtn];
    [alert addAction:fbBtn];
    [alert addAction:twitterBtn];
    [alert addAction:googlePlusBtn];
    [alert addAction:mailBtn];
    [alert addAction:evernoteBtn];
    [self presentViewController:alert animated:YES completion:nil];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
