//
//  HomeViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "HomeViewController.h"
#import "MainMenuViewController.h"
#import "MediaCenterViewController.h"
#import "ExhibitorListViewController.h"
#import "PavilionsViewController.h"
#import "EvevtActivitesViewController.h"
#import "ProgramTableViewController.h"
#import "RegisterViewController.h"
#import "Service.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface HomeViewController () <MainMenuViewControllerDelegate> {
    NSArray *adArray;
}
@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    //-------ADSlider Satrt
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            adArray = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"FrontpageBannerLg"]];
            NSMutableArray *imageUrls = [[NSMutableArray alloc] init];
            for (NSDictionary *ad in adArray) {
                if (![[ad objectForKey:@"imgUrl"] isEqualToString:@""]) {
                    [imageUrls addObject:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
                }
            }
            
            MCLScrollViewSlider *localScrollView = [MCLScrollViewSlider scrollViewSliderWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 432*self.view.frame.size.width/1080) imageURLs:imageUrls placeholderImage:[UIImage imageNamed:@"img_action_bar_bg.png"]];
            localScrollView.pageControlCurrentIndicatorTintColor = [UIColor whiteColor];
            [self.view addSubview:localScrollView];
            UIImageView *topBackImageView = [self.view viewWithTag:99];
            [self.view bringSubviewToFront:topBackImageView];
            
            [localScrollView didSelectItemWithBlock:^(NSInteger clickedIndex) {
                NSLog(@"Clicked main_ad_%ld", (long)clickedIndex+1);
                [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                                parameters:@{ FBSDKAppEventParameterNameContentID:[NSString stringWithFormat:@"main_ad_%ld", (long)clickedIndex+1],
                                              FBSDKAppEventParameterNameDescription:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"] } ];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"]]];
            }];
        }
    }];
    //-------ADSlider End
    
    CGFloat height = 432 * self.view.frame.size.width / 1080;
    
    //九宮格
    MainMenuViewController* mainMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainMenuViewController"];
    mainMenuViewController.delegate = self;
    [self addChildViewController:mainMenuViewController];
    [mainMenuViewController didMoveToParentViewController:self];
    mainMenuViewController.view.frame = CGRectMake(0,height,self.view.bounds.size.width,self.view.frame.size.height-height-15);
    mainMenuViewController.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:mainMenuViewController.view];
    
    UIImageView *topBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,0,self.view.frame.size.width,39*self.view.frame.size.width/1080)];
    topBackground.tag = 99;
    UIImage *topBackgroundImage = [UIImage imageNamed:@"img_tab_bar_bg.png"];
    topBackground.image = topBackgroundImage;
    [self.view addSubview:topBackground];
    
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    NSLog(@"location:::%f,%f",location.x,location.y);
    //Do stuff here...
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - MainMenuViewControllerDelegate

- (void)showMediaCenter {
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"MediaCenterViewController"];
    }
}

- (void)showLocalInfo {
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"LocationInfoViewController"];
    }
}

- (void)showExhibitorList {
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"ExhibitorListViewController"];
    }
}

- (void)showPavilion {
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"PavilionsViewController"];
    }
}

- (void)showEvevtActivite {
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"EvevtActivitesViewController"];
    }
}

-(void)showProgram{
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"ProgramTableViewController"];
    }
}

-(void)showProduct{
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"ProductViewController"];
    }
}

-(void)showRegister{
    if ([_delegate respondsToSelector:@selector(showViewWithStoryBoardID:)]) {
        [_delegate showViewWithStoryBoardID:@"RegisterViewController"];
    }
}

@end
