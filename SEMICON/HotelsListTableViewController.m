//
//  HotelsListTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/13.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "HotelsListTableViewController.h"
#import "HotelsDetailViewController.h"
@interface HotelsListTableViewController ()
@property NSArray *hotelsImageArray;
@property NSArray *hotelsNameArray;
@property NSArray *hotelsDetailsArray;
@property NSInteger selectIndex;

@end

@implementation HotelsListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _hotelsImageArray = [NSArray arrayWithObjects:@"img_hotel_01.png",@"img_hotel_02.png",@"img_hotel_03.png", nil];
    _hotelsNameArray = [NSArray arrayWithObjects:@"GRAND HYATT TAIPEI", @"HUMBLE HOUSE TAIPEI", @"FUSHIN HOTEL",nil];
    _hotelsDetailsArray = [NSArray arrayWithObjects:@"(Full-Service Business Hotel)",@"(Business Hotel)",@"Economy Hotel", nil];
    self.tableView.userInteractionEnabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotelsListCell" forIndexPath:indexPath];
    
    if (indexPath.row == 3) {
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 9, self.view.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        
        UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        moreBtn.frame = CGRectMake(self.view.frame.size.width/2-100, 12, 200, 40);
        [moreBtn setTitle:NSLocalizedString(@"More Hotels Information", "") forState:UIControlStateNormal];
        moreBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
        moreBtn.titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
        //[moreBtn addTarget:self action:@selector(moreAction:) forControlEvents:UIControlEventTouchUpInside];
        [moreBtn.titleLabel setClipsToBounds:NO];
        [moreBtn setUserInteractionEnabled:NO];
        [cell addSubview:moreBtn];

        [cell addSubview:line];
    }
    else{
        UIImageView *hotelsImage = [[UIImageView alloc] initWithFrame: CGRectMake(12,9,cell.frame.size.width-24,(cell.frame.size.width-24)/660*420)];
        UIImage *image = [UIImage imageNamed:[_hotelsImageArray objectAtIndex:indexPath.row]];
        hotelsImage.image = image;
        [cell addSubview:hotelsImage];
        
        UILabel *hotelsNameLabel=[[UILabel alloc] initWithFrame:
                            CGRectMake(17, hotelsImage.frame.size.height*0.824-20, 200, 20)];
        [hotelsNameLabel setFont:[UIFont fontWithName:@"GothamMedium" size:13.5]];
        hotelsNameLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        hotelsNameLabel.text = [_hotelsNameArray objectAtIndex:indexPath.row];
        [hotelsImage addSubview:hotelsNameLabel];
        
        UILabel *hotelsDetailsLabel=[[UILabel alloc] initWithFrame:
                                  CGRectMake(17, hotelsImage.frame.size.height*0.824, 200, 20)];
        [hotelsDetailsLabel setFont:[UIFont fontWithName:@"GothamBook" size:12]];
        hotelsDetailsLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        hotelsDetailsLabel.text = [_hotelsDetailsArray objectAtIndex:indexPath.row];
        [hotelsImage addSubview:hotelsDetailsLabel];
        
        UIButton *reservationBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        reservationBtn.frame = CGRectMake(cell.frame.size.width-112, hotelsImage.frame.size.height*0.824-19, 90, 50);
        [reservationBtn setTitle:NSLocalizedString(@"Reservation", "")  forState:UIControlStateNormal];
        reservationBtn.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        reservationBtn.titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
        [reservationBtn.titleLabel setClipsToBounds:NO];
        [reservationBtn addTarget:self action:@selector(reservationAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:reservationBtn];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 3){
        return 80;
    }
    else
        return (self.view.frame.size.width-24)/660*420+9;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
    HotelsDetailViewController *hotelsDetailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelsDetailViewController"];
    hotelsDetailViewController.dataSource = indexPath.row;
    [self.navigationController pushViewController:hotelsDetailViewController animated:YES];
}


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
}
*/


- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)reservationAction:(id)sender {
}

- (void)moreAction:(id)sender {
    NSLog(@"moreAction");
}


@end
