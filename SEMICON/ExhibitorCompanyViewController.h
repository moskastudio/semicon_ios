//
//  ExhibitorCompanyViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLScrollViewSlider.h"

@protocol ExhibitorCompanyViewControllerDelegate <NSObject>
- (void) showExhibitorIntro:(NSString*) boothNumber;
@end

@interface ExhibitorCompanyViewController : UIViewController<UITableViewDelegate,UITableViewDelegate>

@property id <ExhibitorCompanyViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *ADView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *exhibitorArray;

- (void)reloadIndexPath;

@end
