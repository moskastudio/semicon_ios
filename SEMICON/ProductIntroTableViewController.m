//
//  ProductIntroTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/20.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProductIntroTableViewController.h"
#import "UIImageView+WebCache.h"
#import "ExhibitorIntroTableViewController.h"
#import "Service.h"
#import "SignInViewController.h"

@interface ProductIntroTableViewController () {
    Boolean isNeedToRefresh;
}
@property NSMutableDictionary *dict;
@property NSDictionary *product;
@end

@implementation ProductIntroTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = _navBarTitle;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    _dict = [NSMutableDictionary dictionaryWithObjects:@[
//                                                         @"photo_one.png",
//                                                         @"Category",
//                                                         @"With the aim of promoting Taiwan's semiconductor equipment industry",
//                                                         @"6EC CMP System",
//                                                         @"Taipei City, Taiwan",
//                                                         @"2058",
//                                                         @"With the aim of promoting Taiwan's semiconductor equipment industry, the Metal Industries Research and Development Center (MIRDC) has been commissioned by MOEA's Industrial Development Bureau to assist the industry in the development of semiconductor manufacturing equipment, parts & components, as well as provide logistics support. By attracting foreign investments, building supply chains, facilitating integrated production processes, manufacturing key components, developing alliances with machine-tool makers, securing international technical cooperation, and providing early validation and testing services, the MIRDC is focused on assisting domestic parts and components manufacturers with satisfying costumer demands for product quality, and ensuring a faster turnaround for the products to enter customer's production lines."] forKeys:@[@"photo",@"tag",@"category",@"productName",@"location",@"booth",@"intro"]];
    [self fetchProduct];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    if (isNeedToRefresh) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:5 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        isNeedToRefresh = false;
    }
}

- (void)fetchProduct {
    [[Service sharedInstance] fetchProductByID:_productID withComplete:^(BOOL success, NSDictionary *responseDict) {
        
        if (success) {
            _product = [[NSDictionary alloc] initWithDictionary:responseDict];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductIntroCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (indexPath.row == 0) {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame: CGRectMake(0,114,cell.frame.size.width,36)];
        UIImage *bgImage = [UIImage imageNamed:@"img_gradient_purple.png"];
        backgroundImage.image = bgImage;
        [cell addSubview:backgroundImage];
        
        UIImageView *productImage = [[UIImageView alloc] initWithFrame: CGRectMake((self.view.frame.size.width-260)/2,34,260,159)];
        if (![[_product objectForKey:@"SmallImageURL"] isEqualToString:@""]) {
            [productImage sd_setImageWithURL:[NSURL URLWithString:[_product objectForKey:@"SmallImageURL"]] placeholderImage:nil];
        }
        else if (![[_product objectForKey:@"LargeImageURL"] isEqualToString:@""]) {
            [productImage sd_setImageWithURL:[NSURL URLWithString:[_product objectForKey:@"LargeImageURL"]] placeholderImage:nil];
        }
        
        productImage.contentMode = UIViewContentModeScaleAspectFit;
        productImage.clipsToBounds = YES;
        productImage.backgroundColor = [UIColor whiteColor];
        productImage.layer.borderColor = [[UIColor colorWithRed:0.341 green:0.118 blue:0.549 alpha:1.0] CGColor];
        productImage.layer.borderWidth = 1.0;
        [cell addSubview:productImage];
    }
    if (indexPath.row == 1) {
        UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 0, self.view.frame.size.width-114, 300)];
        NSString *text = [_product objectForKey:@"Name"];
        if ([text isEqualToString:@""]) {
            text = @"(No Name)";
        }
        
        companyLabel.text = text;
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        companyLabel.font = textFont;
        companyLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [companyLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        companyLabel.frame = CGRectMake(36, 10, actualSize.width,actualSize.height);
        [cell addSubview:companyLabel];
    }
    else if (indexPath.row == 2){
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,10,(self.view.frame.size.width-114)/2-20,20)];
        tagLabel.text = @"";
        tagLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
        tagLabel.lineBreakMode = NSLineBreakByWordWrapping;
        tagLabel.textAlignment = NSTextAlignmentCenter;
        tagLabel.textColor = [UIColor whiteColor];
        tagLabel.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [cell addSubview:tagLabel];
    }
    else if (indexPath.row == 3){
        UILabel *categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 10, self.view.frame.size.width-114, 15)];
        NSString *categoryString = [_product objectForKey:@"Description"];
        if (categoryString == nil) {
            categoryString = @"";
        }
        categoryLabel.numberOfLines = 0;
        categoryLabel.lineBreakMode = NSLineBreakByWordWrapping;
        categoryLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:categoryString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [categoryString length])];
        categoryLabel.attributedText = attributedString ;
        categoryLabel.adjustsFontSizeToFitWidth = NO;
        categoryLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:13.5] ;
        categoryLabel.font = textFont;
        [categoryLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [categoryString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        categoryLabel.frame = CGRectMake(36, 10, actualSize.width,actualSize.height*1.7);
        [cell addSubview:categoryLabel];
    }
    else if (indexPath.row == 4){
        UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 10, self.view.frame.size.width-114, 15)];
        NSString *location = [_product objectForKey:@"CompanyName"];
        locationLabel.text = location;
        locationLabel.numberOfLines =1 ;
        [locationLabel setFont: [UIFont fontWithName:@"GothamBook" size:13.5]];
        locationLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        [cell addSubview:locationLabel];
        
        UILabel *boothLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 30, self.view.frame.size.width-114, 15)];
        NSString *booth = [_product objectForKey:@"BoothNumber"];
        if (booth == nil) {
            booth = @"";
        }
        boothLabel.text = [NSString stringWithFormat:@"Booth :  %@ ", booth];
        boothLabel.numberOfLines =1 ;
        [boothLabel setFont: [UIFont fontWithName:@"GothamBook" size:13.5]];
        boothLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        [cell addSubview:boothLabel];
    }
    else if ( indexPath.row ==5){
        UIButton *favoriteBtn = [[UIButton alloc]initWithFrame:CGRectMake(52+((cell.frame.size.width-104)/2-50)/2, 30, 50, 50)];
        UIImage *favoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_normal.png"];
        [favoriteBtn setImage:favoriteBtnImage forState:UIControlStateNormal];
        UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_press.png"];
        [favoriteBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
        [favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:favoriteBtn];
        
        if ([self checkExhibitorFarvorites]) {
            favoriteBtn.selected = true;
        }
        
        UILabel *favoriteLabel = [[UILabel alloc] initWithFrame: CGRectMake(52,80,(cell.frame.size.width-104)/2,20)];
        favoriteLabel.text = NSLocalizedString(@"Add Exhibitor","");
        favoriteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        favoriteLabel.textAlignment = NSTextAlignmentCenter;
        favoriteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:favoriteLabel];
        
        UIButton *companyProfileBtn = [[UIButton alloc]initWithFrame:CGRectMake(cell.frame.size.width-102-((cell.frame.size.width-104)/2-50)/2, 30, 50, 50)];
        UIImage *companyProfileBtnImage = [UIImage imageNamed:@"btn_company.png"];
        [companyProfileBtn setImage:companyProfileBtnImage forState:UIControlStateNormal];
        [companyProfileBtn addTarget:self action:@selector(companyProfileBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:companyProfileBtn];
        
        UILabel *companyProfileLabel = [[UILabel alloc] initWithFrame:
                                        CGRectMake(52+(cell.frame.size.width-104)/2,80,(cell.frame.size.width-100)/2,20)];
        companyProfileLabel.text = NSLocalizedString(@"Company Profile","");
        companyProfileLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        companyProfileLabel.textAlignment = NSTextAlignmentCenter;
        companyProfileLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:companyProfileLabel];
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 118, cell.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    else if (indexPath.row == 6){
        UILabel *introLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,26)];
        introLabel.text = NSLocalizedString(@"Introduction","");
        introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        introLabel.textColor = [UIColor blackColor];
        //[introLabel sizeToFit];
        [cell addSubview:introLabel];
    }
    else if ( indexPath.row == 7){
        UILabel *introLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 50)];
        NSString *contentString = [_product objectForKey:@"Detail"];
        if (contentString == nil) {
            contentString = @"";
        }
        introLabel.numberOfLines = 0;
        introLabel.lineBreakMode = NSLineBreakByWordWrapping;
        introLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        introLabel.attributedText = attributedString ;
        introLabel.adjustsFontSizeToFitWidth = NO;
        introLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
        introLabel.font = textFont;
        [introLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        introLabel.frame = CGRectMake(36, 5, actualSize.width,actualSize.height*1.8);
        [cell addSubview:introLabel];
    }
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 203;
    }
    else if (indexPath.row == 1){
        NSString *contentString = [_product objectForKey:@"Name"];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height+10;
    }
    else if (indexPath.row == 2){
        return 0;
    }
    else if (indexPath.row == 3){
        NSString *contentString = [_product objectForKey:@"Description"];
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:13.5] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.5+15;
    }
    else if (indexPath.row == 4){
        return 45;
    }
    else if (indexPath.row == 5){
        return 120;
    }
    else if (indexPath.row == 6){
        return 60;
    }
    else if (indexPath.row ==7){
        NSString *contentString = [_product objectForKey:@"Detail"];
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    else
        return 50;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) favoriteBtnAction:(id)sender {
    NSLog(@"favoriteBtnAction");
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        NSMutableArray *exhibitorFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"]];
        if ([sender isSelected]) {
            [exhibitorFarvorites addObject:@{
                                             @"companyName": [_product objectForKey:@"CompanyName"],
                                             @"boothNumber": [[_product objectForKey:@"BoothNumber"] description]}];
        }
        else {
            for (int i = 0; i < exhibitorFarvorites.count; i++) {
                if ([[[[exhibitorFarvorites objectAtIndex:i] objectForKey:@"boothNumber"] description] isEqualToString:[[_product objectForKey:@"BoothNumber"] description]]) {
                    [exhibitorFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:exhibitorFarvorites forKey:@"ExhibitorFarvorites"];
    }
}

-(void) companyProfileBtnAction:(id)sender {
    isNeedToRefresh = true;
    ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorIntroTableViewController"];
    exhibitorIntroTableViewController.navBarTitle = NSLocalizedString(@"Exhibitor Company", "") ;
    [exhibitorIntroTableViewController setBoothNumber:[_product objectForKey:@"BoothNumber"]];
    
    [self.navigationController pushViewController:exhibitorIntroTableViewController animated:true];
    NSLog(@"companyProfileBtnAction");
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (Boolean)checkExhibitorFarvorites {
    NSArray *exhibitorFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"];
    
    for (NSDictionary *exhibitorFarvorite in exhibitorFarvorites) {
        if ([[[exhibitorFarvorite objectForKey:@"boothNumber"] description] isEqualToString:[[_product objectForKey:@"BoothNumber"] description]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
