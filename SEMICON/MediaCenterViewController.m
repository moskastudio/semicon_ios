//
//  MediaCenterViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/6.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "MediaCenterViewController.h"
#import "MainViewController.h"
#import "ExhibitionNewsTableViewController.h"

@interface MediaCenterViewController ()

@end

@implementation MediaCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showExhibitionNews"]) {
        ExhibitionNewsTableViewController *exhibitionNewsTableViewController = segue.destinationViewController;
        exhibitionNewsTableViewController.souceButton = @"ExhibitionNews";
    }
    else if ([segue.identifier isEqualToString:@"showExhibitorPressRelease"]) {
        ExhibitionNewsTableViewController *exhibitionNewsTableViewController = segue.destinationViewController;
        exhibitionNewsTableViewController.souceButton = @"ExhibitorPressRelease";
    }
    else if ([segue.identifier isEqualToString:@"showMediaPartners"]) {
        ExhibitionNewsTableViewController *exhibitionNewsTableViewController = segue.destinationViewController;
        exhibitionNewsTableViewController.souceButton = @"MediaPartners";
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
//    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

@end
