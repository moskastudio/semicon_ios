//
//  ShuttleBusViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShuttleBusViewController : UIViewController<UINavigationControllerDelegate,UINavigationBarDelegate>

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *leftButton;

@end
