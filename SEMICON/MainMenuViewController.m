//
//  MainMenuViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/6.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "MainMenuViewController.h"
#import "MediaCenterViewController.h"
#import "LocationInfoViewController.h"

@interface MainMenuViewController ()

@end

@implementation MainMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)mediaCenterBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showMediaCenter)]) {
        [_delegate showMediaCenter];
    }
}

- (IBAction)localInfoBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showLocalInfo)]) {
        [_delegate showLocalInfo];
    }
}

- (IBAction)ExhibitorBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showExhibitorList)]) {
        [_delegate showExhibitorList];
    }
}

- (IBAction)PavilionBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showPavilion)]) {
        [_delegate showPavilion];
    }
}

- (IBAction)EvevtActiviteBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showEvevtActivite)]) {
        [_delegate showEvevtActivite];
    }
}

- (IBAction)ProgramBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showProgram)]) {
        [_delegate showProgram];
    }
}

- (IBAction)ProductBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showProduct)]) {
        [_delegate showProduct];
    }
}

- (IBAction)RegisterBtn:(id)sender {
    if ([_delegate respondsToSelector:@selector(showRegister)]) {
        [_delegate showRegister];
    }
}

- (IBAction)toFacebookPage:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/SEMICONTaiwan/"]];
}
@end
