//
//  ProgramTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/2.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramTableViewController : UITableViewController

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
- (void)reloadIndexPath;
@end
