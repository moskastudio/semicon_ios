//
//  TechXPOTTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "TechXPOTTableViewController.h"

@interface TechXPOTTableViewController ()

@property NSArray *TechXPOTArray;
@property NSString *selectedItem;
@property NSInteger selectedItemIndex;

@end

@implementation TechXPOTTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];

//    _TechXPOTArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"High Tech Facility", "") , NSLocalizedString(@"Human Resources", "") , NSLocalizedString(@"Laser", "") , NSLocalizedString(@"Materials", "") , NSLocalizedString(@"New Product Launch", "") , NSLocalizedString(@"Smart Manufacturing", "") , nil];
    _TechXPOTArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"High Tech Facility  4F", "")  , NSLocalizedString(@"Materials  1F", "") , NSLocalizedString(@"New Product Launch  1F", "") , NSLocalizedString(@"Smart Manufacturing  1F", "") , nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _TechXPOTArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TechXPOTCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (indexPath.row == 0) {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, cell.frame.size.width-30, cell.frame.size.width/660*288)];
        UIImage *image = [UIImage imageNamed:@"img_card.png"];
        backgroundImage.image = image;
        [cell addSubview:backgroundImage];
        
        UILabel *TechXPOTLabel=[[UILabel alloc]initWithFrame:CGRectMake(52, 32, self.view.frame.size.width-104, self.view.frame.size.width/660*288*0.694-40)];
        [TechXPOTLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16.5]];
        TechXPOTLabel.textColor = [UIColor whiteColor];
        TechXPOTLabel.text = [_TechXPOTArray objectAtIndex:indexPath.row];
        TechXPOTLabel.textAlignment = NSTextAlignmentLeft;
        [TechXPOTLabel setNumberOfLines:0];
        TechXPOTLabel.lineBreakMode =NSLineBreakByWordWrapping;
        [cell addSubview:TechXPOTLabel];
        
        UILabel *agendaLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-160, self.view.frame.size.width/660*288*0.694+12, 100, self.view.frame.size.width/660*288*0.306)];
        [agendaLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        agendaLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        agendaLabel.text = NSLocalizedString(@"Agenda", "") ;
        agendaLabel.textAlignment = NSTextAlignmentRight;
        [agendaLabel setNumberOfLines:0];
        agendaLabel.lineBreakMode =NSLineBreakByWordWrapping;
        [cell addSubview:agendaLabel];
        
        UIImageView *goNext = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-43, self.view.frame.size.width/660*288*0.694+7+(self.view.frame.size.width/660*288*0.306/2)-6, 7, 12)];
        UIImage *goNextImage = [UIImage imageNamed:@"img_next_black.png"];
        goNext.image = goNextImage;
        [cell addSubview:goNext];
    }
    
    else {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 7, cell.frame.size.width-30, cell.frame.size.width/660*288)];
        UIImage *image = [UIImage imageNamed:@"img_card.png"];
        backgroundImage.image = image;
        [cell addSubview:backgroundImage];
        
        UILabel *TechXPOTLabel=[[UILabel alloc]initWithFrame:CGRectMake(52, 27, self.view.frame.size.width-104, self.view.frame.size.width/660*288*0.694-40)];
        [TechXPOTLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16.5]];
        TechXPOTLabel.textColor = [UIColor whiteColor];
        TechXPOTLabel.text = [_TechXPOTArray objectAtIndex:indexPath.row];
        TechXPOTLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:TechXPOTLabel];
        
        UILabel *agendaLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width-160, self.view.frame.size.width/660*288*0.694+7, 100, self.view.frame.size.width/660*288*0.306)];
        [agendaLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        agendaLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        agendaLabel.text = NSLocalizedString(@"Agenda", "") ;
        agendaLabel.textAlignment = NSTextAlignmentRight;
        [agendaLabel setNumberOfLines:0];
        agendaLabel.lineBreakMode =NSLineBreakByWordWrapping;
        [cell addSubview:agendaLabel];
        
        UIImageView *goNext = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-43, self.view.frame.size.width/660*288*0.694+7+(self.view.frame.size.width/660*288*0.306/2)-6, 7, 12)];
        UIImage *goNextImage = [UIImage imageNamed:@"img_next_black.png"];
        goNext.image = goNextImage;
        [cell addSubview:goNext];
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return self.view.frame.size.width/660*288+12;
    }
    else if (indexPath.row == _TechXPOTArray.count-1) {
        return self.view.frame.size.width/660*288+30;
    }
    else
        return self.view.frame.size.width/660*288+7;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
    
    if (indexPath.row != 0) {
        self.selectedItemIndex = indexPath.row + 2;
    }
    else{
        self.selectedItemIndex = indexPath.row;
    }
    self.selectedItem = [self.TechXPOTArray objectAtIndex:indexPath.row];
    if ([_delegate respondsToSelector:@selector(showTechXPOTTDetail:)]) {
        [_delegate showTechXPOTTDetail: self.selectedItemIndex];
    }

}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
