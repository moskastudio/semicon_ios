//
//  PavilionsViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "PavilionsViewController.h"
#import "ThemePavilionTableViewController.h"
#import "RegionPavilionTableViewController.h"
#import "PavilionDetailsViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface PavilionsViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate, ThemePavilionTableViewDelegate, RegionPavilionTableDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;

@property NSArray *ThemePavilionsArray;
@property NSArray *RegionPavilionsArray;
@property NSArray *regionIntro;
@property NSArray *themeIntro;
@property NSArray *regionAlias;
@property NSArray *themeAlias;
@property NSString *dataSource;
@property NSInteger selectedPavilionIndex;

@end

@implementation PavilionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _ThemePavilionsArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"AOI Pavilion", "") ,NSLocalizedString(@"CMP Pavilion", ""),NSLocalizedString(@"High-Tech Facility Pavilion", ""),NSLocalizedString(@"Materials Pavilion", ""),NSLocalizedString(@"Precision Machinery Pavilion", ""),NSLocalizedString(@"Secondary Market Pavilion", ""),NSLocalizedString(@"Smart Manufacturing Pavilion", ""),NSLocalizedString(@"Taiwan Localization Pavilion", ""), nil];

    _RegionPavilionsArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Cross-Strait Pavilion", "") ,NSLocalizedString(@"German Pavilion", "") ,NSLocalizedString(@"Holland High Tech Pavilion", "") ,NSLocalizedString(@"Korea Pavilion", "") ,NSLocalizedString( @"Kyushu (Japan) Pavilion", ""),NSLocalizedString(@"Okinawa (Japan) Pavilion", "") ,NSLocalizedString(@"Philippines Pavilion", "") ,NSLocalizedString(@"Singapore Pavilion", "") , nil];
    _themeAlias = [[NSArray alloc] initWithObjects:@"aoi", @"cmp", @"htf", @"material", @"precision", @"secondary", @"smart-manufacturing", @"taiwan-localization", nil];
    
    _regionAlias = [[NSArray alloc] initWithObjects:@"cross-strait", @"german-pavilion", @"holland-high-tech", @"korea", @"kyushu", @"okinawa", @"philippines", @"singapore", nil];
    
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    
    [self setIntro];
    [self setupTopSegment];

}

#pragma - ThemePavilionTableViewDelegate

-(void)showThemePavilionDetail:(NSInteger)index{
    _dataSource = @"Theme";
    NSLog(@"showThemePavilionDetail:%ld", (long)index);
    self.selectedPavilionIndex = index;
    [self performSegueWithIdentifier:@"showPavilionDetails" sender:self];
}

#pragma - RegionPavilionTableDelegate

-(void) showRegionPavilionDetail:(NSInteger)index{
    _dataSource = @"Region";
    NSLog(@"showRegionPavilionDetail:%ld",(long)index);
    self.selectedPavilionIndex = index;
    [self performSegueWithIdentifier:@"showPavilionDetails" sender:self];
}


#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Theme Pavilions", "") ,NSLocalizedString(@"Region Pavilions", "") ]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 2;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            ThemePavilionTableViewController *themePavilionTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ThemePavilionTableViewController"];
            self.controllerTheme = themePavilionTableViewController;
            self.controllerTheme.delegate = self;
            return themePavilionTableViewController.view;
        }
            break;
        case 1:{
            RegionPavilionTableViewController *regionPavilionTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegionPavilionTableViewController"];
            self.controllerRegion = regionPavilionTableViewController;
            self.controllerRegion.delegate = self;
            return regionPavilionTableViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showPavilionDetails"]) {
        if ([_dataSource isEqualToString:@"Theme"]) {
            PavilionDetailsViewController *pavilionDetailsViewController = [segue destinationViewController];
            pavilionDetailsViewController.navBarTitle = [_ThemePavilionsArray objectAtIndex:self.selectedPavilionIndex];
            pavilionDetailsViewController.index = self.selectedPavilionIndex;
            pavilionDetailsViewController.whichPavilionTable = _dataSource;
            pavilionDetailsViewController.contentString = [_themeIntro objectAtIndex:self.selectedPavilionIndex];
            pavilionDetailsViewController.alias = [_themeAlias objectAtIndex:self.selectedPavilionIndex];
        }
        else{
            PavilionDetailsViewController *pavilionDetailsViewController = [segue destinationViewController];
            pavilionDetailsViewController.navBarTitle = [_RegionPavilionsArray objectAtIndex:self.selectedPavilionIndex];
            pavilionDetailsViewController.index = self.selectedPavilionIndex;
            pavilionDetailsViewController.whichPavilionTable = _dataSource;
            pavilionDetailsViewController.contentString = [_regionIntro objectAtIndex:self.selectedPavilionIndex];
            pavilionDetailsViewController.alias = [_regionAlias objectAtIndex:self.selectedPavilionIndex];
        }
    }
}


- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)setIntro {
    NSArray *regionZH = @[
                          @"隨著全球景氣的復甦，半導體產業也隨著逐漸回春。在強大的內需市場與技術成長驅動下，中國將帶頭經濟與產業的回升。也因為中央政府的全力支持和IC市場的高成長潛力，中國IC產業將有更上一層樓的發展。\n\n為開創海峽兩岸更多的商機，SEMICON Taiwan 2016「海峽兩岸專區」，聯合中國大陸地區廠商，展示最新技術及產品，幫助您發掘更多合作機會。",
                          @"歡迎蒞臨參觀2016 SEMICON TAIWAN德國館，南港展覽館一樓，J區 MADE IN GERMANY – 半導體的產業領導者\n\n繼2015年第一屆SEMICON Taiwan德國館成功展出後，德國館將再次進駐2016年SEMICON Taiwan。半導體產業為德國經濟主要支柱之一，2016 SEMICON Taiwan將展示德國高端智能半導體之創新解決方案。德國經濟辦事處本次特邀六家頂尖德國半導體廠商參展，其參展商分別為展締科技股份有限公司 (攤位編號J2811)、 海德漢股份有限公司(攤位編號J2708)、LPKF Laser &Electronics AG (攤位編號J2801)、MUEGGE GmbH(攤位編號J2704) Silicon Saxony e. V. (攤位編號J2016)，及台灣新能智有限公司(攤位編號J2805)。德國館將位於南港展覽館一樓J區盛大舉辦，歡迎各位業界先進不吝光臨指教。\n\n德國館六家參展廠商將展出來自德國的高科技、高品質之半導體產品。展締科技股份有限公司致力於引進德國 NanoFocus先進3D光學自動化量測設備，提供奈米量測精度以滿足半導體微結構之量測需求，並搭配薄晶圓及翹曲晶圓之搬運傳送設備，提供半導體先進製程之所需。同時亦提供石墨烯、TMD等2D材料及SiC長晶原料，為下一世代半導體元件提供材料解決方案；海德漢股份有限公司專長於研發與生產高精度開放式光學尺以及開放式光學尺、模組式角度編碼器，以及ETEL高精度運動平台Vulcano，其主要產品皆經過高標準的檢測，適用於高度精密的電子測量儀器。\n\nLPKF Laser&Electronics AG不僅研發LDS-EMC線路技術，並提供最新玻璃基板微孔成形技術，可在極薄的玻璃上成形盲孔以及通孔，並同時推出高加工精度的LPKF 雷射鋼板切割機；MUEGGE GmbH專精於工業微波加熱系統以及等離子技術；台灣新能智有限公司具前瞻性的思維理念和進步的科技，提供高品質探針卡之設計與製造之服務。\n\nSilicon Saxony e. V. 為在微、納電子學、軟體、智能系統、應用以及能源系統的領域佔有十分重要地位的協會。此外，與Silicon Saxony e. V. 合展的四家公司─camLine GmbH、HSEB Dresden GmbH、HTT High Tech Trade和Saxony Economic Development Corporation亦將共同參與德國館。\n\nHappy Hour 啤酒暢飲\n德國館將於9月8日 (週四) 下午14:30-16:00舉辦啤酒暢飲，由台灣菸酒股份有限公司特別贊助德國卡登堡小麥啤酒，竭誠邀您加入同歡暢飲，機會難得，別錯過了。\n\nGTO & MIRDC --國際半導體先進製程設備技術論壇暨商談會\n展覽期間，國際半導體先進製程設備技術論壇將於9月8日 (週四) 上午10:00-12:20於南港展覽館一樓主要舞台舉行，由金屬工業研究發展中心主辦、德國經濟辦事處協辦，邀請來自德國半導體產業專家於技術論壇中進行光電、半導體主題之專題演講。商談會將於同日下午15:00-17:00舉辦，歡迎台德半導體相關產業廠商參與。更多與研討會以及商談會或是2016 SEMICON的相關的資訊，請至德經處官網查詢。",
                          @"歡迎參觀 1 樓攤位 J2512 荷蘭高科技專區!!\n\n台灣半導體產業自初期的快速發展以來，便一直與飛利浦、ASML和恩智浦(NXP)等荷蘭龍頭業者維持緊密的合作關係，雙方產業鏈間的交流更是持續地進行。\n\n荷蘭的半導體產業主要分佈在荷蘭東部與西部地區，並在微電子、製程/技術研究、生產工具/設備、電子元件、使用者應用等領域享有優勢，年營收達到100億 歐元。\n\nSEMICON Taiwan 2016「荷蘭高科技專區」，特地邀請到荷蘭10家頂尖半導體廠商，以及全國應用研究機構TNO的共同參與。專區中將展示多樣化的技術與產品內容，包括質量流 量計和控制、高科技工程、精密製造、先進表面黏著技術(SMT)、雷射直接成像、原子層沉積(ALD)、先進封裝、先進粒子監測系統、電源保護等世界級半導體研發技術。",
                          @"KOTRA駐台北韓國貿易館，將主持SEMICON Taiwan 2016韓國專區執行工作。KOTRA旨在透過國內外企業貿易推廣與投資並支持產業的技術合作，以促進國家經濟發展。透過86國的126個網絡，KOTRA成為全球商機平台。\n\n在SEMICON Taiwan 2016「韓國專區」中，韓國參展商提供用於生產半導體和電子產品最佳的生產解決方案、零組件、材料、軟體與服務。誠摯地邀請您參觀韓國專區，探索韓國的半導體產業，並加強與韓國公司的業務。",
                          @"九州有日本矽島之稱，是日本半導體產業重鎮。在日本九州島上，總共有19家晶圓製造廠以及超過1,000家半導體製造相關企業，大部分的半導體產業皆集中於熊本市以及大分市。九州擁有完整的半導體產業鏈，從IC設計、製造到封測，更有許多知名的設備材料商進駐，更加支持九州的半導體產業發展。\n\nSEMICON Taiwan 2016「日本九州專區」不僅有政府代表，STK Technology更將展出燒機設備，完整展現日本九州的半導體供應鏈實力，開創更多合作機會。",
                          @"2016 SEMICON Taiwan，沖繩縣政府將首次帶領沖繩企業來台，讓台灣見識沖繩半導體工業之實力!\n\n近年來沖繩縣政府積極提升產業發展環境，並藉由經濟特區的設立，大量增加海內外投資設廠發展利基，讓沖繩逐漸成為東亞轉口加值製造新樞紐。沖繩的半導體產業也在此發展氛圍下逐漸茁壯，已有日本國內的半導體設備商看準沖繩之地理優勢，將沖繩作為日本國內的第二製造據點，成功達到減少生產成本及縮短交貨時間，將貨品快速向海外發送。\n\n除此之外，海外廠商於沖繩經濟特區設廠，可運用多樣的租稅投資優惠，不僅能擁有沖繩既有的轉口加值優勢，更能以低成本取得「日本製造」的品牌加值。\n\n想知道更多沖繩半導體企業及投資資訊，歡迎來到 SEMICON Taiwan日本沖繩專區(南港展覽館一樓K區#3148)，我們將提供更多精彩內容與解說! 一起來體驗沖繩的熱情吧!",
                          @"The Philippine Department of Trade and Industry – Board of Investments (DTI-BOI) brings to the fore its Semicon and Electronics Industry at the Philippine Pavilion in Semicon Taiwan 2016. Other than the Semiconductor Manufacturing Services (SMS) and Electronics Manufacturing Service (EMS), the country is featuring IC Design as the next frontier of S&E Industry.\n\nSee how its local industry is actively stepping forward to build its downstream capabilities to respond to customer needs and move up the value chain. Visit the Philippine Pavilion at 1F Booth J2800 and attend their presentations at the Market Investment Seminar and TechXPOT.",
                          @"自成立於1932年起，新加坡製造商總會致力於輔佐新加坡製造業，幫助企業提升競爭力及永續發展實力。結合10個產業協會以及整合研究中心，新加坡製造商總會藉由鼓勵創新及創造力，強化整體產業優勢。\n\nSEMICON Taiwan 2016 「新加坡專區」期望促進國際廠商與本土企業之合作與交流，共同創造成長動能。"];
    
    NSArray *regionEN = @[
                          @"China will take lead in worldwide economic and industry recovery due to its strong domestic demands and development of advanced technologies. Further with Chinese government policy support and high market growth potentials, China IC industry is expected to develop with prospects.\n\nIn order to expand more business opportunities between China and Taiwan, Cross-Strait Pavilion at SEMICON Taiwan 2016 invites key players in the China semiconductor industry not only to exhibit latest product and technology development but also to facilitate business partnerships.",
                          @"The German Trade Office will again organize an exclusive German Pavilion at the 2016 SEMICON Taiwan Exhibition. Semiconductors are one of the industrial pillars of Germany, and the Made in Germany brand will have the opportunity to showcase the pioneering solutions in the semiconductor industry this year at the J Area of Nangang Exhibition Hall, 1F.\n\nThe renowned German companies participating this year are: Euflex Technology Corp. (J2811), Heidenhain Co.,Ltd. (Taiwan) (J2708) and LPKF Laser&Electronics AG (J2801), MUEGGE GmbH (J2704), Silicon Saxony e. V. (J2016), and Synergie-Cad Taiwan Co. Ltd. (J2805). These exhibitors will showcase the most famous German semiconductor products at the German Pavilion.\n\nThe German pavilion presents its best technologies and products in the field of semiconductors. Euflex Technology commits to the highest standards of 3D confocal measurement technologies, Heidenhain develops and manufactures high-precision linear and angle encoders, LPKF Laser & Electronics AG extends LPKF LDS-EMC Technology, Glass substrate micro-via forming technology and Stencil Laser machine, MUEGGE GmbH specializes in the field of industrial microwave and plasma technologies, Silicon Saxony e. V. is the most successful trade association for the micro- and nano-electronic, software, smart system, application and energy system industries, whilst Synergie-Cad Taiwan provides high-quality probe cards. Moreover, three co-exhibitors will also participate in displaying their products at the German Pavilion this year, in collaboration with Silicon Saxony e.V., which include camLine GmbH、HSEB Dresden GmbH、HTT High Tech Trade and Saxony Economic Development Corporation. For more information, please visit the respective websites of the exhibitors.\n\nHappy Hour\nThe Pavilion will host a Happy Hour event on September 8th (Thursday) from 14:30 to 16:00 at Area J, Nangang Exhibition Hall 1F. You are invited to join the Happy Hour which includes all you can drink free beer, sponsored by Taiwan Tobacco & Liquor Corporation and with Kaltenberg (Hefeweissbier). Don´t miss this opportunity to enjoy the taste of German beer and have a look at the many German semiconductor solutions on display.\n\nGTO & MIRDC -- International Semiconductor Advanced Manufacturing Process and Equipment Technology Forum &Business Matchmaking\nThroughout the show, the International Semiconductor Advanced Manufacturing Process and Equipment Forum at SEMICON Taiwan will be held on September 8th, Thursday, from 10:00 to 12.20., co-organized by Metal Industries Research & Development Centre (MIRDC) and the German Trade Office Taipei (GTO) at 1st Floor Main stage, Nangang Exhibition Center.\n\nGuest speakers from leading German companies in semiconductor industry will be invited to the seminar. The focus of the forum will be on optoelectronics and semiconductors. The forum will provide you with an opportunity to make valuable business connections, and will be held from 15:00 to 17:00, where German companies will also join in our business matchmaking session. For further information about the networking forum or the 2016 SEMICON Taiwan German Pavilion, please visit German Trade Office Taipei",
                          @"Visit the Holland High Tech Pavilion at the 1st floor booth J 2512!\n\nSince the beginning of its rapid development, the Taiwan semiconductor industry has very close ties with the Dutch semiconductor industry through flagship companies like Philips, ASML and NXP and continuous to do so through every element of the value chain.\n\nThe Dutch semiconductor industry is mainly located in the Eastern and Southern parts of the Netherlands and has its strength in microelectronics; process/technology research, production tools/equipment, electronic components and end user applications and generates an annual turnover of 10 Billion Euro.\n\nThis year’s Holland High-Tech Pavilion hosts 10 leading Dutch semiconductor companies and also the Business Cluster Semiconductors Netherlands. The pavilion features technologies and products in the field of mass-flow meters and control, high tech engineering, precision manufacturing, advanced surface mount technology (SMT), advanced packaging, advance particle monitoring systems and power protection.",
                          @"KOTRA, Korea's national trade and foreign investment promotion agency, is hosting the Korea Pavilion at SEMICON TAIWAN 2016. KOTRA is established to contribute to the development of the national economy by performing work such as trade promotion, investment between domestic and foreign companies and support of industrial technology cooperation. Through 122 networks on 84 countries, KOTRA functions as a global business platform.\n\nIn the Korean Pavilion at SEMICON Taiwan 2016, Korean exhibitors offer the best manufacturing solutions, components, materials, software and services for the semiconductor and electronics manufacturing. We would like to cordially invite you to visit the Korean Pavilion, which will provide you excellent opportunities to explore Korea’s semiconductor industry and improve your business with Korean companies. It would be our great pleasure to meet you at Korean Pavilion.",
                          @"Kyusyu is so called Silicon Island in Japan, with 19 wafer fabs and over 1,000 semiconductor related companies. A lot of major companies are located in Kumamoto and Oita.  The island self-provides most of the semiconductor manufacturing services, from IC design to packaging and testing and equipment and materials.\n\nThe Kyushu (Japan) Pavilion at SEMICON Taiwan 2016 invites STK Technology to demonstrate its Burn-in equipment. Government representatives will also be on-site to provide consults for investment and business partnership.",
                          @"In an era of continually advancing industrial globalization, Okinawa's geographical advantages are currently drawing attention. In recent years, Okinawa prefecture government had actively enhanced the industrial environment. By establishing “Okinawa Special Economic zone”, Okinawa has become a new core of value-added manufacturing area in East Asia.\n\nOkinawa semiconductor industries also gradually thrive in this atmosphere. Some Japanese semiconductor equipment companies had targeted the geographical advantages of Okinawa and took it as the second manufacturing base in japan. Overseas companies can also use this model and set factory at Okinawa Special Economic Zone. This model can not only reduce the time cost of delivery but also obtain the quality assurance reputation of ‘Made in Japan’.\n\nIf you want to learn more about Okinawa semiconductor business and investment information, welcome to Okinawa Prefecture Pavilion in 2016 SEMICON Taiwan. We will provide penetrating information and commentary.",
                          @"The Philippine Department of Trade and Industry – Board of Investments (DTI-BOI) brings to the fore its Semicon and Electronics Industry at the Philippine Pavilion in Semicon Taiwan 2016. Other than the Semiconductor Manufacturing Services (SMS) and Electronics Manufacturing Service (EMS), the country is featuring IC Design as the next frontier of S&E Industry.\n\nSee how its local industry is actively stepping forward to build its downstream capabilities to respond to customer needs and move up the value chain. Visit the Philippine Pavilion at 1F Booth J2800 and attend their presentations at the Market Investment Seminar and TechXPOT.",
                          @"Established since 1932, SMF represents the interest of the Singapore manufacturing community, driving its competitiveness and sustainable growth through serving industry-specific needs. Supported by 10 industry groups and its Centres of Excellence, SMF enhances the competitiveness of the industry by encouraging capacity development and capability building, innovation and productivity."];
    
    NSArray *themeZH = @[
                        @"隨著電子產品的小型化、節能化發展，零件尺寸也愈趨精密，人眼目測檢查的難度與錯誤率愈來愈高。因此，導入AOI (Automatic Optical Inspection)，用機器之力代替肉眼檢查的趨勢也變得勢在必行。\n\n在IC檢測精密度需求一再提高以及半導體產業擴大投資的帶動下，自動光學檢測設備的市場需求也隨之持續成長。近年來，國產AOI設備商已在PCB、FPD檢測市場經營有成，未來若能再切入佔台灣檢測設備市場60%的半導體產業，其發展潛力不容小覷。\n\n拜硬體設備與網路環境成熟之賜，虛擬實境、車牌及人臉辨識和各種監控管理的智慧影像分析，未來將成為實現智慧城市的關鍵技術，根據Reportlinker最新资料，2012年全球智慧影像分析產值約135億美元，預估至2020年時，將攀升至390億美元。\n\nSEMICON Taiwan 2016 因應市場趨勢，特別設立自動光學檢測專區將，邀集國內外光學檢測領導大廠，展出最新檢測機台與引領未來科技。",
                        @"追隨著摩爾定律的進程，半導體朝向微型化發展，在半導體製程進入32奈米以下節點之後，對於晶圓的高低起伏、缺陷大小的容忍程度亦進入了奈米等級，使得對於化學機械研磨(CMP)技術的要求更為嚴苛。全球即時相連結的智慧型電子通訊產品及高階電腦，皆需仰賴嚴苛的全面化的化學機械研磨製程。\n\nSEMICON Taiwan 2016隆重推出化學機械研磨專區，展出最尖端的化學機械研磨技術。",
                        @"「高科技廠房設施專區」是世界，也是台灣唯一、首次專為高科技廠房設施 (High-Tech Facility) 所舉辦。參展單位涵蓋高科技廠房建築物與廠務系統之廠家，參展主題涵蓋：節能、奈米微汙染控制、廠房設施資訊模擬、精密儀器檢測、消防安全、機電整合與自動化控制等。期望集結相關廠商，為台灣半導體產業提供最多元化且最精省的製造服務方案。\n\n高科技廠房設施國際論壇\n時間：2016年9月8日 (星期四) 08:30 – 17:30\n今年的主題為 “Less Waste & Less Cost for Sustainable Environment”， 節能減廢與環保是目前國際社會共同關注且攸關國家及產業永續發展的重要議題。台灣的高科技產業在促進經濟之成長上，也關切其廠房設施運作所造成之環境生態永續發展之問題。本論壇將針對在廠房設施之興建營運中，如何保有廢棄物少、成本低的永續環境之議題做深入探討，從源頭控管資源的耗用及回收再使用，到廢棄物的減量及資源化，延伸至終端處理控管，降低對環境的污染，在在都是企業實現社會責任的展現。\n\n論壇中，特別提供全天中英同步口述翻譯服務，讓您無須擔心語言上的隔閡，更能享受精彩、知識性豐富的演講內容，歡迎關心環保及高科技廠房永續經營議題的您出席。\n\n高科技廠房設施技術發表會 TechXPOT\n是公司發表最新技術及產品的絕佳曝光機會。在專區內，您可在連續3天的技術發表會中，得知廠家最新研發成果及尖端產品，並與廠家進行對談。專區的規劃，更能讓您節省看展時間，在短時間內找到合作的夥伴及商機。\n\n高科技廠房設施貴賓晚宴\n時間：2016年9月8日 (星期四) 18:00-21:00\n地點：寒舍餐旅 - 樂樂軒(台北市南港區經貿二路1號3樓)\n晚宴將邀集產業高階主管、業界菁英、及產官學貴賓聯袂與會交流，透過輕鬆的聚會方式，協助高科技廠房設施廠商快速累積人脈。\n\n為使活動更豐富，同時增加企業曝光度，SEMI提供論壇茶點、摸彩獎品等多元化的贊助方案！>>MORE",
                        @"根據SEMI報告指出，2015年晶圓製造材料和封裝材料的總收入分別為241億美元和193億美元。由於台灣擁有大型晶圓廠與先進封裝廠房，已連續第六年獲得全球半導體材料最大買家的頭銜，總計採購金額達94億美元。\n\nSEMICON Taiwan材料專區2015年首次推出即吸引業界注目，且獲得如光玥實業、Dongjin Semichem、Linex Consulting、Namics、TECNISCO等廠商的支持及參與。2016年材料主題擴大徵展，預計展出晶圓製造材料如矽晶、SOI、光罩、抗蝕劑、化學藥品、特殊/大宗氣體等，及封裝材料如導線架、封裝載板、銲線、黏晶片、封膜化合物、封膠材料、石英、陶瓷等內容，並同時結合半導體材料國際論壇以及TechXPOT創新技術發表會，完整展示半導體材料供應鏈，為產業提供一個最專業的交流平台。\n\n半導體材料技術論壇\n日期 & 時間：2016年9月7日13:00 – 17:00\n地點：台北南港展覽館5樓，504a會議室\n主題 : 半導體綠色製造之創新材料平台\n\n 材料在半導體製造技術的演進過程中扮演更加關鍵的角色，而台灣作為最大的半導體材料市場地區更已成為全球半導體材料發展的核心重鎮，因此SEMI Taiwan在2015年成立了半導體材料委員會，作為一個促進需求方與供應方間之產業交流與技術合作的開放性平台。並以此平台提供SEMI Taiwan依產業發展趨勢來規劃半導體材料專案與相關活動的指導性建議。這個以創新為核心的開放性平台已經制定了下列的目標:\n\n-	整合國內外半導體材料產業相關資源\n-	強化台灣半導體材料供應鏈實力\n-	促進關鍵半導體材料技術的合作開發\n-	建立更加優化的半導體材料管理系統及產業標準\n\n這場半天的論壇將邀請來自國內外業界的講師，就最新的半導體材料相關技術、產品、及管理模式分享他們的獨到見解。 這些專家們也將分別從創新研發、 品質管理、及廢棄物處理等三大面向，與聽眾共同探討半導體工業的綠色製造及可持續發展的關鍵議題。同時本論壇也是一場與業界精英們交流互動之絕佳盛會，歡迎您踴躍報名參與，共襄盛舉。\n\nTechXPOT關鍵材料創新技術發表會\n\nTechXPOT邀請各大廠商發表最新技術及產品，您可在連續3天的技術發表會中，得知最新研發成果及尖端產品，並與潛在合作夥伴進行對談。",
                        @"台灣精密機械產值預計從去年的42億美元，成長到70億美元，搭配新政府政策輔助，台灣有望成為機密機械產業全球前五大生產國、第三大出口國。在航太、精密醫療/材及物聯網等領域的蓬勃發展下，精密機械產業的重要性日漸增加；從板金、鑄造等金屬加工，滑軌、螺桿、控制器等關鍵零組件，以及整機的設計製造，台灣業者擁有成熟且完整供應鏈，進一步結合機器人﹑自動化等工業4.0的概念，能有效減輕紅色供應鏈價格戰的威脅，提升產業競爭力。/n/nSEMICON Taiwan 2016精密機械專區，展出台灣精密機械完整供應鏈，提升MIT優質產品競爭力。",
                        @"根據SEMI最新全球晶圓廠預測報告，2016年包括新設備及二手設備在內的前段晶圓廠設備支出預期將增加3.7%，達372億美元，2017年則將再成長13%、達421億美元。其中，隨著超越摩爾定律(More than Moore)的實現，意味著業者正努力尋找擴充晶圓廠產能的替代方案，因此8吋及12吋制程設備也會被用來開發物聯網裝置所需的低功耗等技術，故設備商也能持續為這些舊世代的制程製造新產品。\n\nSEMICON Taiwan 2016二手設備專區，展出內容涵蓋二手設備交易、設備翻新、零配件清洗和修復、安裝調試、替代材料等，期望集結相關廠商，為台灣半導體製造產業提供最多元化且最精省的製造服務方案。",
                        @"根據SEMI最新全球晶圓廠預測報告，2016年包括新設備及二手設備在內的前段晶圓廠設備支出預期將增加3.7%，達372億美元，2017年則將再成長13%、達421億美元。其中，隨著超越摩爾定律(More than Moore)的實現，意味著業者正努力尋找擴充晶圓廠產能的替代方案，因此8吋及12吋制程設備也會被用來開發物聯網裝置所需的低功耗等技術，故設備商也能持續為這些舊世代的制程製造新產品。\n\nSEMICON Taiwan 2016二手設備專區，展出內容涵蓋二手設備交易、設備翻新、零配件清洗和修復、安裝調試、替代材料等，期望集結相關廠商，為台灣半導體製造產業提供最多元化且最精省的製造服務方案。\n\nSmart Manufacturing Forum\nDate & Time: 13:00 – 17:00 Thursday, September 8th, 2016\nVenue: Room 401, Taipei Nangang Exhibition Center, Hall 1, Taipei\n工業4.0風潮席捲全球，智慧製造成為半導體製造升級關鍵！透過巨量資料分析、虛擬化雲端運算、精確製程控制及智慧診斷等技術，朝智慧製造及工業自動化的智慧工廠邁進，有效提升產能與製程良率，將是台灣半導體產業能否持續領先全球的關鍵。\n\nSEMICON Taiwan 「智慧製造專區」於2015年首次推出即引起廣大迴響，並吸引了業界大廠神通資訊、西門子等熱情參與。2016年專區擴大徵展，展出智慧製造相關系統整合、軟體、感測器等最新技術及解決方案，為產業間提供一個一站式的交流平台。\n\n半導體智慧製造論壇\n日期 & 時間：2016年9月8日13:00 – 17:00\n地點：台北南港展覽館 401會議室\n邀請半導體具代表性製造大廠分享智慧智造新方向，並偕同智慧製造領導廠商，共同探討半導體製程智慧化、有效數據取用分析等技術，應用於包括機台預防性維修保養、全線設備差異主動偵測、製程良率提升等熱門議題。",
                        @"經濟部工業局致力於推動國內半導體設備產業茁壯，委由金屬中心協助推動動半導體製程設備暨零組件產業，提供設備及零組件之後勤支援，從引進設備外商在台投資並建立供應鏈，再到整合產、研開發關鍵零組件、整機聯盟、與國際技術合作，最後進而提供先期驗證及測試服務，協助國內零組件廠滿足客戶品質需求，加速產品進入客戶產線時程，長期耕耘以來已獲得實質的成果，並成功推廣至國內晶圓廠、封裝及系統廠。\n\n自2015起SEMICON Taiwan大會邀請金屬中心整合國內成功輔導廠商共同展現國內設備零組件的能力，於專區展示多項關鍵產品，獲得各界迴響；今年度在Semicon 2016將展出如:高壓電漿、潔淨系統、特殊表面處理、陶瓷加工、3D IC製程設備…等重要產業技術，以及金屬中心功能性測試服務、智慧機械服務，希望藉由多角度的服務、開發，讓國內外買主深入了解台灣設備、零組件量能。"];
    
    NSArray *themeEN = @[
                         @"With smaller form factors, higher efficiency, and sophisticated component size, it’s getting more difficult to identify errors manually in electronic devices these days; therefore it’s necessary to introduce Automatic Optical Inspection (AOI) to replace human eyes.\n\nHigher IC inspection requirements and investments both support growth in the AOI market. AOI instrument makers in Taiwan have already established reputations in PCB and FPD inspection markets. Semiconductor industry, which accounts for 60% of inspection facility market in Taiwan, is the logical next step.\n\nWith infrastructure and Internet deployments becoming mature, virtual reality, license plates and facial recognition, as well as smart image analytics for monitor and management, will become key technologies behind smart cities in the future. Latest statistics from Reportlinker project the global smart image analytics market to grow from $13.5 billion to $39 billion between 2012 and 2020.\n\nIn response to market trends, SEMICON Taiwan 2016 launches AOI Pavilion and invites industry leaders, from home and abroad, to demonstrate new products and promising technologies.",
                         @"Following Moore’s law, semiconductor industry continues to grow in sophistication. Advanced processes (32nm or below) have even lower tolerance to defects or inconsistencies on wafers. Higher standards for CMP technology are required for real-time, interconnected electronic communication devices and high-end computers around the world.\n\nSEMICON Taiwan 2016 presents CMP Pavilion with cutting-edge technologies.",
                         @"In the world and in Taiwan, \"High Tech Facility Pavilion\" is the very first pavilion on facility issue conducted at SEMICON. The exhibitors are manufacturers of high-tech facility building and facilities which feature: energy saving, nano-contamination control, facility information modeling (FIM), precision instrumentation and control, fire protection, mechatronics and automation control…etc.. The Pavilion will provide the most various and efficient manufacturing services and programs to the semiconductor industries in Taiwan.\n\nHigh-Tech Facility International Forum\nDate & Time: 8:30 – 17:30, Thursday, September 8, 2016\nTheme: “Less Waste & Less Cost for Sustainable Environment”\nEnergy saving, waste reduction and environmental protection are common concern in the world. They are regarding national and industry sustainable development, while facilitating economic growth forward. High-Tech Industry sincerely concern about the potential environmental impact and problems caused by its facility operation. The forum will have an in-depth discussion about how to maintain the less-waste-and-less-cost sustainable environment during the build of operation process of Fab facilities. The topics will cover from reducing environmental pollution, embodying source consumption control and recycling, increasing waste reduction and reclamation, and assuring terminal process control, and fulfilling corporate social responsibility.\n\nThe forum provides Chinese-English simultaneous interpreting that allows you enjoy the splendid and knowledgeable presentations. We highly welcome everyone concerning environmental protection and high-tech facility sustainability.\n\nHigh-Tech Facility TechXPOT\nYou will learn the latest high tech facility technology developments of major companies through their live shows and interactive displays during 3 days. High Tech Facility TechXPOT is the best platform to identify business partners with low cost and high performance solutions.\n\nHigh-Tech Facility VIP Banquet\n Date & Time: 18:00 – 21:00, Thursday, September 8, 2016\nVenue: Le Xuan Banquet Hall, 3F, Taipei Nangang Exhibition Center, Hall 1\nYou and the executives, elites, government and academia VIPs from High-Tech Facility field are coordinately invited to the Banquet, where you can meet and communicate in an easy and warm atmosphere, quickly accumulate contacts.\n\nSponsorship Opportunities to High-Tech Facility Forum and VIP Banquet\nSponsorship is a great way to extend your reach at SEMICON Taiwan 2016! For more options and opportunities, visit : >>HERE",
                         @"SEMI reports show that wafer materials and form factor materials reported $24.1 billion and $19.3 billion in 2015 respectively. Taiwan remains the largest semiconductor equipment and materials market for six years in a row. The total purchase amount reaches $9.4 billion.\n\nMaterial Pavilion was widely discussed when launched in SEMICON Taiwan 2015. Exhibitors included WaferPlus, Dongjin Semichem, Linex Consulting, Namics, and TECNISCO. The pavilion expands this year to include silicon, SOI, photomask, anti-corrosion, chemicals, specialty/bulk gases, lead frame, package substrate, wire, molding compound, molding materials, quartz, and ceramics. Incorporating with international program and TechXPOT, SEMICON Taiwan Material Pavilion presents a professional platform for semiconductor material supply chains.\n\nSemiconductor Materials Forum\nDate & Time: 13:00 – 17:00 Wednesday, September 7th, 2016\nVenue: Room 504bc, 5F, Taipei Nangang Exhibition Center, Hall 1\nTheme: Innovative Materials Platform for Semiconductor Green Manufacturing\n\nMaterial is playing a critical role in driving semiconductor manufacturing technology advancement. Taiwan region has already become the largest semiconductor material market in the world. Therefore SEMI Taiwan established Semiconductor Materials Committee in 2015 as an open platform to facilitate industry-wide business communications and technical collaboration opportunities among semiconductor manufacturers and materials suppliers, as well as to provide guidance on materials related programs and events to cope with this industry trend. This innovation-centric open platform has set following goals\n\n-	To integrate the related resources of semiconductor materials industry in Taiwan and abroad\n-	To enhance supply chain infrastructure for semiconductor materials\n-	To advance crucial material technology development through collaboration\n-	To facilitate more optimal material management system and standardization\n\nThis half-day event will invite speakers from around the world involved in the semiconductor materials value chain to share insights of the latest technologies, products, and management approaches. These experts will address a wide variety of topics that are critical to the greener and more sustainable development of semiconductor manufacturing industry from materials innovation, quality and waste management perspectives. Meanwhile this forum is an excellent event for networking with your industry peer groups. We warmly welcome your participation.\n\nMaterials TechXPOT\nYou will learn the latest innovative semiconductor material and its applications from major companies. TechXPOT is the best platform not only to learn the latest industry knowledge, but also to interact with potential customers and explore business opportunities.",
                         @"Precision machinery in Taiwan is anticipated to increase from $4.2 billion last year to $7 billion this year in revenue. Taiwan has the opportunity to become the fifth largest device maker and the third largest exporter in precision machinery worldwide.\n\nAs aerospace, precision healthcare and IoT becoming prosperous, precision machinery is gaining importance. Manufacturers in Taiwan own robust and developed supply chains, from metal processing to key components and complete machine design and production. Combined with robotics, automation and other Industry 4.0 developments, the MIT products can be more competitive.\n\nPrecision Machinery Pavilion in SEMICON Taiwan 2016 exhibits precision machinery supply chains in Taiwan and our competitive offers.",
                         @"According to SEMI’s new global wafer forecast, new and secondary facility expenditures for front-end wafer manufacturers are expected to grow by 3.7% to $37.2 billion in 2016, and further 13% to $42.1 billion in 2017. “More than Moore” has prompted industry players to look for alternative ways to boost productivity. Facilities for 200mm and 300mm processes, for example, will also be used to develop low-power technologies for IoT devices.\n\nSecondary Market Pavilion in SEMICON Taiwan 2016 will include transaction, renovation, component cleaning and repair, installation and modification, and alternative materials. SEMICON Taiwan Secondary Market Pavilion offers diverse and cost-effective solutions for semiconductor industry in Taiwan.",
                         @"Industry 4.0 is seen as the next step for global manufacturing industry, and smart manufacturing is the key to fulfill the concept. With the advanced technologies in big data analysis, cloud computing, process control, and intelligent monitoring, Taiwan semiconductor manufacturers are able to develop smart manufacturing and automation to increase productivity and improve yield, remaining the leading position in semiconductor industry around the world.\n\nThe Smart Manufacturing Pavilion at SEMICON Taiwan 2016 will expand its exhibiting content to the latest technology breakthroughs in system integration, software, and sensors, providing a one-stop networking platform for key suppliers in the industry.",
                         @"With the aim of promoting Taiwan's semiconductor equipment industry, the Metal Industries Research and Development Center (MIRDC) has been commissioned by MOEA's Industrial Development Bureau to assist the industry in the development of semiconductor manufacturing equipment, parts & components, as well as provide logistics support. By attracting foreign investments, building supply chains, facilitating integrated production processes, manufacturing key components, developing alliances with machine-tool makers, securing international technical cooperation, and providing early validation and testing services, the MIRDC is focused on assisting domestic parts and components manufacturers with satisfying costumer demands for product quality, and ensuring a faster turnaround for the products to enter customer's production lines. The MIRDC has already accomplished substantial achievements from its long-term efforts, and has successfully promoted its operations in domestic wafer fabrication plants, packaging facilities, and system factories.\n\nSince the enthusiastic response on the debut in Semicon 2015 the first-ever invite of the MIRDC and various domestic manufacturers under its tutelage showcased Taiwan's equipment and component manufacturing capabilities, with a wide array of key products on display. This year, in Semicon 2016, MIRDC and the co-exhibitors bring: high pressure plasmas, high purity system-integration, special surface treatment technologies, ceramic processing technologies, smart-manufacturing sheet metals, Process Solution Provider in IC Advanced Packaging Field, and other breakthrough technologies in the industry. MIRDC will also demonstrate its exclusively constructed functional testing platform. By offering multi-domain services and developments, the MIRDC hopes that both domestic and foreign buyers will gain a better understanding of Taiwan's potential in the production of equipment and components."];
    
    
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        _regionIntro = regionZH;
        _themeIntro = themeZH;
    }
    else {
        _regionIntro = regionEN;
        _themeIntro = themeEN;
    }
    
}

@end
