//
//  AddNoteViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/24.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NoteContentField.h"

@interface AddNoteViewController : UIViewController
- (IBAction)cancelBtn:(id)sender;
- (IBAction)saveBtn:(id)sender;
@property NSString *boothNumber;
@property NSString *companyName;
@property NSString *imgUrl;

@property (weak, nonatomic) IBOutlet UILabel *companyNameLabel;
@property (weak, nonatomic) IBOutlet UITextField *noteTitle;
@property (weak, nonatomic) IBOutlet NoteContentField *noteContent;



@end
