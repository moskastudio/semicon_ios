//
//  ExhibitorBoothNoListTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/15.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitorBoothNoListTableViewController : UITableViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property NSString *navBarTitle;
@property NSIndexPath *selectedFloorIndexPath;
@end
