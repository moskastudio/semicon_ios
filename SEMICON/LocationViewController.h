//
//  LocationViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/14.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <GoogleMaps/GoogleMaps.h>


@interface LocationViewController : UIViewController <GMSMapViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, strong) NSArray *itemsImage;
@property (nonatomic, strong) NSArray *itemsSelectedImage;

@property (strong, nonatomic) IBOutlet GMSMapView *mapView;

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@end
