//
//  CalendarDay1TableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "CalendarDay1TableViewController.h"
#import <EventKit/EventKit.h>

@interface CalendarDay1TableViewController () {
    NSString *currentHour;
    NSIndexPath *selectedIndexPath;
}
@property NSArray *titleArray;
@property NSArray *tagArray;
@property NSArray *startTimeArray;
@property NSArray *timeArray;
@property NSArray *currentArray;
@property NSMutableArray *reminders;

@property NSString *formattedDateString;
@end

@implementation CalendarDay1TableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    _titleArray = [[NSArray alloc] initWithObjects:@"High-Tech Facility Pavilion",@"Maximizing Your Access to the Dynamic Taiwan Market",@"SEMICON 2016", nil];
//    _tagArray = [[NSArray alloc] initWithObjects:@"Exhibitor",@"Program",@"Events/ Activities", nil];
//    _startTimeArray = [[NSArray alloc] initWithObjects:@"13:00",@"15:00",@"9:00", nil];
//    _timeArray = [[NSArray alloc] initWithObjects:@"13:00~14:00",@"15:00~17:00",@"10:00~17:00", nil];
//    _currentArray = [[NSArray alloc] initWithObjects:@"NO",@"NO",@"YES", nil];
    
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
    _reminders = [[NSMutableArray alloc] init];
    
    for (NSDictionary *reminder in temp) {
        if ([[reminder objectForKey:@"date"] isEqualToString:@"9/7"]) {
            [_reminders addObject:reminder];
        }
    }
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    [dateFormat setDateFormat:@"HH"];
    currentHour = [dateFormat stringFromDate:[NSDate date]];
    
    [self.tableView reloadData];
    
//    NSDate *date = [[NSDate alloc] init];
//    NSLog(@"%@", date);
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"hh-mm"];
//    NSString *dateString = [dateFormatter stringFromDate:date];
//    NSLog(@"%@", dateString);
     NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    // @"yyyy-MM-dd HH:mm:ss"
    _formattedDateString = [dateFormatter stringFromDate:[NSDate date]];
    NSLog(@"%@",_formattedDateString);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshCell {
    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
    _reminders = [[NSMutableArray alloc] init];
    
    for (NSDictionary *reminder in temp) {
        if ([[reminder objectForKey:@"date"] isEqualToString:@"9/7"]) {
            [_reminders addObject:reminder];
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([_formattedDateString isEqualToString:@"2016-09-07"]) {
         return _reminders.count + 1;
    }
    else
         return _reminders.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CalendarDay1Cell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    //第一天當日
    if ([_formattedDateString isEqualToString:@"2016-09-07"]) {
        if (indexPath.row == 0) {
            UILabel *currentDate=[[UILabel alloc]initWithFrame:CGRectMake(0,6,cell.frame.size.width/3,14)];
            [currentDate setFont:[UIFont fontWithName:@"GothamMedium" size:14]];
            currentDate.textColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
            currentDate.text = NSLocalizedString(@"Today", "") ;
            currentDate.textAlignment = NSTextAlignmentCenter;
            [cell addSubview:currentDate];
        }
        else{
            //要顯示“Current”
            if ([currentHour isEqualToString:[[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"remindHour"]]) {
                UILabel *statrTime=[[UILabel alloc]initWithFrame:CGRectMake(16,41,40,14)];
                [statrTime setFont:[UIFont fontWithName:@"GothamBook" size:13]];
                statrTime.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
                statrTime.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"remindMin"]];
                statrTime.textAlignment = NSTextAlignmentLeft;
                [statrTime setNumberOfLines:1];
                statrTime.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:statrTime];
                
                UILabel *tagLabel=[[UILabel alloc]initWithFrame:CGRectMake(statrTime.frame.origin.x+statrTime.frame.size.width+16,10,70,20)];
                [tagLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
                tagLabel.textColor = [UIColor whiteColor];
                tagLabel.text = NSLocalizedString(@"Current", "");
                tagLabel.textAlignment = NSTextAlignmentCenter;
                [tagLabel setNumberOfLines:1];
                tagLabel.adjustsFontSizeToFitWidth = NO;
                tagLabel.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
                tagLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:tagLabel];
                
                UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(statrTime.frame.origin.x+statrTime.frame.size.width+16,37,self.view.frame.size.width-89-statrTime.frame.size.width,16)];
                [titleLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
                titleLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
                titleLabel.text = [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"Name"];
                titleLabel.textAlignment = NSTextAlignmentLeft;
                [titleLabel setNumberOfLines:1];
                titleLabel.adjustsFontSizeToFitWidth = NO;
                titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:titleLabel];
                
                UILabel *detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.frame.origin.x,60,self.view.frame.size.width-89-statrTime.frame.size.width,14)];
                [detailLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
                detailLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
                detailLabel.text = [NSString stringWithFormat:@"%@   %@", [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"type"], [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"eventTime"]];
                detailLabel.textAlignment = NSTextAlignmentLeft;
                [detailLabel setNumberOfLines:1];
                detailLabel.adjustsFontSizeToFitWidth = NO;
                detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:detailLabel];
                
                UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
                moreBtn.frame = CGRectMake(cell.frame.size.width-43, 0, 40, 96);
                UIImage *btnImage = [UIImage imageNamed:@"btn_myfavorite_more_grey.png"];
                [moreBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
                [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:moreBtn];
            }
            //不顯示“Current”
            else{
                UILabel *statrTime=[[UILabel alloc]initWithFrame:CGRectMake(16,41,40,14)];
                [statrTime setFont:[UIFont fontWithName:@"GothamBook" size:13]];
                statrTime.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
                statrTime.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"remindMin"]];
                statrTime.textAlignment = NSTextAlignmentLeft;
                [statrTime setNumberOfLines:1];
                statrTime.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:statrTime];
                
                UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(statrTime.frame.origin.x+statrTime.frame.size.width+16,28,self.view.frame.size.width-89-statrTime.frame.size.width,16)];
                [titleLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
                titleLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
                titleLabel.text = [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"Name"];
                titleLabel.textAlignment = NSTextAlignmentLeft;
                [titleLabel setNumberOfLines:1];
                titleLabel.adjustsFontSizeToFitWidth = NO;
                titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:titleLabel];
                
                UILabel *detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.frame.origin.x,51,self.view.frame.size.width-89-statrTime.frame.size.width,14)];
                [detailLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
                detailLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
                detailLabel.text = [NSString stringWithFormat:@"%@   %@", [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"type"], [[_reminders objectAtIndex:indexPath.row - 1] objectForKey:@"eventTime"]];
                detailLabel.textAlignment = NSTextAlignmentLeft;
                [detailLabel setNumberOfLines:1];
                detailLabel.adjustsFontSizeToFitWidth = NO;
                detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                [cell addSubview:detailLabel];
                
                UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
                moreBtn.frame = CGRectMake(cell.frame.size.width-43, 0, 40, 96);
                UIImage *btnImage = [UIImage imageNamed:@"btn_myfavorite_more_grey.png"];
                [moreBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
                [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
                [cell addSubview:moreBtn];
            }
        }
    }
    //非當日
    else{
        UILabel *statrTime=[[UILabel alloc]initWithFrame:CGRectMake(16,41,40,14)];
        [statrTime setFont:[UIFont fontWithName:@"GothamBook" size:13]];
        statrTime.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        statrTime.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindMin"]];
        statrTime.textAlignment = NSTextAlignmentLeft;
        [statrTime setNumberOfLines:1];
        statrTime.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:statrTime];
        
        UILabel *titleLabel=[[UILabel alloc]initWithFrame:CGRectMake(statrTime.frame.origin.x+statrTime.frame.size.width+16,28,self.view.frame.size.width-89-statrTime.frame.size.width,16)];
        [titleLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        titleLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        titleLabel.text = [[_reminders objectAtIndex:indexPath.row] objectForKey:@"Name"];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        [titleLabel setNumberOfLines:1];
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:titleLabel];
        
        UILabel *detailLabel=[[UILabel alloc]initWithFrame:CGRectMake(titleLabel.frame.origin.x,51,self.view.frame.size.width-89-statrTime.frame.size.width,14)];
        [detailLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        detailLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        detailLabel.text = [NSString stringWithFormat:@"%@   %@", [[_reminders objectAtIndex:indexPath.row] objectForKey:@"type"], [[_reminders objectAtIndex:indexPath.row] objectForKey:@"eventTime"]];
        detailLabel.textAlignment = NSTextAlignmentLeft;
        [detailLabel setNumberOfLines:1];
        detailLabel.adjustsFontSizeToFitWidth = NO;
        detailLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:detailLabel];
        
        UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        moreBtn.frame = CGRectMake(cell.frame.size.width-43, 0, 40, 96);
        UIImage *btnImage = [UIImage imageNamed:@"btn_myfavorite_more_grey.png"];
        [moreBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:moreBtn];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([_formattedDateString isEqualToString:@"2016-09-07"]) {
        if (indexPath.row == 0) {
            return 30;
        }
        else
            return 96;
    }
    else
        return 96;
}

- (void)moreBtnAction:(id)sender {
    NSLog(@"moreBtnAction");
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
    selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", "")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* EditBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Edit", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  [_delegate showCalendar:[_reminders objectAtIndex:selectedIndexPath.row]];
                              }];
    
    UIAlertAction* deleteBtn = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Delete", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
                                    
                                    for (int i = 0; i < temp.count; i++) {
                                        NSDictionary *reminder = [temp objectAtIndex:i];
                                        if ([[reminder objectForKey:@"typeID"] isEqualToString:[[_reminders objectAtIndex:selectedIndexPath.row] objectForKey:@"typeID"]]) {
                                            
                                            [self cancelLocalPush:[[_reminders objectAtIndex:selectedIndexPath.row] objectForKey:@"typeID"]];
                                            
                                            [self removeEventFromCalendar:[[_reminders objectAtIndex:selectedIndexPath.row] objectForKey:@"eventID"]];
                                            
                                            [temp removeObjectAtIndex:i];
                                            [_reminders removeObjectAtIndex:selectedIndexPath.row];
                                            
                                            break;
                                        }
                                    }
                                    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:@"Reminder"];
                                    
                                    [self.tableView beginUpdates];
                                    [self.tableView deleteRowsAtIndexPaths:@[selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                    [self.tableView endUpdates];
                                }];
    
    [alert addAction:Cancel];
    [alert addAction:EditBtn];
    [alert addAction:deleteBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)removeEventFromCalendar:(NSString *)eventID {
    EKEventStore *store = [EKEventStore new];
    
    EKEvent *event = [store eventWithIdentifier:eventID];
    
    if (event != nil) {
        NSError *error = nil;
        [store removeEvent:event span:EKSpanThisEvent commit:YES error:&error];
    }
}

- (void)cancelLocalPush:(NSString *)typeID {
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *local in localNotifications) {
        NSDictionary *userInfo = local.userInfo;
        
        if ([[userInfo objectForKey:@"typeID"] isEqualToString:typeID]) {
            [[UIApplication sharedApplication] cancelLocalNotification:local];
        }
    }
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
