//
//  ShuttleBusViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AppDelegate.h"
#import "ShuttleBusViewController.h"
#import "RegisterViewController.h"
#import <MapKit/MapKit.h>
//Slider
#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r)/255.0f green: (g)/255.0f blue: (b)/255.0f alpha:1.0]

@interface ShuttleBusViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;

@property UIView *coverView;
@property NSString *shuttleBusDeparture;
@end

@implementation ShuttleBusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //Slider
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
    
    
    //Departure Time PopUp View
    self.coverView = [[UIView alloc] initWithFrame:
                             CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    self.coverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIButton *registerNowBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    registerNowBtn.frame = CGRectMake(0, self.coverView.frame.size.height-64-(self.coverView.frame.size.width/720*100), self.coverView.frame.size.width, self.coverView.frame.size.width/720*100);
    UIImage *btnImage = [UIImage imageNamed:@"img_register_bg.png"];
    [registerNowBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [registerNowBtn setTitle:NSLocalizedString(@"Register_Shuttle", "")  forState:UIControlStateNormal];
    registerNowBtn.tintColor = [UIColor whiteColor];
    registerNowBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBold" size:14.5];
    [registerNowBtn addTarget:self action:@selector(registerAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.coverView addSubview:registerNowBtn];
    
    UIView *departureTimePopUpView = [[UIView alloc]
                                      initWithFrame:CGRectMake(self.view.frame.size.width/15, (self.view.frame.size.height-64-registerNowBtn.frame.size.height)/15, self.view.frame.size.width/15*13, (self.view.frame.size.height-64-registerNowBtn.frame.size.height)/15*13)];
    departureTimePopUpView.backgroundColor = [UIColor whiteColor];
    departureTimePopUpView.layer.cornerRadius = 10.0;
    departureTimePopUpView.clipsToBounds = YES;
    [self.coverView addSubview:departureTimePopUpView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(departureTimePopUpView.frame.size.width/2-90, 10, 180, 40)];
    titleLabel.text = NSLocalizedString(@"Departure Time", "") ;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:17];
    titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [departureTimePopUpView addSubview:titleLabel];
    
    UIView *lineUp = [[UIView alloc] initWithFrame: CGRectMake(0, 60, departureTimePopUpView.frame.size.width, 1.5)];
    lineUp.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [departureTimePopUpView addSubview:lineUp];
    
    UIView *Wed = [[UIView alloc] initWithFrame: CGRectMake(0, 60, departureTimePopUpView.frame.size.width, (departureTimePopUpView.frame.size.height-120)/3)];
    Wed.backgroundColor = [UIColor clearColor];
    [departureTimePopUpView addSubview:Wed];
    
    UILabel *wedLabel = [[UILabel alloc] initWithFrame:CGRectMake(departureTimePopUpView.frame.size.width/2-90, 0, 180, Wed.frame.size.height/2)];
    wedLabel.text = NSLocalizedString( @"Wed. 9/7","");
    wedLabel.textAlignment = NSTextAlignmentCenter;
    wedLabel.font = [UIFont fontWithName:@"GothamMedium" size:15];
    wedLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    [Wed addSubview:wedLabel];
    
    UIView *Thu = [[UIView alloc] initWithFrame: CGRectMake(0, Wed.frame.origin.y+Wed.frame.size.height, departureTimePopUpView.frame.size.width, (departureTimePopUpView.frame.size.height-120)/3)];
    Thu.backgroundColor = [UIColor clearColor];
    [departureTimePopUpView addSubview:Thu];
    
    UILabel *thuLabel = [[UILabel alloc] initWithFrame:CGRectMake(departureTimePopUpView.frame.size.width/2-90, 0, 180, Thu.frame.size.height/2)];
    thuLabel.text = NSLocalizedString( @"Thu. 9/8", "");
    thuLabel.textAlignment = NSTextAlignmentCenter;
    thuLabel.font = [UIFont fontWithName:@"GothamMedium" size:15];
    thuLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    [Thu addSubview:thuLabel];
    
    UIView *Fri = [[UIView alloc] initWithFrame: CGRectMake(0, Thu.frame.origin.y+Thu.frame.size.height, departureTimePopUpView.frame.size.width, (departureTimePopUpView.frame.size.height-120)/3)];
    Fri.backgroundColor = [UIColor clearColor];
    [departureTimePopUpView addSubview:Fri];
    
    UILabel *frLabel = [[UILabel alloc] initWithFrame:CGRectMake(departureTimePopUpView.frame.size.width/2-90, 0, 180, Thu.frame.size.height/2)];
    frLabel.text = NSLocalizedString( @"Fri. 9/9","");
    frLabel.textAlignment = NSTextAlignmentCenter;
    frLabel.font = [UIFont fontWithName:@"GothamMedium" size:15];
    frLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    [Fri addSubview:frLabel];
    
    if ([_shuttleBusDeparture isEqualToString:@"HsinChu"]) {
        UILabel *wedShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Wed.frame.size.height/2-5, Wed.frame.size.width, Wed.frame.size.height/2)];
        wedShuttleTime.text = @"07:30    07:45    08:00    08:15";
        wedShuttleTime.textAlignment = NSTextAlignmentCenter;
        wedShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        wedShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Wed addSubview:wedShuttleTime];
        
        UILabel *thuShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Thu.frame.size.height/2-5, Thu.frame.size.width, Thu.frame.size.height/2)];
        thuShuttleTime.text = @"07:30    07:45    08:00    08:15";
        thuShuttleTime.textAlignment = NSTextAlignmentCenter;
        thuShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        thuShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Thu addSubview:thuShuttleTime];
        
        UILabel *friShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Fri.frame.size.height/2-5, Fri.frame.size.width, Fri.frame.size.height/2)];
        friShuttleTime.text = @"07:30    07:45    08:00    08:15";
        friShuttleTime.textAlignment = NSTextAlignmentCenter;
        friShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        friShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Fri addSubview:friShuttleTime];
    }
    else{
        UILabel *wedShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Wed.frame.size.height/2-5, Wed.frame.size.width, Wed.frame.size.height/2)];
        wedShuttleTime.text = @"16:45    17:00    17:15    17:30";
        wedShuttleTime.textAlignment = NSTextAlignmentCenter;
        wedShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        wedShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Wed addSubview:wedShuttleTime];
        
        UILabel *thuShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Thu.frame.size.height/2-5, Thu.frame.size.width, Thu.frame.size.height/2)];
        thuShuttleTime.text = @"16:45    17:00    17:15";
        thuShuttleTime.textAlignment = NSTextAlignmentCenter;
        thuShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        thuShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Thu addSubview:thuShuttleTime];
        
        UILabel *friShuttleTime = [[UILabel alloc] initWithFrame:CGRectMake(0,  Fri.frame.size.height/2-5, Fri.frame.size.width, Fri.frame.size.height/2)];
        friShuttleTime.text = @"16:00    16:20";
        friShuttleTime.textAlignment = NSTextAlignmentCenter;
        friShuttleTime.font = [UIFont fontWithName:@"GothamBook" size:14];
        friShuttleTime.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [Fri addSubview:friShuttleTime];
    }
    
    UIView *lineDown = [[UIView alloc] initWithFrame: CGRectMake(0, departureTimePopUpView.frame.size.height-60, departureTimePopUpView.frame.size.width, 1.5)];
    lineDown.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [departureTimePopUpView addSubview:lineDown];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    backBtn.frame = CGRectMake(departureTimePopUpView.frame.size.width/2-100, departureTimePopUpView.frame.size.height-50, 200, 40);
    [backBtn setTitle:NSLocalizedString(@"Back", "")  forState:UIControlStateNormal];
    backBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    backBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamMedium" size:17];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [departureTimePopUpView addSubview:backBtn];
    
    
    [self.view addSubview:self.coverView];
    self.coverView.hidden = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Hsinchu Scince Park", "") , NSLocalizedString(@"Nangang Exhibition Hall", "") ]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 2;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            view.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1.0];
            UIView *mapBackground = [[UIView alloc] initWithFrame:
                                     CGRectMake(17,6,self.view.frame.size.width-34,self.view.frame.size.height-136)];
            mapBackground.backgroundColor = [UIColor whiteColor];
            mapBackground.layer.shadowColor = [UIColor blackColor].CGColor;
            mapBackground.layer.shadowOffset = CGSizeMake(0, 2);
            mapBackground.layer.shadowOpacity = 0.4;
            mapBackground.layer.shadowRadius = 2.0;
            
            //Title
            UIView *titleBackground = [[UIView alloc] initWithFrame:
                            CGRectMake(0, 0, mapBackground.frame.size.width, 90)];
            titleBackground.backgroundColor = [UIColor colorWithRed:0.918 green:0.914 blue:0.918 alpha:1.0];
            [mapBackground addSubview:titleBackground];
            
            UILabel *captions = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, mapBackground.frame.size.width-70, 90)];
            captions.numberOfLines = 0;
            captions.font = [UIFont fontWithName:@"GothamMedium" size:20];
            captions.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            NSString *labelText = NSLocalizedString(@"Hsinchu Science Park to Nangang Exhibition Hall", "");
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:12];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
            captions.attributedText = attributedString ;
            [titleBackground addSubview:captions];
            
            //map image
            UIImageView *map = [[UIImageView alloc] initWithFrame: CGRectMake(0,90,mapBackground.frame.size.width,840*mapBackground.frame.size.width/1944)];
            UIImage *mapImage = [UIImage imageNamed:@"img_science_park_life_Hub.png"];
            map.image = mapImage;
            [mapBackground addSubview:map];
            
            UIButton *googleMapBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            googleMapBtn.frame = CGRectMake(mapBackground.frame.size.width-67, 90+map.frame.size.height-67, 52, 52);
            UIImage *btnImage = [UIImage imageNamed:@"btn_google_map.png"];
            [googleMapBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
            [googleMapBtn addTarget:self action:@selector(googleMapHsinchuAction:) forControlEvents:UIControlEventTouchUpInside];
            [mapBackground addSubview:googleMapBtn];
            
            //boardingSpotView
            UIView *boardingSpotView = [[UIView alloc] initWithFrame:
                            CGRectMake(0, 90+840*mapBackground.frame.size.width/1944, mapBackground.frame.size.width, (mapBackground.frame.size.height-90-52-840*mapBackground.frame.size.width/1944)/2)];
            boardingSpotView.backgroundColor = [UIColor clearColor];
            [mapBackground addSubview:boardingSpotView];
            
            UIImageView *busStation = [[UIImageView alloc] initWithFrame: CGRectMake(29,boardingSpotView.frame.size.height/2-17,34,34)];
            UIImage *busStationImage = [UIImage imageNamed:@"img_bus_station.png"];
            busStation.image = busStationImage;
            [boardingSpotView addSubview:busStation];
            
            UILabel *busStationLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, boardingSpotView.frame.size.height/2-7, 120, 15)];
            busStationLabel.text = NSLocalizedString(@"Boarding Spot", @"");
            busStationLabel.font = [UIFont fontWithName:@"GothamMedium" size:13.5];
            busStationLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [boardingSpotView addSubview:busStationLabel];
            
            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, busStationLabel.frame.origin.y+25, 200, 13)];
            textLabel.text = NSLocalizedString(@"Science Park Life Hub", "");
            textLabel.font = [UIFont fontWithName:@"GothamBook" size:13];
            textLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            [boardingSpotView addSubview:textLabel];
            
            //departureTimeView
            UIView *departureTimeView = [[UIView alloc] initWithFrame:
                                        CGRectMake(0, 90+840*mapBackground.frame.size.width/1944+boardingSpotView.frame.size.height, mapBackground.frame.size.width, (mapBackground.frame.size.height-90-52-840*mapBackground.frame.size.width/1944)/2)];
            departureTimeView.backgroundColor = [UIColor clearColor];
            [mapBackground addSubview:departureTimeView];
            
            UIImageView *busTime = [[UIImageView alloc] initWithFrame: CGRectMake(29,departureTimeView.frame.size.height/2-17,34,34)];
            UIImage *busTimeImage = [UIImage imageNamed:@"img_time.png"];
            busTime.image = busTimeImage;
            [departureTimeView addSubview:busTime];
            
            UILabel *busTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, departureTimeView.frame.size.height/2-7, 120, 15)];
            busTimeLabel.text = NSLocalizedString(@"Departure Time", "") ;
            busTimeLabel.font = [UIFont fontWithName:@"GothamMedium" size:13.5];
            busTimeLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [departureTimeView addSubview:busTimeLabel];
            
            UIButton *viewBusTimeBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            viewBusTimeBtn.frame = CGRectMake(departureTimeView.frame.size.width-75, departureTimeView.frame.size.height/2-14, 50, 28);
            [viewBusTimeBtn setTitle:NSLocalizedString(@"View", "")  forState:UIControlStateNormal];
            viewBusTimeBtn.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
            viewBusTimeBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBook" size:13];
            viewBusTimeBtn.layer.borderColor = [[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0] CGColor];
            viewBusTimeBtn.layer.borderWidth = 0.5;
            viewBusTimeBtn.layer.cornerRadius = 5.0;
            viewBusTimeBtn.clipsToBounds = YES;
            [viewBusTimeBtn addTarget:self action:@selector(viewBusTimeHsinChuAction:) forControlEvents:UIControlEventTouchUpInside];
            [departureTimeView addSubview:viewBusTimeBtn];
            
            //
            UIView *line = [[UIView alloc] initWithFrame:
                            CGRectMake(0, mapBackground.frame.size.height-51, mapBackground.frame.size.width, 1.5)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [mapBackground addSubview:line];
            
            //
            UIButton *registerBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            registerBtn.frame = CGRectMake(mapBackground.frame.size.width/2-75, mapBackground.frame.size.height-50, 150, 50);
            [registerBtn setTitle:NSLocalizedString(@"Register_Shuttle" , "") forState:UIControlStateNormal];
            registerBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
            registerBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBold" size:14.5];
            [registerBtn addTarget:self action:@selector(registerAction:) forControlEvents:UIControlEventTouchUpInside];
            [mapBackground addSubview:registerBtn];
        
            [view addSubview:mapBackground];
        }
            break;
        case 1:{
            view.backgroundColor = [UIColor colorWithRed:0.98 green:0.98 blue:0.98 alpha:1.0];
            UIView *mapBackground = [[UIView alloc] initWithFrame:
                                     CGRectMake(17,6,self.view.frame.size.width-34,self.view.frame.size.height-72)];
            mapBackground.backgroundColor = [UIColor whiteColor];
            mapBackground.layer.shadowColor = [UIColor blackColor].CGColor;
            mapBackground.layer.shadowOffset = CGSizeMake(0, 2);
            mapBackground.layer.shadowOpacity = 0.4;
            mapBackground.layer.shadowRadius = 2.0;
            
            //Title
            UIView *titleBackground = [[UIView alloc] initWithFrame:
                                       CGRectMake(0, 0, mapBackground.frame.size.width, 90)];
            titleBackground.backgroundColor = [UIColor colorWithRed:0.918 green:0.914 blue:0.918 alpha:1.0];
            [mapBackground addSubview:titleBackground];
            
            UILabel *captions = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, mapBackground.frame.size.width-70, 90)];
            captions.numberOfLines = 0;
            captions.font = [UIFont fontWithName:@"GothamMedium" size:20];
            captions.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            NSString *labelText = NSLocalizedString(@"Nangang Exhibition Hall to Hsinchu Science Park", "") ;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:12];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
            captions.attributedText = attributedString ;
            [titleBackground addSubview:captions];
            
            //map image
            UIImageView *map = [[UIImageView alloc] initWithFrame: CGRectMake(0,90,mapBackground.frame.size.width,840*mapBackground.frame.size.width/1944)];
            UIImage *mapImage = [UIImage imageNamed:@"img_jingmao_road.png"];
            map.image = mapImage;
            [mapBackground addSubview:map];
            
            UIButton *googleMapBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            googleMapBtn.frame = CGRectMake(mapBackground.frame.size.width-67, 90+map.frame.size.height-67, 52, 52);
            UIImage *btnImage = [UIImage imageNamed:@"btn_google_map.png"];
            [googleMapBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
            [googleMapBtn addTarget:self action:@selector(googleMapAction:) forControlEvents:UIControlEventTouchUpInside];
            [mapBackground addSubview:googleMapBtn];
            
            //boardingSpotView
            UIView *boardingSpotView = [[UIView alloc] initWithFrame:
                                        CGRectMake(0, 90+840*mapBackground.frame.size.width/1944, mapBackground.frame.size.width, (mapBackground.frame.size.height-90-52-840*mapBackground.frame.size.width/1944)/2)];
            boardingSpotView.backgroundColor = [UIColor clearColor];
            [mapBackground addSubview:boardingSpotView];
            
            UIImageView *busStation = [[UIImageView alloc] initWithFrame: CGRectMake(29,boardingSpotView.frame.size.height/2-17,34,34)];
            UIImage *busStationImage = [UIImage imageNamed:@"img_bus_station.png"];
            busStation.image = busStationImage;
            [boardingSpotView addSubview:busStation];
            
            UILabel *busStationLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, boardingSpotView.frame.size.height/2-7, 120, 15)];
            busStationLabel.text = NSLocalizedString(@"Boarding Spot", @"");
            busStationLabel.font = [UIFont fontWithName:@"GothamMedium" size:13.5];
            busStationLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [boardingSpotView addSubview:busStationLabel];
            
            UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, busStationLabel.frame.origin.y+25, 200, 13)];
            textLabel.text = NSLocalizedString(@"Nangang Exhibition Hall", "") ;
            textLabel.font = [UIFont fontWithName:@"GothamBook" size:13];
            textLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            [boardingSpotView addSubview:textLabel];
            
            //departureTimeView
            UIView *departureTimeView = [[UIView alloc] initWithFrame:
                                         CGRectMake(0, 90+840*mapBackground.frame.size.width/1944+boardingSpotView.frame.size.height, mapBackground.frame.size.width, (mapBackground.frame.size.height-90-52-840*mapBackground.frame.size.width/1944)/2)];
            departureTimeView.backgroundColor = [UIColor clearColor];
            [mapBackground addSubview:departureTimeView];
            
            UIImageView *busTime = [[UIImageView alloc] initWithFrame: CGRectMake(29,departureTimeView.frame.size.height/2-17,34,34)];
            UIImage *busTimeImage = [UIImage imageNamed:@"img_time.png"];
            busTime.image = busTimeImage;
            [departureTimeView addSubview:busTime];
            
            UILabel *busTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(83, departureTimeView.frame.size.height/2-7, 120, 15)];
            busTimeLabel.text = NSLocalizedString(@"Departure Time", "") ;
            busTimeLabel.font = [UIFont fontWithName:@"GothamMedium" size:13.5];
            busTimeLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [departureTimeView addSubview:busTimeLabel];
            
            UIButton *viewBusTimeBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            viewBusTimeBtn.frame = CGRectMake(departureTimeView.frame.size.width-75, departureTimeView.frame.size.height/2-14, 50, 28);
            [viewBusTimeBtn setTitle:NSLocalizedString(@"View", "")  forState:UIControlStateNormal];
            viewBusTimeBtn.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
            viewBusTimeBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBook" size:13];
            viewBusTimeBtn.layer.borderColor = [[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0] CGColor];
            viewBusTimeBtn.layer.borderWidth = 0.5;
            viewBusTimeBtn.layer.cornerRadius = 5.0;
            viewBusTimeBtn.clipsToBounds = YES;
            [viewBusTimeBtn addTarget:self action:@selector(viewBusTimeNangangAction:) forControlEvents:UIControlEventTouchUpInside];
            [departureTimeView addSubview:viewBusTimeBtn];
            
            //
            UIView *line = [[UIView alloc] initWithFrame:
                            CGRectMake(0, mapBackground.frame.size.height-51, mapBackground.frame.size.width, 1.5)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [mapBackground addSubview:line];
            
            //
            UIButton *registerBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            registerBtn.frame = CGRectMake(mapBackground.frame.size.width/2-75, mapBackground.frame.size.height-50, 150, 50);
            [registerBtn setTitle:NSLocalizedString(@"Register_Shuttle", "")  forState:UIControlStateNormal];
            registerBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
            registerBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBold" size:14.5];
            [registerBtn addTarget:self action:@selector(registerAction:) forControlEvents:UIControlEventTouchUpInside];
            [mapBackground addSubview:registerBtn];
            
            [view addSubview:mapBackground];
        }
            break;
        default:
            break;
    }
    return view;
}

//- (void)indexTapped:(id)sender {
//    MainViewController *mvc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainViewController"];
//    [self presentViewController:mvc animated:YES completion:nil];
//}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)registerAction:(id)sender {
    NSLog(@"register");
    RegisterViewController *registerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    registerViewController.title = NSLocalizedString(@"Register_Shuttle", @"");
    [self.navigationController pushViewController:registerViewController animated:YES];
}

-(void) viewBusTimeHsinChuAction:(id)sender {
    self.coverView.hidden = NO;
    _shuttleBusDeparture = @"HsinChu";
    self.navigationItem.leftBarButtonItem.customView.hidden=YES;
}

-(void) viewBusTimeNangangAction:(id)sender {
    self.coverView.hidden = NO;
    _shuttleBusDeparture = @"Nangang";
    self.navigationItem.leftBarButtonItem.customView.hidden=YES;
}

-(void) googleMapHsinchuAction:(id)sender {
    NSLog(@"googleMapAction");
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:@"comgooglemaps://?q=Hsinchu+Science+Park&center=24.781651,121.0037267&zoom=10&views=traffic"]];
    } else {
//        CLLocationCoordinate2D rdOfficeLocation = CLLocationCoordinate2DMake(24.781651,121.0037267);
//        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:rdOfficeLocation addressDictionary:nil];
//        MKMapItem *item = [[MKMapItem alloc] initWithPlacemark:placemark];
//        item.name = @"Hsinchu Science Park";
//        [item openInMapsWithLaunchOptions:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=Hsinchu+Science+Park&center=24.781651,121.0037267&zoom=10&views=traffic"]];
        [[UIApplication sharedApplication] openURL: url];
    }
}

-(void) googleMapAction:(id)sender {
    NSLog(@"googleMapAction");
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:@"comgooglemaps://?q=Taipei+Nangang+Exhibition+Center&center=25.0566068,121.6159273&zoom=10&views=traffic"]];
    } else {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=Taipei+Nangang+Exhibition+Center&center=25.0566068,121.6159273&zoom=10&views=traffic"]];
        [[UIApplication sharedApplication] openURL: url];
    }
}

-(void) backAction:(id)sender {
    self.coverView.hidden = YES;
    self.navigationItem.leftBarButtonItem.customView.hidden=NO;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
