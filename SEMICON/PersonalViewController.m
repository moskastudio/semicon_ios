//
//  PersonalViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "PersonalViewController.h"
#import "Service.h"
#import "SignInViewController.h"

@interface PersonalViewController ()

@end

@implementation PersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    UIImageView *topBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,0,self.view.frame.size.width,72*self.view.frame.size.width/1080)];
//    UIImage *topBackgroundImage = [UIImage imageNamed:@"img_floor_top_bg.png"];
//    topBackground.image = topBackgroundImage;
//    [self.view addSubview:topBackground];
//    
//    UIView *line1 = [[UIView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/10,(self.view.frame.size.height-130)/3,self.view.frame.size.width/10*8,2)];
//    line1.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.727 alpha:1.0f];
//    [self.view addSubview:line1];
//    
//    UIView *line2 = [[UIView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/10,(self.view.frame.size.height-130)/3*2,self.view.frame.size.width/10*8,2)];
//    line2.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.727 alpha:1.0f];
//    [self.view addSubview:line2];
//    
//    UIImageView *calendar = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/10+15,(self.view.frame.size.height-130)/9*1.3,(self.view.frame.size.height-130)/9,(self.view.frame.size.height-130)/9)];
//    UIImage *calendarImage = [UIImage imageNamed:@"img_my_calendar.png"];
//    calendar.image = calendarImage;
//    [self.view addSubview:calendar];
//    
//    UIImageView *favorites = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/10+15,(self.view.frame.size.height-130)/9*4,(self.view.frame.size.height-130)/9,(self.view.frame.size.height-130)/9)];
//    UIImage *favoritesImage = [UIImage imageNamed:@"img_my_favorites.png"];
//    favorites.image = favoritesImage;
//    [self.view addSubview:favorites];
//
//    UIImageView *notes = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/10+15,(self.view.frame.size.height-130)/9*7,(self.view.frame.size.height-130)/9,(self.view.frame.size.height-130)/9)];
//    UIImage *notesImage = [UIImage imageNamed:@"img_my_notes.png"];
//    notes.image = notesImage;
//    [self.view addSubview:notes];
//    
//    UILabel *Calendar=[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/10+(self.view.frame.size.height-130)/6, (self.view.frame.size.height-130)/9*1.3,150,(self.view.frame.size.height-130)/9)];
//    [Calendar setFont:[UIFont fontWithName:@"Gotham Book" size:18]];
//    NSString *CalendarText = @"My Calendar";
//    Calendar.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.727 alpha:1.0f];
//    Calendar.text = CalendarText;
//    [self.view addSubview:Calendar];
//    
//    UILabel *Favorites=[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/10+(self.view.frame.size.height-130)/6, (self.view.frame.size.height-130)/9*4,150,(self.view.frame.size.height-130)/9)];
//    [Calendar setFont:[UIFont fontWithName:@"Gotham Book" size:18]];
//    NSString *favoritesText = @"My Favorites";
//    Favorites.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.727 alpha:1.0f];
//    Favorites.text = favoritesText;
//    [self.view addSubview:Favorites];
//    
//    UILabel *Notes=[[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width/10+(self.view.frame.size.height-130)/6, (self.view.frame.size.height-130)/9*7,150,(self.view.frame.size.height-130)/9)];
//    [Calendar setFont:[UIFont fontWithName:@"Gotham Book" size:18]];
//    NSString *notesText = @"My Notes";
//    Notes.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.727 alpha:1.0f];
//    Notes.text = notesText;
//    [self.view addSubview:Notes];
//    
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(handleSingleTap:)];
//    [self.view addGestureRecognizer:singleFingerTap];
}

//- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
//    CGPoint location = [recognizer locationInView:[recognizer.view superview]];
//    NSLog(@"123321:::%f,%f",location.x,location.y);
//    //Do stuff here...
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "")  style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
