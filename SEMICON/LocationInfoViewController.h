//
//  LocationInfoViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/10.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LocationInfoViewController : UIViewController<UINavigationControllerDelegate,UINavigationBarDelegate>
- (IBAction)indexBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *hotelsView;
@property (strong, nonatomic) IBOutlet UIView *foodBeverageView;
@property (strong, nonatomic) IBOutlet UIView *transportationView;
@property (strong, nonatomic) IBOutlet UIView *shuttleBusView;
@property (strong, nonatomic) IBOutlet UIButton *hotelsBtn;

@end
