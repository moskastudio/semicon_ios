//
//  LocationViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/14.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "LocationViewController.h"
#import <GoogleMaps/GoogleMaps.h>

@interface LocationViewController ()

@property UIView *transportationView;
@property UIButton *googleMapsBtn;
@property UIButton *closeTransportationView;
@property NSInteger selectedIndex;
@property UIScrollView *contentScrollView;

@end

@implementation LocationViewController{
    GMSMapView *mapView_;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.selectedIndex = 0;
    
    //Google Maps
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:25.0566068 longitude:121.6159273 zoom:15];
    self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(25.0566068,121.6159273);
    marker.title = @"南港展覽館";
    marker.snippet = @"Taipei Nangang Exhibition Center";
    marker.map = self.mapView;

    self.googleMapsBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    self.googleMapsBtn.frame = CGRectMake(self.view.frame.size.width-67, 15, 52, 52);
    UIImage *btnImage = [UIImage imageNamed:@"btn_google_map.png"];
    [self.googleMapsBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [self.googleMapsBtn addTarget:self action:@selector(googleMapAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.googleMapsBtn];
    self.googleMapsBtn.hidden = NO;
    self.googleMapsBtn.enabled = YES;
    
    self.closeTransportationView = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    self.closeTransportationView.frame = CGRectMake(self.view.frame.size.width-67, 15, 52, 52);
    UIImage *closeTransportationViewImage = [UIImage imageNamed:@"btn_location_cancel.png"];
    [self.closeTransportationView setBackgroundImage:closeTransportationViewImage forState:UIControlStateNormal];
    [self.closeTransportationView addTarget:self action:@selector(closeTransportationViewAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.closeTransportationView];
    self.closeTransportationView.hidden = YES;
    self.closeTransportationView.enabled = NO;
    
    
    self.transportationView = [[UIView alloc] initWithFrame:
                             CGRectMake(self.view.frame.size.width*0.025,self.view.frame.size.height-264,self.view.frame.size.width*0.95,self.view.frame.size.height-146)];
    self.transportationView.backgroundColor = [UIColor whiteColor];
    self.transportationView.layer.cornerRadius = 5.0;
    self.transportationView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.transportationView.layer.shadowOpacity = 0.4;
    self.transportationView.layer.shadowRadius = 2.0;
    self.transportationView.layer.shadowOffset = CGSizeMake(0, 2);
    [self.view addSubview:self.transportationView];
    
    UIView *line = [[UIView alloc] initWithFrame:
                    CGRectMake(0, 60, self.transportationView.frame.size.width, 1.5)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.transportationView addSubview:line];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(self.transportationView.frame.size.width/2-100, 20, 200, 20)];
    headerLabel.text = NSLocalizedString(@"Transportation", "") ;
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor blackColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:17.0]];
    [self.transportationView addSubview:headerLabel];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 70, self.transportationView.frame.size.width, 120)];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.scrollEnabled=YES;
    scrollView.userInteractionEnabled=YES;
    scrollView.contentSize = CGSizeMake(530,120);
    [self.transportationView addSubview:scrollView];
    
    self.items = @[NSLocalizedString(@"MRT", "") , NSLocalizedString(@"Car", ""), NSLocalizedString(@"Airport", ""), NSLocalizedString(@"TRA", ""), NSLocalizedString(@"THSR", ""), NSLocalizedString(@"Bus", ""), NSLocalizedString(@"Taxi", "")];
    self.itemsImage = @[@"btn_mrt.png", @"btn_car.png", @"btn_airport.png", @"btn_tra.png", @"btn_ths.png", @"btn_bus.png", @"btn_taxi.png"];
    self.itemsSelectedImage =@[@"btn_mrt_press.png", @"btn_car_press.png", @"btn_airport_press.png", @"btn_tra_press.png", @"btn_ths_press.png", @"btn_bus_press.png", @"btn_taxi_press.png"];
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 10, 530, 110) collectionViewLayout:flowLayout];
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.collectionView setDataSource:self];
    [self.collectionView setDelegate:self];
    [self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"Cell"];
    [scrollView addSubview:self.collectionView];
    
}

- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        
        // Do what you want to do with the subview
        NSLog(@"%@", subview);
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *tempLabel = (UILabel*)subview;
            [tempLabel setFrame:CGRectMake(tempLabel.frame.origin.x, tempLabel.frame.origin.y, tempLabel.frame.size.width, tempLabel.frame.size.height+5)];
        }
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    return [self.items count];
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, cell.bounds.size.width, 40)];
    title.textColor =[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
    title.textAlignment = NSTextAlignmentCenter;
    [title setFont:[UIFont fontWithName:@"GothamBook" size:15.0]];
    [title setText:[self.items objectAtIndex:indexPath.row]];
    title.tag = 100;
    [cell.contentView addSubview:title];
    
    UIImageView *transportItem = [[UIImageView alloc] initWithFrame: CGRectMake(15,0,40,40)];
    UIImage *transportItemImage = [UIImage imageNamed:[self.itemsImage objectAtIndex:indexPath.row]];
    transportItem.image = transportItemImage;
    transportItem.tag = 100;
    [cell.contentView addSubview:transportItem];
    
    return cell;
}

#pragma mark – UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(70, 100);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"didselect:::%ld",indexPath.row);
    self.selectedIndex = indexPath.row;
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, cell.bounds.size.width, 40)];
    title.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    title.textAlignment = NSTextAlignmentCenter;
    [title setFont:[UIFont fontWithName:@"GothamBook" size:15.0]];
    [title setText:[self.items objectAtIndex:indexPath.row]];
    title.tag = 100;
    [cell addSubview:title];
    
    UIImageView *transportItem = [[UIImageView alloc] initWithFrame: CGRectMake(15,0,40,40)];
    UIImage *transportItemImage = [UIImage imageNamed:[self.itemsSelectedImage objectAtIndex:indexPath.row]];
    transportItem.image = transportItemImage;
    transportItem.tag = 100;
    [cell addSubview:transportItem];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 85, 70, 2)];
    line.backgroundColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    line.tag = 100;
    [cell addSubview:line];
    
    self.transportationView.frame = CGRectMake(self.view.frame.size.width*0.025,82,self.view.frame.size.width*0.95,self.view.frame.size.height-77);
    
    self.googleMapsBtn.hidden = YES;
    self.googleMapsBtn.enabled = NO;
    
    self.closeTransportationView.hidden = NO;
    self.closeTransportationView.enabled = YES;
    
    self.contentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 180, self.transportationView.frame.size.width, self.transportationView.frame.size.height-190)];
    self.contentScrollView.backgroundColor = [UIColor clearColor];
    self.contentScrollView.showsVerticalScrollIndicator = YES;
    self.contentScrollView.scrollEnabled=YES;
    self.contentScrollView.userInteractionEnabled=YES;
    self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width,1000);
    
    if (indexPath.row == 0) {
        [self.contentScrollView removeFromSuperview];
        
        UILabel *option1Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *option1LabelString = NSLocalizedString(@"Bannan Line","");
        option1Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option1Label.numberOfLines = 0;
        option1Label.lineBreakMode = NSLineBreakByWordWrapping;
        option1Label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:option1LabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1LabelString length])];
        option1Label.attributedText = attributedString ;
        option1Label.adjustsFontSizeToFitWidth = NO;
        option1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option1Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1Label sizeToFit];
        [self.contentScrollView addSubview:option1Label];
        
        UILabel *option1contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option1Label.frame.size.height+option1Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option1contentLabelString = NSLocalizedString(@"Take Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option1contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option1contentLabel.numberOfLines = 0;
        option1contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option1contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option1contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1contentLabelString length])];
        option1contentLabel.attributedText = attributedString ;
        option1contentLabel.adjustsFontSizeToFitWidth = NO;
        option1contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option1contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1contentLabel sizeToFit];
        [self.contentScrollView addSubview:option1contentLabel];
        
        UILabel *option2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option1contentLabel.frame.size.height+option1contentLabel.frame.origin.y+20, self.transportationView.frame.size.width-80, 25)];
        NSString *option2LabelString = NSLocalizedString(@"Wenhu Line","");
        option2Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option2Label.numberOfLines = 0;
        option2Label.lineBreakMode = NSLineBreakByWordWrapping;
        option2Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2LabelString length])];
        option2Label.attributedText = attributedString ;
        option2Label.adjustsFontSizeToFitWidth = NO;
        option2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option2Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2Label sizeToFit];
        [self.contentScrollView addSubview:option2Label];
        
        UILabel *option2contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option2Label.frame.size.height+option2Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabelString = NSLocalizedString(@"Take Wenhu Line to\" Taipei Nangang Exhibition Center\"","");
        option2contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel.numberOfLines = 0;
        option2contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabelString length])];
        option2contentLabel.attributedText = attributedString ;
        option2contentLabel.adjustsFontSizeToFitWidth = NO;
        option2contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2contentLabel sizeToFit];
        
        [self.contentScrollView addSubview:option2contentLabel];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);

        [self.transportationView addSubview:self.contentScrollView];

    }
    else if (indexPath.row == 1){
        [self.contentScrollView removeFromSuperview];
        
        UILabel *option1Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *option1LabelString = NSLocalizedString(@"National Highway No. 1","");
        option1Label.font = [UIFont fontWithName:@"GothamMedium" size:23];
        option1Label.numberOfLines = 0;
        option1Label.lineBreakMode = NSLineBreakByWordWrapping;
        option1Label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:option1LabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1LabelString length])];
        option1Label.attributedText = attributedString ;
        option1Label.adjustsFontSizeToFitWidth = NO;
        option1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option1Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1Label sizeToFit];
        [self.contentScrollView addSubview:option1Label];
        
        UILabel *option1contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option1Label.frame.size.height+option1Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option1contentLabelString = NSLocalizedString(@"→Travelling north bound：Take the exit of Donghu Interchange, turn right at Kangning Rd., merge onto Sanchong Rd.after crossing Nanhu Bridge, turn left at Jingmao 2nd Rd., then you arrive at TWTC Nangang Exhibition hall\r→Travelling south bound : Take the exit of Neihu Interchange, merge onto Chenggong Rd., turn left at Chongyang Rd.after crossing Chenggong Bridge, then you arrive at TWTC Nangang Exhibition hall.","");
        option1contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option1contentLabel.numberOfLines = 0;
        option1contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option1contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option1contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1contentLabelString length])];
        option1contentLabel.attributedText = attributedString ;
        option1contentLabel.adjustsFontSizeToFitWidth = NO;
        option1contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option1contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1contentLabel sizeToFit];
        [self.contentScrollView addSubview:option1contentLabel];
        
        UILabel *option2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option1contentLabel.frame.size.height+option1contentLabel.frame.origin.y+20, self.transportationView.frame.size.width-80, 25)];
        NSString *option2LabelString = NSLocalizedString(@"National Highway No. 3","");
        option2Label.font = [UIFont fontWithName:@"GothamMedium" size:23];
        option2Label.numberOfLines = 0;
        option2Label.lineBreakMode = NSLineBreakByWordWrapping;
        option2Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2LabelString length])];
        option2Label.attributedText = attributedString ;
        option2Label.adjustsFontSizeToFitWidth = NO;
        option2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option2Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2Label sizeToFit];
        [self.contentScrollView addSubview:option2Label];
        
        UILabel *option2contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option2Label.frame.size.height+option2Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabelString = NSLocalizedString(@"→Travelling north bound：Take the exit of Sintai 5th Rd. Interchange, merge onto Sintai 5thRd., turn left at Datong Rd., go straight along and merge onto Nangang Rd., then you arrive at TWTC Nangang Exhibition hall.\r→Travelling south bound：Take the exit of Sintai 5th Rd. Interchange, merge onto Sintai 5thRd., turn left at Datong Rd., go straight along and merge onto Nangang Rd., then you arrive at TWTC Nangang Exhibition hall.","");
        option2contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel.numberOfLines = 0;
        option2contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabelString length])];
        option2contentLabel.attributedText = attributedString ;
        option2contentLabel.adjustsFontSizeToFitWidth = NO;
        option2contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2contentLabel sizeToFit];
        [self.contentScrollView addSubview:option2contentLabel];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        
        [self.transportationView addSubview:self.contentScrollView];

    }
    else if (indexPath.row == 2){
        [self.contentScrollView removeFromSuperview];
        
        UILabel *option1Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *option1LabelString = NSLocalizedString(@"From Taipei SongShan Airport","");
        option1Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option1Label.numberOfLines = 0;
        option1Label.lineBreakMode = NSLineBreakByWordWrapping;
        option1Label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:option1LabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1LabelString length])];
        option1Label.attributedText = attributedString ;
        option1Label.adjustsFontSizeToFitWidth = NO;
        option1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option1Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1Label sizeToFit];
        [self.contentScrollView addSubview:option1Label];
        
        UILabel *option1contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option1Label.frame.size.height+option1Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option1contentLabelString = NSLocalizedString(@"Take Wenhu Line to \"Taipei Nangang Exhibition Center\"","");
        option1contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option1contentLabel.numberOfLines = 0;
        option1contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option1contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option1contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1contentLabelString length])];
        option1contentLabel.attributedText = attributedString ;
        option1contentLabel.adjustsFontSizeToFitWidth = NO;
        option1contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option1contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1contentLabel sizeToFit];
        [self.contentScrollView addSubview:option1contentLabel];
        
        UILabel *option2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option1contentLabel.frame.size.height+option1contentLabel.frame.origin.y+20, self.transportationView.frame.size.width-80, 25)];
        NSString *option2LabelString = NSLocalizedString(@"From Taoyuan International Airport (CKS Airport)","");
        option2Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option2Label.numberOfLines = 0;
        option2Label.lineBreakMode = NSLineBreakByWordWrapping;
        option2Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2LabelString length])];
        option2Label.attributedText = attributedString ;
        option2Label.adjustsFontSizeToFitWidth = NO;
        option2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option2Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2Label sizeToFit];
        [self.contentScrollView addSubview:option2Label];
        
        UILabel *option2contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option2Label.frame.size.height+option2Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabelString = NSLocalizedString(@"・KUO-KUANG Motor Transport No. 1843","");
        option2contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel.numberOfLines = 0;
        option2contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabelString length])];
        option2contentLabel.attributedText = attributedString ;
        option2contentLabel.adjustsFontSizeToFitWidth = NO;
        option2contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [option2contentLabel sizeToFit];
        option2contentLabel.userInteractionEnabled = YES;
        [self.contentScrollView addSubview:option2contentLabel];
        
        UITapGestureRecognizer *aboutTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleKUOKUANGTap:)];
        [option2contentLabel addGestureRecognizer:aboutTap];
        
        UILabel *option2contentLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(40, option2contentLabel.frame.size.height+option2contentLabel.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabel2String = NSLocalizedString(@"・Route：Taoyuan Int’l Airport → NANGANG Exhibition Hall\r・Service hour：\r  - From Taoyuan Int’l Airport 06:20 – 23:20\r  - From NANGANG Exhibition Hall 05:00 – 23:00\r  - Interval：every 46 – 65 minutes\r・Travelling time：80 minutes\r・Fare：Adult NT$115","");
        option2contentLabel2.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel2.numberOfLines = 0;
        option2contentLabel2.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel2.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabel2String];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabel2String length])];
        option2contentLabel2.attributedText = attributedString ;
        option2contentLabel2.adjustsFontSizeToFitWidth = NO;
        option2contentLabel2.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel2.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2contentLabel2 sizeToFit];
        [self.contentScrollView addSubview:option2contentLabel2];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        
        [self.transportationView addSubview:self.contentScrollView];

    }
    else if (indexPath.row == 3){
        [self.contentScrollView removeFromSuperview];
        
        UILabel *option1Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *option1LabelString = NSLocalizedString(@"By TRA","");
        option1Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option1Label.numberOfLines = 0;
        option1Label.lineBreakMode = NSLineBreakByWordWrapping;
        option1Label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:option1LabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1LabelString length])];
        option1Label.attributedText = attributedString ;
        option1Label.adjustsFontSizeToFitWidth = NO;
        option1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option1Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1Label sizeToFit];
        [self.contentScrollView addSubview:option1Label];
        
        UILabel *option1contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option1Label.frame.size.height+option1Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option1contentLabelString = NSLocalizedString(@"Take TRA to Nangang Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option1contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option1contentLabel.numberOfLines = 0;
        option1contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option1contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option1contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1contentLabelString length])];
        option1contentLabel.attributedText = attributedString ;
        option1contentLabel.adjustsFontSizeToFitWidth = NO;
        option1contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option1contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1contentLabel sizeToFit];
        [self.contentScrollView addSubview:option1contentLabel];
        
        UILabel *option2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option1contentLabel.frame.size.height+option1contentLabel.frame.origin.y, self.transportationView.frame.size.width-80, 0)];
        NSString *option2LabelString = NSLocalizedString(@"Taipei Station","");
        option2Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option2Label.numberOfLines = 0;
        option2Label.lineBreakMode = NSLineBreakByWordWrapping;
        option2Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2LabelString length])];
        option2Label.attributedText = attributedString ;
        option2Label.adjustsFontSizeToFitWidth = NO;
        option2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option2Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2Label sizeToFit];
        
        UILabel *option2contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option2Label.frame.size.height+option2Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabelString = NSLocalizedString(@"Take TRA to Taipei Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option2contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel.numberOfLines = 0;
        option2contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabelString length])];
        option2contentLabel.attributedText = attributedString ;
        option2contentLabel.adjustsFontSizeToFitWidth = NO;
        option2contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2contentLabel sizeToFit];
        [self.contentScrollView addSubview:option2contentLabel];
        
        UILabel *option3Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option2contentLabel.frame.size.height+option2contentLabel.frame.origin.y, self.transportationView.frame.size.width-80, 0)];
        NSString *option3LabelString = NSLocalizedString(@"Banqiao Station","");
        option3Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option3Label.numberOfLines = 0;
        option3Label.lineBreakMode = NSLineBreakByWordWrapping;
        option3Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option3LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option3LabelString length])];
        option3Label.attributedText = attributedString ;
        option3Label.adjustsFontSizeToFitWidth = NO;
        option3Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option3Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option3Label sizeToFit];
        
        UILabel *option3contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option3Label.frame.size.height+option3Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option3contentLabelString = NSLocalizedString(@"Take TRA to Banqiao Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option3contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option3contentLabel.numberOfLines = 0;
        option3contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option3contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option3contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option3contentLabelString length])];
        option3contentLabel.attributedText = attributedString ;
        option3contentLabel.adjustsFontSizeToFitWidth = NO;
        option3contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option3contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option3contentLabel sizeToFit];
        [self.contentScrollView addSubview:option3contentLabel];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        
        [self.transportationView addSubview:self.contentScrollView];

    }
    else if (indexPath.row == 4){
        [self.contentScrollView removeFromSuperview];
        
        UILabel *option1Label = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *option1LabelString = NSLocalizedString(@"By THSR","");
        option1Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option1Label.numberOfLines = 0;
        option1Label.lineBreakMode = NSLineBreakByWordWrapping;
        option1Label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:option1LabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1LabelString length])];
        option1Label.attributedText = attributedString ;
        option1Label.adjustsFontSizeToFitWidth = NO;
        option1Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option1Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1Label sizeToFit];
        [self.contentScrollView addSubview:option1Label];
        
        UILabel *option1contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option1Label.frame.size.height+option1Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option1contentLabelString = NSLocalizedString(@"Take THSR to Nangang Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option1contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option1contentLabel.numberOfLines = 0;
        option1contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option1contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option1contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option1contentLabelString length])];
        option1contentLabel.attributedText = attributedString ;
        option1contentLabel.adjustsFontSizeToFitWidth = NO;
        option1contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option1contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option1contentLabel sizeToFit];
        [self.contentScrollView addSubview:option1contentLabel];
        
        UILabel *option2Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option1contentLabel.frame.size.height+option1contentLabel.frame.origin.y, self.transportationView.frame.size.width-80, 0)];
        NSString *option2LabelString = NSLocalizedString(@"Taipei Station","");
        option2Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option2Label.numberOfLines = 0;
        option2Label.lineBreakMode = NSLineBreakByWordWrapping;
        option2Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2LabelString length])];
        option2Label.attributedText = attributedString ;
        option2Label.adjustsFontSizeToFitWidth = NO;
        option2Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option2Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2Label sizeToFit];
        
        UILabel *option2contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option2Label.frame.size.height+option2Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option2contentLabelString = NSLocalizedString(@"Take THSR to Taipei Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option2contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option2contentLabel.numberOfLines = 0;
        option2contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option2contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option2contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option2contentLabelString length])];
        option2contentLabel.attributedText = attributedString ;
        option2contentLabel.adjustsFontSizeToFitWidth = NO;
        option2contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option2contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option2contentLabel sizeToFit];
        [self.contentScrollView addSubview:option2contentLabel];
        
        UILabel *option3Label = [[UILabel alloc] initWithFrame:CGRectMake(40, option2contentLabel.frame.size.height+option2contentLabel.frame.origin.y, self.transportationView.frame.size.width-80, 0)];
        NSString *option3LabelString = NSLocalizedString(@"Banqiao Station","");
        option3Label.font = [UIFont fontWithName:@"GothamMedium" size:25];
        option3Label.numberOfLines = 0;
        option3Label.lineBreakMode = NSLineBreakByWordWrapping;
        option3Label.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option3LabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option3LabelString length])];
        option3Label.attributedText = attributedString ;
        option3Label.adjustsFontSizeToFitWidth = NO;
        option3Label.lineBreakMode = NSLineBreakByTruncatingTail;
        option3Label.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option3Label sizeToFit];
        
        UILabel *option3contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, option3Label.frame.size.height+option3Label.frame.origin.y+10, self.transportationView.frame.size.width-80, 25)];
        NSString *option3contentLabelString = NSLocalizedString(@"Take THSR to Banqiao Station and transfer to MRT Bannan Line to\" Taipei Nangang Exhibition Center\"","");
        option3contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        option3contentLabel.numberOfLines = 0;
        option3contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        option3contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:option3contentLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [option3contentLabelString length])];
        option3contentLabel.attributedText = attributedString ;
        option3contentLabel.adjustsFontSizeToFitWidth = NO;
        option3contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        option3contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [option3contentLabel sizeToFit];
        [self.contentScrollView addSubview:option3contentLabel];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        
        [self.transportationView addSubview:self.contentScrollView];

    }
    else if (indexPath.row == 5){
        
        [self.contentScrollView removeFromSuperview];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *titleLabelString = NSLocalizedString(@"By Bus","");
        titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
        titleLabel.attributedText = attributedString ;
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [titleLabel sizeToFit];
        [self.contentScrollView addSubview:titleLabel];
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, titleLabel.frame.origin.y+titleLabel.frame.size.height+10, self.transportationView.frame.size.width-80, 100)];
        NSString *contentString = NSLocalizedString(@"Bus info: Taipei bus information","");
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        contentLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [contentLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        [contentLabel sizeToFit];
        contentLabel.userInteractionEnabled = YES;
        [self.contentScrollView addSubview:contentLabel];
        
        UITapGestureRecognizer *aboutTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleBusTap:)];
        [contentLabel addGestureRecognizer:aboutTap];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        [self.transportationView addSubview:self.contentScrollView];
    }
    else if (indexPath.row == 6){
        [self.contentScrollView removeFromSuperview];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 20, self.transportationView.frame.size.width-80, 25)];
        NSString *titleLabelString = NSLocalizedString(@"By Taxi","");
        titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
        titleLabel.attributedText = attributedString ;
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [titleLabel sizeToFit];
        [self.contentScrollView addSubview:titleLabel];
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, titleLabel.frame.size.height+titleLabel.frame.origin.y+10, self.transportationView.frame.size.width-80, 100)];
        NSString *contentString = NSLocalizedString(@"Taxis are available on the main roads with a starting rate of NT$ 70 for the first 1.5 km, then NT$ 5 for every extra 300 meters. Surcharge and different calculations will be applied between 23:00 ~ 06:00 at a commencing rate of NT$ 70 for the first 1.25 km and NT$ 5 for every extra 250 meters, and a NT$ 20 on top of the meter rate.\r・Taxi meters start at NT$70.\r・Taxi fare is based on the meter plus a 15% surcharge (highway tolls not included).\r・Receipts are available upon request.\r・Tipping is not necessary","");
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [contentLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        [contentLabel sizeToFit];
        [self.contentScrollView addSubview:contentLabel];
        
        CGRect contentRect = CGRectZero;
        for (UIView *view in self.contentScrollView.subviews) {
            contentRect = CGRectUnion(contentRect, view.frame);
        }
        self.contentScrollView.contentSize = CGSizeMake(self.transportationView.frame.size.width, contentRect.size.height+10);
        [self.transportationView addSubview:self.contentScrollView];

    }
    
    [self listSubviewsOfView:self.contentScrollView];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.contentScrollView removeFromSuperview];

    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0, 40, cell.bounds.size.width, 40)];
    title.textColor =[UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
    title.textAlignment = NSTextAlignmentCenter;
    [title setFont:[UIFont fontWithName:@"GothamBook" size:15.0]];
    [title setText:[self.items objectAtIndex:indexPath.row]];
    [cell addSubview:title];
    
    UIImageView *transportItem = [[UIImageView alloc] initWithFrame: CGRectMake(15,0,40,40)];
    UIImage *transportItemImage = [UIImage imageNamed:[self.itemsImage objectAtIndex:indexPath.row]];
    transportItem.image = transportItemImage;
    [cell addSubview:transportItem];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 85, 70, 2)];
    line.backgroundColor = [UIColor whiteColor];
    [cell addSubview:line];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) googleMapAction:(id)sender {
    NSLog(@"googleMapAction");
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:
         [NSURL URLWithString:@"comgooglemaps://?q=Taipei+Nangang+Exhibition+Center&center=25.0566068,121.6159273&zoom=10&views=traffic"]];
    } else {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=Taipei+Nangang+Exhibition+Center&center=25.0566068,121.6159273&zoom=10&views=traffic"]];
        [[UIApplication sharedApplication] openURL: url];
    }
}

-(void) closeTransportationViewAction:(id)sender {
    NSLog(@"closeTransportationViewAction");
    
    self.transportationView.frame = CGRectMake(self.view.frame.size.width*0.025,self.view.frame.size.height-200,self.view.frame.size.width*0.95,self.view.frame.size.height-146);
    [self.contentScrollView removeFromSuperview];
    self.googleMapsBtn.hidden = NO;
    self.googleMapsBtn.enabled = YES;
    
    self.closeTransportationView.hidden = YES;
    self.closeTransportationView.enabled = NO;
}

- (void)handleBusTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"handleBusTap");
    NSURL *url = [NSURL URLWithString:@"http://www.5284.com.tw/Dybus.aspx?Lang=En"];
    [[UIApplication sharedApplication] openURL: url];
}

- (void)handleKUOKUANGTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"handleKUOKUANGTap");
    NSURL *url = [NSURL URLWithString:@"http://www.kingbus.com.tw/index.php"];
    [[UIApplication sharedApplication] openURL: url];
}

@end
