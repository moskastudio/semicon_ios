//
//  CalendarDay1TableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarDay1TableViewControllerDelegate <NSObject>

- (void)showCalendar:(NSDictionary *)reminder;

@end
@interface CalendarDay1TableViewController : UITableViewController

@property id<CalendarDay1TableViewControllerDelegate> delegate;
- (void)refreshCell;
@end
