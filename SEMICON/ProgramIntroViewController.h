//
//  ProgramIntroViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/3.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <GoogleMaps/GoogleMaps.h>

@protocol ProgramIntroViewControllerDelegate <NSObject>

- (void)showMap;
- (void)showCalender:(NSDictionary *)forumInfo;
- (void)showRegister;

@end
@interface ProgramIntroViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property id<ProgramIntroViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIButton *priceBtn;
- (IBAction)priceBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *registerView;
@property NSDictionary *forumInfo;
- (void)refreshCell;
@property (strong, nonatomic) IBOutlet UIImageView *registerImageView;
@property (strong, nonatomic) IBOutlet UIImageView *registerImageViewZH;

@end
