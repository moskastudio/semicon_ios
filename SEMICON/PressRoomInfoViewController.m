//
//  PressRoomInfoViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/18.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AppDelegate.h"
#import "PressRoomInfoViewController.h"

@interface PressRoomInfoViewController ()

@end

@implementation PressRoomInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([_source isEqualToString:@"aboutSEMICON"]) {
        self.title = @"SEMICON Taiwan";
        UILabel *introLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, 200, 45)];
        introLabel.text = NSLocalizedString( @"Introduction_semi", "");
        introLabel.font = [AppDelegate getSystemFont:@"GothamMedium" size:25];
        introLabel.textColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1.0];
        [self.view addSubview:introLabel];
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 100, self.view.frame.size.width-54, 250)];
        NSString *contentString = NSLocalizedString(@"SEMICON Taiwan is the region's most influential annual event in micro-electronics industry. The exhibition annually brings together the leading companies in the industry from across the supply chain and around the world! With more than 700 international exhibitors in more than 1,500 booths, SEMICON Taiwan is the one must-attend event of the year!", "");
        contentLabel.font = [AppDelegate getSystemFont:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [self.view addSubview:contentLabel];
        
}
    else{
        self.title = NSLocalizedString( @"Press Center", "");
        
        UIView *line1 = [[UIView alloc] initWithFrame: CGRectMake(72, 134, self.view.frame.size.width-72, 2)];
        line1.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self.view addSubview:line1];
        
        UIView *line2 = [[UIView alloc] initWithFrame: CGRectMake(72, 270, self.view.frame.size.width-72, 2)];
        line2.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self.view addSubview:line2];
        
        UIImageView *timeIcon = [[UIImageView alloc] initWithFrame: CGRectMake(11,34,30,30)];
        UIImage *timeIconImage = [UIImage imageNamed:@"img_time_purple.png"];
        timeIcon.image = timeIconImage;
        [self.view addSubview:timeIcon];
        
        UILabel *weekLabel = [[UILabel alloc] initWithFrame:CGRectMake(72, 34, 40, 63)];
        weekLabel.numberOfLines = 0;
        weekLabel.font = [UIFont fontWithName:@"GothamBook" size:15];
        weekLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        NSString *weekLabelString = NSLocalizedString(@"Web.Thu.Fri.", "");
        NSMutableAttributedString *weekAttributedString = [[NSMutableAttributedString alloc] initWithString:weekLabelString];
        NSMutableParagraphStyle *weekParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [weekParagraphStyle setLineSpacing:6];
        [weekAttributedString addAttribute:NSParagraphStyleAttributeName value:weekParagraphStyle range:NSMakeRange(0, [weekLabelString length])];
        weekLabel.attributedText = weekAttributedString ;
        [self.view addSubview:weekLabel];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(122, 34, 48, 63)];
        dateLabel.numberOfLines = 0;
        dateLabel.font = [UIFont fontWithName:@"GothamBook" size:15];
        dateLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        NSString *dateLabelString = NSLocalizedString(@"Sep. 7 Sep. 8 Sep. 9", "");
        NSMutableAttributedString *dateAttributedString = [[NSMutableAttributedString alloc] initWithString:dateLabelString];
        NSMutableParagraphStyle *dateParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [dateParagraphStyle setLineSpacing:6];
        [dateAttributedString addAttribute:NSParagraphStyleAttributeName value:dateParagraphStyle range:NSMakeRange(0, [dateLabelString length])];
        dateLabel.attributedText = dateAttributedString ;
        [self.view addSubview:dateLabel];
        
        UILabel *TimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(180, 34, 90, 63)];
        TimeLabel.numberOfLines = 0;
        TimeLabel.font = [UIFont fontWithName:@"GothamBook" size:15];
        TimeLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        NSString *TimeLabelString = @"10:00-17:00 10:00-17:00 10:00-16:00";
        NSMutableAttributedString *TimeAttributedString = [[NSMutableAttributedString alloc] initWithString:TimeLabelString];
        NSMutableParagraphStyle *TimeParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [TimeParagraphStyle setLineSpacing:6];
        [TimeAttributedString addAttribute:NSParagraphStyleAttributeName value:TimeParagraphStyle range:NSMakeRange(0, [TimeLabelString length])];
        TimeLabel.attributedText = TimeAttributedString ;
        [self.view addSubview:TimeLabel];
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame: CGRectMake(11,182,30,30)];
        UIImage *locationIconImage = [UIImage imageNamed:@"img_location_purple.png"];
        locationIcon.image = locationIconImage;
        [self.view addSubview:locationIcon];
        
        UILabel *locationLabel=[[UILabel alloc] initWithFrame:CGRectMake(72, 182, self.view.frame.size.width-101, 42)];
        [locationLabel setFont:[UIFont fontWithName:@"GothamBook" size:15]];
        locationLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        locationLabel.numberOfLines = 0;
        NSString *locationLabelString = NSLocalizedString(@"4F, Taipei Nangang Exhibition Center, Hall 1", "");
        NSMutableAttributedString *locationAttributedString = [[NSMutableAttributedString alloc] initWithString:locationLabelString];
        NSMutableParagraphStyle *locationParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [locationParagraphStyle setLineSpacing:6];
        [locationAttributedString addAttribute:NSParagraphStyleAttributeName value:locationParagraphStyle range:NSMakeRange(0, [locationLabelString length])];
        locationLabel.attributedText = locationAttributedString ;
        [self.view addSubview:locationLabel];
        
        UIImageView *infoIcon = [[UIImageView alloc] initWithFrame: CGRectMake(11,318,30,30)];
        UIImage *infoIconImage = [UIImage imageNamed:@"img_info_purple.png"];
        infoIcon.image = infoIconImage;
        [self.view addSubview:infoIcon];
        
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(72, 318, self.view.frame.size.width-101, 42)];
        [label setFont:[UIFont fontWithName:@"GothamBook" size:15]];
        label.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        label.numberOfLines = 0;
        NSString *labelString = NSLocalizedString(@"Please prepare two business cards to register for media badge.", "");
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelString length])];
        label.attributedText = attributedString ;
        [self.view addSubview:label];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
