//
//  FloorViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FloorViewController.h"
#import "WebViewController.h"

@interface FloorViewController ()

@end

@implementation FloorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    UIImageView *topBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,0,self.view.frame.size.width,72*self.view.frame.size.width/1080)];
    UIImage *topBackgroundImage = [UIImage imageNamed:@"img_floor_top_bg.png"];
    topBackground.image = topBackgroundImage;
    
    UIImageView *floorBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,self.view.frame.size.height/2-21*self.view.frame.size.width/2160-55,self.view.frame.size.width,21*self.view.frame.size.width/1080)];
    UIImage *floorBackgroundImage = [UIImage imageNamed:@"img_floor.png"];
    floorBackground.image = floorBackgroundImage;
    
    UIImageView *floorFourBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,(self.view.frame.size.height/2-21*self.view.frame.size.width/2160-55)+(21*self.view.frame.size.width/1080)-(400*self.view.frame.size.width/720),self.view.frame.size.width,400*self.view.frame.size.width/720)];
    UIImage *floorFourBackgroundImage = [UIImage imageNamed:@"img_floor_four.png"];
    floorFourBackground.image = floorFourBackgroundImage;
    [floorFourBackground setUserInteractionEnabled:YES];
    
    UIImageView *floorOneBackground = [[UIImageView alloc] initWithFrame: CGRectMake(0,self.view.frame.size.height-400*self.view.frame.size.width/720-120,self.view.frame.size.width,400*self.view.frame.size.width/720)];
    UIImage *floorOneBackgroundImage = [UIImage imageNamed:@"img_floor_one.png"];
    floorOneBackground.image = floorOneBackgroundImage;
    [floorOneBackground setUserInteractionEnabled:YES];
    
    [self.view addSubview:floorOneBackground];
    [self.view addSubview:floorFourBackground];
    [self.view addSubview:topBackground];
    [self.view addSubview:floorBackground];
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(fourFloorTap:)];
    [floorFourBackground addGestureRecognizer:singleFingerTap];
    
    UITapGestureRecognizer *firstFloorTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(firstFloorTap:)];
    [floorOneBackground addGestureRecognizer:firstFloorTap];
}

- (void)fourFloorTap:(UITapGestureRecognizer *)recognizer {
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //NSLog(@"floorOneBackground:::%f,%f",location.x,location.y);
    
    NSString *langID = @"1";
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        langID = @"2";
    }
    //Do stuff here...
     NSString *link =[NSString stringWithFormat:@"http://expo.semi.org/taiwan2016/Public/EventMap.aspx?LangID=%@&mapid=260", langID];
    //NSURL *url = [NSURL URLWithString:link];
    //[[UIApplication sharedApplication] openURL: url];
    
    if (![link isEqualToString:@""]) {
        [_delegate showMapWithTitle:link title:NSLocalizedString(@"Floor Plan - 4F", @"")];
    }
}

- (void)firstFloorTap:(UITapGestureRecognizer *)recognizer {
    //CGPoint location = [recognizer locationInView:[recognizer.view superview]];
    //NSLog(@"floorOneBackground:::%f,%f",location.x,location.y);
    
    NSString *langID = @"1";
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        langID = @"2";
    }
    //Do stuff here...
    NSString *link =[NSString stringWithFormat:@"http://expo.semi.org/taiwan2016/Public/EventMap.aspx?LangID=%@&mapid=259", langID];
    //NSURL *url = [NSURL URLWithString:link];
    //[[UIApplication sharedApplication] openURL: url];
    
    if (![link isEqualToString:@""]) {
        [_delegate showMapWithTitle:link title:NSLocalizedString(@"Floor Plan - 1F", @"")];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
