//
//  ProgramAgendaViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/3.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AppDelegate.h"
#import "ProgramAgendaViewController.h"
#import "ProgramSpeakerViewController.h"

@interface ProgramAgendaViewController ()
@property NSArray *programTime;
@property NSArray *programDetails;
@property UIView *coverView;
@end

@implementation ProgramAgendaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        _registerImageView.hidden = YES;
        _registerImageViewZH.hidden = NO;
        [_priceBtn setImage:[UIImage imageNamed:@"btn_price_ch@2x.png"] forState:UIControlStateNormal];
    }else{
        _registerImageView.hidden = NO;
        _registerImageViewZH.hidden = YES;
    }
     self.coverView = [[UIView alloc] initWithFrame:
                      CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    self.coverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIView *costPopUpView = [[UIView alloc]
                             initWithFrame:CGRectMake(self.view.frame.size.width/15, (self.view.frame.size.height-64)/15, self.view.frame.size.width/15*13, (self.view.frame.size.height-64-51)/15*13)];
    costPopUpView.backgroundColor = [UIColor whiteColor];
    costPopUpView.layer.cornerRadius = 10.0;
    costPopUpView.clipsToBounds = YES;
    [self.coverView addSubview:costPopUpView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(costPopUpView.frame.size.width/2-90, 10, 180, 40)];
    titleLabel.text = NSLocalizedString(@"Cost", @"");
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:17];
    titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:titleLabel];
    
    UIView *lineUp = [[UIView alloc] initWithFrame: CGRectMake(0, 60, costPopUpView.frame.size.width, 1.5)];
    lineUp.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineUp];
    
    UILabel *beforeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineUp.frame.origin.y + 31, costPopUpView.frame.size.width, 40)];
    NSString *before = [[_forumInfo objectForKey:@"fee"] objectForKey:@"before"];
    before = [before stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"\00a0" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@" " withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@" " withString:@""];
    before = before.uppercaseString;
    before = [before stringByReplacingOccurrencesOfString:@"/" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"PERPERSON" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"每人" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"NT" withString:@" NT"];
    beforeLabel.text = before;
    beforeLabel.textAlignment = NSTextAlignmentCenter;
    beforeLabel.font = [UIFont fontWithName:@"GothamMedium" size:30];
    beforeLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:59.0/255.0 blue:184.0/255.0 alpha:1.0];
    [costPopUpView addSubview:beforeLabel];
    
    UILabel *beforeTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, beforeLabel.frame.origin.y + 50, costPopUpView.frame.size.width, 20)];
    beforeTipLabel.text = NSLocalizedString(@"Early Bird Price (before Aug. 5)", "") ;
    beforeTipLabel.textAlignment = NSTextAlignmentCenter;
    beforeTipLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
    beforeTipLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:beforeTipLabel];
    
    UIView *lineMid = [[UIView alloc] initWithFrame: CGRectMake(costPopUpView.frame.size.width / 2 - 40, costPopUpView.frame.size.height / 2, 80, 1.5)];
    lineMid.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineMid];
    
    UILabel *afterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineMid.frame.origin.y + 31, costPopUpView.frame.size.width, 40)];
    NSString *after = [[_forumInfo objectForKey:@"fee"] objectForKey:@"after"];
    after = [after stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\00a0" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@" " withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@" " withString:@""];
    after = after.uppercaseString;
    after = [after stringByReplacingOccurrencesOfString:@"/" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"PERPERSON" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"每人" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"NT" withString:@" NT"];
    afterLabel.text = after;
    afterLabel.textAlignment = NSTextAlignmentCenter;
    afterLabel.font = [UIFont fontWithName:@"GothamMedium" size:30];
    afterLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:59.0/255.0 blue:184.0/255.0 alpha:1.0];
    [costPopUpView addSubview:afterLabel];
    
    UILabel *afterTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, afterLabel.frame.origin.y + 50, costPopUpView.frame.size.width, 20)];
    afterTipLabel.text = NSLocalizedString(@"Original Price (after Aug. 6)", "") ;
    afterTipLabel.textAlignment = NSTextAlignmentCenter;
    afterTipLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
    afterTipLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:afterTipLabel];
    
    UIView *lineDown = [[UIView alloc] initWithFrame: CGRectMake(0, costPopUpView.frame.size.height-60, costPopUpView.frame.size.width, 1.5)];
    lineDown.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineDown];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    backBtn.frame = CGRectMake(costPopUpView.frame.size.width/2-100, costPopUpView.frame.size.height-50, 200, 40);
    [backBtn setTitle:NSLocalizedString(@"Back", @"") forState:UIControlStateNormal];
    backBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    backBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamMedium" size:17];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [costPopUpView addSubview:backBtn];
    
    [self.view addSubview:self.coverView];
    self.coverView.hidden = YES;
    
    UITapGestureRecognizer *singleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRegisterTap:)];
    [self.registerView addGestureRecognizer:singleTap];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_forumInfo objectForKey:@"events"] count] ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AgendaCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    NSString *timeString = [[[_forumInfo objectForKey:@"events"] objectAtIndex:indexPath.row ] objectForKey:@"time"];
    timeString = [timeString stringByReplacingOccurrencesOfString:@" " withString:@""];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"–" withString:@"-"];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\\U2013" withString:@"-"];
    NSArray* dateArray = [timeString componentsSeparatedByString:@"-"];
    NSLog(@"dateArray:%@", dateArray);

    NSString *startString = [dateArray objectAtIndex:0];
    
    NSString *endString = @"";
    if (dateArray.count >= 2) {
         endString = [dateArray objectAtIndex:1];
    }
    
    UILabel *startTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(32, cell.frame.size.height/2-32, 48, 20)];
    [startTimeLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    startTimeLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    startTimeLabel.text = startString;
    startTimeLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *lineLabel=[[UILabel alloc] initWithFrame:CGRectMake(32, startTimeLabel.frame.origin.y+25, 48, 14)];
    [lineLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    lineLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    lineLabel.text = @"|";
    lineLabel.textAlignment = NSTextAlignmentCenter;
    
    UILabel *endTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(32, lineLabel.frame.origin.y+19, 48, 20)];
    [endTimeLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    endTimeLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    endTimeLabel.text = endString;
    endTimeLabel.textAlignment = NSTextAlignmentCenter;
    
    UIView *timeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, cell.frame.size.height)];
    [timeView addSubview:startTimeLabel];
    [timeView addSubview: lineLabel];
    [timeView addSubview:endTimeLabel];
    [cell addSubview:timeView];
    
    NSArray *activities = [[[_forumInfo objectForKey:@"events"] objectAtIndex:indexPath.row ] objectForKey:@"activity"];
    NSArray *speakers = [[[_forumInfo objectForKey:@"events"] objectAtIndex:indexPath.row ] objectForKey:@"speakers"];
    
    int labelOffset = 0;
    float lastLabelY = 0;
    for (int i = 0; i < [activities count]; i++) {
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 18 + labelOffset + lastLabelY, self.view.frame.size.width-156, cell.frame.size.height-36)];
        NSString *contentString = [activities objectAtIndex:i];
        if ([contentString hasPrefix:@" "]) {
            contentString = [contentString substringFromIndex:1];
        }
        
        for (int j = 0; j < [speakers count] ; j++) {
            if ([contentString rangeOfString:[[speakers objectAtIndex:j] objectForKey:@"name"]].location != NSNotFound) {
                contentLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:59.0/255.0 blue:184.0/255.0 alpha:1.0];
                contentLabel.tag = indexPath.row * 100 + j;
                
                UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(labelTapped:)];
                tapGestureRecognizer.numberOfTapsRequired = 1;
                [contentLabel addGestureRecognizer:tapGestureRecognizer];
                contentLabel.userInteractionEnabled = YES;
                
                break;
            }
            else {
                contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            }
        }
        
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:13];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = YES;
        
        [cell addSubview:contentLabel];
        
        if (activities.count > 1) {
            [contentLabel sizeToFit];
            labelOffset = CGRectGetHeight(contentLabel.frame) + 0;
            lastLabelY = contentLabel.frame.origin.y;
        }
        [contentLabel setFrame:CGRectMake(contentLabel.frame.origin.x, contentLabel.frame.origin.y, contentLabel.frame.size.width, contentLabel.frame.size.height+5)];
    }
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-2, self.view.frame.size.width, 2)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *activities = [[[_forumInfo objectForKey:@"events"] objectAtIndex:indexPath.row ] objectForKey:@"activity"];
    int cellHeight = 0;
    
    for (NSString *activity in activities) {
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:13] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-156, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [activity boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        cellHeight += actualSize.height;
    }
    
    
    if ((cellHeight*1.5+40) < 89 ) {
        return 88;
    }
    else {
        if (activities.count > 1) {
            return (cellHeight*1.5+40) + ((activities.count - 1) * 24);
        }
        else {
            return cellHeight*1.5+40;
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)handleRegisterTap:(UITapGestureRecognizer *)recognizer {
    NSURL *url = [NSURL URLWithString:@"http://prod7.semicontaiwan.org/en/sustainable-manufacturing-forum-1"];
    [[UIApplication sharedApplication] openURL: url];
}

- (IBAction)priceBtn:(id)sender {
    self.coverView.hidden = NO;
}

-(void) backAction:(id)sender {
    self.coverView.hidden = YES;
}

- (void)labelTapped:(UITapGestureRecognizer *)recognizer {
    
    int row = [[recognizer view] tag] / 100;
    int index = [[recognizer view] tag] - (row * 100);
    
    NSDictionary *speaker = [[[[_forumInfo objectForKey:@"events"] objectAtIndex:row ] objectForKey:@"speakers"] objectAtIndex:index];
    NSLog(@"speaker:%@",speaker);
    if ([_delegate respondsToSelector:@selector(showSpearkerDetail:)]) {
        [_delegate showSpearkerDetail:[speaker objectForKey:@"link"]];
    }
//    [_delegate showWebView:[speaker objectForKey:@"link"] navTitle:[speaker objectForKey:@"name"]];
}
@end
