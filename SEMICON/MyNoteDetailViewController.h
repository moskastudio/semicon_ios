//
//  MyNoteDetailViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyNoteDetailViewController : UIViewController

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property NSString *selectNote;
@property NSDictionary *note;
@end
