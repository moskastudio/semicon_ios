//
//  ExhibitorCompanyViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorCompanyViewController.h"
#import "Service.h"
#import "SignInViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ExhibitorCompanyViewController (){
    //NSDictionary *companyName;
    NSDictionary *visited;
    //NSDictionary *floor;
    //NSDictionary *boothNo;
    NSArray *companyNameSectionTitles;
    NSArray *companyNameIndexTitles;
    NSArray *adArray;
}
@property NSInteger starBtnPress;
@property NSString *selectedItem;
@property NSMutableDictionary *exhibitorDictionary;
@property NSIndexPath *selectedIndexPath;
@end

@implementation ExhibitorCompanyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //-------ADSlider Satrt
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            adArray = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"TopBannerLg"]];
            
            NSMutableArray *imageUrls = [[NSMutableArray alloc] init];
            
            for (NSDictionary *ad in adArray) {
                if (![[ad objectForKey:@"imgUrl"] isEqualToString:@""]) {
                    [imageUrls addObject:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
                }
            }
            
            MCLScrollViewSlider *localScrollView = [MCLScrollViewSlider scrollViewSliderWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 432*self.view.frame.size.width/1080) imageURLs:imageUrls placeholderImage:[UIImage imageNamed:@"img_action_bar_bg.png"]];
            localScrollView.pageControlCurrentIndicatorTintColor = [UIColor whiteColor];
            [self.ADView addSubview:localScrollView];
            
            [localScrollView didSelectItemWithBlock:^(NSInteger clickedIndex) {
                NSLog(@"Clicked inner_ad_%ld", clickedIndex+1);
                [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                              parameters:@{ FBSDKAppEventParameterNameContentID:[NSString stringWithFormat:@"inner_ad_%ld", clickedIndex+1],
                                            FBSDKAppEventParameterNameDescription:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"] } ];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"]]];
            }];
        }
    }];
    
    //-------ADSlider End

    self.exhibitorDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                 @"#" : @[],
                                                                                 @"A" : @[],
                                                                                 @"B" : @[],
                                                                                 @"C" : @[],
                                                                                 @"D" : @[],
                                                                                 @"E" : @[],
                                                                                 @"F" : @[],
                                                                                 @"G" : @[],
                                                                                 @"H" : @[],
                                                                                 @"I" : @[],
                                                                                 @"J" : @[],
                                                                                 @"K" : @[],
                                                                                 @"L" : @[],
                                                                                 @"M" : @[],
                                                                                 @"N" : @[],
                                                                                 @"O" : @[],
                                                                                 @"P" : @[],
                                                                                 @"Q" : @[],
                                                                                 @"R" : @[],
                                                                                 @"S" : @[],
                                                                                 @"T" : @[],
                                                                                 @"U" : @[],
                                                                                 @"V" : @[],
                                                                                 @"W" : @[],
                                                                                 @"X" : @[],
                                                                                 @"Y" : @[],
                                                                                 @"Z" : @[]}];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchAllExhibitorSuccessNotification:) name:FetchAllExhibitorSuccessNotification object:nil];
        
    [[Service sharedInstance] fetchAllExhibitor];
    
    
    companyNameSectionTitles = [[self.exhibitorDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    companyNameIndexTitles = @[@"#", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
    [self.tableView reloadData];
    
    _starBtnPress = 0;
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadIndexPath {
    if (_selectedIndexPath != nil) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)onFetchAllExhibitorSuccessNotification:(NSNotification*) notify {
    self.exhibitorArray = [[NSMutableArray alloc] initWithArray:notify.object];
    
    for (NSDictionary *exhibitor in self.exhibitorArray) {
        NSString *upperCompanyName = [[exhibitor objectForKey:@"CompanyName"] uppercaseString];
        NSString *firstAlphabet = [upperCompanyName substringToIndex:1];
        
        NSMutableArray *subExhibitorArray = [[NSMutableArray alloc] initWithArray:[self.exhibitorDictionary objectForKey:firstAlphabet] copyItems:YES];
        if (subExhibitorArray == nil) {
            firstAlphabet = @"#";
            subExhibitorArray = [[NSMutableArray alloc] initWithArray:[self.exhibitorDictionary objectForKey:firstAlphabet] copyItems:YES];
        }
        
        [subExhibitorArray addObject:exhibitor];
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"CompanyName"
                                                     ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [subExhibitorArray sortedArrayUsingDescriptors:sortDescriptors];
        
        [self.exhibitorDictionary setObject:sortedArray forKey:firstAlphabet];
    }
    
    [self.tableView reloadData];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [companyNameSectionTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [companyNameSectionTitles objectAtIndex:section];
    NSArray *sectionExhibitors = [self.exhibitorDictionary objectForKey:sectionTitle];
    return [sectionExhibitors count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [companyNameSectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanyCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    // Configure the cell...
    //    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    //    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    //    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    
    
    NSDictionary *exhibitor = [[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 24, self.view.frame.size.width-114, 20)];
    companyLabel.text = [exhibitor objectForKey:@"CompanyName"];
    [companyLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    companyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    companyLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:companyLabel];
    
    NSString *boothNumber = [exhibitor objectForKey:@"BoothNumber"];
    
    NSString *floor = @"1F";
    if ([boothNumber integerValue] < 2000) {
        floor = @"4F";
    }
    
    if ([self checkVisited:indexPath]) {
        UILabel *visitedLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 50, 70, 20)];
        visitedLabel.text = NSLocalizedString(@"Visited", "") ;
        [visitedLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        visitedLabel.textColor = [UIColor whiteColor];
        visitedLabel.textAlignment = NSTextAlignmentCenter;
        visitedLabel.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [cell addSubview:visitedLabel];
        
        UILabel *floorLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 50, 20, 20)];
        floorLabel.text = floor;
        [floorLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        floorLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        floorLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:floorLabel];
        
        UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(154, 50, 120, 20)];
        boothNoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@", "") , boothNumber];
        [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        boothNoLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:boothNoLabel];
    }
    else{
        UILabel *floorLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 50, 20, 20)];
        floorLabel.text = floor;
        [floorLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        floorLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        floorLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:floorLabel];
        
        UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 50, 120, 20)];
        boothNoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@", "") , boothNumber];
        [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        boothNoLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:boothNoLabel];
    }

    UIButton *starBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-65, 24, 40, 40)];
    [starBtn setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"] forState:UIControlStateNormal];
    UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_star_press.png"];
    [starBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
    [starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:starBtn];
    
    if ([self checkExhibitorFarvorites:indexPath]) {
        starBtn.selected = true;
    }
    
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //  return animalSectionTitles;
    return companyNameIndexTitles;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(36, 0, tableView.frame.size.width-72, 44)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(36, 10, sectionHeaderView.frame.size.width, 24)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    headerLabel.text = [companyNameIndexTitles objectAtIndex:section];
    [sectionHeaderView addSubview:headerLabel];
    
    return sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [companyNameSectionTitles indexOfObject:title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
    
    self.selectedItem = [[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    NSString *boothNumber = [[[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"];
    
    _selectedIndexPath = indexPath;
    
    if ([_delegate respondsToSelector:@selector(showExhibitorIntro:)]) {
        [_delegate showExhibitorIntro:boothNumber];
    }
}

-(void) starBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
    
        CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
        _selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        
        NSDictionary *exhibitor = [[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:_selectedIndexPath.section]] objectAtIndex:_selectedIndexPath.row];
        
        NSString *boothNumber = [[exhibitor objectForKey:@"BoothNumber"] description];
        
        NSMutableArray *exhibitorFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"]];
        if ([sender isSelected]) {
            [exhibitorFarvorites addObject:@{
                                             @"companyName": [exhibitor objectForKey:@"CompanyName"],
                                             @"boothNumber": boothNumber}];
        }
        else {
            for (int i = 0; i < exhibitorFarvorites.count; i++) {
                if ([[[exhibitorFarvorites objectAtIndex:i] objectForKey:@"boothNumber"] isEqualToString:boothNumber]) {
                    [exhibitorFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:exhibitorFarvorites forKey:@"ExhibitorFarvorites"];
    }
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (Boolean)checkVisited:(NSIndexPath *)indexPath {
    NSArray *visitedExhibitor = [[NSUserDefaults standardUserDefaults] objectForKey:@"visited"];
//    NSLog(@"visit:%@", visitedExhibitor);
    NSString *boothNumber = [[[[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"] description];
    
    for (NSString *visitedBoothNumber in visitedExhibitor) {
        if ([visitedBoothNumber isEqualToString:boothNumber]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkExhibitorFarvorites:(NSIndexPath *)indexPath {
    NSArray *exhibitorFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"];
    NSString *boothNumber = [[[[self.exhibitorDictionary objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"] description];
    
    for (NSDictionary *exhibitorFarvorite in exhibitorFarvorites) {
        if ([[exhibitorFarvorite objectForKey:@"boothNumber"] isEqualToString:boothNumber]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
