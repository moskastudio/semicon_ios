//
//  SearchViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
- (IBAction)infoBtn:(id)sender;
- (IBAction)cancelBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) UISearchController *searchController;

@end
