//
//  ExhibitorBoothNoListTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/15.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorBoothNoListTableViewController.h"
#import "ExhibitorIntroTableViewController.h"
#import "Service.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "SignInViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ExhibitorBoothNoListTableViewController () {
    NSString *adUrl;
    BOOL adIsClosed;
}

@property NSArray *companyArray;
@property NSArray *companyFloorArray;
@property NSArray *companyBoothNoArray;
@property NSArray *visitedArray;

@property NSInteger starBtnPress;
@property NSString *selectedItem;
@property NSInteger fromNumber;
@property NSInteger toNumber;
@property NSString *selectedBoothNumber;
@property UIView *adView;
@property NSIndexPath *selectedIndexPath;
@end

@implementation ExhibitorBoothNoListTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _navBarTitle;
    
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            
            NSDictionary *ad = [[NSDictionary alloc] initWithDictionary:[responseDict objectForKey:@"BottomBannerLg"]];
            adUrl = [ad objectForKey:@"actionUrl"];
            _adView = [[UIView alloc] initWithFrame:CGRectMake(20, [[UIScreen mainScreen] bounds].size.height - 60, self.view.frame.size.width - 40, 60)];
            _adView.backgroundColor = [UIColor clearColor];
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40, 60)];
            [image sd_setImageWithURL:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
            image.userInteractionEnabled = true;
            [_adView addSubview:image];
            
            UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adTap:)];
            [image addGestureRecognizer:tgr];
            
            UIButton *close = [[UIButton alloc] initWithFrame:CGRectMake(_adView.frame.size.width - 50, 10, 40, 40)];
            [close setImage:[UIImage imageNamed:@"btn_cancel.png"] forState:UIControlStateNormal];
            [close addTarget:self action:@selector(closeAD) forControlEvents:UIControlEventTouchUpInside];
            [_adView addSubview:close];
            [[[[UIApplication sharedApplication] windows] firstObject] addSubview:_adView];
        }
    }];
    
    if (_selectedFloorIndexPath.section == 0) {
        _fromNumber = 500 * _selectedFloorIndexPath.row + 1;
        _toNumber = 500 * (_selectedFloorIndexPath.row + 1);
    }
    else {
        switch (_selectedFloorIndexPath.row) {
            case 0:
                _fromNumber = 2000;
                _toNumber = 2500;
                break;
            case 1:
                _fromNumber = 2501;
                _toNumber = 3000;
                break;
            case 2:
                _fromNumber = 3001;
                _toNumber = 3500;
                break;
                
            default:
                break;
        }
    }
    
    [self fetchExhibitor];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_adView removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated {
    if (_selectedIndexPath != nil) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if (!adIsClosed) {
        [[[[UIApplication sharedApplication] windows] firstObject] addSubview:_adView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)adTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"Clicked bottom_ad_1");
    [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                  parameters:@{ FBSDKAppEventParameterNameContentID:@"bottom_ad_1",
                                FBSDKAppEventParameterNameDescription:adUrl } ];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adUrl]];
}

- (void)closeAD {
    [_adView removeFromSuperview];
    adIsClosed = true;
}

- (void)fetchExhibitor {
    
    [[Service sharedInstance] fetchExhibitorFromNumber:[NSString stringWithFormat:@"%i", _fromNumber] toNumber:[NSString stringWithFormat:@"%i", _toNumber] complete:^(BOOL success, NSArray *responseAry) {
//        NSLog(@"%@", responseAry);
        if (success) {
            _companyArray = responseAry;
            [self.tableView reloadData];
        }
        else {
            
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _companyArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExhibitorBoothNoCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 24, self.view.frame.size.width-114, 20)];
    companyLabel.text = [[_companyArray objectAtIndex:indexPath.row] objectForKey:@"CompanyName"];
    [companyLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    companyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    companyLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:companyLabel];
    
    if ([self checkVisited:indexPath]) {
        UILabel *visitedLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 50, 70, 20)];
        visitedLabel.text = NSLocalizedString(@"Visited", "") ;
        [visitedLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        visitedLabel.textColor = [UIColor whiteColor];
        visitedLabel.textAlignment = NSTextAlignmentCenter;
        visitedLabel.backgroundColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [cell addSubview:visitedLabel];
        
        UILabel *floorLabel = [[UILabel alloc] initWithFrame:CGRectMake(120, 50, 20, 20)];
        floorLabel.text = [NSString stringWithFormat:@"%iF", _selectedFloorIndexPath.section * 3 + 1];
        [floorLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        floorLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        floorLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:floorLabel];
        
        UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(154, 50, 120, 20)];
        boothNoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@", "") ,[[_companyArray objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"]];
        [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        boothNoLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:boothNoLabel];
    }
    else{
        UILabel *floorLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 50, 20, 20)];
        floorLabel.text = [NSString stringWithFormat:@"%iF", 4 - _selectedFloorIndexPath.section * 3];
        [floorLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        floorLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        floorLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:floorLabel];
        
        UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(70, 50, 120, 20)];
        boothNoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@", "") ,[[_companyArray objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"]];
        [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        boothNoLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:boothNoLabel];
    }
    
    UIButton *starBtn = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-65, 24, 40, 40)];
    [starBtn setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"] forState:UIControlStateNormal];
    UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_star_press.png"];
    [starBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
    [starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:starBtn];
    
    if ([self checkExhibitorFarvorites:indexPath]) {
        starBtn.selected = true;
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
    _selectedIndexPath = indexPath;
    _selectedBoothNumber = [[_companyArray objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"];
    [self performSegueWithIdentifier:@"showExhibitorIntro" sender:self];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 60;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

-(void) starBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
        _selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        
        NSDictionary *exhibitor = [_companyArray objectAtIndex:_selectedIndexPath.row];
        
        NSString *boothNumber = [[exhibitor objectForKey:@"BoothNumber"] description];
        
        NSMutableArray *exhibitorFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"]];
        if ([sender isSelected]) {
            [exhibitorFarvorites addObject:@{
                                             @"companyName": [exhibitor objectForKey:@"CompanyName"],
                                             @"boothNumber": boothNumber}];
        }
        else {
            for (int i = 0; i < exhibitorFarvorites.count; i++) {
                if ([[[exhibitorFarvorites objectAtIndex:i] objectForKey:@"boothNumber"] isEqualToString:boothNumber]) {
                    [exhibitorFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:exhibitorFarvorites forKey:@"ExhibitorFarvorites"];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showExhibitorIntro"]) {
        ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [segue destinationViewController];
        exhibitorIntroTableViewController.navBarTitle =NSLocalizedString(@"Exhibitor Company", "") ;
        [exhibitorIntroTableViewController setBoothNumber:self.selectedBoothNumber];
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (Boolean)checkVisited:(NSIndexPath *)indexPath {
    NSArray *visitedExhibitor = [[NSUserDefaults standardUserDefaults] objectForKey:@"visited"];
//    NSLog(@"visit:%@", visitedExhibitor);
    NSString *boothNumber = [[[_companyArray objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"] description];
    
    for (NSString *visitedBoothNumber in visitedExhibitor) {
        if ([visitedBoothNumber isEqualToString:boothNumber]) {
            return true;
        }
    }
    
    return false;
}

- (Boolean)checkExhibitorFarvorites:(NSIndexPath *)indexPath {
    NSArray *exhibitorFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"];
    NSString *boothNumber = [[[_companyArray objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"] description];
    
    for (NSDictionary *exhibitorFarvorite in exhibitorFarvorites) {
        if ([[exhibitorFarvorite objectForKey:@"boothNumber"] isEqualToString:boothNumber]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
