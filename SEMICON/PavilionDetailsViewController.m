//
//  PavilionDetailsViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "PavilionDetailsViewController.h"
#import "PavilionIntroductionViewController.h"
#import "ExhibitorIntroTableViewController.h"
#import "Service.h"

@interface PavilionDetailsViewController ()

@property NSArray *AOIArray;
@property NSArray *CMPArray;
@property NSArray *HighTechArray;
@property NSArray *LaserArray;
@property NSArray *MaterialsArray;
@property NSArray *PrecisionArray;
@property NSArray *SecondaryMarketArray;
@property NSArray *SmartManufacturingArray;
@property NSArray *TaiwanArray;

@property NSArray *CrossStraitArray;
@property NSArray *GermanArray;
@property NSArray *HollandArray;
@property NSArray *KoreaArray;
@property NSArray *KyushuArray;
@property NSArray *OkinawaArray;
@property NSArray *PhilippinesArray;
@property NSArray *SingaporeArray;

@property NSArray *pavilionArray;
@property NSString *labelString;

@end

@implementation PavilionDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _navBarTitle;
    // Do any additional setup after loading the view.
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    //Theme
//    _AOIArray = [[NSArray alloc] initWithObjects:@"AVVA R&D Corp.",@"KohYoung Technology Inc.",@"Samwell Testing Inc.",@"OTSUKA TECH ELECTRONICS CO.,LTD.", nil];
//    _CMPArray = [[NSArray alloc] initWithObjects:@"ACE NANOCHEM",@"Ensinger Asia Holding PTE. LTD. Taiwan Branch",@"Zhengzhou Research Institute for Abrasives & Grinding Co., Ltd", nil];
//    _HighTechArray = [[NSArray alloc] initWithObjects:@"AccuDevice Co., Ltd.",@"Camfil Taiwan Co. Ltd.",@"GST Taiwan Ltd.",@"Hantech Engineering Ltd., Co.",@"Lumax International Corp., Ltd.",@"Mega Union Technology Inc.",@"Organo Corporation",@"PAF Corporation",@"Particle Measuring Systems Inc.",@"Rockwell Automation Taiwan Co., Ltd",@"Techgo Industrial Co., Ltd.",@"Trusval Technology Co. Ltd.",@"VIVOTEK INC.",@"Weltall Technology Corporation", nil];
//    _LaserArray = [[NSArray alloc] initWithObjects:@"Hortech Company", nil];
//    _MaterialsArray = [[NSArray alloc] initWithObjects:@"Atotech Taiwan Ltd",@"AI Technology, Inc.",@"BCnC CO., LTD",@"CohPros International Co., Ltd",@"CSI Chemical Co. Ltd.",@"Marketech International Corp.",@"Nippon Pulse Motor Trading (Taiwan) Co., Ltd.",@"Uniwave Enterprise Co., Ltd.",@"Wonik QnC",@"Yuhon Enterprise Corporation", nil];
//    _PrecisionArray = [[NSArray alloc] initWithObjects:@"Chen Hung Tech., Inc.",@"Chieftek Precision Co., Ltd.",@"Kaiyi Technology Co., Ltd.",@"Kenturn Nano.Tec Co.,Ltd.",@"Preco Corp.",@"Yotec Company",@"Yu Shyan System Technology Materials Co., Ltd.", nil];
//    _SecondaryMarketArray = [[NSArray alloc] initWithObjects:@"Adelis Associates Pte Ltd.",@"CIS Corporation",@"CSE Co., Ltd.",@"EFAB International Technology Co., Ltd.",@"EQUVO Pte Ltd.",@"E-Tech Solution Co., Ltd.",@"Hong Yang Laser Service Co., Ltd.",@"Mapsuka Industries Co., Ltd.",@"PosanTek, Ltd.",@"Semiconductor Equipment Manufacturing, Inc. (SEM)",@"Sumitomo Mitsui Finance and Leasing Co, Ltd.",@"SurplusGLOBAL Taiwan Inc.",@"WESolution Co., Ltd.",@"Wisdom Power International Corporation LTD", nil];
//    _SmartManufacturingArray = [[NSArray alloc] initWithObjects:@"Balluff Taiwan Ltd.",@"Cimetrix Incorporated",@"Dah Hsing Electric Co. Ltd",@"Gallant Precision Machining Co., Ltd.",@"NADA ELITE TECHNOLOGIES INC.",@"National Instruments Corporation"@"Robots and Design Co., Ltd.",@"Sanwa Engineering Corp.", nil];
//    _TaiwanArray = [[NSArray alloc] initWithObjects:@"Hanbell Vacuum Technology Co., Ltd.",@"Creating Nano Technologies Inc.",@"Coalition Technology Co., Ltd.",@"Foxsemicon Integrated Technology Inc.",@"ZENITH Materials Technology Corp.",@"Metal Industries Research and DevelopmentCenter",@"Industrial Technology Research Institute", nil];
//    
//    //Region
//    _CrossStraitArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _GermanArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _HollandArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _KoreaArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _KyushuArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _OkinawaArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _PhilippinesArray = [[NSArray alloc] initWithObjects:@"", nil];
//    _SingaporeArray = [[NSArray alloc] initWithObjects:@"", nil];
    [self fetchPavilion];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchPavilion {
    [[Service sharedInstance] fetchPavilionWithAlias:_alias complete:^(BOOL success, NSArray *responseAry) {
        
        if (success) {
            _pavilionArray = [[NSArray alloc] initWithArray:responseAry];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return _pavilionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExhibitorsCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *exhibitorsLabel=[[UILabel alloc]initWithFrame:
                              CGRectMake(36, 15, self.view.frame.size.width-72, 58)];
    [exhibitorsLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
    exhibitorsLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    exhibitorsLabel.lineBreakMode = NSLineBreakByWordWrapping;
    exhibitorsLabel.numberOfLines = 0;
    exhibitorsLabel.textAlignment = NSTextAlignmentLeft;
    
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        if ([[[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"CH"] description] isEqualToString:@"0"]) {
            self.labelString = [[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"EN"] description];
        }
        else {
            self.labelString = [[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"CH"] description];
        }
    }
    else {
        if ([[[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"EN"] description] isEqualToString:@"0"]) {
            self.labelString = [[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"CH"] description];
        }
        else {
            self.labelString = [[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"EN"] description];
        }
    }
    NSLog(@"%@", self.labelString);
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:self.labelString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [self.labelString length])];
    exhibitorsLabel.attributedText = attributedString ;
    exhibitorsLabel.adjustsFontSizeToFitWidth = NO;
    exhibitorsLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:exhibitorsLabel];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 87, self.view.frame.size.width, 1)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
    ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorIntroTableViewController"];
    exhibitorIntroTableViewController.navBarTitle = NSLocalizedString(@"Exhibitor Company", "") ;
    [exhibitorIntroTableViewController setBoothNumber:[[_pavilionArray objectAtIndex:indexPath.row] objectForKey:@"Booth"]];
    
    [self.navigationController pushViewController:exhibitorIntroTableViewController animated:true];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showPavilionInto"]) {
        PavilionIntroductionViewController *pavilionIntroductionVC = [segue destinationViewController];
        pavilionIntroductionVC.navBarTitle = _navBarTitle;
        pavilionIntroductionVC.contentString = _contentString;
    }
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)showIntroduction:(id)sender {
}
@end
