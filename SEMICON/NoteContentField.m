//
//  NoteContentField.m
//  semicon
//
//  Created by MuRay Lin on 2016/7/9.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "NoteContentField.h"

@implementation NoteContentField

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 0 , 10 );
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 0 , 10 );
}

- (CGRect)placeholderRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 0 , 10 );
}

@end
