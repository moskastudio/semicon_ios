//
//  ExhibitionNewsTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitionNewsTableViewController : UITableViewController
@property NSString *exhibitorTitle;
@property NSString *pressReleaseTitle;
@property NSString *souceButton;

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@end
