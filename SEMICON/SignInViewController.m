//
//  SignInViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/11.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "SignInViewController.h"
#import "SignUpViewController.h"
#import "Service.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface SignInViewController ()<UITextFieldDelegate>

@end

@implementation SignInViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _textField1.delegate = self;
    _textField2.delegate = self;
    // Do any additional setup after loading the view.
    /*  Edit navigationBar
    UIImage *backButtonImage = [UIImage imageNamed:@"btn_back.png"];
    UIBarButtonItem *backBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backButtonImage
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(backTapped:)];
    self.navigationItem.leftBarButtonItem = backBarButtonItem;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
     [self setTitle:@"Sign in"];
    */
}

- (void)viewWillAppear:(BOOL)animated {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    
    if (isLogin) {
        [self dismissViewControllerAnimated:true completion:^{
            
        }];
    }
}

//- (void)backTapped:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelSignBtn:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signIn:(id)sender {
    if ([_textField1.text isEqualToString:@""] || [_textField2.text isEqualToString:@""]) {
        
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please fill in the mailbox and password" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [ac addAction:cancel];
        [self presentViewController:ac animated:true completion:nil];
    }
    else {
        [[Service sharedInstance] signInWithEmail:_textField1.text password:_textField2.text complete:^(BOOL success, NSDictionary *responseDict) {
            NSLog(@"%@", responseDict);
            if ([[[responseDict objectForKey:@"ERR_CODE"] description] isEqualToString:@"1"]) {
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:kUserLogin];
                [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
            }
            else {
                UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Login failed" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                [ac addAction:cancel];
                [self presentViewController:ac animated:true completion:nil];
            }
        }];
    }
}

- (IBAction)facebookLogin:(id)sender {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             if ([FBSDKAccessToken currentAccessToken]) {
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"name, email, id"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                      if (!error) {
                          NSLog(@"fetched user:%@", result);
                          [self signInByFB:result];
                      }else{
                          NSLog(@"%@",error);
                      }
                  }];
             }
         }
     }];
}

- (void)signInByFB:(NSDictionary *) result{
    
    [[Service sharedInstance] signInWithFB:[[result objectForKey:@"id"] description] complete:^(BOOL success, NSDictionary *responseDict) {
        
        if ([[[responseDict objectForKey:@"ERR_CODE"] description] isEqualToString:@"1"]) {
            
            [[NSUserDefaults standardUserDefaults] setBool:true forKey:kUserLogin];
            [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
        }
        else {
            SignUpViewController *suvc = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
            suvc.email = [result objectForKey:@"email"];
            suvc.fbID = [[result objectForKey:@"id"] description];
            suvc.name = [result objectForKey:@"name"];
            suvc.isFB = true;
            suvc.fbToken = [[FBSDKAccessToken currentAccessToken] tokenString];
            [self presentViewController:suvc animated:true completion:^{
                [[[FBSDKLoginManager alloc] init] logOut];
            }];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField *)textField up:(BOOL)up
{
    const int movementDistance = 216;
    const float movementDuration = 0.3f;
    int movement = (up?-movementDistance:movementDistance);
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
@end
