//
//  TechXPOTTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TechXPOTTableViewDelegate <NSObject>

- (void) showTechXPOTTDetail:(NSInteger)index;

@end

@interface TechXPOTTableViewController : UITableViewController

@property id <TechXPOTTableViewDelegate> delegate;

@end
