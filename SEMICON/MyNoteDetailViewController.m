//
//  MyNoteDetailViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "MyNoteDetailViewController.h"

@interface MyNoteDetailViewController ()
@property NSMutableDictionary *dict;
@end

@implementation MyNoteDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _selectNote;
//    _dict = [NSMutableDictionary dictionaryWithObjects:@[
//                                                         @"Note's Title",
//                                                         @"With the aim of promoting Taiwan's semiconductor equipment industry, the Metal Industries Research and Development Center (MIRDC) has been commissioned by MOEA's Industrial Development Bureau to assist the industry in the development of semiconductor manufacturing equipment, parts & components, as well as provide logistics support.",
//                                                         @"Company's Name"]
//                                               forKeys:@[@"title",@"content",@"companyName"]];
    
    // Do any additional setup after loading the view.
    
    UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 300)];
    [titleLabel setFont:[UIFont fontWithName:@"GothamMedium" size:20]];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    NSString *titleLabelString = [_note objectForKey:@"title"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
    titleLabel.attributedText = attributedString ;
    titleLabel.adjustsFontSizeToFitWidth = NO;
    titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [titleLabel sizeToFit];
    titleLabel.frame = CGRectMake(titleLabel.frame.origin.x, titleLabel.frame.origin.y, titleLabel.frame.size.width, titleLabel.frame.size.height + 10);
    [self.view addSubview:titleLabel];
    
    UILabel *conentLabel=[[UILabel alloc] initWithFrame:CGRectMake(27, titleLabel.frame.size.height+titleLabel.frame.origin.y+20, self.view.frame.size.width-54, 300)];
    [conentLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    conentLabel.textColor = [UIColor blackColor];
    conentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    conentLabel.numberOfLines = 0;
    conentLabel.textAlignment = NSTextAlignmentLeft;
    NSString *conentLabelString = [_note objectForKey:@"content"];
    attributedString = [[NSMutableAttributedString alloc] initWithString:conentLabelString];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [conentLabelString length])];
    conentLabel.attributedText = attributedString ;
    conentLabel.adjustsFontSizeToFitWidth = NO;
    conentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [conentLabel sizeToFit];
    conentLabel.frame = CGRectMake(conentLabel.frame.origin.x, conentLabel.frame.origin.y, conentLabel.frame.size.width, conentLabel.frame.size.height + 5);
    [self.view addSubview:conentLabel];
    
    UILabel *aboutLabel=[[UILabel alloc] initWithFrame:CGRectMake(27, conentLabel.frame.size.height+conentLabel.frame.origin.y+30, 100, 16)];
    [aboutLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    aboutLabel.textColor = [UIColor grayColor];
    aboutLabel.text = NSLocalizedString(@"About", "") ;
    [aboutLabel sizeToFit];
    [self.view addSubview:aboutLabel];
    
    UILabel *companyNameLabel=[[UILabel alloc] initWithFrame:CGRectMake(aboutLabel.frame.size.width+aboutLabel.frame.origin.x+10, aboutLabel.frame.origin.y, self.view.frame.size.width-aboutLabel.frame.size.width+aboutLabel.frame.origin.x-37, 300)];
    [companyNameLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    companyNameLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    companyNameLabel.lineBreakMode = NSLineBreakByWordWrapping;
    companyNameLabel.numberOfLines = 0;
    companyNameLabel.textAlignment = NSTextAlignmentLeft;
    NSString *companyNameLabelString = [_note objectForKey:@"companyName"];
    attributedString = [[NSMutableAttributedString alloc] initWithString:companyNameLabelString];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [companyNameLabelString length])];
    companyNameLabel.attributedText = attributedString ;
    companyNameLabel.adjustsFontSizeToFitWidth = NO;
    companyNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [companyNameLabel sizeToFit];
    companyNameLabel.frame = CGRectMake(companyNameLabel.frame.origin.x, companyNameLabel.frame.origin.y - 3, companyNameLabel.frame.size.width, companyNameLabel.frame.size.height + 6);
    
    [self listSubviewsOfView:self.view];
    [self.view addSubview:companyNameLabel];
}

- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        
        // Do what you want to do with the subview
        NSLog(@"%@", subview);
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *tempLabel = (UILabel*)subview;
            [tempLabel setFrame:CGRectMake(tempLabel.frame.origin.x, tempLabel.frame.origin.y, tempLabel.frame.size.width, tempLabel.frame.size.height+5)];
        }
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
