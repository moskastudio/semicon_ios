//
//  PavilionDetailsViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PavilionDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
- (IBAction)showIntroduction:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *introductionView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSString *navBarTitle;
@property NSInteger index;
@property NSString *whichPavilionTable;
@property NSString *contentString;
@property NSString *alias;

@end
