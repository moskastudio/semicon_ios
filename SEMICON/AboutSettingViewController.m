//
//  AboutSettingViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AppDelegate.h"
#import "AboutSettingViewController.h"
#import "ASDetailsViewController.h"
#import "FAQTableViewController.h"

@interface AboutSettingViewController ()

@end

@implementation AboutSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([_soure isEqualToString:@"Setting"]) {
        self.title = NSLocalizedString(@"Setting", "") ;
        
        UIFont *Boldfont = [AppDelegate getSystemFont:@"GothamBook" size:14];
        NSDictionary *attributes = [NSDictionary dictionaryWithObject:Boldfont
                                                               forKey: NSFontAttributeName];
        //Choose Reminder
        UILabel *reminderLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 50, 100, 30)];
        reminderLabel.text = NSLocalizedString(@"Reminder", "") ;
        reminderLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        [self.view addSubview:reminderLabel];
        
        NSArray *reminder =[NSArray arrayWithObjects:NSLocalizedString(@"On", "") , NSLocalizedString(@"Off", "") , nil];
        UISegmentedControl *segmentedControlReminder = [[UISegmentedControl alloc] initWithItems:reminder];
        segmentedControlReminder.frame = CGRectMake(self.view.frame.size.width-170, 50, 140, 30);
        segmentedControlReminder.selectedSegmentIndex = 0;
        [segmentedControlReminder setTitleTextAttributes:attributes
                                                forState:UIControlStateNormal];
        segmentedControlReminder.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        [segmentedControlReminder addTarget:self action:@selector(chooseReminder:) forControlEvents:UIControlEventValueChanged];
        [self.view addSubview:segmentedControlReminder];
        
    }
    else{
        self.title = NSLocalizedString(@"About", "") ;
        
        UILabel *aboutLabel = [[UILabel alloc] initWithFrame:
                               CGRectMake(45, 40, self.view.frame.size.width-90, 50)];
        aboutLabel.text = NSLocalizedString(@"About SEMI", "") ;
        aboutLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        aboutLabel.userInteractionEnabled = YES;
        [self.view addSubview:aboutLabel];
        
        UITapGestureRecognizer *aboutTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAboutTap:)];
        [aboutLabel addGestureRecognizer:aboutTap];
        
        
        UILabel *policyLabel = [[UILabel alloc] initWithFrame:
                                CGRectMake(45, 100, self.view.frame.size.width-90, 50)];
        policyLabel.text = NSLocalizedString(@"Privacy Policy", "") ;
        policyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        policyLabel.userInteractionEnabled = YES;
        [self.view addSubview:policyLabel];
        
        UITapGestureRecognizer *policyTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handlePolicyTap:)];
        [policyLabel addGestureRecognizer:policyTap];
        
        
        UILabel *FAQLabel = [[UILabel alloc] initWithFrame:
                             CGRectMake(45, 160, self.view.frame.size.width-90, 50)];
        FAQLabel.text = NSLocalizedString(@"FAQ", "") ;
        FAQLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        FAQLabel.userInteractionEnabled = YES;
        [self.view addSubview:FAQLabel];
        
        UITapGestureRecognizer *FAQTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleFAQTap:)];
        [FAQLabel addGestureRecognizer:FAQTap];
        
        UILabel *teamLabel = [[UILabel alloc] initWithFrame:
                             CGRectMake(45, 220, self.view.frame.size.width-90, 50)];
        teamLabel.text = NSLocalizedString(@"Production Team", "") ;
        teamLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        teamLabel.userInteractionEnabled = YES;
        [self.view addSubview:teamLabel];
        
        UITapGestureRecognizer *teamTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTeamTap:)];
        [teamLabel addGestureRecognizer:teamTap];
    }
}

- (void)handleAboutTap:(UITapGestureRecognizer *)recognizer {
    ASDetailsViewController *ASDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
    ASDetailsViewController.source = @"About";
    [self.navigationController pushViewController:ASDetailsViewController animated:YES];
}

- (void)handlePolicyTap:(UITapGestureRecognizer *)recognizer {
    ASDetailsViewController *ASDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
    ASDetailsViewController.source = @"Policy";
    [self.navigationController pushViewController:ASDetailsViewController animated:YES];
}

- (void)handleFAQTap:(UITapGestureRecognizer *)recognizer {
    FAQTableViewController *FAQTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FAQTableViewController"];
    [self.navigationController pushViewController:FAQTableViewController animated:YES];
}

- (void)handleTeamTap:(UITapGestureRecognizer *)recognizer {
    ASDetailsViewController *ASDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
    ASDetailsViewController.source = @"Team";
    [self.navigationController pushViewController:ASDetailsViewController animated:YES];
}

- (void)chooseGenderofTTS:(id)sender {
    if ([sender selectedSegmentIndex] == 0) {
//        NSLog(@"英文");
    }
    else{
//        NSLog(@"中文");
    }
}

- (void)chooseReminder:(id)sender {
    if ([sender selectedSegmentIndex] == 0) {
//        NSLog(@"開");
    }
    else{
//        NSLog(@"關");
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
