//
//  EventOverviewViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "EventOverviewViewController.h"
#import "PressRoomInfoViewController.h"
#import "LocationViewController.h"

@interface EventOverviewViewController ()

@end

@implementation EventOverviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *aboutTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleAboutTap:)];
    [_aboutSEMICON addGestureRecognizer:aboutTap];
    
    UITapGestureRecognizer *locationTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleLocationTap:)];
    [_locationView addGestureRecognizer:locationTap];
    
    UITapGestureRecognizer *telTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTelTap:)];
    [_telView addGestureRecognizer:telTap];
    
    UITapGestureRecognizer *mailTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleMailTap:)];
    [_mailView addGestureRecognizer:mailTap];

}

- (void)handleAboutTap:(UITapGestureRecognizer *)recognizer {
    PressRoomInfoViewController *pressRoomInfoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PressRoomInfoViewController"];
    pressRoomInfoViewController.source = @"aboutSEMICON";
    [self.navigationController pushViewController:pressRoomInfoViewController animated:YES];
}

- (void)handleLocationTap:(UITapGestureRecognizer *)recognizer {
    LocationViewController *locationViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LocationViewController"];
    [self.navigationController pushViewController:locationViewController animated:YES];
}

-(void)handleTelTap:(UITapGestureRecognizer *)recognizer {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* CallBtn = [UIAlertAction
                              actionWithTitle:@"Call SEMICON Taiwan"
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  NSString *phoneNumber = [@"tel://" stringByAppendingString:@"+886-3-560-1777"];
                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                              }];
    
    [alert addAction:Cancel];
    [alert addAction:CallBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)handleMailTap:(UITapGestureRecognizer *)recognizer {
    NSString *emailTitle = @"From SEMICON APP";
    NSString *messageBody = @"";
    NSArray *toRecipents = [NSArray arrayWithObject:@"semicontaiwan@semi.org"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc.navigationBar setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1]];
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
