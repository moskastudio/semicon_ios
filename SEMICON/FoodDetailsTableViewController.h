//
//  FoodDetailsTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <GoogleMaps/GoogleMaps.h>

@interface FoodDetailsTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property NSInteger foodRegion;

@end
