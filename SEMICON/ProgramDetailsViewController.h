//
//  ProgramDetailsViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/2.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProgramIntroViewController.h"
#import "ProgramAgendaViewController.h"

@interface ProgramDetailsViewController : UIViewController

- (IBAction) backBtn:(id)sender;
- (IBAction) indexBtn:(id)sender;
@property NSString *navBarTitle;
@property NSString *node;

@property(nonatomic, strong) ProgramIntroViewController *controllerIntro;
@property(nonatomic, strong) ProgramAgendaViewController *controllerAgenda;

@end
