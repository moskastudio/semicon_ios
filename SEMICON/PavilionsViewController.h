//
//  PavilionsViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThemePavilionTableViewController.h"
#import "RegionPavilionTableViewController.h"

@interface PavilionsViewController : UIViewController

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property(nonatomic, strong) ThemePavilionTableViewController *controllerTheme;
@property(nonatomic, strong) RegionPavilionTableViewController *controllerRegion;

@property NSString *ThemeOrRegion;

@end
