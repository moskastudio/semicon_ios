//
//  FavoritesViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FavoritesViewController.h"
#import "FavoritesExhibitorTableViewController.h"
#import "FavoritesProgramTableViewController.h"
#import "FavoritesEventsTableViewController.h"

#import "ExhibitorIntroTableViewController.h"
#import "ProductIntroTableViewController.h"
#import "ProgramDetailsViewController.h"
#import "EvevtActiviteIntroTableViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface FavoritesViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate, FavoritesExhibitorTableViewControllerDelegate, FavoritesProgramTableViewControllerDelegate, FavoritesEventsTableViewControllerDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;
@end

@implementation FavoritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"My Favorites","");
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Exhibitor", "") , NSLocalizedString(@"Program", "") , NSLocalizedString(@"Events/Activities", "") ]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 3;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            FavoritesExhibitorTableViewController *favoritesExhibitorTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoritesExhibitorTableViewController"];
            favoritesExhibitorTableViewController.delegate = self;
            self.controllerFavoritesExhibitor = favoritesExhibitorTableViewController;
            return favoritesExhibitorTableViewController.view;
        }
            break;
        case 1:{
            FavoritesProgramTableViewController *favoritesProgramTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoritesProgramTableViewController"];
            favoritesProgramTableViewController.delegate = self;
            self.controllerFavoritesProgram = favoritesProgramTableViewController;
            return favoritesProgramTableViewController.view;
        }
            break;
        case 2:{
            FavoritesEventsTableViewController *favoritesEventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FavoritesEventsTableViewController"];
            favoritesEventsTableViewController.delegate = self;
            self.controllerFavoritesEvents = favoritesEventsTableViewController;
            return favoritesEventsTableViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)showExhibitor:(NSString *)boothNumber {
    ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorIntroTableViewController"];
    exhibitorIntroTableViewController.navBarTitle = NSLocalizedString(@"Exhibitor Company", "");
    [exhibitorIntroTableViewController setBoothNumber:boothNumber];
    
    [self.navigationController pushViewController:exhibitorIntroTableViewController animated:true];
}

- (void)showProgram:(NSString *)node title:(NSString *)title {
    ProgramDetailsViewController *programDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramDetailsViewController"];
    programDetailsViewController.navBarTitle = title;
    programDetailsViewController.node = node;
    [self.navigationController pushViewController:programDetailsViewController animated:true];
}

- (void)showEvent:(NSDictionary *)eventInfo dataSource:(NSString *)dataSource {
    EvevtActiviteIntroTableViewController *evevtActiviteIntro = [self.storyboard instantiateViewControllerWithIdentifier:@"EvevtActiviteIntroTableViewController"];
    evevtActiviteIntro.navBarTitle = [eventInfo objectForKey:@"topic"];
    evevtActiviteIntro.dataSource = dataSource;
    evevtActiviteIntro.dateString = [eventInfo objectForKey:@"date"];
    evevtActiviteIntro.infoID = [eventInfo objectForKey:@"id"];
    
    [self.navigationController pushViewController:evevtActiviteIntro animated:true];
}

@end
