//
//  EvevtActiviteIntroTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/31.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "EvevtActiviteIntroTableViewController.h"
#import "AddCalendarViewController.h"
#import "ProgramMapImageViewController.h"
#import "SignInViewController.h"
#import "Service.h"
#import "RegisterViewController.h"

@interface EvevtActiviteIntroTableViewController () {
    BOOL isNeedToRefresh;
}

@property NSInteger titleLabelHeight;
@property NSInteger introHeight;
@property NSDictionary *info;

@end

@implementation EvevtActiviteIntroTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(_navBarTitle, @"");
    self.tableView.separatorColor = [UIColor whiteColor];
    self.titleLabelHeight = 0;
    self.introHeight = 0;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    //TechXPOT Events VisitorActivite
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self fetchTechXpot];
    
}

-(void) viewDidAppear:(BOOL)animated{
//    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]
//                          atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    if (isNeedToRefresh) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        isNeedToRefresh = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchTechXpot {
    [[Service sharedInstance] fetchTechXpotWithid:_infoID Complete:^(BOOL success, NSDictionary *responseDict) {
        
        if (success) {
            _info = [[NSDictionary alloc] initWithDictionary:responseDict];
        }
        [self.tableView reloadData];
    }];
}

- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        
        // Do what you want to do with the subview
        NSLog(@"%@", subview);
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *tempLabel = (UILabel*)subview;
            [tempLabel setFrame:CGRectMake(tempLabel.frame.origin.x, tempLabel.frame.origin.y, tempLabel.frame.size.width, tempLabel.frame.size.height+5)];
        }
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        return 5;
    }
    else
        return 4;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventsIntroCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        if (indexPath.row == 0) {
            //標題
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,25)];
            
            NSString *contentString = [_info objectForKey:@"topic"];
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            
            if (contentString != nil) {
                titleLabel.numberOfLines = 0;
                titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
                titleLabel.textAlignment = NSTextAlignmentLeft;
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
                
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
                titleLabel.attributedText = attributedString ;
                titleLabel.adjustsFontSizeToFitWidth = NO;
                titleLabel.textColor = [UIColor blackColor];
                UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
                titleLabel.font = textFont;
                [titleLabel setNumberOfLines:0];
                CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
                CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
                titleLabel.frame = CGRectMake(36, 5, actualSize.width,actualSize.height*1.8);
                self.titleLabelHeight = actualSize.height*1.5+20+5;
                [cell addSubview:titleLabel];
            }
        }
        else if (indexPath.row == 1){
            //時間、地點
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,10,cell.frame.size.width-72,35)];
            NSString *date = _dateString;
            NSString *time = [_info objectForKey:@"time"];
            NSString *titleLabelString = [NSString stringWithFormat:@"%@\r%@",date,time];
            timeLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
            timeLabel.numberOfLines = 2;
            timeLabel.lineBreakMode = NSLineBreakByWordWrapping;
            timeLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:6];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
            timeLabel.attributedText = attributedString ;
            timeLabel.adjustsFontSizeToFitWidth = NO;
            timeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            timeLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
            [timeLabel sizeToFit];
            [cell addSubview:timeLabel];
            
            UILabel *positionLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,50,cell.frame.size.width-72,35+5)];
            NSString *positionLabelString = [_info objectForKey:@"location"];
            if (positionLabelString != nil) {
                positionLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
                positionLabel.numberOfLines = 2;
                positionLabel.lineBreakMode = NSLineBreakByWordWrapping;
                positionLabel.textAlignment = NSTextAlignmentLeft;
                attributedString = [[NSMutableAttributedString alloc] initWithString:positionLabelString];
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [positionLabelString length])];
                positionLabel.attributedText = attributedString ;
                positionLabel.adjustsFontSizeToFitWidth = NO;
                positionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                positionLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
                [positionLabel sizeToFit];
                [cell addSubview:positionLabel];
            }
        }
        else if (indexPath.row == 2){
            //三個按鈕
            UIButton *calendarBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/6-25, 15, 50, 50)];
            UIImage *btnImage = [UIImage imageNamed:@"btn_add_calendar.png"];
            [calendarBtn setImage:btnImage forState:UIControlStateNormal];
            UIImage *selectedBtnImage = [UIImage imageNamed:@"btn_my_calendar.png"];
            [calendarBtn setImage:selectedBtnImage forState:UIControlStateSelected];
            [calendarBtn addTarget:self action:@selector(calendarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:calendarBtn];
            
            if ([self checkCalendar]) {
                calendarBtn.selected = true;
            }
            
            UILabel *calendarLabel = [[UILabel alloc] initWithFrame:
                                      CGRectMake(18,65,(cell.frame.size.width-36)/3,20+5)];
            calendarLabel.text = NSLocalizedString(@"Add Calendar","");
            calendarLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            calendarLabel.textAlignment = NSTextAlignmentCenter;
            calendarLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:calendarLabel];
            
            UIButton *mapitBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/2-25, 15, 50, 50)];
            UIImage *mapitBtnImage = [UIImage imageNamed:@"btn_map_it.png"];
            [mapitBtn setImage:mapitBtnImage forState:UIControlStateNormal];
            [mapitBtn addTarget:self action:@selector(mapitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:mapitBtn];
            
            UILabel *mapitLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20+5)];
            mapitLabel.text = NSLocalizedString(@"Map It","");
            mapitLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            mapitLabel.textAlignment = NSTextAlignmentCenter;
            mapitLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:mapitLabel];
            
            UIButton *favoriteBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+5*(cell.frame.size.width-36)/6-25, 15, 50, 50)];
            UIImage *favoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_normal.png"];
            [favoriteBtn setImage:favoriteBtnImage forState:UIControlStateNormal];
            UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_press.png"];
            [favoriteBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
            [favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:favoriteBtn];
            
            if ([self checkTechXPOTFarvorites]) {
                favoriteBtn.selected = true;
            }
            
            UILabel *favoriteLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+2*(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20+5)];
            favoriteLabel.text = NSLocalizedString(@"Add Favorite","");
            favoriteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            favoriteLabel.textAlignment = NSTextAlignmentCenter;
            favoriteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:favoriteLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.view.frame.size.width, 2)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell addSubview:line];
        }
        else if (indexPath.row == 3){
            //Speaker
            UILabel *speakerLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,24,cell.frame.size.width-72,26+10)];
            speakerLabel.text = NSLocalizedString(@"Speaker","");
            speakerLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
            speakerLabel.textColor = [UIColor blackColor];
            [speakerLabel sizeToFit];
            [cell addSubview:speakerLabel];
            
            UILabel *speakerNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,speakerLabel.frame.origin.y
                                                +speakerLabel.frame.size.height+24,cell.frame.size.width-72,24+5)];
            speakerNameLabel.text = [_info objectForKey:@"speaker"];
            speakerNameLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            speakerNameLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [speakerNameLabel sizeToFit];
            [cell addSubview:speakerNameLabel];
            
            speakerNameLabel.frame = CGRectMake(speakerNameLabel.frame.origin.x, speakerNameLabel.frame.origin.y, speakerNameLabel.frame.size.width, speakerNameLabel.frame.size.height + 10);
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.view.frame.size.width, 2)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell addSubview:line];
        }
        else if (indexPath.row == 4){
            //Company
            UILabel *companyLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,24,cell.frame.size.width-72,26+10)];
            companyLabel.text = NSLocalizedString(@"Company_Speaker","");
            companyLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
            companyLabel.textColor = [UIColor blackColor];
            [companyLabel sizeToFit];
            [cell addSubview:companyLabel];
            
            UILabel *companyNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,companyLabel.frame.origin.y+companyLabel.frame.size.height+24,cell.frame.size.width-72,24+5)];
            companyNameLabel.text = [_info objectForKey:@"speakerCompany"];
            companyNameLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            companyNameLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [companyNameLabel sizeToFit];
            [cell addSubview:companyNameLabel];
            
            companyNameLabel.frame = CGRectMake(companyNameLabel.frame.origin.x, companyNameLabel.frame.origin.y, companyNameLabel.frame.size.width, companyNameLabel.frame.size.height + 10);
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.view.frame.size.width, 2)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell addSubview:line];
        }
        
        else{
            //Introduction
            UILabel *introLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,24,cell.frame.size.width-72,26+5)];
            introLabel.text = NSLocalizedString(@"Introduction","");
            introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
            introLabel.textColor = [UIColor blackColor];
            [introLabel sizeToFit];
            [cell addSubview:introLabel];
            
            UILabel *introContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 70, self.view.frame.size.width-72, 250+5)];
            NSString *contentString = NSLocalizedString(@"With the aim of promoting Taiwan's semiconductor equipment industry, the Metal Industries Research and Development Center (MIRDC) has been commissioned by MOEA's Industrial Development Bureau to assist the industry in the development of semiconductor manufacturing equipment, parts & components, as well as provide logistics support. By attracting foreign investments, building supply chains, facilitating integrated production processes, manufacturing key components, developing alliances with machine-tool makers, securing international technical cooperation, and providing early validation and testing services, the MIRDC is focused on assisting domestic parts and components manufacturers with satisfying costumer demands for product quality, and ensuring a faster turnaround for the products to enter customer's production lines. ","");
            introContentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            introContentLabel.numberOfLines = 0;
            introContentLabel.lineBreakMode = NSLineBreakByWordWrapping;
            introContentLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            introContentLabel.attributedText = attributedString ;
            introContentLabel.adjustsFontSizeToFitWidth = NO;
            introContentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [introContentLabel sizeToFit];
            introContentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            
            CGSize size = [introContentLabel sizeThatFits:CGSizeMake(introContentLabel.frame.size.width, MAXFLOAT)];
            self.introHeight = size.height+80+5;
            introContentLabel.frame =CGRectMake(36, 24, self.view.window.frame.size.width-72, self.introHeight);
            [cell addSubview:introContentLabel];
        }
    }
    
    else if([_dataSource isEqualToString:@"Events"] || [_dataSource isEqualToString:@"VisitorActivite"]){
        if (indexPath.row == 0) {
            //標題
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,25)];
            NSString *contentString = NSLocalizedString(@"How to deliver a VOC free - Green PM strategyHow to deliver a VOC free - Green PM strategy","");
            titleLabel.numberOfLines = 0;
            titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
            titleLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            titleLabel.attributedText = attributedString ;
            titleLabel.adjustsFontSizeToFitWidth = NO;
            titleLabel.textColor = [UIColor blackColor];
            UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
            titleLabel.font = textFont;
            [titleLabel setNumberOfLines:0];
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            titleLabel.frame = CGRectMake(36, 5, actualSize.width,actualSize.height*1.8);
            self.titleLabelHeight = actualSize.height*1.5+20+5;
            [cell addSubview:titleLabel];
        }
        else if (indexPath.row == 1){
            //時間、地點
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,10,cell.frame.size.width-72,35+5)];
            NSString *date = NSLocalizedString(@"Tuesday, Sep. 7, 2016","");
            NSString *time = NSLocalizedString(@"13:00~14:00","");
            NSString *titleLabelString = [NSString stringWithFormat:@"%@\r%@",date,time];
            timeLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
            timeLabel.numberOfLines = 2;
            timeLabel.lineBreakMode = NSLineBreakByWordWrapping;
            timeLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:6];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
            timeLabel.attributedText = attributedString ;
            timeLabel.adjustsFontSizeToFitWidth = NO;
            timeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            timeLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
            [timeLabel sizeToFit];
            [cell addSubview:timeLabel];
            
            UILabel *positionLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,50,cell.frame.size.width-72,35+5)];
            NSString *positionLabelString = NSLocalizedString(@"Le Xuan Banquet Hall, 3F, Taipei Nangang Exhibition Center, Hall 1","");;
            positionLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
            positionLabel.numberOfLines = 2;
            positionLabel.lineBreakMode = NSLineBreakByWordWrapping;
            positionLabel.textAlignment = NSTextAlignmentLeft;
            attributedString = [[NSMutableAttributedString alloc] initWithString:positionLabelString];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [positionLabelString length])];
            positionLabel.attributedText = attributedString ;
            positionLabel.adjustsFontSizeToFitWidth = NO;
            positionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            positionLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
            [positionLabel sizeToFit];
            [cell addSubview:positionLabel];
        }
        else if (indexPath.row == 2){
            //三個按鈕
            UIButton *calendarBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/6-25, 15, 50, 50)];
            UIImage *btnImage = [UIImage imageNamed:@"btn_add_calendar.png"];
            [calendarBtn setImage:btnImage forState:UIControlStateNormal];
            [calendarBtn addTarget:self action:@selector(calendarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:calendarBtn];
            
            UILabel *calendarLabel = [[UILabel alloc] initWithFrame:
                                      CGRectMake(18,65,(cell.frame.size.width-36)/3,20+5)];
            calendarLabel.text = NSLocalizedString(@"Add Calendar","");
            calendarLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            calendarLabel.textAlignment = NSTextAlignmentCenter;
            calendarLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:calendarLabel];
            
            UIButton *mapitBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/2-25, 15, 50, 50)];
            UIImage *mapitBtnImage = [UIImage imageNamed:@"btn_map_it.png"];
            [mapitBtn setImage:mapitBtnImage forState:UIControlStateNormal];
            [mapitBtn addTarget:self action:@selector(mapitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:mapitBtn];
            
            UILabel *mapitLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20+5)];
            mapitLabel.text = NSLocalizedString(@"Map It","");
            mapitLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            mapitLabel.textAlignment = NSTextAlignmentCenter;
            mapitLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:mapitLabel];
            
            UIButton *favoriteBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+5*(cell.frame.size.width-36)/6-25, 15, 50, 50)];
            UIImage *favoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_normal.png"];
            [favoriteBtn setImage:favoriteBtnImage forState:UIControlStateNormal];
            [favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:favoriteBtn];
            
            UILabel *favoriteLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+2*(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20+5)];
            favoriteLabel.text = NSLocalizedString(@"Add Favorite","");
            favoriteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
            favoriteLabel.textAlignment = NSTextAlignmentCenter;
            favoriteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
            [cell addSubview:favoriteLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.view.frame.size.width, 2)];
            line.backgroundColor = [UIColor groupTableViewBackgroundColor];
            [cell addSubview:line];
        }
        else{
            //Introduction
            UILabel *introLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,24,cell.frame.size.width-72,26+5)];
            introLabel.text = NSLocalizedString(@"Introduction","");
            introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
            introLabel.textColor = [UIColor blackColor];
            [introLabel sizeToFit];
            [cell addSubview:introLabel];
            
            UILabel *introContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 70, self.view.frame.size.width-72, 250)];
            NSString *contentString = NSLocalizedString(@"With the aim of promoting Taiwan's semiconductor equipment industry, the Metal Industries Research and Development Center (MIRDC) has been commissioned by MOEA's Industrial Development Bureau to assist the industry in the development of semiconductor manufacturing equipment, parts & components, as well as provide logistics support. By attracting foreign investments, building supply chains, facilitating integrated production processes, manufacturing key components, developing alliances with machine-tool makers, securing international technical cooperation, and providing early validation and testing services, the MIRDC is focused on assisting domestic parts and components manufacturers with satisfying costumer demands for product quality, and ensuring a faster turnaround for the products to enter customer's production lines. ","");
            introContentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            introContentLabel.numberOfLines = 0;
            introContentLabel.lineBreakMode = NSLineBreakByWordWrapping;
            introContentLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            introContentLabel.attributedText = attributedString ;
            introContentLabel.adjustsFontSizeToFitWidth = NO;
            introContentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [introContentLabel sizeToFit];
            introContentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            
            CGSize size = [introContentLabel sizeThatFits:CGSizeMake(introContentLabel.frame.size.width, MAXFLOAT)];
            self.introHeight = size.height+80+5;
            introContentLabel.frame =CGRectMake(36, 24, self.view.window.frame.size.width-72, self.introHeight);
            [cell addSubview:introContentLabel];
        }
        
    }
    [self listSubviewsOfView:cell];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        if (indexPath.row == 0) {
            NSString *contentString = NSLocalizedString(@"How to deliver a VOC free - Green PM strategyHow to deliver a VOC free - Green PM strategy","");
            UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.5+20+5;
        }
        else if (indexPath.row == 1){
            return 100;
        }
        else if (indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 4){
            return 120;
        }
        else
            return self.introHeight;
    }
    else if([_dataSource isEqualToString:@"Events"] || [_dataSource isEqualToString:@"VisitorActivite"]){
        if (indexPath.row == 0) {
            NSString *contentString = NSLocalizedString(@"How to deliver a VOC free - Green PM strategyHow to deliver a VOC free - Green PM strategy","");
            UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.5+20+5;
        }
        else if (indexPath.row == 1){
            return 100;
        }
        else if (indexPath.row == 2){
            return 105;
        }
        else
            return self.introHeight;
    }
    else
        return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 51;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    
    UIView *registerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 51)];
    
    UIImageView *bg = [[UIImageView alloc] initWithFrame:registerView.frame];
    bg.image = [UIImage imageNamed:@"btn_register_bg.png"];
    [registerView addSubview:bg];
    
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        UIImageView *text = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-45, 15,91, 21)];
        text.image = [UIImage imageNamed:@"img_register_event_text_tw.png"];
        text.center = registerView.center;
        [registerView addSubview:text];
    }
    else{
        UIImageView *text = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-66, 15,132, 21)];
        text.image = [UIImage imageNamed:@"img_register_text_en.png"];
        text.center = registerView.center;
        [registerView addSubview:text];
    }
    
    UITapGestureRecognizer *singleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRegisterTap:)];
    [registerView addGestureRecognizer:singleTap];
    
    return registerView;
}

- (void)handleRegisterTap:(UITapGestureRecognizer *)recognizer {
    RegisterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    vc.title = NSLocalizedString(@"Register", @"");
    [self.navigationController pushViewController:vc animated:true];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) calendarBtnAction:(id)sender {
    NSLog(@"calendarBtnAction");
    if ([self checkLogin]) {
        isNeedToRefresh = true;
        AddCalendarViewController *addCalendarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCalendarViewController"];
        addCalendarViewController.scheduleName = [_info objectForKey:@"topic"];
        addCalendarViewController.type = @"TechXPOT";
        addCalendarViewController.typeID = [NSString stringWithFormat:@"%@",[_info objectForKey:@"id"]];
        
        addCalendarViewController.timeString = [_info objectForKey:@"time"];
        
        NSRange date7 = [_dateString rangeOfString:@"7"];
        NSRange date8 = [_dateString rangeOfString:@"8"];
        
        if (date7.location != NSNotFound) {
            addCalendarViewController.dateString = @"9/7";
        }
        else if (date8.location != NSNotFound) {
            addCalendarViewController.dateString = @"9/8";
        }
        else {
            addCalendarViewController.dateString = @"9/9";
        }
        [self.navigationController pushViewController:addCalendarViewController animated:YES];
    }
}

-(void) mapitBtnAction:(id)sender {
    ProgramMapImageViewController *pmivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramMapImageViewController"];
    [self.navigationController pushViewController:pmivc animated:true];
}

-(void) favoriteBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        NSMutableArray *techXPOTFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"TechXPOTFarvorites"]];
        if ([sender isSelected]) {
            NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:_info];
            [new setObject:_dateString forKey:@"date"];
            
            [techXPOTFarvorites addObject:new];
        }
        else {
            for (int i = 0; i < techXPOTFarvorites.count; i++) {
                if ([[[techXPOTFarvorites objectAtIndex:i] objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[_info objectForKey:@"id"]]]) {
                    [techXPOTFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:techXPOTFarvorites forKey:@"TechXPOTFarvorites"];
    }
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (Boolean)checkTechXPOTFarvorites {
    NSArray *techXPOTFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"TechXPOTFarvorites"];
    
    for (NSDictionary *techXPOTFarvorite in techXPOTFarvorites) {
        if ([[techXPOTFarvorite objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[_info objectForKey:@"id"]]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkCalendar {
    NSArray *reminders = [[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"];
    
    for (NSDictionary *reminder in reminders) {
        if ([[reminder objectForKey:@"typeID"] isEqualToString:[NSString stringWithFormat:@"%@",[_info objectForKey:@"id"]]] && [[reminder objectForKey:@"type"] isEqualToString:@"TechXPOT"]) {
            
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
