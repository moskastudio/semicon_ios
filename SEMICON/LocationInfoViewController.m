//
//  LocationInfoViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/10.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "LocationInfoViewController.h"
#import "MainMenuViewController.h"

@interface LocationInfoViewController ()

@end

@implementation LocationInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backBtn:(id)sender {
//    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:true];
}
@end
