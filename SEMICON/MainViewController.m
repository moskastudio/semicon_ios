//
//  ViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "MainViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#import "HomeViewController.h"
#import "FloorViewController.h"
#import "ScheduleViewController.h"
#import "PersonalViewController.h"
#import "EventOverviewViewController.h"
#import "AboutSettingViewController.h"
#import "EvevtActivitesViewController.h"
#import "ProgramTableViewController.h"
#import "SearchViewController.h"
#import "Service.h"
#import "WebViewController.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface MainViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate, HomeViewControllerDelegate, ScheduleViewControllerDelegate, FloorViewControllerDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    //NavigationBar
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"img_action_bar_bg.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    
    UIBarButtonItem *moreItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button_more.png"] style:UIBarButtonItemStylePlain target:self action:@selector(moreBtnAction:)];
    [moreItem setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *personalItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"img_secret_name.png"] style:UIBarButtonItemStylePlain target:self action:@selector(personalBtnAction:)];
    [personalItem setTintColor:[UIColor whiteColor]];
    
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"button_search.png"] style:UIBarButtonItemStylePlain target:self action:@selector(searchBtnAction:)];
    [searchItem setTintColor:[UIColor whiteColor]];
    
    NSArray *actionButtonItems = @[moreItem, personalItem, searchItem];
    self.navigationItem.rightBarButtonItems = actionButtonItems;
    
    UIBarButtonItem *logo = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"img_logo_semicon.png"] style:UIBarButtonItemStylePlain target:self action:nil];
    [logo setTintColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = logo;
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
    //
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}

-(void) viewDidAppear:(BOOL)animated{
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"img_action_bar_bg.png"]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.translucent = NO;
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] init];
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[[UIImage imageNamed:@"img_home_normal_tw.png"], [UIImage imageNamed:@"img_floor_normal_tw.png"], [UIImage imageNamed:@"img_schedule_normal_tw.png"]]];
    }
    else{
        segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[[UIImage imageNamed:@"img_home_normal.png"], [UIImage imageNamed:@"img_floor_normal.png"], [UIImage imageNamed:@"img_schedule_normal.png"]]];
    }
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont systemFontOfSize:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 3;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            HomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
            homeViewController.delegate = self;
            self.controllerHome = homeViewController;
            return homeViewController.view;
        }
            break;
        case 1:{
            FloorViewController *floorViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"FloorViewController"];
            floorViewController.delegate = self;
            self.controllerFloor = floorViewController;
            return floorViewController.view;
        }
            break;
        case 2:{
            ScheduleViewController *scheduleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ScheduleViewController"];
            scheduleViewController.delegate = self;
            self.controllerSchedule = scheduleViewController;
            return scheduleViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

#pragma mark - Button Action
- (void)searchBtnAction:(id)sender {
//    [self performSegueWithIdentifier:@"SearchView" sender:self];
    SearchViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:svc];
    [self presentViewController:nav animated:true completion:^{
        
    }];
}
- (void)personalBtnAction:(id)sender {
    PersonalViewController *personalViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalViewController"];
    [self.navigationController pushViewController:personalViewController animated:YES];
    ;
}
- (void)moreBtnAction:(id)sender {
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Cancel", "")
                              style:UIAlertActionStyleCancel
                              handler:^(UIAlertAction * action)
                              {
                                  //  UIAlertController will automatically dismiss the view
                              }];
    
    UIAlertAction* ExhibitionInfoBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Event Overview", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  EventOverviewViewController *eventOverviewViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EventOverviewViewController"];
                                  [self.navigationController pushViewController:eventOverviewViewController animated:YES];
                                  ;
                              }];
    
    [alert addAction:Cancel];
    [alert addAction:ExhibitionInfoBtn];
    
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    UIAlertAction* SignInBtn;
    if (!isLogin) {
        SignInBtn = [UIAlertAction
                     actionWithTitle:NSLocalizedString(@"Sign in", "")
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction * action)
                     {
                         [self performSegueWithIdentifier:@"SignInView" sender:self];
                     }];
        [alert addAction:SignInBtn];
    }
    
    
    UIAlertAction* SettingsBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Settings", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  AboutSettingViewController *aboutSettingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutSettingViewController"];
                                  aboutSettingViewController.soure = @"Setting";
                                  [self.navigationController pushViewController:aboutSettingViewController animated:YES];
                              }];
    
    UIAlertAction* AboutBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"About", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  AboutSettingViewController *aboutSettingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutSettingViewController"];
                                  aboutSettingViewController.soure = @"About";
                                  [self.navigationController pushViewController:aboutSettingViewController animated:YES];
                              }];
    
    
    
    [alert addAction:SettingsBtn];
    [alert addAction:AboutBtn];
    [self presentViewController:alert animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - HomeViewControllerDelegate
- (void)showViewWithStoryBoardID:(NSString *)storyBoardID {
    id vc = [self.storyboard instantiateViewControllerWithIdentifier:storyBoardID];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark - ScheduleViewControllerDelegate
- (void)presentWebView:(NSString *)urlString title:(NSString *)navTitle {
    WebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    wvc.urlString = urlString;
    wvc.navTitle = navTitle;
    [self.navigationController pushViewController:wvc animated:true];
}

#pragma mark - FloorViewControllerDelegate

- (void)showMapWithTitle:(NSString *)urlString title:(NSString *)navTitle {
    WebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    wvc.urlString = urlString;
    wvc.navTitle = navTitle;
    [self.navigationController pushViewController:wvc animated:true];
}
@end
