//
//  FoodDetailsTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FoodDetailsTableViewController.h"

@interface FoodDetailsTableViewController ()
//展覽館內
@property NSMutableArray *restaurantNameArray1;
@property NSMutableArray *foodTypeArray1;
@property NSMutableArray *foodTelArray1;
//展覽館週邊
@property NSMutableArray *restaurantNameArray2;
@property NSMutableArray *foodTypeArray2;
@property NSMutableArray *foodTelArray2;
@property NSMutableArray *restaurantAddressArray;
//中國信託園區
@property NSMutableArray *restaurantNameArray3;
@property NSMutableArray *foodTypeArray3;
@property NSMutableArray *foodTelArray3;

@property UIWebView *webView;
@property NSString *telNumber;


@end

@implementation FoodDetailsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%ld",(long)_foodRegion);
    
    //展覽館內 _foodRegion == 0
    NSArray *oneFloorArray = [[NSArray alloc] initWithObjects:@"MOS BURGER",@"Easy Going (Vagetarian)",@"Mr. Brown Coffe",@"Hi-Life International CO.,LTD.",@"RealBakery",@"CoCo Fresh Tea & Juice",@"KFC",@"Curry Inn",@"Chimei Food",@"Chicken Master",@"Michael Tu Messe Bistro", nil];
    NSArray *threeFloorArray = [[NSArray alloc] initWithObjects:@"The Banquet Hall", nil];
    NSArray *fourFloorArray = [[NSArray alloc] initWithObjects:@"Miss Croissant",@"ZOOM CAFE",@"Pizza Hut", nil];
    
    NSArray *oneFloorArrayChinese = [[NSArray alloc] initWithObjects:@"摩斯漢堡",@"輕鬆食(素食)",@"伯朗咖啡館",@"萊爾富便利商店",@"臻.歐式烘焙坊",@"都可茶飲",@"肯德基",@"咖哩家",@"奇美食品",@"炸雞大師",@"魅色時尚餐飲", nil];
    NSArray *threeFloorArrayChinese = [[NSArray alloc] initWithObjects:@"寒舍 樂樂軒/自助餐", nil];
    NSArray *fourFloorArrayChinese = [[NSArray alloc] initWithObjects:@"樂利餐廳",@"聚焦咖啡",@"必勝客", nil];
    
    if ([[NSLocale preferredLanguages][0]  isEqual: @"zh-TW"]) {
        self.restaurantNameArray1 = [[NSMutableArray alloc] initWithObjects:oneFloorArrayChinese, threeFloorArrayChinese,fourFloorArrayChinese, nil];
    }
    else{
        self.restaurantNameArray1 = [[NSMutableArray alloc] initWithObjects:oneFloorArray, threeFloorArray,fourFloorArray, nil];
    }
    
    NSArray *oneFloorTypeArray = [[NSArray alloc] initWithObjects:@"Fast Food",@"Fast Food",@"Coffe&Meal",@"Convenient Store",@"Bakery",@"Beverage Store",@"Fast Food",@"Meal",@"Chinese Food",@"Fast Food",@"Coffe&Meal", nil];
    NSArray *threeFloorTypeArray = [[NSArray alloc] initWithObjects:@"Chinese Food/Buffet/Coffe&Meal", nil];
    NSArray *fourFloorTypeArray = [[NSArray alloc] initWithObjects:@"Coffe&Meal",@"Coffe&Meal",@"Fast Food", nil];
    
    NSArray *oneFloorTypeArrayChinese = [[NSArray alloc] initWithObjects:@"速食",@"速食",@"咖啡簡餐",@"便利商店",@"麵包店",@"飲料店",@"速食",@"簡餐",@"中餐廳",@"速食",@"咖啡簡餐", nil];
    NSArray *threeFloorTypeArrayChinese = [[NSArray alloc] initWithObjects:@"中餐廳/自助餐/咖啡簡餐", nil];
    NSArray *fourFloorTypeArrayChinese = [[NSArray alloc] initWithObjects:@"咖啡簡餐",@"咖啡簡餐",@"速食", nil];
    if ([[NSLocale preferredLanguages][0]  isEqual: @"zh-TW"]) {
        self.foodTypeArray1 = [[NSMutableArray alloc] initWithObjects:oneFloorTypeArrayChinese, threeFloorTypeArrayChinese,fourFloorTypeArrayChinese, nil];
    }
    else{
        self.foodTypeArray1 = [[NSMutableArray alloc] initWithObjects:oneFloorTypeArray, threeFloorTypeArray,fourFloorTypeArray, nil];
    }
    
    NSArray *oneFloorTelArray = [[NSArray alloc] initWithObjects:@"+886-2-2783-4138",@"+886-2-2651-9394",@"+886-2-2783-6963",@"0800-022-118",@"+886-2-7709-4320",@"+886-2-2782-1622",@"+886-2-2633-0345",@"+886-917-657-326",@"+886-0800-783-703",@"886-2-7746-2978",@"+886-2-2786-7800", nil];
    NSArray *threeFloorTelArray = [[NSArray alloc] initWithObjects:@"+886-2-2653-0230", nil];
    NSArray *fourFloorTelArray = [[NSArray alloc] initWithObjects:@"+886-2-2783-3521",@"+886-2-2731-1119",@"+886-2-2346-0777", nil];
    self.foodTelArray1 = [[NSMutableArray alloc] initWithObjects:oneFloorTelArray, threeFloorTelArray,fourFloorTelArray, nil];
    
    //展覽館週邊 _foodRegion == 1
    NSArray *surroundingCArray = [[NSArray alloc] initWithObjects:@"A.S. Cafe Lounge 中研咖啡廳",@"Lizard Kitchen",@"Magic 廚房",@"三竹園客家粵菜",@"中研院學術活動中心附設中餐廳",@"北大荒水餃",@"北雲中餐廳",@"四季鮮餐坊",@"巧家福日本料理",@"好康南洋風味小吃店",@"宏亮日本料理",@"阿亮平價日式料亭",@"食健家什錦蓋飯",@"恩花園韓國料理",@"高樂餐飲",@"船屋日式料理",@"陶膳",@"鹿野苑",@"尊上食坊",@"彭家屯牛肉館(南港分店)",@"粥老爺",@"雲泰萊風味館",@"飯糰屋",@"黃金175豬排",@"粵香園港式燒臘",@"樂利餐廳", @"餡老滿",nil];
    NSArray *surroundingWArray = [[NSArray alloc] initWithObjects:@"Awesome Pasta 歐森義式餐飲",@"Café Academic",@"Ikari Coffee 怡客咖啡",@"Lavazza",@"LuLuHouse乳酪蛋糕 (南港店)",@"麥當勞",@"摩斯漢堡",@"My o My café",@"樂雅樂餐廳",@"星巴克咖啡",@"Subway",@"T & V automata bufe",@"LAVAZZA",@"幸福空間歐式料理", nil];
    self.restaurantNameArray2 = [[NSMutableArray alloc] initWithObjects:surroundingCArray, surroundingWArray, nil];
    
    NSArray *surroundingAddressCArray = [[NSArray alloc] initWithObjects:@"25.042683,121.6119792",@"25.0565859,121.6114842",@"25.0820309,121.3518469",@"25.0555356,121.6111411",@"25.041,121.6103641",@"25.055054,121.6095303",@"25.045209,121.6134301",@"25.0561281,121.6114119",@"25.0543783,121.6030122",@"25.0550167,121.6119528",@"25.037395,121.6149373",@"25.0417085,121.6158968",@"25.0566126,121.6097058",@"25.0816026,121.570961",@"25.057204,121.6132673",@"25.0557347,121.6095287",@"25.057452,121.6117867",@"25.0548792,121.6118204",@"25.055443,121.6122813",@"25.0551929,121.6105992",@"25.0561142,121.6109554",@"25.055767,121.6095823",@"25.0759117,121.4997585",@"25.047296,121.6129883",@"25.0554526,121.6111732",@"25.0575526,121.6115779", @"25.0572973,121.6142816",nil];
    NSArray *surroundingAddressWArray = [[NSArray alloc] initWithObjects:@"25.0421852,121.612359",@"25.0551086,121.6117682",@"25.057878,121.6113063",@"25.056586,121.6114843",@"25.0551369,121.61346",@"25.0522393,121.6141438",@"25.057518,121.6115504",@"25.056586,121.6114843",@"25.0571503,121.613707",@"25.057878,121.6113063",@"25.0578615,121.5434546",@"25.055499,121.6082103",@"25.056586,121.6114843",@"25.069868,121.6147803", nil];
    self.restaurantAddressArray = [[NSMutableArray alloc] initWithObjects:surroundingAddressCArray, surroundingAddressWArray, nil];
    
    NSArray *surroundingCTypeArray = [[NSArray alloc] initWithObjects:@"飲品、會議室",@"Lizard Kitchen",@"咖哩飯",@"中式簡餐、麵類",@"客家粵菜",@"日式",@"水餃",@"合菜、聚餐包廂",@"中日複合式簡餐",@"日式",@"馬來西亞咖哩/新加坡肉骨茶",@"平價日式料理 套餐 咖哩",@"日式定食、蓋飯",@"日式, 麵食, 便當",@"韓式料理",@"鐵板燒&迴轉壽司複合式餐廳",@"平價日式料理、日式蓋飯",@"日式定食、便當",@"牛肉麵",@"素食",@"牛肉麵",@"粥",@"雲泰萊料理",@"飯糰、飯盒",@"炸豬排",@"燒臘、便當",@"", nil];
    NSArray *surroundingWTypeArray = [[NSArray alloc] initWithObjects:@"義大利麵、燉飯",@"牛排、自助無限吧",@"咖啡、餐點",@"咖啡、餐點",@"乳酪蛋糕",@"速食",@"速食",@"咖啡簡餐",@"簡餐",@"咖啡",@"速食",@"西歐料理",@"西式簡餐、咖啡飲料",@"歐式料理", nil];
    self.foodTypeArray2 = [[NSMutableArray alloc] initWithObjects:surroundingCTypeArray, surroundingWTypeArray, nil];
    
    NSArray *surroundingTelCArray = [[NSArray alloc] initWithObjects:@"+886-2-27852712#1219",@"+886-2-26552418 ",@"+886-2-26550029",@"+886-2-27885810",@"+886-2-26511286 ",@"+886-2-27880455",@"+886-2-26518532",@"+886-2-26546233",@"+886-2-27836909",@"+886-2-26536756",@"+886-26521584",@"+886-2-27834658",@"+886-2-2782-7986",@"+886-2-26547822",@"+886-2-26589682",@"+886-2-27889149",@"+886-2-26511008 ",@"+886-2-26516500",@"+886-2-2782-5167",@"+886-2-2782-2058",@"+886-2-2653-5869",@"+886-2-27892645",@"+886-2-26550954",@"+886-2-2788-6338 ",@"+886-2-26533900",@"+886-2-2653-2048",@"+886-2-27893423", nil];
    NSArray *surroundingTelWArray = [[NSArray alloc] initWithObjects:@"+886-2-26521079",@"+886-2-27856954",@"+886-2-26552782",@"+886-2-26552105",@"+886-2-27827811",@"+886-2-27890595",@"+886-2-2655-1873",@"+886-2-26552053",@"+886-2-27831861 ",@"+886-2-26551456",@"+886-2-26552087",@"+886-2-27856738",@"+886-2-26552105",@"+886-2-26321882", nil];
    self.foodTelArray2 = [[NSMutableArray alloc] initWithObjects:surroundingTelCArray, surroundingTelWArray, nil];
    
    //展覽館週邊 _foodRegion == 2
    NSArray *financeArray = [[NSArray alloc] initWithObjects:@"鶴越烏龍麵",@"朱記",@"斑鳩的窩",@"飯樂丼(Fun Rice)",@"古拉爵(Café Grazie)",@"The Dinner 樂子",@"樂麵屋",@"乾杯",@"黑面菜-滷春秋",@"開飯川食堂KAIFUN",@"Pepper Lunch 胡椒廚房",@"havana-papa",@"老董牛肉麵",@"勝博殿",@"牛車水鐵板燒",@"鴻屋木桶燒",@"一信麵家",@"車屋咖哩",@"于記杏仁豆腐",@"1010湘",@" 非常泰",@"哈根達斯 Häagen-Dazs",@"Mr.Onion牛排餐廳",@"西雅圖咖啡",@"漢堡王",@"吉品海鮮餐廳",@"春水堂", nil];
    self.restaurantNameArray3 = [[NSMutableArray alloc] initWithObjects:financeArray, nil];
    NSArray *financeTypeArray = [[NSArray alloc] initWithObjects:@"烏龍麵",@"小吃、餡餅粥",@"日式炸物",@"日本料理、丼飯",@"義式餐廳",@"美式餐廳",@"日式拉麵",@"燒烤",@"小吃、滷味、飲料店",@"川菜",@"日式料理",@"異國料理",@"中式料理",@"日式炸物",@"中式法式泰式複合",@"日式咖哩鐵板",@"日式拉麵",@"日式咖哩",@"甜品類",@"川菜",@"泰國料理",@"冰品甜點",@"排餐類",@"咖啡、簡餐類",@"美式餐廳",@"合菜類",@"中式餐廳", nil];
    self.foodTypeArray3 = [[NSMutableArray alloc] initWithObjects:financeTypeArray, nil];
    
    NSArray *financeTelArray = [[NSArray alloc] initWithObjects:@"+886-2-6613-0967",@"+886-2-2653-2800",@"+886-2-2652-2259",@"0800-252-522",@"+886-2-2789-2688",@"+886-2-2788-3330",@"+886-2-2786-1377",@"+886-2-2786-5669",@"+886-2-2786-2239",@"+886-2-2786-5778",@"+886-2-2789-1758",@"+886-2-2651-1617",@"+886-2-2783-4485",@"+886-2-6617-2977",@"",@"",@"",@"",@"+886-906-086-669",@"+886-2-2782-0866",@"+886-2-2782-0899",@"+886-2-2651-6866",@"+886-2-2786-5877",@"+886-2-2783-6442",@"+886-2-2785-7119",@"+886-2-2654-7979",@"+886-2-2783-6151", nil];
    self.foodTelArray3 = [[NSMutableArray alloc] initWithObjects:financeTelArray, nil];
    
    if (_foodRegion == 0) {
        self.title = NSLocalizedString(@"Taipei Nangang ExhibitionCenter", "") ;
    }
    else if (_foodRegion == 1) {
        self.title = @"南港展覽館 週邊餐飲服務";
    }
    else {
        self.title = @"中國信託金融園區";
    }
    
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_foodRegion == 0 ) {
        return [self.restaurantNameArray1 count];
    }
    else if (_foodRegion == 1 ) {
        return [self.restaurantNameArray2 count];
    }
    else
        return [self.restaurantNameArray3 count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (_foodRegion == 0 ) {
        return [[self.restaurantNameArray1 objectAtIndex:section] count];
    }
    else if (_foodRegion == 1 ) {
        return [[self.restaurantNameArray2 objectAtIndex:section] count];
    }
    else
        return [[self.restaurantNameArray3 objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (_foodRegion == 0) {
        UILabel *restaurantNameLabel=[[UILabel alloc] initWithFrame:
                                      CGRectMake(30, 22, cell.frame.size.width-130, 18)];
        [restaurantNameLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        restaurantNameLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        restaurantNameLabel.text = [[self.restaurantNameArray1 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        restaurantNameLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:restaurantNameLabel];
        
        UILabel *foodTypeLabel=[[UILabel alloc] initWithFrame:
                                CGRectMake(30, 45, cell.frame.size.width-130, 15)];
        [foodTypeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        foodTypeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        foodTypeLabel.text = [[self.foodTypeArray1 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        foodTypeLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:foodTypeLabel];
        
        UIButton *telBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        telBtn.frame = CGRectMake(self.view.frame.size.width-50, 24, 40, 40);
        UIImage *btnImage = [UIImage imageNamed:@"btn_call_phone.png"];
        [telBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [telBtn addTarget:self action:@selector(telBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:telBtn];
    }
    
    else if (_foodRegion == 1){
        UILabel *restaurantNameLabel=[[UILabel alloc] initWithFrame:
                                      CGRectMake(30, 22, cell.frame.size.width-130, 18)];
        [restaurantNameLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        restaurantNameLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        restaurantNameLabel.text = [[self.restaurantNameArray2 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        restaurantNameLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:restaurantNameLabel];
        
        UILabel *foodTypeLabel=[[UILabel alloc] initWithFrame:
                                CGRectMake(30, 45, cell.frame.size.width-130, 15)];
        [foodTypeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        foodTypeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        foodTypeLabel.text = [[self.foodTypeArray2 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        foodTypeLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:foodTypeLabel];
        
        UIButton *telBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        telBtn.frame = CGRectMake(self.view.frame.size.width-50, 24, 40, 40);
        UIImage *btnImage = [UIImage imageNamed:@"btn_call_phone.png"];
        [telBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [telBtn addTarget:self action:@selector(telBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:telBtn];
        
        UIButton *mapBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        mapBtn.frame = CGRectMake(self.view.frame.size.width-100, 24, 40, 40);
        UIImage *mapbtnImage = [UIImage imageNamed:@"btn_store_location.png"];
        [mapBtn setBackgroundImage:mapbtnImage forState:UIControlStateNormal];
        [mapBtn addTarget:self action:@selector(mapBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:mapBtn];
    }
    
    else{
        UILabel *restaurantNameLabel=[[UILabel alloc] initWithFrame:
                                      CGRectMake(30, 22, cell.frame.size.width-130, 18)];
        [restaurantNameLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        restaurantNameLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        restaurantNameLabel.text = [[self.restaurantNameArray3 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        restaurantNameLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:restaurantNameLabel];
        
        UILabel *foodTypeLabel=[[UILabel alloc] initWithFrame:
                                CGRectMake(30, 45, cell.frame.size.width-130, 15)];
        [foodTypeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        foodTypeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        foodTypeLabel.text = [[self.foodTypeArray3 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        foodTypeLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:foodTypeLabel];
        
        UIButton *telBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        telBtn.frame = CGRectMake(self.view.frame.size.width-50, 24, 40, 40);
        UIImage *btnImage = [UIImage imageNamed:@"btn_call_phone.png"];
        [telBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [telBtn addTarget:self action:@selector(telBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:telBtn];
        
        UIButton *mapBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        mapBtn.frame = CGRectMake(self.view.frame.size.width-100, 24, 40, 40);
        UIImage *mapbtnImage = [UIImage imageNamed:@"btn_store_location.png"];
        [mapBtn setBackgroundImage:mapbtnImage forState:UIControlStateNormal];
        [mapBtn addTarget:self action:@selector(mapBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:mapBtn];
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 50)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 10, sectionHeaderView.frame.size.width, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    if (_foodRegion == 0) {
        switch (section) {
            case 0:
                headerLabel.text = NSLocalizedString(@"Taipei Nangang ExhibitionCenter 1F", "") ;
                return sectionHeaderView;
                break;
            case 1:
                headerLabel.text = NSLocalizedString(@"Taipei Nangang ExhibitionCenter 3F", "") ;
                return sectionHeaderView;
                break;
            case 2:
                headerLabel.text = NSLocalizedString(@"Taipei Nangang ExhibitionCenter 4F", "") ;
                return sectionHeaderView;
                break;
            default:
                break;
        }
    }
    
    else if (_foodRegion == 1) {
        switch (section) {
            case 0:
                headerLabel.text = NSLocalizedString(@"Restaurant Near by Nangang Exhibition Hall Chinese/Japanese Food", "");
                return sectionHeaderView;
                break;
            case 1:
                headerLabel.text = NSLocalizedString(@"Restaurant Near by Nangang Exhibition Hall Western Style Food", "");
                return sectionHeaderView;
                break;
            default:
                break;
        }
    }
    
    else {
        switch (section) {
            case 0:
                headerLabel.text = @"CTBC Financial Park (walking distance from Taipei Nangang Center, Hall1)";
                return sectionHeaderView;
                break;
            default:
                break;
        }
    }
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}
-(NSIndexPath*)GetIndexPathFromSender:(id)sender{
    if(!sender) {
        return nil;
    }
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        return [self.tableView indexPathForCell:cell];
    }
    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
}

-(void) telBtnAction:(id)sender {
    NSLog(@"telBtnAction");
    NSIndexPath *indexPath = [self GetIndexPathFromSender:sender];
    if (_foodRegion == 0 ) {
        self.telNumber = [[self.foodTelArray1 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    else if (_foodRegion == 1 ) {
        self.telNumber = [[self.foodTelArray2 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    else{
        self.telNumber = [[self.foodTelArray3 objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
    
    if (![self.telNumber isEqualToString:@""]) {
        UIAlertController* alert = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleActionSheet];
        [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
        
        UIAlertAction* Cancel = [UIAlertAction
                                 actionWithTitle:NSLocalizedString(@"Cancel", "")
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction * action)
                                 {
                                     //  UIAlertController will automatically dismiss the view
                                 }];
        
        UIAlertAction* CallBtn = [UIAlertAction
                                  actionWithTitle:NSLocalizedString(@"Call Restaurant", "")
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      NSString *phoneNumber = [@"tel://" stringByAppendingString:self.telNumber];
                                      NSLog(@"phoneNumber:%@",phoneNumber);
                                      [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                                  }];
        
        [alert addAction:Cancel];
        [alert addAction:CallBtn];
        [self presentViewController:alert animated:YES completion:nil];
    }
    else{
        UIAlertController* alert = [UIAlertController
                                    alertControllerWithTitle:nil
                                    message:nil
                                    preferredStyle:UIAlertControllerStyleActionSheet];
        [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
        UIAlertAction* NoCallBtn = [UIAlertAction
                                    actionWithTitle: NSLocalizedString(@"No phone number", "")
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                    }];
        
        [alert addAction:NoCallBtn];
        [self presentViewController:alert animated:YES completion:nil];
    }
}


-(void) mapBtnAction:(id)sender {
    if (_foodRegion == 1) {
        NSIndexPath *indexPath = [self GetIndexPathFromSender:sender];
        NSString *restaurantAddress =[[self.restaurantAddressArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        NSString* stringURL = [NSString stringWithFormat:@"comgooglemapsurl://?q=%@&zoom=10&views=traffic", restaurantAddress];
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
        }
        else {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@&zoom=10&views=traffic",restaurantAddress]];
            [[UIApplication sharedApplication] openURL: url];
        }
    }
    else if (_foodRegion == 2){
        NSString* stringURL = @"comgooglemapsurl://?q=25.0596987,121.6130575&zoom=10&views=traffic";
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
        }
        else {
            NSURL *url = [NSURL URLWithString:@"http://maps.google.com/maps?q=25.0596987,121.6130575&zoom=10&views=traffic"];
            [[UIApplication sharedApplication] openURL: url];
        }
    }
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
