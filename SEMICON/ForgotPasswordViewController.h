//
//  ForgotPasswordViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgotPasswordViewController : UIViewController
- (IBAction)cancelSignBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
- (IBAction)getPassword:(id)sender;

@end
