//
//  ASDetailsViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ASDetailsViewController.h"

@interface ASDetailsViewController ()

@property NSArray *FAQquestionArray;
@property NSMutableArray *FAQAnswerArray;
@property NSInteger contentLabelHeight;
@property UILabel *cellLabel;
@property NSArray *ansArray;

@end

@implementation ASDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    self.ansArray = [[NSArray alloc] initWithObjects:
                     NSLocalizedString( @"Registered users can access to personalized functions like My Favorite, My Calendar, and My Notes. We can record your preferences and provided customized services.",""),
                     NSLocalizedString(@"A user can register using email address or connecting via Facebook account.",""),
                     NSLocalizedString(@"Click “Forgot Password” on the sign-in page and enter your registered email address, and you will receive a link to create a new password.",""),
                     NSLocalizedString(@"Enter setting page from the main page of App, you can choose to disable reminder. However, you will still receive notifications. To completely turn off notifications, please go to smart phone setting page.",""),
                     NSLocalizedString(@"You may use the keyword search function on App main page for information like exhibitors, booth numbers, programs, and events.",""),
                     NSLocalizedString(@"Enter Exhibitor List, click the exhibitor to enter the exhibitor profile, and click “Add Favorite” icon to add the exhibitor into My Favorite List. You can check My Favorite List anytime on Personal Page.",""),
                     NSLocalizedString(@"By clicking the “Add Calendar” icon on pages, you can add meeting time with exhibitors, programs, and events to your personal calendar within APP. You may also synchronize your APP calendar and smart phone calendar.",""),
                     NSLocalizedString(@"You can share “My Favorite”, “My Calendar”, and “My note” via linkage, Facebook, Twitter, Google+, or email by pressing the item on the list.",""),
                     NSLocalizedString(@"You may delete any item on “My Favorite”, “My Calendar”, and “My note” by pressing the item on the list.",""),
                     nil];
    
    if ([_source isEqualToString:@"About"]) {
        self.title = NSLocalizedString(@"About SEMI", "") ;
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 250)];
        NSString *contentString = NSLocalizedString(@"SEMI is the global industry association serving the manufacturing supply chains for the microelectronic, display and photovoltaic industries. SEMI member companies are the engine of the future, enabling smarter, faster and more economical products that improve our lives. Since 1971, SEMI has been committed to helping members grow more profitably, create new markets and meet common industry challenges. SEMI maintains offices in Austin, Beijing, Brussels, Hsinchu, Moscow, San Jose, Seoul, Shanghai, Singapore, Tokyo, India and Washington, D.C. For more information, visit www.semi.org", "") ;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamBook" size:14],NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        contentLabel.frame = CGRectMake(36, 50, actualSize.width,actualSize.height*1.72+10);
        
        UILabel *websiteLabel= [[UILabel alloc] initWithFrame:CGRectMake(27, contentLabel.frame.origin.y+contentLabel.frame.size.height+24, self.view.frame.size.width-54, 30)];
        [websiteLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        websiteLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        websiteLabel.textAlignment = NSTextAlignmentLeft;
        websiteLabel.text = @"http://www.semi.org";
        websiteLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(didTapLabelWithGesture:)];
        [websiteLabel addGestureRecognizer:tapGesture];
        [self.view addSubview:websiteLabel];
        
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [self.view addSubview:contentLabel];
        
        _webView.hidden = YES;
    }
    else if ([_source isEqualToString:@"Policy"]){
        self.title = NSLocalizedString(@"Privacy Policy", "") ;
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 250)];
        NSString *contentString = NSLocalizedString(@"To register on SEMICON Taiwan App, you are required to provide your name, cellphone number, and email address, for us to provide personalized user experiences and to avoid unauthorized abuse. If you don’t want to provide your name, cellphone number, and email address, you may also register via Facebook. Please note, SEMI won’t collect your Facebook password. To use SEMICON Taiwan App, you are not required to provide your location information. However, SEMI provides features that utilize geolocation for your convenience and to improve your user experience. Other than as described in this Privacy Policy and, where relevant, other applicable privacy policies or addendums, SEMI will never provide your information to any third parties without your consent, unless SEMI believes in good faith that we are required or permitted to do so under applicable laws or to protect and defend SEMI’s rights and/or property.", "");
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamBook" size:14],NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        contentLabel.frame = CGRectMake(36, 50, actualSize.width,actualSize.height*1.72+10);
        [self.view addSubview:contentLabel];
        
        _webView.hidden = YES;
    }
    else if ([_source isEqualToString:@"FAQ"]){
        self.title = @"FAQ";
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 250)];
        NSString *contentString = [self.ansArray objectAtIndex:_indexPath];
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [self.view addSubview:contentLabel];
        
        _webView.hidden = YES;
    }
    else{
        self.title = NSLocalizedString(@"Powered by", "");
        
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, self.view.frame.size.width-54, 250)];
        NSString *contentString = NSLocalizedString(@"Production Team\n\nMoska Studio focuses on developing of native mobile applications and provides user experience research and user interface design. Our services include: \"Wireframe Prototype\", \"UX Flow\", \"Style Development\", \"Native iOS/Android App Development\" and \"Back-end Server and Database Implementation\". From designing with an idea to develop a complete native mobile application service, we could help you at any stage. Contact us:", "") ;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"GothamBook" size:14],NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        contentLabel.frame = CGRectMake(36, 50, actualSize.width,actualSize.height*1.72+10);
        
        UILabel *websiteLabel= [[UILabel alloc] initWithFrame:CGRectMake(27, contentLabel.frame.origin.y+contentLabel.frame.size.height+24, self.view.frame.size.width-54, 30)];
        [websiteLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        websiteLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        websiteLabel.textAlignment = NSTextAlignmentLeft;
        websiteLabel.text = @"www.moskastudio.tw";
        websiteLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]
                                              initWithTarget:self action:@selector(didTapTeamWithGesture:)];
        [websiteLabel addGestureRecognizer:tapGesture];
        [self.view addSubview:websiteLabel];
        
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        [self.view addSubview:contentLabel];
        
        _webView.hidden = YES;
    }
}

- (void)didTapLabelWithGesture:(UITapGestureRecognizer *)tapGesture {
    NSString *str = @"http://www.semi.org";
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *result = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSURL *url = [NSURL URLWithString:result];
    [[UIApplication sharedApplication] openURL: url];
}

- (void)didTapTeamWithGesture:(UITapGestureRecognizer *)tapGesture {
    NSString *str = @"http://www.moskastudio.tw/";
    NSCharacterSet *set = [NSCharacterSet URLQueryAllowedCharacterSet];
    NSString *result = [str stringByAddingPercentEncodingWithAllowedCharacters:set];
    NSURL *url = [NSURL URLWithString:result];
    [[UIApplication sharedApplication] openURL: url];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
