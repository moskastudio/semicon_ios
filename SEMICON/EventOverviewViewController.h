//
//  EventOverviewViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>


@interface EventOverviewViewController : UIViewController<MFMailComposeViewControllerDelegate>
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *aboutSEMICON;
@property (strong, nonatomic) IBOutlet UIView *locationView;
@property (strong, nonatomic) IBOutlet UIView *telView;
@property (strong, nonatomic) IBOutlet UIView *mailView;

@end
