//
//  ExhibitorProductViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLScrollViewSlider.h"

@protocol ExhibitorProductViewControllerDelegate <NSObject>
-(void)showExhibitorProductList:(NSString*) categoryID;
@end

@interface ExhibitorProductViewController : UIViewController<UITableViewDelegate,UITableViewDelegate>
@property id <ExhibitorProductViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *ADView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSMutableArray *exhibitorArray;

@end
