//
//  MainMenuViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/6.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainMenuViewControllerDelegate <NSObject>

- (void) showMediaCenter;
- (void) showLocalInfo;
- (void) showExhibitorList;
- (void) showPavilion;
- (void) showEvevtActivite;
- (void) showProgram;
- (void) showProduct;
- (void) showRegister;

@end

@interface MainMenuViewController : UIViewController <UINavigationBarDelegate,UINavigationControllerDelegate>

@property id <MainMenuViewControllerDelegate> delegate;

- (IBAction)mediaCenterBtn:(id)sender;
- (IBAction)localInfoBtn:(id)sender;
- (IBAction)ExhibitorBtn:(id)sender;
- (IBAction)PavilionBtn:(id)sender;
- (IBAction)EvevtActiviteBtn:(id)sender;
- (IBAction)ProgramBtn:(id)sender;
- (IBAction)ProductBtn:(id)sender;
- (IBAction)RegisterBtn:(id)sender;

@end
