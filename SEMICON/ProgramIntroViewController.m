//
//  ProgramIntroViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/3.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AppDelegate.h"
#import "ProgramIntroViewController.h"
#import "ProgramMapImageViewController.h"
#import "AddCalendarViewController.h"
#import "SignInViewController.h"
#import "Service.h"

@interface ProgramIntroViewController () {
    Boolean isNeedToRefresh;
}

@property NSMutableDictionary *dict;

@property NSInteger titleLabelHeight;
@property NSInteger themeHeight;
@property NSInteger outlineHeight;
@property NSInteger moderatorHeight;
@property NSInteger chairmanHeight;

@property UILabel *outlineLabel;
@property UILabel *moderatorLabel;
@property UILabel *chairmanLabel;

@property NSInteger outlineTouch;
@property NSInteger moderatorTouch;
@property NSInteger chairmanTouch;

@property UIView *coverView;

@end

@implementation ProgramIntroViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        _registerImageView.hidden = YES;
        _registerImageViewZH.hidden = NO;
        [_priceBtn setImage:[UIImage imageNamed:@"btn_price_ch@2x.png"] forState:UIControlStateNormal];
    }else{
        _registerImageView.hidden = NO;
        _registerImageViewZH.hidden = YES;
    }
    
    self.tableView.separatorColor = [UIColor whiteColor];
    self.titleLabelHeight = 0;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.view.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    
    self.outlineTouch = 0;
    self.moderatorTouch = 0;
    self.chairmanTouch = 0;
    
    self.coverView = [[UIView alloc] initWithFrame:
                      CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    self.coverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIView *costPopUpView = [[UIView alloc]
                                      initWithFrame:CGRectMake(self.view.frame.size.width/15, (self.view.frame.size.height-64)/15, self.view.frame.size.width/15*13, (self.view.frame.size.height-64-51)/15*13)];
    costPopUpView.backgroundColor = [UIColor whiteColor];
    costPopUpView.layer.cornerRadius = 10.0;
    costPopUpView.clipsToBounds = YES;
    [self.coverView addSubview:costPopUpView];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(costPopUpView.frame.size.width/2-90, 10, 180, 40)];
    titleLabel.text = NSLocalizedString(@"Cost", "") ;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:17];
    titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:titleLabel];
    
    UIView *lineUp = [[UIView alloc] initWithFrame: CGRectMake(0, 60, costPopUpView.frame.size.width, 1.5)];
    lineUp.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineUp];
    
    UILabel *beforeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineUp.frame.origin.y + 31, costPopUpView.frame.size.width, 40)];
    NSString *before = [[_forumInfo objectForKey:@"fee"] objectForKey:@"before"];
    before = [before stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"\00a0" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@" " withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@" " withString:@""];
    before = before.uppercaseString;
    before = [before stringByReplacingOccurrencesOfString:@"/" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"PERPERSON" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"每人" withString:@""];
    before = [before stringByReplacingOccurrencesOfString:@"NT" withString:@" NT"];
    beforeLabel.text = before;
    beforeLabel.textAlignment = NSTextAlignmentCenter;
    beforeLabel.font = [UIFont fontWithName:@"GothamMedium" size:30];
    beforeLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:59.0/255.0 blue:184.0/255.0 alpha:1.0];
    [costPopUpView addSubview:beforeLabel];
    
    UILabel *beforeTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, beforeLabel.frame.origin.y + 50, costPopUpView.frame.size.width, 20)];
    beforeTipLabel.text = NSLocalizedString(@"Early Bird Price (before Aug. 5)", "");
    beforeTipLabel.textAlignment = NSTextAlignmentCenter;
    beforeTipLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
    beforeTipLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:beforeTipLabel];
    
    UIView *lineMid = [[UIView alloc] initWithFrame: CGRectMake(costPopUpView.frame.size.width / 2 - 40, costPopUpView.frame.size.height / 2, 80, 1.5)];
    lineMid.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineMid];
    
    UILabel *afterLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, lineMid.frame.origin.y + 31, costPopUpView.frame.size.width, 40)];
    NSString *after = [[_forumInfo objectForKey:@"fee"] objectForKey:@"after"];
    after = [after stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\t" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\00a0" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@" " withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@" " withString:@""];
    after = after.uppercaseString;
    after = [after stringByReplacingOccurrencesOfString:@"/" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"PERPERSON" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"每人" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    after = [after stringByReplacingOccurrencesOfString:@"NT" withString:@" NT"];
    afterLabel.text = after;
    afterLabel.textAlignment = NSTextAlignmentCenter;
    afterLabel.font = [UIFont fontWithName:@"GothamMedium" size:30];
    afterLabel.textColor = [UIColor colorWithRed:103.0/255.0 green:59.0/255.0 blue:184.0/255.0 alpha:1.0];
    [costPopUpView addSubview:afterLabel];
    
    UILabel *afterTipLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, afterLabel.frame.origin.y + 50, costPopUpView.frame.size.width, 20)];
    afterTipLabel.text = NSLocalizedString(@"Original Price (after Aug. 6)", "") ;
    afterTipLabel.textAlignment = NSTextAlignmentCenter;
    afterTipLabel.font = [UIFont fontWithName:@"GothamMedium" size:14];
    afterTipLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [costPopUpView addSubview:afterTipLabel];
    
    UIView *lineDown = [[UIView alloc] initWithFrame: CGRectMake(0, costPopUpView.frame.size.height-60, costPopUpView.frame.size.width, 1.5)];
    lineDown.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [costPopUpView addSubview:lineDown];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    backBtn.frame = CGRectMake(costPopUpView.frame.size.width/2-100, costPopUpView.frame.size.height-50, 200, 40);
    [backBtn setTitle:NSLocalizedString(@"Back", @"")  forState:UIControlStateNormal];
    backBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    backBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamMedium" size:17];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [costPopUpView addSubview:backBtn];
    
    [self.view addSubview:self.coverView];
    self.coverView.hidden = YES;
    
    UITapGestureRecognizer *singleTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleRegisterTap:)];
    [self.registerView addGestureRecognizer:singleTap];
    
    [self.tableView reloadData];
}

- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        
        // Do what you want to do with the subview
        //NSLog(@"%@", subview);
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *tempLabel = (UILabel*)subview;
            [tempLabel setFrame:CGRectMake(tempLabel.frame.origin.x, tempLabel.frame.origin.y, tempLabel.frame.size.width, tempLabel.frame.size.height+5)];
        }
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

- (void)refreshCell {
    if (isNeedToRefresh) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        isNeedToRefresh = false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        return 11;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProgramIntroCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    if (indexPath.row == 0) {
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,25,(self.view.frame.size.width-72)/2,20)];
        tagLabel.text = [_forumInfo objectForKey:@"Category"];
        tagLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
        tagLabel.lineBreakMode = NSLineBreakByWordWrapping;
        tagLabel.textAlignment = NSTextAlignmentCenter;
        tagLabel.textColor = [UIColor whiteColor];
        tagLabel.backgroundColor = [UIColor colorWithRed:0 green:0.443 blue:0.698 alpha:1.0];
        [cell addSubview:tagLabel];
    }
    if (indexPath.row == 1) {
        //標題
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,8,cell.frame.size.width-72,25)];
        NSString *contentString = [_forumInfo objectForKey:@"forumName"];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        titleLabel.attributedText = attributedString ;
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.textColor = [UIColor blackColor];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        titleLabel.font = textFont;
        [titleLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        titleLabel.frame = CGRectMake(36, 8, actualSize.width,actualSize.height*1.5);
        [cell addSubview:titleLabel];
    }
    else if (indexPath.row == 2){
        cell.backgroundColor = [UIColor whiteColor];
        //時間、地點
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,5,cell.frame.size.width-72,35)];
        NSString *date = [_forumInfo objectForKey:@"date"];
        NSString *time = [_forumInfo objectForKey:@"time"];
        NSString *titleLabelString = [NSString stringWithFormat:@"%@\r%@",date,time];
        timeLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
        timeLabel.numberOfLines = 2;
        timeLabel.lineBreakMode = NSLineBreakByWordWrapping;
        timeLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleLabelString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleLabelString length])];
        timeLabel.attributedText = attributedString ;
        timeLabel.adjustsFontSizeToFitWidth = NO;
        timeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        timeLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
//        [timeLabel sizeToFit];
        [cell addSubview:timeLabel];
        
        UILabel *positionLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,40,cell.frame.size.width-72,40)];
        NSString *positionLabelString = [_forumInfo objectForKey:@"Location"];
        positionLabel.font = [UIFont fontWithName:@"GothamBook" size:13.5];
        positionLabel.numberOfLines = 2;
        positionLabel.lineBreakMode = NSLineBreakByWordWrapping;
        positionLabel.textAlignment = NSTextAlignmentLeft;
        attributedString = [[NSMutableAttributedString alloc] initWithString:positionLabelString];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [positionLabelString length])];
        positionLabel.attributedText = attributedString ;
        positionLabel.adjustsFontSizeToFitWidth = NO;
        positionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        positionLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
//        [positionLabel sizeToFit];
        [cell addSubview:positionLabel];
    }
    else if (indexPath.row == 3){
        cell.backgroundColor = [UIColor whiteColor];
        //三個按鈕
        UIButton *calendarBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/6-25, 15, 50, 50)];
        UIImage *btnImage = [UIImage imageNamed:@"btn_add_calendar.png"];
        [calendarBtn setImage:btnImage forState:UIControlStateNormal];
        UIImage *selectedBtnImage = [UIImage imageNamed:@"btn_my_calendar.png"];
        [calendarBtn setImage:selectedBtnImage forState:UIControlStateSelected];
        [calendarBtn addTarget:self action:@selector(calendarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:calendarBtn];
        
        if ([self checkCalendar]) {
            calendarBtn.selected = true;
        }
        
        UILabel *calendarLabel = [[UILabel alloc] initWithFrame:
                                  CGRectMake(18,65,(cell.frame.size.width-36)/3,20)];
        calendarLabel.text = NSLocalizedString(@"Add Calendar","");
        calendarLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        calendarLabel.textAlignment = NSTextAlignmentCenter;
        calendarLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:calendarLabel];
        
        UIButton *mapitBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/2-25, 15, 50, 50)];
        UIImage *mapitBtnImage = [UIImage imageNamed:@"btn_map_it.png"];
        [mapitBtn setImage:mapitBtnImage forState:UIControlStateNormal];
        [mapitBtn addTarget:self action:@selector(mapitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:mapitBtn];
        
        UILabel *mapitLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20)];
        mapitLabel.text = NSLocalizedString(@"Map It","");
        mapitLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        mapitLabel.textAlignment = NSTextAlignmentCenter;
        mapitLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:mapitLabel];
        
        UIButton *favoriteBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+5*(cell.frame.size.width-36)/6-25, 15, 50, 50)];
        UIImage *favoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_normal.png"];
        [favoriteBtn setImage:favoriteBtnImage forState:UIControlStateNormal];
        UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_press.png"];
        [favoriteBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
        [favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:favoriteBtn];
        
        if ([self checkForumFarvorites]) {
            favoriteBtn.selected = true;
        }
        
        UILabel *favoriteLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+2*(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20)];
        favoriteLabel.text = NSLocalizedString(@"Add Favorite","");
        favoriteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        favoriteLabel.textAlignment = NSTextAlignmentCenter;
        favoriteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:favoriteLabel];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 103, self.view.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    else if (indexPath.row == 4){
        //Theme
        cell.backgroundColor = [UIColor colorWithRed:0 green:0.443 blue:0.698 alpha:1.0];
        UILabel *introLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,24,cell.frame.size.width-72,26)];
        introLabel.text = NSLocalizedString(@"Theme","");
        introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        introLabel.textColor = [UIColor whiteColor];
        [introLabel sizeToFit];
        [cell addSubview:introLabel];
        
        UILabel *introContentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 55, self.view.frame.size.width-72, 50)];
        NSString *contentString = [_forumInfo objectForKey:@"title"];
        introContentLabel.numberOfLines = 0;
        introContentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        introContentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        introContentLabel.attributedText = attributedString ;
        introContentLabel.adjustsFontSizeToFitWidth = NO;
        introContentLabel.textColor = [UIColor whiteColor];
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
        introContentLabel.font = textFont;
        [introContentLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        introContentLabel.frame = CGRectMake(36, 55, actualSize.width,actualSize.height*2);
        self.themeHeight = actualSize.height*2+70;
        [cell addSubview:introContentLabel];
    }
    else if (indexPath.row == 5){
        cell.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,26)];
        outlineLabel.text = NSLocalizedString(@"Forum Outline","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [outlineLabel sizeToFit];
        [cell addSubview:outlineLabel];
    }
    else if (indexPath.row == 6){
        cell.backgroundColor = [UIColor whiteColor];
        if (self.outlineTouch == 0) {
            _outlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 100)];
            NSString *contentString = [_forumInfo objectForKey:@"Summary"];
            _outlineLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _outlineLabel.numberOfLines = 0;
            _outlineLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _outlineLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _outlineLabel.attributedText = attributedString ;
            _outlineLabel.adjustsFontSizeToFitWidth = NO;
            _outlineLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [cell addSubview:_outlineLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 63)];
            line.backgroundColor = [UIColor whiteColor];
            [cell addSubview:line];
            
            UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(36, 114, self.view.frame.size.width-72, 15)];
            more.text = NSLocalizedString(@"More","");
            more.font = [UIFont fontWithName:@"GothamMedium" size:15];
            more.textAlignment = NSTextAlignmentCenter;
            more.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
            [cell addSubview:more];
        }
        else{
            _outlineLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
            NSString *contentString =  [_forumInfo objectForKey:@"Summary"];
            _outlineLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _outlineLabel.numberOfLines = 0;
            _outlineLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _outlineLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _outlineLabel.attributedText = attributedString ;
            _outlineLabel.adjustsFontSizeToFitWidth = NO;
            _outlineLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [_outlineLabel sizeToFit];
            _outlineLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            
            CGSize size = [_outlineLabel sizeThatFits:CGSizeMake(_outlineLabel.frame.size.width, MAXFLOAT)];
            self.outlineHeight = size.height+30;
            _outlineLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, self.outlineHeight);
            [cell addSubview:_outlineLabel];
        }
    }
    else if (indexPath.row == 7){
        cell.backgroundColor = [UIColor whiteColor];
        //Forum Moderator
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,26)];
        titleLabel.text = NSLocalizedString(@"Forum Moderator","");
        titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        titleLabel.textColor = [UIColor blackColor];
        [titleLabel sizeToFit];
        [cell addSubview:titleLabel];
    }
    else if (indexPath.row == 8){
        cell.backgroundColor = [UIColor whiteColor];
        if (self.moderatorTouch == 0) {
            _moderatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 100)];
            NSString *contentString = [[_forumInfo objectForKey:@"host"] objectForKey:@"content"];
            _moderatorLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _moderatorLabel.numberOfLines = 0;
            _moderatorLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _moderatorLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _moderatorLabel.attributedText = attributedString ;
            _moderatorLabel.adjustsFontSizeToFitWidth = NO;
            _moderatorLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [cell addSubview:_moderatorLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 63)];
            line.backgroundColor = [UIColor whiteColor];
            [cell addSubview:line];
            
            UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(36, 114, self.view.frame.size.width-72, 15)];
            more.text = NSLocalizedString(@"More","");
            more.font = [UIFont fontWithName:@"GothamMedium" size:15];
            more.textAlignment = NSTextAlignmentCenter;
            more.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
            [cell addSubview:more];
        }
        else{
            _moderatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 50)];
            NSString *contentString = [[_forumInfo objectForKey:@"host"] objectForKey:@"content"];
            _moderatorLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _moderatorLabel.numberOfLines = 0;
            _moderatorLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _moderatorLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _moderatorLabel.attributedText = attributedString ;
            _moderatorLabel.adjustsFontSizeToFitWidth = NO;
            _moderatorLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [_moderatorLabel sizeToFit];
            _moderatorLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            
            CGSize size = [_moderatorLabel sizeThatFits:CGSizeMake(_moderatorLabel.frame.size.width, MAXFLOAT)];
            self.moderatorHeight = size.height+30;
            _moderatorLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, self.moderatorHeight);
            [cell addSubview:_moderatorLabel];
        }
    }
    else if (indexPath.row == 9){
        cell.backgroundColor = [UIColor whiteColor];
        //Forum Chairman
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,26)];
        titleLabel.text = NSLocalizedString(@"Forum Chairman","");
        titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        titleLabel.textColor = [UIColor blackColor];
        [titleLabel sizeToFit];
        [cell addSubview:titleLabel];
    }
    else if (indexPath.row == 10){
        cell.backgroundColor = [UIColor whiteColor];
        if (self.chairmanTouch == 0) {
            _chairmanLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 100)];
            NSString *contentString = [[_forumInfo objectForKey:@"chairman"] objectForKey:@"content"];
            _chairmanLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _chairmanLabel.numberOfLines = 0;
            _chairmanLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _chairmanLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _chairmanLabel.attributedText = attributedString ;
            _chairmanLabel.adjustsFontSizeToFitWidth = NO;
            _chairmanLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            _chairmanLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [cell addSubview:_chairmanLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 63)];
            line.backgroundColor = [UIColor whiteColor];
            [cell addSubview:line];
            
            UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(36, 114, self.view.frame.size.width-72, 15)];
            more.text = NSLocalizedString(@"More","");
            more.font = [UIFont fontWithName:@"GothamMedium" size:15];
            more.textAlignment = NSTextAlignmentCenter;
            more.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
            [cell addSubview:more];
            
        }
        else{
            _chairmanLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 50)];
            NSString *contentString = [[_forumInfo objectForKey:@"chairman"] objectForKey:@"content"];
            _chairmanLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _chairmanLabel.numberOfLines = 0;
            _chairmanLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _chairmanLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _chairmanLabel.attributedText = attributedString ;
            _chairmanLabel.adjustsFontSizeToFitWidth = NO;
            _chairmanLabel.lineBreakMode = NSLineBreakByTruncatingTail;
            [_chairmanLabel sizeToFit];
            _chairmanLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            
            CGSize size = [_chairmanLabel sizeThatFits:CGSizeMake(_chairmanLabel.frame.size.width, MAXFLOAT)];
            self.chairmanHeight = size.height+30;
            _chairmanLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, self.chairmanHeight);
            [cell addSubview:_chairmanLabel];
        }
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    [self listSubviewsOfView:cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 6) {
        if (self.outlineTouch == 0) {
            self.outlineTouch = 1;
        }
        else{
            self.outlineTouch = 0;
        }
        [self.outlineLabel removeFromSuperview];
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:6 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    if (indexPath.row == 8) {
        if (self.moderatorTouch == 0) {
            self.moderatorTouch = 1;
        }
        else{
            self.moderatorTouch = 0;
        }
        [self.moderatorLabel removeFromSuperview];
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:8 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    else if (indexPath.row == 10) {
        if (self.chairmanTouch == 0) {
            self.chairmanTouch = 1;
        }
        else{
            self.chairmanTouch = 0;
        }
        [self.chairmanLabel removeFromSuperview];
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:10 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        //tag
        return 45;
    }
    else if (indexPath.row == 1){
        NSString *contentString = [_forumInfo objectForKey:@"forumName"];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.5+20;
    }
    else if (indexPath.row == 2){
        //Time&Position
        return 90;
    }
    else if (indexPath.row == 3){
        //Calendar Map Favorite
        return 105;
    }
    else if(indexPath.row == 4){
        //Theme
        return self.themeHeight;
    }
    else if (indexPath.row == 5 ){
        // Forum Outline Title
        return 60;
    }
    else if (indexPath.row == 6){
        // Outline Content
        if (self.outlineTouch == 0) {
            return 150;
        }
        else{
            NSString *contentString = [_forumInfo objectForKey:@"Summary"];
            UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.8+20;
        }
    }
    else if(indexPath.row == 7){
        // Forum Moderator Title
        return 60;
    }
    else if(indexPath.row == 8){
        // Moderator Content
        if (self.moderatorTouch == 0) {
            return 150;
        }
        else{
            NSString *contentString = [[_forumInfo objectForKey:@"host"] objectForKey:@"content"];
            UIFont *textFont =  [UIFont fontWithName:@"GothamBook" size:14];;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.8+20;
        }
    }
    else if(indexPath.row == 9){
        // Forum Chairman Title
        return 60;
    }
    else if(indexPath.row == 10){
        // Chairman Content
        if (self.chairmanTouch == 0) {
            return 150;
        }
        else{
            NSString *contentString = [[_forumInfo objectForKey:@"chairman"] objectForKey:@"content"];
            UIFont *textFont =  [UIFont fontWithName:@"GothamBook" size:14]; ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.8+20;
        }
    }
    else
        return 0;
}

-(void) calendarBtnAction:(id)sender {
    NSLog(@"calendarBtnAction");
    if ([self checkLogin]) {
        isNeedToRefresh = true;
        
        [_delegate showCalender:_forumInfo];
    }
}

-(void) mapitBtnAction:(id)sender {
    NSLog(@"mapitBtnAction:::%@",[_forumInfo objectForKey:@"Location"]);
    
    if ([[_forumInfo objectForKey:@"Location"] isEqualToString:@"Grande Luxe Banquet Grand Ballroom, Taipei, Taiwan"] || [[_forumInfo objectForKey:@"Location"] isEqualToString:@"中信企業總部三樓雅悅會館馥儷廳"] ) {
        NSString* stringURL = @"comgooglemapsurl://?q=25.058751,121.616312&zoom=10&views=traffic";
        if ([[UIApplication sharedApplication] canOpenURL:
             [NSURL URLWithString:@"comgooglemaps://"]]) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
        }
        else {
            NSURL *url = [NSURL URLWithString:@"http://maps.google.com/maps?q=25.058751,121.616312&zoom=10&views=traffic"];
            [[UIApplication sharedApplication] openURL: url];
        }
    }
    else{
        [_delegate showMap];
    }
}

-(void) favoriteBtnAction:(id)sender {
    NSLog(@"favoriteBtnAction");
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        NSMutableArray *forumFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ForumFarvorites"]];
        
        if ([sender isSelected]) {
            [forumFarvorites addObject:@{
                                         @"node": [[_forumInfo objectForKey:@"node"] description],
                                         @"title": [_forumInfo objectForKey:@"title"],
                                         @"Location": [_forumInfo objectForKey:@"Location"]
                                         }];
        }
        else {
            for (int i = 0; i < forumFarvorites.count; i++) {
                if ([[[[forumFarvorites objectAtIndex:i] objectForKey:@"node"] description] isEqualToString:[[_forumInfo objectForKey:@"node"] description]]) {
                    [forumFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:forumFarvorites forKey:@"ForumFarvorites"];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)handleRegisterTap:(UITapGestureRecognizer *)recognizer {
    [_delegate showRegister];
}

- (IBAction)priceBtn:(id)sender {
    self.coverView.hidden = NO;
}

-(void) backAction:(id)sender {
    self.coverView.hidden = YES;
}

- (Boolean)checkForumFarvorites {
    NSArray *forumFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ForumFarvorites"];
    
    for (NSDictionary *forumFarvorite in forumFarvorites) {
        if ([[[forumFarvorite objectForKey:@"node"] description] isEqualToString:[[_forumInfo objectForKey:@"node"] description]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkCalendar {
    NSArray *reminders = [[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"];
    
    for (NSDictionary *reminder in reminders) {
        if ([[reminder objectForKey:@"typeID"] isEqualToString:[[_forumInfo objectForKey:@"node"] description]] && [[reminder objectForKey:@"type"] isEqualToString:@"Program"]) {
            
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [[[[[UIApplication sharedApplication] windows] firstObject] rootViewController] presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
