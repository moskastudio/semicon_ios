//
//  FavoritesProgramTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoritesProgramTableViewControllerDelegate <NSObject>

- (void)showProgram:(NSString *)node title:(NSString *)title;

@end

@interface FavoritesProgramTableViewController : UITableViewController
@property id<FavoritesProgramTableViewControllerDelegate> delegate;
@end
