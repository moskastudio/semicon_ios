//
//  ThemePavilionTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ThemePavilionTableViewDelegate <NSObject>

- (void) showThemePavilionDetail:(NSInteger)index;

@end


@interface ThemePavilionTableViewController : UITableViewController

@property id <ThemePavilionTableViewDelegate> delegate;

@end
