//
//  ExhibitorListViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/23.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ExhibitorCompanyViewController.h"
#import "ExhibitorProductViewController.h"
#import "ExhibitorBoothNoViewController.h"

@interface ExhibitorListViewController : UIViewController<UINavigationControllerDelegate,UINavigationBarDelegate>

- (IBAction)BackBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
- (IBAction)searchBtn:(id)sender;

@property(nonatomic, strong) ExhibitorCompanyViewController *controllerCompany;
@property(nonatomic, strong) ExhibitorProductViewController *controllerProduct;
@property(nonatomic, strong) ExhibitorBoothNoViewController *controllerBoothNo;

@end
