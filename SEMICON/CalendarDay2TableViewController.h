//
//  CalendarDay2TableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CalendarDay2TableViewControllerDelegate <NSObject>

- (void)showCalendar:(NSDictionary *)reminder;

@end
@interface CalendarDay2TableViewController : UITableViewController

@property id<CalendarDay2TableViewControllerDelegate> delegate;
- (void)refreshCell;
@end