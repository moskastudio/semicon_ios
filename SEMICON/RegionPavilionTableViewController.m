//
//  RegionPavilionTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "RegionPavilionTableViewController.h"

@interface RegionPavilionTableViewController ()

@property NSArray *RegionPavilionsArray;

@end

@implementation RegionPavilionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _RegionPavilionsArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Cross-Strait Pavilion", "") ,NSLocalizedString(@"German Pavilion", "") ,NSLocalizedString(@"Holland High Tech Pavilion", "") ,NSLocalizedString(@"Korea Pavilion", "") ,NSLocalizedString( @"Kyushu (Japan) Pavilion", ""),NSLocalizedString(@"Okinawa (Japan) Pavilion", "") ,NSLocalizedString(@"Philippines Pavilion", "") ,NSLocalizedString(@"Singapore Pavilion", "") , nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _RegionPavilionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RegionPavilionCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *PavilionsLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 34)];
    [PavilionsLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16.5]];
    PavilionsLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    PavilionsLabel.text = [_RegionPavilionsArray objectAtIndex:indexPath.row];
    PavilionsLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:PavilionsLabel];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 92.5, self.view.frame.size.width, 1.5)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 94;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
    
    if ([_delegate respondsToSelector:@selector(showRegionPavilionDetail:)]) {
        [_delegate showRegionPavilionDetail:indexPath.row];
    }
}


@end
