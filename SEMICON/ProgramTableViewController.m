//
//  ProgramTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/2.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProgramTableViewController.h"
#import "ProgramDetailsViewController.h"
#import "Service.h"
#import "SignInViewController.h"
#import "AppDelegate.h"

@interface ProgramTableViewController ()

@property NSArray *programArray;
@property NSArray *programTimeArray;
@property NSArray *programPositionArray;
@property NSInteger viewBtnIndex;
@property NSInteger viewBtnSection;
@property NSIndexPath *selectedIndexPath;
@end

@implementation ProgramTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    [self fetchAllForum];
}

- (void)viewWillAppear:(BOOL)animated {
    [self reloadIndexPath];
}

- (void)reloadIndexPath {
    if (_selectedIndexPath != nil) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchAllForum {
    [[Service sharedInstance] fetchAllForumWithComplete:^(BOOL success, NSArray *responseAry) {
        if (success) {
            NSMutableArray *day1 = [[NSMutableArray alloc] init];
            NSMutableArray *day2 = [[NSMutableArray alloc] init];
            NSMutableArray *day3 = [[NSMutableArray alloc] init];
            
            for (NSDictionary *forum in responseAry) {
                if ([[forum objectForKey:@"date"] hasPrefix:@"2016年9月7日"] || [[forum objectForKey:@"date"] hasPrefix:@"Wednesday"]) {
                    [day1 addObject:forum];
                }
                else if ([[forum objectForKey:@"date"] hasPrefix:@"2016年9月8日"] || [[forum objectForKey:@"date"] hasPrefix:@"Thursday"]) {
                    [day2 addObject:forum];
                }
                else if ([[forum objectForKey:@"date"] hasPrefix:@"2016年9月9日"] || [[forum objectForKey:@"date"] hasPrefix:@"Friday"]) {
                    [day3 addObject:forum];
                }
            }
            _programArray = @[day1, day2, day3];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_programArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[_programArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProgramCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, cell.frame.size.width-30, 146)];
    UIImage *image = [UIImage imageNamed:@"img_content_card_bg.png"];
    backgroundImage.image = image;
    [cell addSubview:backgroundImage];
    
    UIImageView *activities = [[UIImageView alloc] initWithFrame:CGRectMake(28, 10, 50, 50)];
    UIImage *activitieImage = [UIImage imageNamed:@"img_speaker.png"];
    activities.image = activitieImage;
    [cell addSubview:activities];
    
    UILabel *activitiesLabel=[[UILabel alloc]initWithFrame:CGRectMake(78,10,self.view.frame.size.width-156,58)];
    [activitiesLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    activitiesLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    NSString *labelText = [[[self.programArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"forumName"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
    activitiesLabel.attributedText = attributedString;
    activitiesLabel.textAlignment = NSTextAlignmentLeft;
    [activitiesLabel setNumberOfLines:0];
    activitiesLabel.lineBreakMode =NSLineBreakByWordWrapping;
    //if overflow --> ...
    activitiesLabel.adjustsFontSizeToFitWidth = NO;
    activitiesLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:activitiesLabel];
    
    UIButton *starBtn = [[UIButton alloc] initWithFrame:CGRectMake(cell.frame.size.width-68, 10, 40, 40)];
    [starBtn setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"] forState:UIControlStateNormal];
    UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_star_press.png"];
    [starBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
    [starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:starBtn];
    
    if ([self checkForumFarvorites:indexPath]) {
        starBtn.selected = true;
    }

    
    UIView *line = [[UIView alloc] initWithFrame: CGRectMake(28, 68, cell.frame.size.width-56, 1)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    UILabel *positionLabel=[[UILabel alloc]initWithFrame:CGRectMake(78,80,self.view.frame.size.width-165,60)];
    [positionLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    positionLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    NSString *time = [[[self.programArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"time"];
    NSString *position = [[[self.programArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"Location"];
    NSString *TimePositionLabelText = [NSString stringWithFormat:@"%@\r%@", time, position];
    attributedString = [[NSMutableAttributedString alloc] initWithString:TimePositionLabelText];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [TimePositionLabelText length])];
    positionLabel.attributedText = attributedString;
    positionLabel.textAlignment = NSTextAlignmentLeft;
    [positionLabel setNumberOfLines:0];
    positionLabel.lineBreakMode =NSLineBreakByWordWrapping;
    //if overflow --> ...
    positionLabel.adjustsFontSizeToFitWidth = NO;
    positionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:positionLabel];
    
    UIButton *viewBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    viewBtn.frame = CGRectMake(cell.frame.size.width-87, 90, 50, 28);
    [viewBtn setTitle:NSLocalizedString(@"View", "") forState:UIControlStateNormal];
    viewBtn.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    viewBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBook" size:13];
    viewBtn.layer.borderColor = [[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0] CGColor];
    viewBtn.layer.borderWidth = 0.5;
    viewBtn.layer.cornerRadius = 5.0;
    viewBtn.clipsToBounds = YES;
    viewBtn.tag = indexPath.row;
    [viewBtn addTarget:self action:@selector(viewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:viewBtn];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, self.view.frame.size.width, 55)];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:
                        CGRectMake(0, 0, self.view.frame.size.width, 55)];
    img.image = [UIImage imageNamed:@"img_section_white_bg.png"];
    img.contentMode = UIViewContentModeScaleAspectFill;
    img.clipsToBounds = YES;
    [sectionHeaderView addSubview:img];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 13, sectionHeaderView.frame.size.width, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = NSLocalizedString(@"Wednesday, Sep. 7, 2016", "") ;
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = NSLocalizedString(@"Thursday, Sep. 8, 2016", "") ;
            return sectionHeaderView;
            break;
        case 2:
            headerLabel.text = NSLocalizedString(@"Friday, Sep. 9, 2016", "") ;
            return sectionHeaderView;
            break;
        default:
            break;
    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 146;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showProgramDetails"]) {
        ProgramDetailsViewController *programDetailsViewController = [segue destinationViewController];
        programDetailsViewController.navBarTitle = [[[self.programArray objectAtIndex:_viewBtnSection] objectAtIndex:_viewBtnIndex] objectForKey:@"title"];
        programDetailsViewController.node = [[[self.programArray objectAtIndex:_viewBtnSection] objectAtIndex:_viewBtnIndex] objectForKey:@"node"];
    }
}

-(void) starBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
        _selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        NSDictionary *program = [[self.programArray objectAtIndex:_selectedIndexPath.section] objectAtIndex:_selectedIndexPath.row];
        
        NSString *node = [[program objectForKey:@"node"] description];
        
        NSMutableArray *forumFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ForumFarvorites"]];
        if ([sender isSelected]) {
            [forumFarvorites addObject:@{
                                         @"node": [[program objectForKey:@"node"] description],
                                         @"title": [program objectForKey:@"title"],
                                         @"Location": [program objectForKey:@"Location"]
                                         }];
        }
        else {
            for (int i = 0; i < forumFarvorites.count; i++) {
                if ([[[[forumFarvorites objectAtIndex:i] objectForKey:@"node"] description] isEqualToString:node]) {
                    [forumFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:forumFarvorites forKey:@"ForumFarvorites"];
    }
}

-(void) viewBtnAction:(id)sender {
    NSLog(@"viewBtnAction");
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    _selectedIndexPath = indexPath;
//    NSLog(@"indexPath.row:%ld,indexPath.section%ld",indexPath.row,indexPath.section);
    _viewBtnIndex = indexPath.row;
    _viewBtnSection = indexPath.section;
    [self performSegueWithIdentifier:@"showProgramDetails" sender:self];
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (Boolean)checkForumFarvorites:(NSIndexPath *)indexPath {
    NSArray *forumFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ForumFarvorites"];
    
    NSDictionary *program = [[self.programArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    for (NSDictionary *forumFarvorite in forumFarvorites) {
        if ([[[forumFarvorite objectForKey:@"node"] description] isEqualToString:[[program objectForKey:@"node"] description]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "")  style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
