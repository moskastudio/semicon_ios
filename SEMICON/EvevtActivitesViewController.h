//
//  EvevtActivitesViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TechXPOTTableViewController.h"
#import "EventsTableViewController.h"
#import "VisitorActiviteTableViewController.h"

@interface EvevtActivitesViewController : UIViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property(nonatomic, strong) TechXPOTTableViewController *controllerTechXPOT;
@property(nonatomic, strong) EventsTableViewController *controllerEvent;
@property(nonatomic, strong) VisitorActiviteTableViewController *controllerVisitorActivite;

@end
