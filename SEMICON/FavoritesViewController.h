//
//  FavoritesViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FavoritesExhibitorTableViewController.h"
#import "FavoritesProgramTableViewController.h"
#import "FavoritesEventsTableViewController.h"

@interface FavoritesViewController : UIViewController
@property(nonatomic, strong) FavoritesExhibitorTableViewController *controllerFavoritesExhibitor;
@property(nonatomic, strong) FavoritesProgramTableViewController *controllerFavoritesProgram;
@property(nonatomic, strong) FavoritesEventsTableViewController *controllerFavoritesEvents;

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@end
