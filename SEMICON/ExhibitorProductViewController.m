//
//  ExhibitorProductViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorProductViewController.h"
#import "Service.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ExhibitorProductViewController (){
    NSMutableDictionary *companyName;
    NSMutableArray *companyNameSectionTitles;
    NSMutableArray *companyNameIndexTitles;
    NSArray *adArray;
}

@property NSString *selectedItem;

@end

@implementation ExhibitorProductViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //-------ADSlider Satrt
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            adArray = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"TopBannerLg"]];
            
            NSMutableArray *imageUrls = [[NSMutableArray alloc] init];
            
            for (NSDictionary *ad in adArray) {
                if (![[ad objectForKey:@"imgUrl"] isEqualToString:@""]) {
                    [imageUrls addObject:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
                }
            }
            
            MCLScrollViewSlider *localScrollView = [MCLScrollViewSlider scrollViewSliderWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 432*self.view.frame.size.width/1080) imageURLs:imageUrls placeholderImage:[UIImage imageNamed:@"img_action_bar_bg.png"]];
            localScrollView.pageControlCurrentIndicatorTintColor = [UIColor whiteColor];
            [self.ADView addSubview:localScrollView];
            
            [localScrollView didSelectItemWithBlock:^(NSInteger clickedIndex) {
                NSLog(@"Clicked inner_ad_%ld", clickedIndex+1);
                [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                              parameters:@{ FBSDKAppEventParameterNameContentID:[NSString stringWithFormat:@"inner_ad_%ld", clickedIndex+1],
                                            FBSDKAppEventParameterNameDescription:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"] } ];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"]]];
            }];
        }
    }];
    //-------ADSlider End
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchAllStructuredCategorySuccessNotification:) name:FetchAllStructuredCategorySuccessNotification object:nil];
    
    [[Service sharedInstance] fetchAllStructuredCategory];
    
    /*
    companyName = @{
                    @"1" : @[@"600", @"666 Swan", @"6 Buffalo"],
                    @"2" : @[@"Bear", @"Black Swan", @"Buffalo"],
                    @"3" : @[@"Camel", @"Cockatoo"],
                    @"4" : @[@"Dog", @"Donkey"],
                    @"5" : @[@"Emu"],
                    @"6" : @[@"Giraffe", @"Greater Rhea"],
                    @"7" : @[@"HippopotamusHippopotamusHippopotamusHippopotamusHippopotamusHippopotamus", @"Horse"],
                    @"8" : @[@"Koala"],
                    @"9" : @[@"Lion", @"Llama"],
                    @"0" : @[@"Manatus", @"Meerkat"]
                    };
    companyNameSectionTitles = [[companyName allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    companyNameIndexTitles = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"0"];
     */
    companyName = [[NSMutableDictionary alloc] init];
    companyNameSectionTitles = [[NSMutableArray alloc] init];
    companyNameIndexTitles = [[NSMutableArray alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)onFetchAllStructuredCategorySuccessNotification:(NSNotification*) notify {
    //NSLog(@"onFetchAllStructuredCategorySuccessNotification:%@", notify.object);
    
    for (NSDictionary *category in notify.object) {
        [companyNameIndexTitles addObject:[category objectForKey:@"Text"]];
        NSArray *subCategoryArray = [category objectForKey:@"SubCategory"];
        NSMutableArray *subCategoryTitleArray = [[NSMutableArray alloc] init];
        for (NSDictionary *subCategory in subCategoryArray) {
            [subCategoryTitleArray addObject:subCategory];
        }
        [companyName setObject:subCategoryTitleArray forKey:[category objectForKey:@"Text"]];
    }
    
    companyNameSectionTitles = [[companyName allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [companyNameSectionTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSString *sectionTitle = [companyNameSectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [companyName objectForKey:sectionTitle];
    return [sectionAnimals count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [companyNameSectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    // Configure the cell...
    //    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    //    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    //    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    
    UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 12, self.view.frame.size.width-114, 20)];
    companyLabel.text = [[[companyName objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"Text"];
    [companyLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    companyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    companyLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:companyLabel];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}
/*
- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    return companyNameIndexTitles;
}
*/

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(36, 0, tableView.frame.size.width-72, 44)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(36, 10, sectionHeaderView.frame.size.width, 24)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    headerLabel.text = [companyNameIndexTitles objectAtIndex:section];
    [sectionHeaderView addSubview:headerLabel];
    
    return sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [companyNameSectionTitles indexOfObject:title];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
    self.selectedItem = [[[companyName objectForKey:[companyNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row] objectForKey:@"categoryId"];
    NSLog(@"self.selectedItem:%@",self.selectedItem);
    if ([_delegate respondsToSelector:@selector(showExhibitorProductList:)]) {
        [_delegate showExhibitorProductList:self.selectedItem];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
