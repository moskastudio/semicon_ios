//
//  ProgramAgendaViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/3.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProgramAgendaViewControllerDelegate <NSObject>

- (void)showWebView:(NSString *)urlString navTitle:(NSString *)navTitle;
- (void) showSpearkerDetail:(NSString*)link;

@end

@interface ProgramAgendaViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property id<ProgramAgendaViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *priceBtn;
- (IBAction)priceBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *registerView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property NSDictionary *forumInfo;
@property (strong, nonatomic) IBOutlet UIImageView *registerImageView;
@property (strong, nonatomic) IBOutlet UIImageView *registerImageViewZH;
@end
