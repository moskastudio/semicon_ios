//
//  ScheduleViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScheduleViewControllerDelegate <NSObject>

- (void)presentWebView:(NSString *)urlString title:(NSString *)navTitle;

@end

@interface ScheduleViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property id<ScheduleViewControllerDelegate> delegate;

@end
