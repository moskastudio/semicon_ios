//
//  CalendarViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarDay1TableViewController.h"
#import "CalendarDay2TableViewController.h"
#import "CalendarDay3TableViewController.h"

@interface CalendarViewController : UIViewController

@property(nonatomic, strong) CalendarDay1TableViewController *controllerDay1;
@property(nonatomic, strong) CalendarDay2TableViewController *controllerDay2;
@property(nonatomic, strong) CalendarDay3TableViewController *controllerDay3;

- (IBAction)indexBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@end
