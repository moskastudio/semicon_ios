//
//  FAQTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/24.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FAQTableViewController.h"
#import "ASDetailsViewController.h"

@interface FAQTableViewController ()

@property NSArray *questionArray;

@end

@implementation FAQTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"FAQ", "") ;
    
    self.questionArray = [[NSArray alloc] initWithObjects:
                          NSLocalizedString(@"Why do I register?", "") ,
                          NSLocalizedString(@"Is there other option for registration besides using email address?","") ,
                          NSLocalizedString(@"How to retrieve my password?","") ,
                          NSLocalizedString(@"How to disable reminder?","") ,
                          NSLocalizedString(@"How to locate specific information in App quickly?","") ,
                          NSLocalizedString(@"How to add an exhibitor as favorite?","") ,
                          NSLocalizedString(@"How to add an event into calendar?","") ,
                          NSLocalizedString(@"How to share my events?","") ,
                          NSLocalizedString(@"How to delete items from my lists?","") , nil];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.questionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FAQCell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor whiteColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,0,cell.frame.size.width-72,25)];
    NSString *contentString = [self.questionArray objectAtIndex:indexPath.row];
    titleLabel.numberOfLines = 0;
    titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    titleLabel.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
    titleLabel.attributedText = attributedString ;
    titleLabel.textColor = [UIColor blackColor];
    UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:16] ;
    titleLabel.font = textFont;
    [titleLabel setNumberOfLines:0];
    CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
    CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    titleLabel.frame = CGRectMake(36, 20, actualSize.width,actualSize.height*1.5+10);
    [cell addSubview:titleLabel];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *contentString = [self.questionArray objectAtIndex:indexPath.row];
    UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:16] ;
    CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
    CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return actualSize.height*1.5+40;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ASDetailsViewController *ASDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ASDetailsViewController"];
    ASDetailsViewController.source = @"FAQ";
    ASDetailsViewController.indexPath = indexPath.row;
    [self.navigationController pushViewController:ASDetailsViewController animated:YES];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
