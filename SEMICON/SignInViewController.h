//
//  SignInViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/11.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;

- (IBAction)cancelSignBtn:(id)sender;
- (IBAction)signIn:(id)sender;
- (IBAction)facebookLogin:(id)sender;

@end
