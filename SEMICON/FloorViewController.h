//
//  FloorViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FloorViewControllerDelegate <NSObject>

- (void)showMapWithTitle:(NSString *)urlString title:(NSString *)navTitle;

@end

@interface FloorViewController : UIViewController <UIGestureRecognizerDelegate>

@property id <FloorViewControllerDelegate> delegate;

@end
