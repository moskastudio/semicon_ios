//
//  ScheduleViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ScheduleViewController.h"
#import "Service.h"

@interface ScheduleViewController ()

@property NSMutableArray *scheduleArray;
@property NSMutableArray *scheduleTimeArray;

@end

@implementation ScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
//    NSArray *preDayArray = [[NSArray alloc] initWithObjects:@"Press Conference", nil];
//    NSArray *firstDayArray = [[NSArray alloc] initWithObjects:@"Sustainable Manufacturing Forum",@"Advanced Packaging Technology Symposium",@"MEMS Forum",@"Opening Ceremony",@"TechXPOT",@"Executive Summit",@"Semiconductor Materials Forum",@"Gala Dinner", nil];
//    NSArray *secondDayArray = [[NSArray alloc] initWithObjects:@"Laser Seminar",@"High-Tech Facility International Forum",@"eMDC Forum",@"Market Trends Forum",@"SiP Global Summit 2015 - 3D IC Technology Forum",@"International Semiconductor Advanced Manufacturing Process and  Equipment Technology Forum & Business Matchmaking",@"TechXPOT",@"Smart Manufacturing",@"CFO and Investor Executive Summit", nil];
//    NSArray *lastDayArray = [[NSArray alloc] initWithObjects:@"CMP Forum",@"SiP Global Summit 2015 - Embedded and Wafer Level Package Technology Forum",@"IC Forum",@"Memory Technology Forum",@"Cross-Strait Integrated Circut Cooperation Seminar",@"TechXPOT",@"UMC Key Components Localization Seminar", nil];
//    self.scheduleArray = [[NSMutableArray alloc] initWithObjects:preDayArray, firstDayArray,secondDayArray,lastDayArray, nil];
//    
//    NSArray *preDayTimeArray = [[NSArray alloc] initWithObjects:@"14:00 - 15:30", nil];
//    NSArray *firstDayTimeArray = [[NSArray alloc] initWithObjects:@"08:30 - 17:00",@"08:30 - 17:00",@"08:30 - 17:00",@"09:00 - 10:00",@"10:00 - 17:00",@"13:00 - 17:00",@"13:00 - 17:00",@"18:00 – 21:00", nil];
//    NSArray *secondDayTimeArray = [[NSArray alloc] initWithObjects:@"08:30 - 12:00",@"08:30 - 17:00",@"08:30 - 17:00",@"08:30 - 12:00",@"08:30 - 17:00",@"09:00 - 17:00",@"10:00 - 17:00",@"13:00 - 17:00",@"13:00 - 15:00", nil];
//    NSArray *lastDayTimeArray = [[NSArray alloc] initWithObjects:@"08:15 - 12:00",@"08:30 – 17:00",@"08:30 – 17:00",@"08:30 – 12:00",@"08:30 – 12:00",@"10:00 – 16:00",@"10:30 - 12:00", nil];
//    self.scheduleTimeArray = [[NSMutableArray alloc] initWithObjects:preDayTimeArray, firstDayTimeArray,secondDayTimeArray,lastDayTimeArray, nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self fetchSchedule];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchSchedule {
    
    NSMutableArray *preDayTimeArray = [[NSMutableArray alloc] init];
    NSMutableArray *firstDayTimeArray = [[NSMutableArray alloc] init];
    NSMutableArray *secondDayTimeArray = [[NSMutableArray alloc] init];
    NSMutableArray *lastDayTimeArray = [[NSMutableArray alloc] init];
    
    [[Service sharedInstance] fetchScheduleWithComplete:^(BOOL success, NSArray *responseAry) {
        if (success) {
            for (int i = 0; i < responseAry.count; i++) {
                for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                    
                    switch (i) {
                        case 0:
                            [preDayTimeArray addObject:event];
                            break;
                        case 1:
                            [firstDayTimeArray addObject:event];
                            break;
                        case 2:
                            [secondDayTimeArray addObject:event];
                            break;
                        case 3:
                            [lastDayTimeArray addObject:event];
                            break;
                        default:
                            break;
                    }
                }
            }
            NSLog(@"%@", preDayTimeArray);
            self.scheduleArray = [[NSMutableArray alloc] initWithObjects:preDayTimeArray, firstDayTimeArray,secondDayTimeArray,lastDayTimeArray, nil];
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.scheduleArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.scheduleArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScheduleCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *scheduleLabel=[[UILabel alloc] initWithFrame:CGRectMake(115, 14, self.view.frame.size.width-152, 60)];
    [scheduleLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
    scheduleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    scheduleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    scheduleLabel.numberOfLines = 0;
    scheduleLabel.textAlignment = NSTextAlignmentLeft;
    NSString *labelString = [[[self.scheduleArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"activity"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelString length])];
    scheduleLabel.attributedText = attributedString ;
    scheduleLabel.adjustsFontSizeToFitWidth = NO;
    scheduleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:scheduleLabel];
    
    NSString *timeString = [[[self.scheduleArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"time"];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\\U00a0" withString:@""];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"\\U2013" withString:@"-"];
    timeString = [timeString stringByReplacingOccurrencesOfString:@"–" withString:@"-"];
    NSArray* dateArray = [timeString componentsSeparatedByString:@"-"];
    
    NSString *startString = [dateArray objectAtIndex:0];
    NSString *endString = [dateArray objectAtIndex:1];
    
    UILabel *startTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(36, 13, 43, 20)];
    [startTimeLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    startTimeLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    startTimeLabel.text = startString;
    startTimeLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:startTimeLabel];
    
    UILabel *lineLabel=[[UILabel alloc] initWithFrame:CGRectMake(36, 33, 43, 14)];
    [lineLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    lineLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    lineLabel.text = @"|";
    lineLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:lineLabel];
    
    UILabel *endTimeLabel=[[UILabel alloc] initWithFrame:CGRectMake(36, 47, 43, 20)];
    [endTimeLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    endTimeLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    endTimeLabel.text = endString;
    endTimeLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:endTimeLabel];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",(long)indexPath.section,indexPath.row);
    
    NSString *link = [[[self.scheduleArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"link"];
    
    if (![link isEqualToString:@""]) {
        [_delegate presentWebView:link title:[[[self.scheduleArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"activity"]];
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, self.view.frame.size.width, 55)];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:
                        CGRectMake(0, 0, self.view.frame.size.width, 55)];
    img.image = [UIImage imageNamed:@"img_section_title_bg.png"];
    img.contentMode = UIViewContentModeScaleAspectFill;
    img.clipsToBounds = YES;
    [sectionHeaderView addSubview:img];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 13, sectionHeaderView.frame.size.width, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor whiteColor];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = NSLocalizedString(@"Tuesday, Sep. 6, 2016", "") ;
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = NSLocalizedString(@"Wednesday, Sep. 7, 2016", "");
            return sectionHeaderView;
            break;
        case 2:
            headerLabel.text = NSLocalizedString(@"Thursday, Sep. 8, 2016", "");
            return sectionHeaderView;
            break;
        case 3:
            headerLabel.text =  NSLocalizedString(@"Friday, Sep. 9, 2016", "");
            return sectionHeaderView;
            break;
        default:
            break;
    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 88;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
