//
//  ProgramMapImageViewController.m
//  semicon
//
//  Created by MuRay Lin on 2016/7/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProgramMapImageViewController.h"

@interface ProgramMapImageViewController ()<UIScrollViewDelegate>
@property UIImageView *imageView;
@end

@implementation ProgramMapImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _navTitle;
    
    UIImage* image = [UIImage imageNamed:@"floorplan2016.png"];
    float rate = image.size.width / self.view.frame.size.width;
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, image.size.height / rate)];
    self.imageView.image = image;
//    self.imageView.center = CGPointMake(self.view.center.x, self.view.center.y - 64) ;
//    self.imageView.contentMode = UIViewContentModeScaleToFill;
//    self.imageView.frame = self.scrollView.frame;
    [self.scrollView addSubview:_imageView];
    
    self.scrollView.contentSize = _imageView.frame.size;
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale = 1.0;
    self.scrollView.maximumZoomScale = 100.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
