//
//  EventsTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EventsTableViewDelegate <NSObject>

- (void) showEventDetail:(NSInteger)index;

@end

@interface EventsTableViewController : UITableViewController

@property id <EventsTableViewDelegate> delegate;

@end
