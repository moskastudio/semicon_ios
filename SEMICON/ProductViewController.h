//
//  ProductViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLScrollViewSlider.h"

@interface ProductViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UIView *ADView;
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
