//
//  ExhibitionNewsTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitionNewsTableViewController.h"
#import "ExhibitionNewsDetailViewController.h"
#import "Service.h"

@interface ExhibitionNewsTableViewController ()

@property NSMutableArray *PRNewsArray;
@property NSArray *exhibitionNewsDateArray;
@property NSMutableArray *exhibitorPRsArray;
@property NSArray *exhibitorPRsDateArray;
@property NSArray *mediaPartnersArray;
@property NSArray *mediaPartnersImageArray;
@property NSArray *mediaPartnersWebsiteArray;

@property NSDictionary *exhibitorPRDict;
@property NSDictionary *pressReleaseDict;

@property NSString *NewsString;
@property NSString *yearString;
@property NSString *monthString;
@property NSString *dateString;
@property NSString *URLString;

@end

@implementation ExhibitionNewsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _PRNewsArray = [[NSMutableArray alloc] init];
    _exhibitionNewsDateArray = [[NSArray alloc] init];
    
    _exhibitorPRsArray = [[NSMutableArray alloc] init];
    _exhibitorPRsDateArray = [[NSArray alloc] init];
    
    _mediaPartnersArray = [[NSArray alloc] initWithObjects:
                           @"3D InCites is an online media platform for 3D IC integration and 3D packaging technologies. It is the primary resource for key decision makers seeking information about technology development, design, standards, and infrastructure in order to realize commercial production of 2.5D and 3D technologies. 3D InCites is powered by Impress Labs, a global creative and marketing communications agency specializing in building brands for technology companies in the semiconductor, solar energy and life science industries",
                           NSLocalizedString(@"AEI Published monthly, focusing on Asian approaches to components, devices, modules and manufacturing, including technologies, new products, and business strategies. AEI features the latest test and measurement tools, as well as inspection machines, and technologies and products related to the SMT industry. AEI’s niche lies on presenting technical stories with detailed discussions on processes that drive new functions of future devices. www.dempa.net", "") ,
                           @"AZoNetwork is a global information provider for scientists, engineers, technologists, researchers and anyone with an active mind who’s interested in the latest in tech. The suite of sites operated by AZoNetwork covers the following sectors; Materials, Nanotechnology, Medical News and Devices, Clean Technologies, Sensors, Robotics, Optics, Mining, Building and Quantum Technologies.",
                           @"Chip Scale Review is the leading international test, assembly, and packaging magazine for WLP, TSVs, 2D/3D device packaging and high-density interconnection of microelectronics, ICs, MEMS, RF/ wireless, optoelectronic and other wafer-fabricated devices for the 21st century. CSR in print and digital publishes in-depth packaging related editorial. CSR delivers exclusive state of the art technical editorial, news, and commentary on critical and topical industry matters not available elsewhere. CSR co-sponsors the visionary 7th annual International Wafer-level Packaging Conference (IWLPC) with SMTA. Sign up or renew your free subscription atwww.ChipScaleReview.com.",
                           @"Compound Semiconductor has been the leading provider of news, analysis, opinion, information and services for the worldwide Compound Semiconductor industry since 1995. With the largest global circulation and history of excellence, Compound Semiconductor remains the most effective and authoritative title for the industry and a must read platform for the compound semiconductor community worldwide. www.compoundsemiconductor.net",
                           NSLocalizedString(@"CTIMES, a Taiwan base media, founded in 1991, have three language version of web site, English version was founded in February 2014. We focus on providing the latest technology news, emerging businesses, industry celebrities, and market analysis, is the best media for understanding Taiwan's electronics industry.\rIn addition to Taiwan's industry, CTIMES English also care about the development of the electronics industry in China and entire Asia, to provide more comprehensive vision to our readers. en.ctimes.com.tw", "") ,
                            NSLocalizedString(@"DIGITIMES, established in 1998, is a unique information source for readers who need to know about the supply side of the semiconductor, electronics, computer and communications industries. Daily coverage of Taiwan's IT companies and news from China and other regions provide a lifeline to industry professionals, channel players, investment analysts and media around the world. www.digitimes.com/index.asp", "") ,
                           @"The global assembly journal for SMT and advanced packaging professionals, Global SMT & Packaging magazine and its websites contain authoritative technical articles on practical issues affecting SMT assembly and packaging. The magazine comes out in five region- targeted editions--Americas, Europe, China, Korea and South East Asia. There is also an international website, an international daily newsletter, and a number of regional websites and monthly regional newsletters. Global SMT TV provides timely, industry-based video content.",
                           @"Industrial Photonics, a Photonics Media Publication\rIndustrial Photonics is a global resource on lasers, sensors, machine vision and automation systems for materials processing, process control and production. Photonics Media – the Pulse of the Industry -- invites you to explore the world of light-based technology at www.photonics.com. As the publisher of Industrial Photonics, Photonics Spectra, BioPhotonics, and Europhotonics magazines, Photonics Buyers’ Guide, Photonics.com, and more, we bring you the news, research and applications articles you need to succeed.",
                           @"於1986年創刊，以台灣資訊電子上下游產業的訊息橋樑自居，提供國際與國內電子產業重點資訊，以利產業界人士掌握自有競爭力。新電子雜誌徹底執行各專欄內容品質，透過讀者回函瞭解讀者意見，調整方向以專業豐富的內容建立特色；定期舉辦研討會、座談會、透過產業廠商的參與度，樹立專業形象；透過網際網路豐富資訊的提供，資訊擴及華人世界。",
                           @"PlantAutomation-Technology is essentially a B2B online business and technology media platform that has under its wraps the largest global database of automation buyers and suppliers. Plantautomation-technology.com covers in-depth trends that shape industry dynamics and metamorphose global economics.\r With services like search engine optimization, social media marketing, product video showcase, e-mail marketing, e-newsletter sponsorship, banner advertising, event marketing and micro-website within our platform, plantautomation-technology.com has created a recognition that spans over a global audience, thereby revolutionizing how businesses transact. plantautomation-technology.com",
                           @"Power Electronics World  focuses on the rapidly changing technology of how we generate, manage, distribute and utilize electrical energy. More than ever, this will include examining new technologies that enable both fossil-fueled and renewable energy. We’re the best resource for looking beyond product announcements for in-depth analysis that demonstrates how individuals, communities and populations are impacted by technology.",
                           @"Semiconductor Packaging News is a daily e-mail newsletter & website reaching over 200,000 readers. SPN covers the world of semiconductor packaging and mirco-electronics, publishing important industry news articles. We package this information into a convenient newsletter that we deliver to our subscribers via e-mail at no cost. We deliver over 1,000 white paper downloads every month and you'll also find commentaries, cartoons, an industry event calendar and much more.",
                           @"Silicon Semiconductor follows the semiconductor manufacturing trends, industry opportunities and announcements to provide the decision making information our reader's need to do a difficult job in a challenging industry. SiS also provides exclusive content on the manufacturing issues affecting the industry wherever in the world it may be. With so many industry challenges on the horizon it is more important than ever to gain a worldwide perspective. Silicon Semiconductor is the catalyst for the global semiconductor community.",
                           @"Solid State Technology is the longest-running and most complete source of information for engineers, operators, managers, tool and materials suppliers and semiconductor researchers. Solid State Technology covers semiconductor manufacturing, advanced packaging, wafer fabrication, integrated circuits, thin-film microelectronics, flat-panel displays, and microstructure technologies, processes and equipment and more.",
                           NSLocalizedString(@"TechNews is a news website established in the second half of 2013, and is currently led by global stem cell scientist, Dr. Chen. Consisting of members of different professional and academic circles, the website provides extensive coverage on topics such as information technology, clean energy, semiconductors, biotech, and medical sciences. In addition to industry-related news, Technews also offers its own original content and analysis. The website is currently available in three different languages. technews.co", "") ,
                           @"Over the past 26 years U.S. Tech has become the major publication for the electronics industry. We report fresh news and emphasize coverage of new products and services that can help technology in the U.S. to continue to thrive. U.S. Tech has expanded its reach to Canada, Mexico, Europe and China. We participate in all major trade conferences to further expose the publication, its advertisers, and the products and services it covers to an ever-expanding readership of decision makers.",
                           nil];
    _mediaPartnersImageArray = [[NSArray alloc] initWithObjects:@"3dIncites-logo-web.jpg",
                                @"logo_aei.png",
                                @"AZONano-logo-web.jpg",
                                @"ChipScaleReview-logo-web.gif",
                                @"compound-semiconductor-logo-web.png",
                                @"logo_ctimes.png",
                                @"logo_digitimes.jpg",
                                @"GlobalSMT-logo-web.jpg",
                                @"IndPhotonics-logo-web.gif",
                                @"logo_micro_e.jpg",
                                @"logo_plantautomation.jpg",
                                @"Power-Electronics-World-Logo-web-large.jpg",
                                @"Semiconductor_Pkg-logo-web.jpg",
                                @"Silicon_Semiconductor-logo-web.jpg",
                                @"SolidStateTechnology-logo-web.gif",
                                @"TechNews-logo_200x72.jpg",
                                @"US-Tech-logo-web.jpg",
                                nil];
    _mediaPartnersWebsiteArray = [[NSArray alloc] initWithObjects:@"http://www.3dincites.com",
                                  @"http://www.dempa.net",
                                  @"www.azonetwork.com",
                                  @"http://www.chipscalereview.com",
                                  @"http://www.compoundsemiconductor.net",
                                  NSLocalizedString(@"en.ctimes.com.tw", "") ,
                                  NSLocalizedString(@"www.digitimes.com/index.asp", "") ,
                                  @"www.globalsmt.net",
                                  @"http://www.photonics.com",
                                  @"www.mem.com.tw",
                                  @"http://www.plantautomation-technology.com",
                                  @"http://www.powerelectronicsworld.net",
                                  @"http://www.semiconductorpackagingnews.com",
                                  @"http://www.siliconsemiconductor.net",
                                  @"www.electroiq.com/index/Semiconductors.html",
                                  NSLocalizedString(@"technews.co", "") ,
                                  @"http://www.us-tech.com",
                                  nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated{
    NSLog(@"%@",_souceButton);
    
    if ([_souceButton isEqualToString:@"ExhibitionNews"]) {
        self.title = NSLocalizedString(@"Press Release", "") ;
        [self PressRelease];
    }
    else if ([_souceButton isEqualToString:@"ExhibitorPressRelease"]) {
        self.title = NSLocalizedString(@"Exhibitor PRs", "") ;
        [self ExhibitorPressRelease];
    }
    else if ([_souceButton isEqualToString:@"MediaPartners"]){
        self.title = NSLocalizedString(@"Media Partners", "") ;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) PressRelease{
    [[Service sharedInstance] fetchPressReleases:_pressReleaseTitle Complete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            _pressReleaseDict = [[NSDictionary alloc] initWithDictionary:responseDict];
            NSLog(@"PR:::%@",[_pressReleaseDict objectForKey:@"releases"]);
            
            _PRNewsArray = [[NSMutableArray alloc] initWithArray:[_pressReleaseDict objectForKey:@"releases"]];
            for (NSDictionary *dict in _PRNewsArray) {
                if ([[dict objectForKey:@"title"] length] == 1) {
                    [_PRNewsArray removeObject:dict];
                }
            }
            [self.tableView reloadData];
        }
    }];
}

-(void) ExhibitorPressRelease{
    [[Service sharedInstance] fetchExhibitorPR:_exhibitorTitle Complete:^(BOOL success, NSDictionary *responseDict) {
        
        if (success) {
            _exhibitorPRDict = [[NSDictionary alloc] initWithDictionary:responseDict];
            NSLog(@"exhibitorPR:::%@",[_exhibitorPRDict objectForKey:@"releases"]);
            
            _exhibitorPRsArray = [[NSMutableArray alloc] initWithArray:[_exhibitorPRDict objectForKey:@"releases"]];
            for (NSDictionary *dict in _exhibitorPRsArray) {
                if ([[dict objectForKey:@"title"] length] == 1) {
                    [_exhibitorPRsArray removeObject:dict];
                }
            }
            [self.tableView reloadData];
            
        }
    }];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([_souceButton isEqualToString:@"ExhibitionNews"]) {
        return  _PRNewsArray.count;
    }
    else if ([_souceButton isEqualToString:@"ExhibitorPressRelease"]){
        return _exhibitorPRsArray.count;
    }
    else 
        return _mediaPartnersArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mediaCenterCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (![_souceButton isEqualToString:@"MediaPartners"]) {
        if ([_souceButton isEqualToString:@"ExhibitionNews"]) {
            
            NSDictionary *PRDict = [[NSDictionary alloc] initWithDictionary:[_PRNewsArray objectAtIndex:indexPath.row]];
            _NewsString = [PRDict objectForKey:@"title"];
            
            NSString* NewsDateString = [PRDict objectForKey:@"time"];
            NSArray* dateArray = [NewsDateString componentsSeparatedByString:@"."];
            
            _yearString = [dateArray objectAtIndex:0];
            if ([[dateArray objectAtIndex:1] isEqualToString:@"6"]) {
                _monthString = @"JUN";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"7"]) {
                _monthString = @"JUL";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"8"]) {
                _monthString = @"AUG";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"9"]) {
                _monthString = @"SEP";
            }
            _dateString = [dateArray objectAtIndex:2];
        }
        else if ([_souceButton isEqualToString:@"ExhibitorPressRelease"]){
            
            NSDictionary *ExhibitorPRDict = [[NSDictionary alloc] initWithDictionary:[_exhibitorPRsArray objectAtIndex:indexPath.row]];
            _NewsString = [ExhibitorPRDict objectForKey:@"title"];
            
            NSString* NewsDateString = [ExhibitorPRDict objectForKey:@"time"];
            NSArray* dateArray = [NewsDateString componentsSeparatedByString:@"."];
            
            _yearString = [dateArray objectAtIndex:0];
            _monthString = [dateArray objectAtIndex:1];
            if ([[dateArray objectAtIndex:1] isEqualToString:@"6"]) {
                _monthString = @"JUN";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"7"]) {
                _monthString = @"JUL";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"8"]) {
                _monthString = @"AUG";
            }
            else if ([[dateArray objectAtIndex:1] isEqualToString:@"9"]) {
                _monthString = @"SEP";
            }
            _dateString = [dateArray objectAtIndex:2];

        }
        
        UILabel *dateLabel=[[UILabel alloc] initWithFrame:CGRectMake(36, 38, 30, 20+5)];
        [dateLabel setFont:[UIFont fontWithName:@"GothamBold" size:22]];
        dateLabel.textColor = [UIColor blackColor];
        dateLabel.text = _dateString;
        dateLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:dateLabel];
        
        UILabel *monthLabel=[[UILabel alloc] initWithFrame:CGRectMake(36, 21, 30, 15+5)];
        [monthLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
        monthLabel.textColor = [UIColor blackColor];
        monthLabel.text = _monthString;
        monthLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:monthLabel];
        
        UILabel *yearLabel=[[UILabel alloc] initWithFrame:CGRectMake(33, 60, 36, 15+5)];
        [yearLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
        yearLabel.textColor = [UIColor blackColor];
        yearLabel.text = _yearString;
        yearLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:yearLabel];
        
        UILabel *newsLabel=[[UILabel alloc] initWithFrame:CGRectMake(102, 18, cell.frame.size.width-158, 60+5)];
        [newsLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        newsLabel.textColor = [UIColor blackColor];
        newsLabel.lineBreakMode = NSLineBreakByWordWrapping;
        newsLabel.numberOfLines = 0;
        newsLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_NewsString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_NewsString length])];
        newsLabel.attributedText = attributedString ;
        newsLabel.adjustsFontSizeToFitWidth = NO;
        newsLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:newsLabel];
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 94.5, cell.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    
    else if ([_souceButton isEqualToString:@"MediaPartners"]){
        UILabel *mediaPartnerLabel=[[UILabel alloc] initWithFrame:CGRectMake(142, 30, cell.frame.size.width-195, 65)];
        [mediaPartnerLabel setFont:[UIFont fontWithName:@"GothamBook" size:12]];
        mediaPartnerLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
        mediaPartnerLabel.lineBreakMode = NSLineBreakByWordWrapping;
        mediaPartnerLabel.numberOfLines = 0;
        mediaPartnerLabel.textAlignment = NSTextAlignmentLeft;
        NSString *mediaPartnerString = [_mediaPartnersArray objectAtIndex:indexPath.row];
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:mediaPartnerString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:6];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [mediaPartnerString length])];
        mediaPartnerLabel.attributedText = attributedString ;
        mediaPartnerLabel.adjustsFontSizeToFitWidth = NO;
        mediaPartnerLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:mediaPartnerLabel];
        
        UIImageView *mediaPartner = [[UIImageView alloc] initWithFrame: CGRectMake(26,32,90,60)];
        UIImage *mediaPartnerImage = [UIImage imageNamed:[_mediaPartnersImageArray objectAtIndex:indexPath.row]];
        mediaPartner.image = mediaPartnerImage;
        mediaPartner.contentMode = UIViewContentModeScaleAspectFit;
        [cell addSubview:mediaPartner];
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 123.5, cell.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_souceButton isEqualToString:@"MediaPartners"]) {
        return 125;
    }
    else if ([_souceButton isEqualToString:@"showPressRoomInfo"]){
        return 136;
    }
    else
        return 96;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
//    NSURL *url = [NSURL URLWithString:@"http://www.google.com/"];
//    [[UIApplication sharedApplication] openURL: url];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showExhibitionNewsDetail"]) {
        if ([_souceButton isEqualToString:@"ExhibitionNews"]) {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            NSDictionary *PRDict = [[NSDictionary alloc] initWithDictionary:[_PRNewsArray objectAtIndex:indexPath.row]];
            ExhibitionNewsDetailViewController *exhibitionNewsDetailViewController = segue.destinationViewController;
            exhibitionNewsDetailViewController.newsTitle =[PRDict objectForKey:@"title"];
            exhibitionNewsDetailViewController.partnerWebsite = [PRDict objectForKey:@"link"];
            exhibitionNewsDetailViewController.navBartitle = @"PressRelease";
        }
        else if([_souceButton isEqualToString:@"ExhibitorPressRelease"]){
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            NSDictionary *ExhibitorPRDict = [[NSDictionary alloc] initWithDictionary:[_exhibitorPRsArray objectAtIndex:indexPath.row]];
            ExhibitionNewsDetailViewController *exhibitionNewsDetailViewController = segue.destinationViewController;
            exhibitionNewsDetailViewController.newsTitle =[ExhibitorPRDict objectForKey:@"title"];
            exhibitionNewsDetailViewController.partnerWebsite = [ExhibitorPRDict objectForKey:@"link"];
            exhibitionNewsDetailViewController.navBartitle = @"ExhibitorPRs";
        }
        else {
            NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
            ExhibitionNewsDetailViewController *exhibitionNewsDetailViewController = segue.destinationViewController;
            exhibitionNewsDetailViewController.newsTitle = [_mediaPartnersArray objectAtIndex:indexPath.row];
            exhibitionNewsDetailViewController.partnerWebsite = [_mediaPartnersWebsiteArray objectAtIndex:indexPath.row];
            exhibitionNewsDetailViewController.navBartitle = @"MediaPartners" ;
        }
    }
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
