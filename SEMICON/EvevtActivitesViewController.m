//
//  EvevtActivitesViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "EvevtActivitesViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#import "TechXPOTTableViewController.h"
#import "EventsTableViewController.h"
#import "VisitorActiviteTableViewController.h"
#import "EvevtActiviteIntroTableViewController.h"
#import "ProgramMapImageViewController.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface EvevtActivitesViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate,TechXPOTTableViewDelegate, EventsTableViewDelegate, VisitorActiviteTableViewDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;

@property NSArray *TechXPOTArray;
@property NSArray *eventsArray;
@property NSInteger selectedIndex;
@property NSString *dataSource;

@end

@implementation EvevtActivitesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    self.view.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _TechXPOTArray = [[NSArray alloc] initWithObjects:@"High Tech Facility", @"Human Resources", @"Laser", @"Materials", @"New Product Launch", @"Smart Manufacturing", nil];
    
    _eventsArray = [[NSArray alloc] initWithObjects:@"Hospitality Suite and Meeting Room",@"Exhibitor Activities",@"Networking Events", nil];
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[ NSLocalizedString(@"TechXPOT", "") , NSLocalizedString(@"Events", "") , NSLocalizedString(@"Visitor Activites", "") ]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
    
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 3;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            TechXPOTTableViewController *techXPOTTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TechXPOTTableViewController"];
            self.controllerTechXPOT = techXPOTTableViewController;
            self.controllerTechXPOT.delegate = self;
            return techXPOTTableViewController.view;
        }
            break;
        case 1:{
            EventsTableViewController *eventsTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"EventsTableViewController"];
            self.controllerEvent = eventsTableViewController;
            self.controllerEvent.delegate = self;
            return eventsTableViewController.view;
        }
            break;
        case 2:{
            VisitorActiviteTableViewController *visitorActiviteTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"VisitorActiviteTableViewController"];
            visitorActiviteTableViewController.navBarTitle = NSLocalizedString(@"Visitor Activities", "") ;
            self.controllerVisitorActivite = visitorActiviteTableViewController;
            self.controllerVisitorActivite.delegate = self;
            return visitorActiviteTableViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

#pragma - TechXPOTTableViewDelegate

-(void)showTechXPOTTDetail:(NSInteger)index{
    _dataSource = @"TechXPOT";
    NSLog(@"showTechXPOTTDetail:%ld", index);
    self.selectedIndex = index;
    [self performSegueWithIdentifier:@"showEventDetails" sender:self];
}

#pragma - EventsTableViewDelegate

-(void)showEventDetail:(NSInteger)index{
    _dataSource = @"Events";
    NSLog(@"showEventDetail:%ld", index);
    self.selectedIndex = index;
    [self performSegueWithIdentifier:@"showEventDetails" sender:self];
}

#pragma - TechXPOTTableViewDelegate

-(void)showVisitorActiviteDetail:(NSInteger)index{
//    _dataSource = @"VisitorActivite";
//    NSLog(@"showVisitorActiviteDetail:%ld", index);
//    self.selectedIndex = index;
//    [self performSegueWithIdentifier:@"showEventIntro" sender:self];
    ProgramMapImageViewController *pmivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramMapImageViewController"];
    [self.navigationController pushViewController:pmivc animated:true];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showEventDetails"]) {
        if ([_dataSource isEqualToString:@"TechXPOT"]) {
            VisitorActiviteTableViewController *visitorActiviteTableViewController = [segue destinationViewController];
            visitorActiviteTableViewController.navBarTitle = [_TechXPOTArray objectAtIndex:self.selectedIndex];
            visitorActiviteTableViewController.index = self.selectedIndex;
            visitorActiviteTableViewController.dataSource = self.dataSource;
        }
        else if ([_dataSource isEqualToString:@"Events"]){
            VisitorActiviteTableViewController *visitorActiviteTableViewController = [segue destinationViewController];
            visitorActiviteTableViewController.navBarTitle = [_eventsArray objectAtIndex:self.selectedIndex];
            visitorActiviteTableViewController.index = self.selectedIndex;
            visitorActiviteTableViewController.dataSource = self.dataSource;
        }
    }
    else if ([[segue identifier] isEqualToString:@"showEventIntro"]) {
        if ([_dataSource isEqualToString:@"VisitorActivite"]) {
            EvevtActiviteIntroTableViewController *evevtActiviteIntro = [segue destinationViewController];
            evevtActiviteIntro.navBarTitle = [_TechXPOTArray objectAtIndex:self.selectedIndex];
            evevtActiviteIntro.index = self.selectedIndex;
            evevtActiviteIntro.dataSource = self.dataSource;
        }
    }
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
