//
//  ProductViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProductViewController.h"
#import "ProgramIntroViewController.h"
#import "Service.h"
#import "UIImageView+WebCache.h"
#import "ProductIntroTableViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ProductViewController (){
    NSDictionary *productName;
    NSDictionary *productPhoto;
    NSDictionary *boothNo;
    NSArray *productNameSectionTitles;
    NSArray *productNameIndexTitles;
    NSArray *adArray;
    
}
@property NSInteger starBtnPress;
@property NSMutableDictionary *productDictionary;
@property NSIndexPath *selectedIndexPath;
@end

@implementation ProductViewController

- (void)viewDidLoad {
    self.title = NSLocalizedString(@"Product_list", "") ;
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //-------ADSlider Satrt
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            adArray = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"TopBannerLg"]];
            
            NSMutableArray *imageUrls = [[NSMutableArray alloc] init];
            
            for (NSDictionary *ad in adArray) {
                if (![[ad objectForKey:@"imgUrl"] isEqualToString:@""]) {
                    [imageUrls addObject:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
                }
            }
            
            MCLScrollViewSlider *localScrollView = [MCLScrollViewSlider scrollViewSliderWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 432*self.view.frame.size.width/1080) imageURLs:imageUrls placeholderImage:[UIImage imageNamed:@"img_action_bar_bg.png"]];
            localScrollView.pageControlCurrentIndicatorTintColor = [UIColor whiteColor];
            [self.ADView addSubview:localScrollView];
            
            [localScrollView didSelectItemWithBlock:^(NSInteger clickedIndex) {
                NSLog(@"Clicked inner_ad_%ld", (long)clickedIndex+1);
                [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                              parameters:@{ FBSDKAppEventParameterNameContentID:[NSString stringWithFormat:@"inner_ad_%ld", (long)clickedIndex+1],
                                            FBSDKAppEventParameterNameDescription:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"] } ];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"]]];
            }];
        }
    }];
    //-------ADSlider End

    self.productDictionary = [[NSMutableDictionary alloc] initWithDictionary:@{
                                                                                 @"#" : @[],
                                                                                 @"A" : @[],
                                                                                 @"B" : @[],
                                                                                 @"C" : @[],
                                                                                 @"D" : @[],
                                                                                 @"E" : @[],
                                                                                 @"F" : @[],
                                                                                 @"G" : @[],
                                                                                 @"H" : @[],
                                                                                 @"I" : @[],
                                                                                 @"J" : @[],
                                                                                 @"K" : @[],
                                                                                 @"L" : @[],
                                                                                 @"M" : @[],
                                                                                 @"N" : @[],
                                                                                 @"O" : @[],
                                                                                 @"P" : @[],
                                                                                 @"Q" : @[],
                                                                                 @"R" : @[],
                                                                                 @"S" : @[],
                                                                                 @"T" : @[],
                                                                                 @"U" : @[],
                                                                                 @"V" : @[],
                                                                                 @"W" : @[],
                                                                                 @"X" : @[],
                                                                                 @"Y" : @[],
                                                                                 @"Z" : @[]}];
    
    productNameSectionTitles = [[self.productDictionary allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    productNameIndexTitles = @[@"#", @"A", @"B", @"C", @"D", @"E", @"F", @"G", @"H", @"I", @"J", @"K", @"L", @"M", @"N", @"O", @"P", @"Q", @"R", @"S", @"T", @"U", @"V", @"W", @"X", @"Y", @"Z"];
    
    _starBtnPress = 0;
    
    [self fetchAllProduct];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)fetchAllProduct {
    [[Service sharedInstance] fetchAllProductWithComplete:^(BOOL success, NSArray *responseAry) {
        
        if (success) {
            for (NSDictionary *product in responseAry) {
                NSString *upperProductName = [[product objectForKey:@"Name"] uppercaseString];
                
                NSString *firstAlphabet;
                
                if ([upperProductName isEqualToString:@""]) {
                    firstAlphabet = @"Z";
                }
                else {
                    firstAlphabet = [upperProductName substringToIndex:1];
                }
                
                NSCharacterSet *sets = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLMNOPQRSTUVWXYZ"];
                
                NSRange r = [firstAlphabet rangeOfCharacterFromSet:sets];
                if (r.location == NSNotFound) {
                    firstAlphabet = @"#";
                }
                
                NSMutableArray *subExhibitorArray = [[NSMutableArray alloc] initWithArray:[self.productDictionary objectForKey:firstAlphabet] copyItems:YES];
                
                [subExhibitorArray addObject:product];
                
                [self.productDictionary setObject:subExhibitorArray forKey:firstAlphabet];
            }
            [self.tableView reloadData];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [productNameSectionTitles count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *sectionTitle = [productNameSectionTitles objectAtIndex:section];
    NSArray *sectionExhibitors = [self.productDictionary objectForKey:sectionTitle];
    return [sectionExhibitors count];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [productNameSectionTitles objectAtIndex:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    NSDictionary *product = [[self.productDictionary objectForKey:[productNameSectionTitles objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    
    UIImageView *productImage = [[UIImageView alloc] initWithFrame: CGRectMake(30,9,70,70)];
    
    if (![[product objectForKey:@"SmallImageURL"] isEqualToString:@""]) {
        [productImage sd_setImageWithURL:[NSURL URLWithString:[product objectForKey:@"SmallImageURL"]] placeholderImage:nil];
    }
    else if (![[product objectForKey:@"LargeImageURL"] isEqualToString:@""]) {
        [productImage sd_setImageWithURL:[NSURL URLWithString:[product objectForKey:@"LargeImageURL"]] placeholderImage:nil];
    }
    
    productImage.contentMode = UIViewContentModeScaleAspectFill;
    productImage.clipsToBounds = YES;
    productImage.backgroundColor = [UIColor whiteColor];
    productImage.layer.borderColor = [[UIColor colorWithRed:0.341 green:0.118 blue:0.549 alpha:1.0] CGColor];
    productImage.layer.borderWidth = 1.0;
    [cell addSubview:productImage];
    
    UILabel *productLabel = [[UILabel alloc] initWithFrame:CGRectMake(114, 24, self.view.frame.size.width-193, 20)];
    productLabel.text = [product objectForKey:@"Name"];
    
    if ([productLabel.text isEqualToString:@""]) {
        productLabel.text = @"(No Name)";
    }
    
    [productLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    productLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    productLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:productLabel];
    
    UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(114, 50, 120, 20)];
    boothNoLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@", ""),[product objectForKey:@"BoothNumber"]];
    [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
    boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
    boothNoLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:boothNoLabel];
    
//    UIButton *starBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
//    starBtn.frame = CGRectMake(self.view.frame.size.width-65, 24, 40, 40);
//    [starBtn setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"] forState:UIControlStateNormal];
//    [starBtn addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//    [cell addSubview:starBtn];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    //  return animalSectionTitles;
    return productNameIndexTitles;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(36, 0, tableView.frame.size.width-72, 44)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(36, 10, sectionHeaderView.frame.size.width, 24)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    headerLabel.text = [productNameIndexTitles objectAtIndex:section];
    [sectionHeaderView addSubview:headerLabel];
    
    return sectionHeaderView;
}

- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return [productNameSectionTitles indexOfObject:title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",(long)indexPath.section,indexPath.row);
    _selectedIndexPath = indexPath;
    [self performSegueWithIdentifier:@"showProductIntro" sender:self];
}

-(void) starBtnAction:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (self.starBtnPress == 0) {
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_star_press.png"]
                       forState:UIControlStateNormal];
        self.starBtnPress = 1;
    }
    else{
        [btn setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"]
                       forState:UIControlStateNormal];
        self.starBtnPress = 0;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showProductIntro"]) {
        
        NSDictionary *product = [[self.productDictionary objectForKey:[productNameSectionTitles objectAtIndex:_selectedIndexPath.section]] objectAtIndex:_selectedIndexPath.row];
        
        ProductIntroTableViewController *productIntroTableViewController = [segue destinationViewController];
        productIntroTableViewController.navBarTitle = [product objectForKey:@"Name"];
        productIntroTableViewController.productID = [product objectForKey:@"productId"];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
