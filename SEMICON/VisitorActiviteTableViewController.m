//
//  VisitorActiviteTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "VisitorActiviteTableViewController.h"
#import "EvevtActiviteIntroTableViewController.h"
#import "ProgramMapImageViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Service.h"
#import "SignInViewController.h"
#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

#define DEFAULT_COLOR_HEADER [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0]

@interface VisitorActiviteTableViewController () {
    NSString *adUrl;
    BOOL adIsClosed;
}

@property NSArray *VAArray;
@property NSArray *VATimeArray;
@property NSArray *VAPositionArray;
@property NSInteger viewBtnTag;
@property NSMutableArray *dateArray;
@property NSIndexPath *selectedIndexPath;
@property NSArray* response;
@property UIView *adView;
@end

@implementation VisitorActiviteTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%@,%@,%ld",_dataSource,_navBarTitle,_index);
    self.title = NSLocalizedString(_navBarTitle, @"");;

    self.view.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];

    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            
            NSDictionary *ad = [[NSDictionary alloc] initWithDictionary:[responseDict objectForKey:@"BottomBannerLg"]];
            adUrl = [ad objectForKey:@"actionUrl"];
            _adView = [[UIView alloc] initWithFrame:CGRectMake(20, [[UIScreen mainScreen] bounds].size.height - 60, self.view.frame.size.width - 40, 60)];
            _adView.backgroundColor = [UIColor clearColor];
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 40, 60)];
            [image sd_setImageWithURL:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
            image.userInteractionEnabled = true;
            [_adView addSubview:image];
            
            UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(adTap:)];
            [image addGestureRecognizer:tgr];
            
            UIButton *close = [[UIButton alloc] initWithFrame:CGRectMake(_adView.frame.size.width - 50, 10, 40, 40)];
            [close setImage:[UIImage imageNamed:@"btn_cancel.png"] forState:UIControlStateNormal];
            [close addTarget:self action:@selector(closeAD) forControlEvents:UIControlEventTouchUpInside];
            [_adView addSubview:close];
            [[[[UIApplication sharedApplication] windows] firstObject] addSubview:_adView];
        }
    }];
    
//    NSArray *firstDayArray = [[NSArray alloc] initWithObjects:@"早安咖啡",@"晶晶問早",@"20轉轉樂",@"20歡樂大遊行",@"20歡樂大遊行",@"天天驚喜抽好禮 (獎品: Apple Watch)",@"20歡樂大遊行",@"天天驚喜抽好禮 (獎品: A CUT 牛排館餐券)",@"Happy Birthday 生日趴", nil];
//    NSArray *secondDayArray = [[NSArray alloc] initWithObjects:@"早安咖啡",@"晶晶問早",@"20轉轉樂",@"天天驚喜抽好禮 (獎品: 四軸飛行器)",@"20歡樂大遊行",@"天天驚喜抽好禮 (獎品: A CUT 牛排館餐券)",@"20歡樂大遊行",@"20歡樂大遊行",@"Happy Birthday 生日趴", nil];
//    NSArray *lastDayArray = [[NSArray alloc] initWithObjects:@"早安咖啡",@"晶晶問早",@"20轉轉樂",@"20歡樂大遊行",@"20歡樂大遊行",@"天天驚喜抽好禮 (獎品: 四軸飛行器)",@"20歡樂大遊行",@"Happy Birthday 生日趴 及 天天驚喜抽好禮 (獎品: A CUT 牛排館餐券)", nil];
//    self.VAArray = [[NSMutableArray alloc] initWithObjects:firstDayArray,secondDayArray,lastDayArray, nil];
//    
//    NSArray *firstDayTimeArray = [[NSArray alloc] initWithObjects:@"09:30~10:30",@"10:00~10:15",@"10:00~17:00",@"10:20~10:50",@"11:20~11:50",@"11:20~11:40",@"13:20~13:50",@"14:20~14:40",@"16:20~16:40", nil];
//    NSArray *secondDayTimeArray = [[NSArray alloc] initWithObjects:@"09:30~10:30",@"10:00~10:15",@"10:00~17:00",@"10:20~10:50",@"11:20~11:50",@"11:20~11:40",@"13:20~13:50",@"14:20~14:40",@"16:20~16:40", nil];
//    NSArray *lastDayTimeArray = [[NSArray alloc] initWithObjects:@"09:30~10:30",@"10:00~10:15",@"10:00~17:00",@"10:20~10:50",@"11:20~11:50",@"11:20~11:40",@"13:20~13:50",@"14:20~14:40", nil];
//    self.VATimeArray = [[NSMutableArray alloc] initWithObjects:firstDayTimeArray,secondDayTimeArray,lastDayTimeArray, nil];
//    
//    NSArray *firstDayPositionArray = [[NSArray alloc] initWithObjects:@"L、M、N、J、K 各入口處",@"1樓 J區入口",@"1樓 TechXPOT主舞台(攤位號3322)",@"四樓 漢民 (攤位號 436)",@"一樓",@"一樓 近攤位號724",@"四樓",@"一樓 工業局設備零組件本土化專區",@"一樓 近產品發表平台(攤位號3255)", nil];
//    NSArray *secondDayPositionArray = [[NSArray alloc] initWithObjects:@"L、M、N、J、K 各入口處",@"1樓 J區入口",@"1樓 TechXPOT主舞台(攤位號3322)",@"四樓 漢民 (攤位號 436)",@"一樓",@"一樓 近攤位號724",@"四樓",@"一樓 工業局設備零組件本土化專區",@"一樓 近產品發表平台(攤位號3255)", nil];
//    NSArray *lastDayPositionArray = [[NSArray alloc] initWithObjects:@"L、M、N、J、K 各入口處",@"1樓 J區入口",@"1樓 TechXPOT主舞台(攤位號3322)",@"四樓 漢民 (攤位號 436)",@"一樓",@"一樓 近攤位號724",@"四樓",@"一樓 工業局設備零組件本土化專區", nil];
//    self.VAPositionArray = [[NSMutableArray alloc] initWithObjects:firstDayPositionArray,secondDayPositionArray,lastDayPositionArray, nil];
    [self fetchData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_adView removeFromSuperview];
}

- (void)viewDidAppear:(BOOL)animated {
    if (!adIsClosed) {
        [[[[UIApplication sharedApplication] windows] firstObject] addSubview:_adView];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    if (_selectedIndexPath != nil) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[_selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
}

- (void)adTap:(UITapGestureRecognizer *)recognizer {
    NSLog(@"Clicked bottom_ad_1");
    [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                  parameters:@{ FBSDKAppEventParameterNameContentID:@"bottom_ad_1",
                                FBSDKAppEventParameterNameDescription:adUrl } ];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:adUrl]];
}

- (void)closeAD {
    [_adView removeFromSuperview];
}

- (void)fetchData {
    NSString *tempString = [_navBarTitle stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    if ([tempString isEqualToString:@"Semiconductor-Materials"]) {
        tempString = @"materials";
    }
    _dateArray = [[NSMutableArray alloc] init];
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        
        [[Service sharedInstance] fetchTechXPOTWithTitle:tempString Complete:^(BOOL success, NSArray* responseAry) {
            _response = [[NSArray alloc] initWithArray:responseAry];
            
            NSMutableArray *firstDayArray = [[NSMutableArray alloc] init];
            NSMutableArray *secondDayArray = [[NSMutableArray alloc] init];
            NSMutableArray *lastDayArray = [[NSMutableArray alloc] init];
            
            if (success) {
                for (int i = 0; i < responseAry.count; i++) {
                    
                    NSString *day = [[responseAry objectAtIndex:i] objectForKey:@"time"];
                    day = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
                    day = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [_dateArray addObject:day];
                    
                    switch (i) {
                        case 0:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [firstDayArray addObject:event];
                            }
                            break;
                        case 1:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [secondDayArray addObject:event];
                            }
                            break;
                        case 2:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [lastDayArray addObject:event];
                            }
                            break;
                        default:
                            break;
                    }
                }
                _VAArray = [[NSMutableArray alloc] initWithObjects:firstDayArray,secondDayArray,lastDayArray, nil];
                
                [self.tableView reloadData];
            }
        }];
    }
    else {
        if ([tempString isEqualToString:@"Hospitality-Suite-and-Meeting-Room"]) {
            tempString = @"hospitality-suite";
        }
        [[Service sharedInstance] fetchActivitiesWithTitle:tempString Complete:^(BOOL success, NSArray *responseAry) {
            
            NSMutableArray *firstDayArray = [[NSMutableArray alloc] init];
            NSMutableArray *secondDayArray = [[NSMutableArray alloc] init];
            NSMutableArray *lastDayArray = [[NSMutableArray alloc] init];
            
            if (success) {
                for (int i = 0; i < responseAry.count; i++) {
                    
                    NSString *day = [[responseAry objectAtIndex:i] objectForKey:@"time"];
                    day = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
                    day = [day stringByReplacingOccurrencesOfString:@" " withString:@""];
                    [_dateArray addObject:day];
                    
                    switch (i) {
                        case 0:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [firstDayArray addObject:event];
                            }
                            break;
                        case 1:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [secondDayArray addObject:event];
                            }
                            break;
                        case 2:
                            for (NSDictionary *event in [[responseAry objectAtIndex:i] objectForKey:@"events"]) {
                                [lastDayArray addObject:event];
                            }
                            break;
                        default:
                            break;
                    }
                }
                _VAArray = [[NSMutableArray alloc] initWithObjects:firstDayArray,secondDayArray,lastDayArray, nil];
                
                [self.tableView reloadData];
            }
        }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.VAArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.VAArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VisitorActiviteCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor colorWithRed:0.906 green:0.902 blue:0.906 alpha:1.0];
    
    UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, cell.frame.size.width-30, 146)];
    UIImage *image = [UIImage imageNamed:@"img_content_card_bg.png"];
    backgroundImage.image = image;
    [cell addSubview:backgroundImage];
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        UIImageView *activities = [[UIImageView alloc] initWithFrame:CGRectMake(28, 10, 50, 50)];
        UIImage *activitieImage = [UIImage imageNamed:@"img_speaker.png"];
        activities.image = activitieImage;
        [cell addSubview:activities];
    }
    else{
        UIImageView *activities = [[UIImageView alloc] initWithFrame:CGRectMake(28, 10, 50, 50)];
        UIImage *activitieImage = [UIImage imageNamed:@"img_activities.png"];
        activities.image = activitieImage;
        [cell addSubview:activities];
    }
    
    UILabel *activitiesLabel=[[UILabel alloc]initWithFrame:CGRectMake(78,10,self.view.frame.size.width-156,58)];
    [activitiesLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    activitiesLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    
    NSString *labelText;
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        labelText = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"topic"];
    }
    else {
        if ([_navBarTitle isEqualToString:@"Hospitality Suite and Meeting Room"]) {
            labelText = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"company"];
        }
        else {
            labelText = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"event"];
        }
    }
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    
    if (labelText != nil) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:labelText];
        
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [labelText length])];
        activitiesLabel.attributedText = attributedString;
        activitiesLabel.textAlignment = NSTextAlignmentLeft;
        [activitiesLabel setNumberOfLines:0];
        activitiesLabel.lineBreakMode =NSLineBreakByWordWrapping;
        //if overflow --> ...
        activitiesLabel.adjustsFontSizeToFitWidth = NO;
        activitiesLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:activitiesLabel];
    }
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        UIButton *star = [[UIButton alloc] initWithFrame:CGRectMake(cell.frame.size.width-68, 10, 40, 40)];
        [star setBackgroundImage:[UIImage imageNamed:@"btn_star_normal.png"] forState:UIControlStateNormal];
        UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_star_press.png"];
        [star setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
        [star addTarget:self action:@selector(starBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:star];
        
        if ([self checkTechXPOTFarvorites:indexPath]) {
            star.selected = true;
        }
    }
    
    UIView *line = [[UIView alloc] initWithFrame: CGRectMake(28, 68, cell.frame.size.width-56, 1)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    UILabel *positionLabel=[[UILabel alloc]initWithFrame:CGRectMake(78,80,self.view.frame.size.width-165,60)];
    [positionLabel setFont:[UIFont fontWithName:@"GothamBook" size:13]];
    positionLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    NSString *time = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"time"];
    
    NSString *position;
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        position = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"location"];
    }
    else {
        if (![_navBarTitle isEqualToString:@"Hospitality Suite and Meeting Room"]) {
            position = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"venue"];
        }
        else {
            position = [[[self.VAArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] objectForKey:@"location"];
        }
    }
    
    NSString *TimePositionLabelText = [NSString stringWithFormat:@"%@\r%@", time, position];
    
    if (TimePositionLabelText != nil) {
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:TimePositionLabelText];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [TimePositionLabelText length])];
        positionLabel.attributedText = attributedString;
        positionLabel.textAlignment = NSTextAlignmentLeft;
        [positionLabel setNumberOfLines:0];
        positionLabel.lineBreakMode =NSLineBreakByWordWrapping;
        //if overflow --> ...
        positionLabel.adjustsFontSizeToFitWidth = NO;
        positionLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [cell addSubview:positionLabel];
    }
    
    if (![_navBarTitle isEqualToString:@"Networking Events"] && ![_navBarTitle isEqualToString:@"Exhibitor Activities"]) {
        UIButton *viewBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        viewBtn.frame = CGRectMake(cell.frame.size.width-87, 90, 50, 28);
        
        if ([_dataSource isEqualToString:@"TechXPOT"]) {
            [viewBtn setTitle:NSLocalizedString(@"View", "")  forState:UIControlStateNormal];
        }
        else {
            [viewBtn setTitle:NSLocalizedString(@"Map", "")  forState:UIControlStateNormal];
        }
        [viewBtn.titleLabel setFrame:CGRectMake(viewBtn.titleLabel.frame.origin.x, viewBtn.titleLabel.frame.origin.y, viewBtn.titleLabel.frame.size.width, viewBtn.titleLabel.frame.size.height+5)];
        
        viewBtn.tintColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        viewBtn.titleLabel.font = [AppDelegate getSystemFont:@"GothamBook" size:13];
        viewBtn.layer.borderColor = [[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0] CGColor];
        viewBtn.layer.borderWidth = 0.5;
        viewBtn.layer.cornerRadius = 5.0;
        viewBtn.clipsToBounds = NO;
        viewBtn.tag = indexPath.row;
        _viewBtnTag = viewBtn.tag;
        [viewBtn addTarget:self action:@selector(viewBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:viewBtn];

    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, self.view.frame.size.width, 55)];
    
    UIImageView *img = [[UIImageView alloc] initWithFrame:
                        CGRectMake(0, 0, self.view.frame.size.width, 55)];
    img.image = [UIImage imageNamed:@"img_section_white_bg.png"];
    img.contentMode = UIViewContentModeScaleAspectFill;
    img.clipsToBounds = YES;
    [sectionHeaderView addSubview:img];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 13, sectionHeaderView.frame.size.width, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = DEFAULT_COLOR_HEADER;
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    headerLabel.text = [_dateArray objectAtIndex:section];
//    
//    switch (section) {
//        case 0:
//            headerLabel.text = @"Tuesday, Sep. 7, 2016";
//            return sectionHeaderView;
//            break;
//        case 1:
//            headerLabel.text = @"Wednesday, Sep. 8, 2016";
//            return sectionHeaderView;
//            break;
//        case 2:
//            headerLabel.text = @"Thursday, Sep. 9, 2016";
//            return sectionHeaderView;
//            break;
//        default:
//            break;
//    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([[_VAArray objectAtIndex:section] count] == 0) return 0;
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 146;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == [_VAArray count] - 1) {
        return 60;
    }
    else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 60)];
    view.backgroundColor = [UIColor clearColor];
    
    return view;
}

-(void) viewBtnAction:(id)sender {
    NSLog(@"viewBtnAction");
    
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
    _selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    
    if ([_dataSource isEqualToString:@"TechXPOT"]) {
        [self performSegueWithIdentifier:@"showEventsIntro" sender:self];
    }
    else if ([_dataSource isEqualToString:@"Events"]) {
        ProgramMapImageViewController *pmivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramMapImageViewController"];
        pmivc.navTitle = @"Map";
        [self.navigationController pushViewController:pmivc animated:true];
    }
    else{
        if ([_delegate respondsToSelector:@selector(showVisitorActiviteDetail:)]) {
            [_delegate showVisitorActiviteDetail:self.viewBtnTag];
        }
    }
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showEventsIntro"]) {
        
        NSDictionary *activity = [self.response objectAtIndex:_selectedIndexPath.section];
        
        EvevtActiviteIntroTableViewController *evevtActiviteIntro = [segue destinationViewController];
        evevtActiviteIntro.navBarTitle = _navBarTitle;
        evevtActiviteIntro.dataSource = _dataSource;
        evevtActiviteIntro.dateString = [_dateArray objectAtIndex:_selectedIndexPath.section];
        evevtActiviteIntro.infoID = [[[activity objectForKey:@"events"] objectAtIndex:_selectedIndexPath.row] objectForKey:@"id"];
    }
}


- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void) starBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
        _selectedIndexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
        
        NSDictionary *info = [[[self.response objectAtIndex:_selectedIndexPath.section] objectForKey:@"events"] objectAtIndex:_selectedIndexPath.row];
        NSMutableArray *techXPOTFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"TechXPOTFarvorites"]];
        
        if ([sender isSelected]) {
            NSMutableDictionary *new = [[NSMutableDictionary alloc] initWithDictionary:info];
            [new setObject:[[self.response objectAtIndex:_selectedIndexPath.section] objectForKey:@"time"] forKey:@"date"];
            
            [techXPOTFarvorites addObject:new];
        }
        else {
            for (int i = 0; i < techXPOTFarvorites.count; i++) {
                if ([[[techXPOTFarvorites objectAtIndex:i] objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[info objectForKey:@"id"]]]) {
                    [techXPOTFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:techXPOTFarvorites forKey:@"TechXPOTFarvorites"];
    }
}

- (Boolean)checkTechXPOTFarvorites:(NSIndexPath *) indexPath{
    NSArray *techXPOTFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"TechXPOTFarvorites"];
    NSDictionary *info = [[[self.response objectAtIndex:indexPath.section] objectForKey:@"events"] objectAtIndex:indexPath.row];
    
    for (NSDictionary *techXPOTFarvorite in techXPOTFarvorites) {
        if ([[techXPOTFarvorite objectForKey:@"id"] isEqualToString:[NSString stringWithFormat:@"%@",[info objectForKey:@"id"]]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}

@end
