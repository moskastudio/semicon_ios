//
//  AddCalendarViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/23.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCalendarViewController : UIViewController
- (IBAction)cancelBtn:(id)sender;
- (IBAction)saveBtn:(id)sender;
@property NSString *dateString;
@property NSString *timeString;
@property NSString *scheduleName;
@property NSString *type;
@property NSString *typeID;

@property (weak, nonatomic) IBOutlet UILabel *eventTitleLabel;
@property (weak, nonatomic) IBOutlet UITextField *dateLabel;
@property (weak, nonatomic) IBOutlet UITextField *timeLabel;
@property (weak, nonatomic) IBOutlet UISwitch *oneDaySwitch;
@property (weak, nonatomic) IBOutlet UISwitch *twoHourSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *thirtyMinSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *addCalendarSwitch;

@end
