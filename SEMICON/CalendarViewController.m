//
//  CalendarViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "CalendarViewController.h"

#import "CalendarDay1TableViewController.h"
#import "CalendarDay2TableViewController.h"
#import "CalendarDay3TableViewController.h"
#import "AddCalendarViewController.h"
#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface CalendarViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate, CalendarDay1TableViewControllerDelegate, CalendarDay2TableViewControllerDelegate, CalendarDay3TableViewControllerDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;

@end


@implementation CalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"My Calendar", "") ;
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}

- (void)viewWillAppear:(BOOL)animated {
    if (_controllerDay1 != nil) {
        [_controllerDay1 refreshCell];
    }
    
    if (_controllerDay2 != nil) {
        [_controllerDay2 refreshCell];
    }
    
    if (_controllerDay3 != nil) {
        [_controllerDay3 refreshCell];
    }
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[@"Web. Sep. 7", @"Thu Sep. 8", @"Fri. Sep. 9"]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:15]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 3;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            CalendarDay1TableViewController *calendarDay1TableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarDay1TableViewController"];
            calendarDay1TableViewController.delegate = self;
            self.controllerDay1 = calendarDay1TableViewController;
            return calendarDay1TableViewController.view;
        }
            break;
        case 1:{
            CalendarDay2TableViewController *calendarDay2TableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarDay2TableViewController"];
            calendarDay2TableViewController.delegate = self;
            self.controllerDay2 = calendarDay2TableViewController;
            return calendarDay2TableViewController.view;
        }
            break;
        case 2:{
            CalendarDay3TableViewController *calendarDay3TableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CalendarDay3TableViewController"];
            calendarDay3TableViewController.delegate = self;
            self.controllerDay3 = calendarDay3TableViewController;
            return calendarDay3TableViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showCalendar:(NSDictionary *)reminder {
    AddCalendarViewController *addCalendarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCalendarViewController"];
    addCalendarViewController.scheduleName = [reminder objectForKey:@"Name"];
    addCalendarViewController.type = [reminder objectForKey:@"type"];
    addCalendarViewController.typeID = [reminder objectForKey:@"typeID"];
    [self.navigationController pushViewController:addCalendarViewController animated:YES];
}
@end
