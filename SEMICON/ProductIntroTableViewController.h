//
//  ProductIntroTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/20.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductIntroTableViewController : UITableViewController

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property NSString *navBarTitle;
@property NSString *productID;
//@property NSDictionary *product;

@end
