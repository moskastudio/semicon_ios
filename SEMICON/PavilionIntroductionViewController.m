//
//  PavilionIntroductionViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/27.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "PavilionIntroductionViewController.h"

@interface PavilionIntroductionViewController ()

@end

@implementation PavilionIntroductionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _navBarTitle;
    
    UILabel *introLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 44, 200, 45)];
    introLabel.text = NSLocalizedString(@"Introduction_Pavilion", "") ;
    introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
    introLabel.textColor = [UIColor colorWithRed:0.271 green:0.271 blue:0.271 alpha:1.0];
    [self.scrollView addSubview:introLabel];
    
    UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(27, 100, self.view.frame.size.width-54, 250)];
    contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
    contentLabel.numberOfLines = 0;
    contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
    contentLabel.textAlignment = NSTextAlignmentJustified;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:_contentString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:10];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [_contentString length])];
    contentLabel.attributedText = attributedString ;
    contentLabel.adjustsFontSizeToFitWidth = NO;
    contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    [contentLabel sizeToFit];
    contentLabel.frame = CGRectMake(contentLabel.frame.origin.x, contentLabel.frame.origin.y, contentLabel.frame.size.width, contentLabel.frame.size.height + 10);
    contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    
    [self.scrollView addSubview:contentLabel];
    
    CGRect contentRect = CGRectZero;
    for (UIView *view in self.scrollView.subviews) {
        contentRect = CGRectUnion(contentRect, view.frame);
    }
    self.scrollView.contentSize = CGSizeMake(self.view.frame.size.width, contentRect.size.height+36);
    
    [self.view addSubview:self.scrollView];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
