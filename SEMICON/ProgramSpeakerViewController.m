//
//  ProgramSpeakerViewController.m
//  semicon
//
//  Created by aisoter on 2016/8/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProgramSpeakerViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "Service.h"

@interface ProgramSpeakerViewController ()
@property  NSInteger educationLabel;
@property NSDictionary *speakerDict;
@property NSString *educationString;
@property NSString *experiencesString;
@property NSString *biographyString;
@property NSString *speechAbstractTitleString;
@property NSString *speechAbstractString;
@property NSString *imgURL;
@property NSString *name;
@property NSString *speakerTitleEn;
@property NSString *speakerCompanyEn;

@end

@implementation ProgramSpeakerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    self.title = NSLocalizedString(@"Speaker Profile", "");
    self.educationString = @"";
    self.experiencesString = @"";
    self.biographyString = @"";
    self.speechAbstractTitleString = @"";
    self.speechAbstractString = @"";
    self.imgURL = @"";
    self.name = @"";
    self.speakerTitleEn = @"";
    self.speakerCompanyEn = @"";
    
    [[Service sharedInstance] fetchSpeaker:_link Complete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            _speakerDict = [[NSDictionary alloc] initWithDictionary:responseDict];
            self.educationString = [_speakerDict objectForKey:@"education"];
            self.experiencesString = [_speakerDict objectForKey:@"experiences"];
            self.biographyString = [_speakerDict objectForKey:@"biography"];
            self.speechAbstractTitleString = [_speakerDict objectForKey:@"speechAbstractTitle"];
            self.speechAbstractString =  [_speakerDict objectForKey:@"speechAbstract"];
            self.imgURL = [_speakerDict objectForKey:@"image"];
            self.name = [_speakerDict objectForKey:@"name"];
            self.speakerTitleEn = [_speakerDict objectForKey:@"speakerTitleEn"];
            self.speakerCompanyEn = [_speakerDict objectForKey:@"speakerCompanyEn"];
            
            if ([self.speakerTitleEn isEqualToString:@""] || self.speakerTitleEn == nil) {
                self.speakerTitleEn =[_speakerDict objectForKey:@"speakerTitleZn"];
            }
            if ([self.speakerCompanyEn isEqualToString:@""] || self.speakerCompanyEn == nil) {
                self.speakerCompanyEn =[_speakerDict objectForKey:@"speakerCompanyZn"];
            }
        }
        [self.tableView reloadData];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 11 ;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpeakerCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
    if (indexPath.row == 0) {
        UIImageView *productImage = [[UIImageView alloc] initWithFrame: CGRectMake(30,(cell.frame.size.height-140)/2,110,140)];
        [productImage sd_setImageWithURL:[NSURL URLWithString:self.imgURL] placeholderImage:nil];
        productImage.contentMode = UIViewContentModeScaleAspectFill;
        productImage.clipsToBounds = YES;
        productImage.backgroundColor = [UIColor whiteColor];
        productImage.layer.borderColor = [[UIColor colorWithRed:0.341 green:0.118 blue:0.549 alpha:1.0] CGColor];
        productImage.layer.borderWidth = 1.0;
        [cell addSubview:productImage];

        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(160,20,cell.frame.size.width-180,25)];
        NSString *contentString = self.name;
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        titleLabel.attributedText = attributedString ;
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.textColor = [UIColor blackColor];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:24] ;
        titleLabel.font = textFont;
        [titleLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-180, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        titleLabel.frame = CGRectMake(160, 20, actualSize.width,actualSize.height*1.5);
        [cell addSubview:titleLabel];

        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(160,20,cell.frame.size.width-180,25)];
        NSString *labelString = [NSString stringWithFormat:@"%@\n\n%@", self.speakerTitleEn, self.speakerCompanyEn];
        label.numberOfLines = 0;
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *labelAttributedString = [[NSMutableAttributedString alloc] initWithString:labelString];
        NSMutableParagraphStyle *labelParagraphStyle = [[NSMutableParagraphStyle alloc] init];
        [labelParagraphStyle setLineSpacing:5];
        [labelAttributedString addAttribute:NSParagraphStyleAttributeName value:labelParagraphStyle range:NSMakeRange(0, [labelString length])];
        label.attributedText = labelAttributedString ;
        label.adjustsFontSizeToFitWidth = NO;
        label.textColor = [UIColor blackColor];
        UIFont *labelTextFont = [UIFont fontWithName:@"GothamBook" size:14] ;
        label.font = labelTextFont;
        [label setNumberOfLines:0];
        CGSize labelSize = CGSizeMake(self.view.frame.size.width-180, 1000);
        NSDictionary *labelDic = [NSDictionary dictionaryWithObjectsAndKeys:labelTextFont,NSFontAttributeName, nil];
        CGSize labelActualSize = [labelString boundingRectWithSize:labelSize options:NSStringDrawingUsesLineFragmentOrigin attributes:labelDic context:nil].size;
        label.frame = CGRectMake(160, titleLabel.frame.size.height+titleLabel.frame.origin.y+10, labelActualSize.width,labelActualSize.height*1.5);
        [cell addSubview:label];

    }
    
    else if (indexPath.row == 1) {
        cell.backgroundColor = [UIColor whiteColor];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,30)];
        outlineLabel.text = NSLocalizedString(@"Education","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [cell addSubview:outlineLabel];
    }
    else if (indexPath.row == 2) {
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
        NSString *contentString =  self.educationString;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(contentLabel.frame.size.width, MAXFLOAT)];
        NSInteger contentLabelHeight = size.height+30;
        contentLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, contentLabelHeight);
        [cell addSubview:contentLabel];
    }
    
    else if (indexPath.row == 3) {
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,30)];
        outlineLabel.text = NSLocalizedString(@"Experiences","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [cell addSubview:outlineLabel];
    }
    
    else if (indexPath.row == 4) {
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
        NSString *contentString =  self.experiencesString;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(contentLabel.frame.size.width, MAXFLOAT)];
        NSInteger contentLabelHeight = size.height+30;
        contentLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, contentLabelHeight);
        [cell addSubview:contentLabel];
    }
    
    else if (indexPath.row == 5) {
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,30)];
        outlineLabel.text = NSLocalizedString(@"Biography","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [cell addSubview:outlineLabel];
    }
    
    else if (indexPath.row == 6) {
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
        NSString *contentString =  self.biographyString;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(contentLabel.frame.size.width, MAXFLOAT)];
        NSInteger contentLabelHeight = size.height+30;
        contentLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, contentLabelHeight);
        [cell addSubview:contentLabel];
    }
    
    else if (indexPath.row == 7) {
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,30)];
        outlineLabel.text = NSLocalizedString(@"Speech Topic","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [cell addSubview:outlineLabel];
    }
    
    else if (indexPath.row == 8) {
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
        NSString *contentString =  self.speechAbstractTitleString;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(contentLabel.frame.size.width, MAXFLOAT)];
        NSInteger contentLabelHeight = size.height+30;
        contentLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, contentLabelHeight);
        [cell addSubview:contentLabel];
    }
    
    else if (indexPath.row == 9) {
        UILabel *outlineLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,30)];
        outlineLabel.text = NSLocalizedString(@"Speech Abstract","");
        outlineLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        outlineLabel.textColor = [UIColor blackColor];
        [cell addSubview:outlineLabel];
    }
    
    else if (indexPath.row == 10) {
        UILabel *contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 25)];
        NSString *contentString =  self.speechAbstractString;
        contentLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
        contentLabel.numberOfLines = 0;
        contentLabel.lineBreakMode = NSLineBreakByWordWrapping;
        contentLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        contentLabel.attributedText = attributedString ;
        contentLabel.adjustsFontSizeToFitWidth = NO;
        contentLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        [contentLabel sizeToFit];
        contentLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
        
        CGSize size = [contentLabel sizeThatFits:CGSizeMake(contentLabel.frame.size.width, MAXFLOAT)];
        NSInteger contentLabelHeight = size.height+30;
        contentLabel.frame =CGRectMake(36, 5, self.view.window.frame.size.width-72, contentLabelHeight);
        [cell addSubview:contentLabel];
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {

        NSString *contentString = self.name;
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:24];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        
        NSString *string = [NSString stringWithFormat:@"%@\n\n%@", self.speakerTitleEn, self.speakerCompanyEn];
        UIFont *stringFont = [UIFont fontWithName:@"GothamBook" size:14];
        CGSize stringSize = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *stringdic = [NSDictionary dictionaryWithObjectsAndKeys:stringFont,NSFontAttributeName, nil];
        CGSize stringActualSize = [string boundingRectWithSize:stringSize options:NSStringDrawingUsesLineFragmentOrigin attributes:stringdic context:nil].size;
        
        if (actualSize.height*1.8+stringActualSize.height*1.8+60 >180) {
            return actualSize.height*1.8+stringActualSize.height*1.8+60;
        }
        else
            return 180;
    }
    else if (indexPath.row == 1 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 9) {
        return 60;
    }
    else if (indexPath.row == 2){
        NSString *contentString = self.educationString;
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    else if (indexPath.row == 4){
        NSString *contentString = self.experiencesString;
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    else if (indexPath.row == 6){
        NSString *contentString = self.biographyString;
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    else if (indexPath.row == 8){
        NSString *contentString = self.speechAbstractTitleString;
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    else if (indexPath.row == 10){
        NSString *contentString = self.speechAbstractString;
        UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14]; ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.8+20;
    }
    return 100;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
