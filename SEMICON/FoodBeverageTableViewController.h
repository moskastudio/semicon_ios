//
//  FoodBeverageTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/13.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FoodBeverageTableViewController : UITableViewController<UITableViewDelegate,UITableViewDataSource>

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@end
