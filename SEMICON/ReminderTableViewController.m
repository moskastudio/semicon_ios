//
//  ReminderTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ReminderTableViewController.h"

@interface ReminderTableViewController ()
@property NSArray *titleArray;
@property NSArray *tagArray;
@property NSArray *timeArray;
@property NSArray *dateArray;
@property NSMutableArray *reminders;
@property NSString *formattedDateString;
@property NSString *todayDateString;
@property NSString *tomorrowDateString;

@end

@implementation ReminderTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"My Reminder", "") ;
    
//    _titleArray = [[NSArray alloc] initWithObjects:@"High-Tech Facility Pavilion",@"International Visitor Incentive Program is NOW Open for Registration!",@"Maximizing Your Access to the Dynamic Taiwan Market",@"SEMICON 2016", nil];
//    _tagArray = [[NSArray alloc] initWithObjects:@"Exhibitor",@"Program",@"Events/ Activities",@"Program", nil];
//    _timeArray = [[NSArray alloc] initWithObjects:@"13:00~14:00",@"15:00~17:00",@"9:00~12:00",@"10:00~17:00", nil];
//    _dateArray = [[NSArray alloc] initWithObjects:@"2016-06-26",@"2016-06-27",@"2016-06-24",@"2016-06-25", nil];
    
    _reminders = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
    
    self.tableView.backgroundColor = [UIColor colorWithRed:0.953 green:0.957 blue:0.957 alpha:1.0];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    // @"yyyy-MM-dd HH:mm:ss"
    _formattedDateString = [dateFormatter stringFromDate:[NSDate date]];
    
    if ([_formattedDateString isEqualToString:@"2016-09-07"]) {
        _todayDateString = @"9/7";
        _tomorrowDateString = @"9/8";
    }
    else if ([_formattedDateString isEqualToString:@"2016-09-08"]) {
        _todayDateString = @"9/8";
        _tomorrowDateString = @"9/9";
    }
    else if ([_formattedDateString isEqualToString:@"2016-09-09"]) {
        _todayDateString = @"9/9";
        _tomorrowDateString = @"";
    }
    else {
        _todayDateString = @"";
        _tomorrowDateString = @"";
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _reminders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReminderCell" forIndexPath:indexPath];
    
    // Configure the cell...
    UIView *whiteView = [[UIImageView alloc] initWithFrame :CGRectMake(5,4,cell.frame.size.width-10,60)];
    whiteView.backgroundColor = [UIColor whiteColor];
    whiteView.layer.shadowColor = [UIColor blackColor].CGColor;
    whiteView.layer.shadowOffset = CGSizeMake(0, 0);
    whiteView.layer.shadowOpacity = 0.4;
    whiteView.layer.shadowRadius = 3.0;
    [cell addSubview:whiteView];
    
    UIImageView *purpleImageView = [[UIImageView alloc] initWithImage :
                                    [UIImage imageNamed :@"img_tip_purple.png"]];
    UIView *purpleView = [[UIImageView alloc] initWithFrame :CGRectMake(5,4,80,68)];
    purpleView = purpleImageView;
    [cell addSubview:purpleView];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,11,60,30)];
    tagLabel.numberOfLines = 2;
    tagLabel.text = [[_reminders objectAtIndex:indexPath.row] objectForKey:@"type"];
    [tagLabel setFont:[UIFont fontWithName:@"GothamBook" size:12]];
    tagLabel.textColor = [UIColor whiteColor];
    [purpleView addSubview:tagLabel];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(85,13,cell.frame.size.width-110,15)];
    titleLabel.numberOfLines = 1;
    titleLabel.text = [[_reminders objectAtIndex:indexPath.row] objectForKey:@"Name"];
    [titleLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
    titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
    [whiteView addSubview:titleLabel];
    
    //show "Today"
    if ([[[_reminders objectAtIndex:indexPath.row] objectForKey:@"date"] isEqualToString:_todayDateString]) {
        UILabel *todayLabel = [[UILabel alloc] initWithFrame:CGRectMake(85,35,62,16)];
        todayLabel.text = NSLocalizedString(@"Today", "") ;
        [todayLabel setFont:[UIFont fontWithName:@"GothamBook" size:12]];
        todayLabel.textColor = [UIColor whiteColor];
        todayLabel.backgroundColor = [UIColor colorWithRed:0.957 green:0.667 blue:0 alpha:1];
        todayLabel.textAlignment = NSTextAlignmentCenter;
        [whiteView addSubview:todayLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(161,36,120,14)];
        timeLabel.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindMin"]];
        [timeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        timeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1];
        [whiteView addSubview:timeLabel];
    }
    //show "Tomorrow"
    else if ([[[_reminders objectAtIndex:indexPath.row] objectForKey:@"date"] isEqualToString:_tomorrowDateString]) {
        UILabel *tomorrowLabel = [[UILabel alloc] initWithFrame:CGRectMake(85,35,90,16)];
        tomorrowLabel.text = NSLocalizedString(@"Tomorrow", "") ;
        [tomorrowLabel setFont:[UIFont fontWithName:@"GothamBook" size:12]];
        tomorrowLabel.textColor = [UIColor whiteColor];
        tomorrowLabel.backgroundColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1];
        tomorrowLabel.textAlignment = NSTextAlignmentCenter;
        [whiteView addSubview:tomorrowLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(189,36,120,14)];
        timeLabel.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindMin"]];
        [timeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        timeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1];
        [whiteView addSubview:timeLabel];
    }
    //Not "Today" || "Tomorrow"
    else{
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(85,36,120,14)];
        timeLabel.text = [NSString stringWithFormat:@"%@:%@", [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindHour"], [[_reminders objectAtIndex:indexPath.row] objectForKey:@"remindMin"]];
        [timeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        timeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1];
        [whiteView addSubview:timeLabel];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 68;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
