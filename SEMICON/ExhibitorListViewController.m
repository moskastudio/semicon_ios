//
//  ExhibitorListViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/23.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorListViewController.h"
#import "MainMenuViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#import "ExhibitorCompanyViewController.h"
#import "ExhibitorProductViewController.h"
#import "ExhibitorBoothNoViewController.h"
#import "ExhibitorProductListTableViewController.h"
#import "ExhibitorBoothNoListTableViewController.h"
#import "ExhibitorIntroTableViewController.h"
#import "SearchViewController.h"

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface ExhibitorListViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate,ExhibitorCompanyViewControllerDelegate,ExhibitorProductViewControllerDelegate,ExhibitorBoothNoViewControllerDelegate> {
    
}

@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;
@property NSString *dataSource;
@property NSMutableArray *exhibitorArray;
@property NSMutableDictionary *exhibitorDictionary;
@property NSString *selectedBoothNumber;
@property NSString *selectedCategoryID;
@property NSIndexPath *selectedFloorIndexPath;

@end

@implementation ExhibitorListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //
    self.view.backgroundColor = [UIColor whiteColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    // Ray 2016/07/05
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}

- (void)viewWillAppear:(BOOL)animated {

    // Ray 2016/07/05
    if (_controllerCompany != nil) {
        [_controllerCompany reloadIndexPath];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    //[[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
- (void)onFetchAllExhibitorSuccessNotification:(NSNotification*) notify {
    self.exhibitorArray = [[NSMutableArray alloc] initWithArray:notify.object];
    
    BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
    [self.view addSubview:pagingView];
    [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
    }];
    pagingView.dataSource = self;
    pagingView.delegate = self;
    self.pagingView = pagingView;
    [self setupTopSegment];
}
*/

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Company", "") , NSLocalizedString(@"Product", ""),NSLocalizedString(@"BoothNo.", "")]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 3;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            ExhibitorCompanyViewController *exhibitorCompany = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorCompanyViewController"];
            self.controllerCompany = exhibitorCompany;
            //[self.controllerCompany setExhibitorArray:self.exhibitorArray];
            self.controllerCompany.delegate = self;
            return exhibitorCompany.view;
        }
            break;
        case 1:{
            ExhibitorProductViewController *exhibitorProduct = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorProductViewController"];
            self.controllerProduct = exhibitorProduct;
            //[self.controllerProduct setExhibitorArray:self.exhibitorArray];
            self.controllerProduct.delegate = self;
            return exhibitorProduct.view;
        }
            break;
        case 2:{
            ExhibitorBoothNoViewController *exhibitorBoothNo = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorBoothNoViewController"];
            self.controllerBoothNo = exhibitorBoothNo;
            //[self.controllerBoothNo setExhibitorArray:self.exhibitorArray];
            self.controllerBoothNo.delegate = self;
            return exhibitorBoothNo.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showExhibitorIntro"]) {
        if ([_dataSource isEqualToString:@"ExhibitorCompany"]) {
            ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [segue destinationViewController];
            exhibitorIntroTableViewController.navBarTitle = NSLocalizedString(@"Exhibitor Company", "") ;
            [exhibitorIntroTableViewController setBoothNumber:self.selectedBoothNumber];
        }
    }
    else if ([[segue identifier] isEqualToString:@"showExhibitorProductList"]) {
        if ([_dataSource isEqualToString:@"ExhibitorProduct"]) {
            ExhibitorProductListTableViewController *exhibitorProductListTableViewController = [segue destinationViewController];
            exhibitorProductListTableViewController.navBarTitle = NSLocalizedString(@"ExhibitorProduct", "") ;
            [exhibitorProductListTableViewController setCategoryID:self.selectedCategoryID];
        }
    }
    else if ([[segue identifier] isEqualToString:@"showExhibitorBoothNoList"]) {
        if ([_dataSource isEqualToString:@"ExhibitorBoothNo"]) {
            ExhibitorBoothNoListTableViewController *exhibitorBoothNoListTableViewController = [segue destinationViewController];
            exhibitorBoothNoListTableViewController.selectedFloorIndexPath = _selectedFloorIndexPath;
            exhibitorBoothNoListTableViewController.navBarTitle = NSLocalizedString(@"ExhibitorBoothNo", "") ;
        }
    }
}

- (NSDictionary*) getExhibitor:(NSString*) boothNumber{
    NSDictionary *returnDict = [NSDictionary dictionary];
    for (NSDictionary *exhibitor in self.exhibitorArray) {
        NSString *num = [NSString stringWithFormat:@"%@", [exhibitor objectForKey:@"BoothNumber"]];
        boothNumber = [NSString stringWithFormat:@"%@", boothNumber];
        if ([num isEqualToString:boothNumber]) {
            returnDict = exhibitor;
        }
    }
    return returnDict;
}

-(void)showExhibitorIntro:(NSString*) boothNumber{
    _dataSource = @"ExhibitorCompany";
    NSLog(@"showExhibitorIntro:%@", boothNumber);
    self.selectedBoothNumber = boothNumber;
    [self performSegueWithIdentifier:@"showExhibitorIntro" sender:self];
}

-(void)showExhibitorProductList:(NSString*) categoryID{
    _dataSource = @"ExhibitorProduct";
    NSLog(@"showExhibitorProductList:%@", categoryID);
    self.selectedCategoryID = categoryID;
    [self performSegueWithIdentifier:@"showExhibitorProductList" sender:self];
}

-(void)showExhibitorBoothNoList:(NSIndexPath *)indexPath{
    _dataSource = @"ExhibitorBoothNo";
    _selectedFloorIndexPath = indexPath;
//    NSLog(@"showExhibitorBoothNoList:%ld", index);
    [self performSegueWithIdentifier:@"showExhibitorBoothNoList" sender:self];
}

- (IBAction)BackBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)searchBtn:(id)sender {
    SearchViewController *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchViewController"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:svc];
    [self presentViewController:nav animated:true completion:^{
        
    }];
}
@end
