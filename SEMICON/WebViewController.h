//
//  WebViewController.h
//  semicon
//
//  Created by MuRay Lin on 2016/7/15.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController <UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property NSString *urlString;
@property NSString *navTitle;

- (IBAction)BackBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@end
