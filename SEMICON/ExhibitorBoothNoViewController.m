//
//  ExhibitorBoothNoViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorBoothNoViewController.h"
#import "Service.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>

@interface ExhibitorBoothNoViewController () {
    NSArray *adArray;
}
@property NSMutableArray *boothNoArray;
@property NSString *selectedItem;
@end

@implementation ExhibitorBoothNoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //-------ADSlider Satrt
    [[Service sharedInstance] fetchAllADWithComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            adArray = [[NSArray alloc] initWithArray:[responseDict objectForKey:@"TopBannerLg"]];
            
            NSMutableArray *imageUrls = [[NSMutableArray alloc] init];
            
            for (NSDictionary *ad in adArray) {
                if (![[ad objectForKey:@"imgUrl"] isEqualToString:@""]) {
                    [imageUrls addObject:[NSURL URLWithString:[ad objectForKey:@"imgUrl"]]];
                }
            }
            
            MCLScrollViewSlider *localScrollView = [MCLScrollViewSlider scrollViewSliderWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 432*self.view.frame.size.width/1080) imageURLs:imageUrls placeholderImage:[UIImage imageNamed:@"img_action_bar_bg.png"]];
            localScrollView.pageControlCurrentIndicatorTintColor = [UIColor whiteColor];
            [self.ADView addSubview:localScrollView];
            
            [localScrollView didSelectItemWithBlock:^(NSInteger clickedIndex) {
                NSLog(@"Clicked inner_ad_%ld", clickedIndex+1);
                [FBSDKAppEvents logEvent:FBSDKAppEventNameViewedContent
                              parameters:@{ FBSDKAppEventParameterNameContentID:[NSString stringWithFormat:@"inner_ad_%ld", clickedIndex+1],
                                            FBSDKAppEventParameterNameDescription:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"] } ];
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[[adArray objectAtIndex:clickedIndex] objectForKey:@"actionUrl"]]];
            }];
        }
    }];
    //-------ADSlider End
    
    NSArray *firstFloorArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Booth：2000 ~ 2500", "") ,NSLocalizedString(@"Booth：2501 ~ 3000", ""),NSLocalizedString(@"Booth：3001 ~ 3500", ""), nil];
    NSArray *fourthFloorArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"Booth：1 ~ 500", ""),NSLocalizedString(@"Booth：501 ~ 1000", ""),NSLocalizedString(@"Booth：1001 ~ 1500", ""),nil];
    self.boothNoArray = [[NSMutableArray alloc] initWithObjects:fourthFloorArray, firstFloorArray, nil];
    // Do any additional setup after loading the view.
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.boothNoArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.boothNoArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BoothNoCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 12, self.view.frame.size.width-114, 20)];
    companyLabel.text = [[self.boothNoArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [companyLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
    companyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
    companyLabel.textAlignment = NSTextAlignmentLeft;
    [cell addSubview:companyLabel];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
    
    self.selectedItem = [[self.boothNoArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSLog(@"self.selectedItem:%@",self.selectedItem);
    if ([_delegate respondsToSelector:@selector(showExhibitorBoothNoList:)]) {
        [_delegate showExhibitorBoothNoList:indexPath];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(36, 0, tableView.frame.size.width-72, 44)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(36, 10, sectionHeaderView.frame.size.width, 24)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = NSLocalizedString(@"4F", "");
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = NSLocalizedString(@"1F", "");
            return sectionHeaderView;
            break;
        default:
            break;
    }
    
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
