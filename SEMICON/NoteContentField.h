//
//  NoteContentField.h
//  semicon
//
//  Created by MuRay Lin on 2016/7/9.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NoteContentField : UITextField

@end
