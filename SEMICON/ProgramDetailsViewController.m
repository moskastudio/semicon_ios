//
//  ProgramDetailsViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/2.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ProgramDetailsViewController.h"
#import "AddCalendarViewController.h"

#import "BSSegmentPagingView.h"
#import "Masonry.h"
#import "DZNSegmentedControl.h"

#import "ProgramIntroViewController.h"
#import "ProgramAgendaViewController.h"
#import "ProgramMapImageViewController.h"
#import "ProgramSpeakerViewController.h"
#import "Service.h"
#import "WebViewController.h"
#import "RegisterViewController.h";

#define ColorWithRGB(r, g, b) [UIColor colorWithRed: (r) / 255.0f green: (g) / 255.0f blue: (b) / 255.0f alpha:1.0]

@interface ProgramDetailsViewController ()<BSSegmentPagingViewDataSource, BSSegmentPagingViewDelegate, ProgramIntroViewControllerDelegate, ProgramAgendaViewControllerDelegate>
@property (weak, nonatomic) DZNSegmentedControl *segmentControl;
@property (weak, nonatomic) BSSegmentPagingView *pagingView;
@property NSDictionary *forumInfo;
@property NSString *speakerLink;

@end

@implementation ProgramDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.title = _navBarTitle;
    self.title = NSLocalizedString(@"Program", "") ;
    
    [self fetchForum];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    if (_controllerIntro != nil) {
        [_controllerIntro refreshCell];
    }
}

- (void)fetchForum {
    [[Service sharedInstance] fetchForumByID:_node withComplete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            
            _forumInfo = [[NSDictionary alloc] initWithDictionary:responseDict];
            
            BSSegmentPagingView *pagingView = [[BSSegmentPagingView alloc] init];
            [self.view addSubview:pagingView];
            [pagingView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.bottom.left.right.equalTo(self.view);
            }];
            pagingView.dataSource = self;
            pagingView.delegate = self;
            self.pagingView = pagingView;
            
            [self setupTopSegment];
        }
    }];
}

#pragma - mark Setup Methods

- (void)setupTopSegment {
    
    DZNSegmentedControl *segmentControl = [[DZNSegmentedControl alloc] initWithItems:@[NSLocalizedString(@"Introduction", "") , NSLocalizedString(@"Agenda", "") ]];
    self.segmentControl = segmentControl;
    [self.view addSubview:segmentControl];
    
    [segmentControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.pagingView.mas_top);
    }];
    
    segmentControl.bouncySelectionIndicator = YES;
    segmentControl.adjustsFontSizeToFitWidth = NO;
    segmentControl.autoAdjustSelectionIndicatorWidth = NO;
    segmentControl.showsCount = NO;
    
    [segmentControl setBackgroundColor:[UIColor whiteColor]];
    [segmentControl setTintColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setHairlineColor:ColorWithRGB(169, 0, 131)];
    [segmentControl setFont:[UIFont fontWithName:@"GothamMedium" size:13.0]];
    [segmentControl setSelectionIndicatorHeight:1.5];
    [segmentControl setAnimationDuration:0.125];
    
    [self.segmentControl addTarget:self action:@selector(handleSegmentAction:) forControlEvents:UIControlEventValueChanged];
    
    self.segmentControl.selectedSegmentIndex = 0;
    [self handleSegmentAction:self.segmentControl];
    
}

#pragma - mark Actions
- (void)handleSegmentAction:(DZNSegmentedControl *)topSegment {
    self.pagingView.selectedIndex = topSegment.selectedSegmentIndex;
}

#pragma - mark BSSegmentPagingViewDelegate
- (void)bsPagingView:(BSSegmentPagingView *)pagingView didScrollToPage:(NSUInteger)pageIndex {
    self.segmentControl.selectedSegmentIndex = pageIndex;
}

#pragma - mark BSSegmentPagingViewDataSource
- (NSUInteger)numberOfPageInPagingView:(BSSegmentPagingView *)pagingView {
    return 2;
}

- (UIView *)pageAtIndex:(NSUInteger)index {
    UIView *view = [[UIView alloc] init];
    
    switch (index) {
        case 0:{
            ProgramIntroViewController *programIntroViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramIntroViewController"];
            programIntroViewController.forumInfo = _forumInfo;
            self.controllerIntro = programIntroViewController;
            
            self.controllerIntro.delegate = self;
            return programIntroViewController.view;
        }
            break;
        case 1:{
            ProgramAgendaViewController *programAgendaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramAgendaViewController"];
            programAgendaViewController.delegate = self;
            programAgendaViewController.forumInfo = _forumInfo;
            self.controllerAgenda = programAgendaViewController;
            return programAgendaViewController.view;
        }
            break;
            
        default:
            break;
    }
    return view;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"showSpeaker"]) {
        ProgramSpeakerViewController *programSpeakerViewController = [segue destinationViewController];
        programSpeakerViewController.link = self.speakerLink;
    }
}

#pragma - ProgramSpeakerDelegate
-(void) showSpearkerDetail:(NSString*)link{
    NSLog(@"showSpearkerDetail:%@",link);
    self.speakerLink = link;
    [self performSegueWithIdentifier:@"showSpeaker" sender:self];
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)showMap {
    ProgramMapImageViewController *pmivc = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramMapImageViewController"];
    [self.navigationController pushViewController:pmivc animated:true];
}

- (void)showCalender:(NSDictionary *)forumInfo {
    AddCalendarViewController *addCalendarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCalendarViewController"];
    addCalendarViewController.scheduleName = [_forumInfo objectForKey:@"forumName"];
    addCalendarViewController.type = @"Program";
    addCalendarViewController.typeID = [NSString stringWithFormat:@"%@",[_forumInfo objectForKey:@"node"]];
    NSString *date = [_forumInfo objectForKey:@"date"];
    addCalendarViewController.timeString = [_forumInfo objectForKey:@"time"];
    
    NSRange date7 = [date rangeOfString:@"7"];
    NSRange date8 = [date rangeOfString:@"8"];
    
    if (date7.location != NSNotFound) {
        addCalendarViewController.dateString = @"9/7";
    }
    else if (date8.location != NSNotFound) {
        addCalendarViewController.dateString = @"9/8";
    }
    else {
        addCalendarViewController.dateString = @"9/9";
    }
    
    [self.navigationController pushViewController:addCalendarViewController animated:YES];
}

- (void)showRegister {
    RegisterViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    vc.title = NSLocalizedString(@"Register", @"");
    [self.navigationController pushViewController:vc animated:true];
}

- (void)showWebView:(NSString *)urlString navTitle:(NSString *)navTitle {
    WebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    wvc.urlString = urlString;
    wvc.navTitle = navTitle;
    [self.navigationController pushViewController:wvc animated:true];
}
@end
