//
//  AddCalendarViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/23.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AddCalendarViewController.h"
#import <EventKit/EventKit.h>

@interface AddCalendarViewController () {
    NSString *eventID;
    NSString *remindTime;
    UIDatePicker *datePicker;
    UIDatePicker *timePicker;
    NSString *month;
    NSString *day;
    NSString *hours;
    NSString *mins;
    NSString *aHour;
    NSString *ampm;
    NSDictionary *currentInfo;
}

@end

@implementation AddCalendarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self checkCalendar]) {
        eventID = [currentInfo objectForKey:@"eventID"];
        month = [currentInfo objectForKey:@"month"];
        day = [currentInfo objectForKey:@"day"];
        ampm = [currentInfo objectForKey:@"ampm"];
        aHour = [currentInfo objectForKey:@"aHour"];
        mins = [currentInfo objectForKey:@"remindMin"];
        hours = [currentInfo objectForKey:@"remindHour"];
        _timeString = [currentInfo objectForKey:@"eventTime"];
        
        [_oneDaySwitch setOn:[[currentInfo objectForKey:@"oneDay"] boolValue]];
        [_twoHourSwitch setOn:[[currentInfo objectForKey:@"twoHour"] boolValue]];
        [_thirtyMinSwitch setOn:[[currentInfo objectForKey:@"thirtyMin"] boolValue]];
        [_addCalendarSwitch setOn:[[currentInfo objectForKey:@"addCalendar"] boolValue]];
        
        _dateString = [NSString stringWithFormat:@"%@/%@", month, day];
        
        _dateLabel.text = _dateString;
        _timeLabel.text = [NSString stringWithFormat:@"%@%@:%@", ampm, aHour, mins];
        
        if ([[currentInfo objectForKey:@"type"] isEqualToString:@"Program"]) {
            _dateLabel.enabled = false;
            _timeLabel.enabled = false;
        }
    }
    else {

//        ampm = [currentInfo objectForKey:@"ampm"];
//        aHour = [currentInfo objectForKey:@"aHour"];
//        mins = [currentInfo objectForKey:@"remindMin"];
//        hours = [currentInfo objectForKey:@"remindHour"];

        
        eventID = @"";
        
        if (_dateString == nil) {
            _dateString = @"";
        }
        else {
            _dateLabel.text = _dateString;
            
            NSArray *dateComponent = [_dateString componentsSeparatedByString:@"/"];
            month = [dateComponent firstObject];
            day = [dateComponent lastObject];
            _dateLabel.enabled = false;
        }
        
        if (_timeString == nil) {
            _timeString = @"";
        }
        else {
            _timeString = [_timeString stringByReplacingOccurrencesOfString:@" " withString:@""];
            _timeString = [_timeString stringByReplacingOccurrencesOfString:@" " withString:@""];
            _timeString = [_timeString stringByReplacingOccurrencesOfString:@"－" withString:@"-"];
            _timeString = [_timeString stringByReplacingOccurrencesOfString:@"–" withString:@"-"];
            _timeString = [_timeString stringByReplacingOccurrencesOfString:@"：" withString:@":"];
            
            NSArray *timeComponent = [_timeString componentsSeparatedByString:@"-"];
            NSString *eventStartTime = [timeComponent firstObject];
            
            NSArray *eventTimeComponent = [eventStartTime componentsSeparatedByString:@":"];
            hours = [eventTimeComponent firstObject];
            mins = [eventTimeComponent lastObject];
            
            if ([hours hasPrefix:@"0"]) {
                aHour = [hours substringFromIndex:hours.length - 1];
            }
            else {
                aHour = hours;
            }
            
            if ([hours integerValue] < 12) {
                if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
                    ampm = @"上午";
                }
                else {
                    ampm = @"am";
                }
            }
            else {
                if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
                    ampm = @"下午";
                }
                else {
                    ampm = @"pm";
                }
            }
            
            _timeLabel.text = [NSString stringWithFormat:@"%@%@:%@", ampm, aHour, mins];
            _timeLabel.enabled = false;
        }
    }
    
    [self setDatePicker];
    [self setTimePicker];
    _eventTitleLabel.text = _scheduleName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDatePicker {
    datePicker = [[UIDatePicker alloc]init];
    
    [datePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    [formatter setDateFormat:@"yyyy-M-d"];
    NSDate *minimimDate = [formatter dateFromString:@"2016-9-7"];
    datePicker.minimumDate = minimimDate;
    
    NSDate *maximumDate = [formatter dateFromString:@"2016-9-9"];
    
    datePicker.maximumDate = maximumDate;
    
    _dateLabel.inputView = datePicker;
    
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self
                                                                          action:@selector(cancelDatePicker)];
    
    toolBar.items = [NSArray arrayWithObject:right];
    
    _dateLabel.inputAccessoryView = toolBar;
}

- (void)setTimePicker {
    timePicker = [[UIDatePicker alloc]init];
    
    [timePicker setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    timePicker.datePickerMode = UIDatePickerModeTime;
    
    _timeLabel.inputView = timePicker;
    
    // 建立 UIToolbar
    UIToolbar *toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self
                                                                          action:@selector(cancelTimePicker)];
    
    toolBar.items = [NSArray arrayWithObject:right];
    
    _timeLabel.inputAccessoryView = toolBar;
}

- (void)cancelDatePicker {
    [_dateLabel resignFirstResponder];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    
    NSDate *date = [datePicker date];
    
    [dateFormat setDateFormat:@"LLLL"];
    month = [dateFormat stringFromDate:date];
    month = [month substringToIndex:month.length - 1];
    
    [dateFormat setDateFormat:@"d"];
    day = [dateFormat stringFromDate:date];
    
    _dateString = [NSString stringWithFormat:@"%@/%@", month, day];
    _dateLabel.text = _dateString;
}

- (void)cancelTimePicker {
    [_timeLabel resignFirstResponder];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
    
    NSDate *date = [timePicker date];
    
    [dateFormat setDateFormat:@"HH"];
    hours = [dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"mm"];
    mins = [dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"h"];
    aHour = [dateFormat stringFromDate:date];
    
    [dateFormat setDateFormat:@"a"];
    ampm = [dateFormat stringFromDate:date];
    
//    _timeString = [NSString stringWithFormat:@"%@:%@", hours, mins];
    _timeLabel.text = [NSString stringWithFormat:@"%@%@:%@", ampm, aHour, mins];
}

//- (NSString *)convertDate:(NSDate *)date {
//    
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZZ"];
//    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];

//    NSString *
//    NSDate *_date = [dateFormat dateFromString:str];
//    
    // Convert date object to desired output format
//    [dateFormat setDateFormat:@"LLLL"];
//    NSString *monthStr = [dateFormat stringFromDate:_date];
//    monthStr = [monthStr substringToIndex:monthStr.length - 1];
//    self.month.text = monthStr;
//    [dateFormat setDateFormat:@"EEE"];
//    self.week.text = [dateFormat stringFromDate:_date];
//    [dateFormat setDateFormat:@"d"];
//    self.day.text = [dateFormat stringFromDate:_date];

//    [dateFormat setDateFormat:@"h:mm"];
//    self.time.text = [dateFormat stringFromDate:_date];
//
//    [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
//    [dateFormat setDateFormat:@"a"];
//    self.ampm.text = [dateFormat stringFromDate:_date];
//}

- (IBAction)cancelBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBtn:(id)sender {
    
    if ([_dateLabel.text isEqualToString:@""] || [_timeLabel.text isEqualToString:@""]) {
        return;
    }
    
    if ([_addCalendarSwitch isOn]) {
        [self addToCalendar];
    }
    else {
        [self setLocalPush];
    }
}

- (void)addToCalendar {
    [self removeEventFromCalendar:eventID];
    
    EKEventStore *store = [EKEventStore new];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"加入行事曆失敗" message:@"請至隱私權設定中開啟行事曆權限" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
            return;
        }
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-M-d HH:mm"];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
        
        NSDate *date = [dateFormat dateFromString:[NSString stringWithFormat:@"2016-%@-%@ %@:%@", month, day, hours, mins]];
        
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = _scheduleName;
        event.startDate = date;
        event.endDate = [event.startDate dateByAddingTimeInterval:60 * 60];  //set 1 hour meeting
        event.calendar = [store defaultCalendarForNewEvents];
        NSError *err = nil;
        
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
        eventID = event.eventIdentifier;
        
        [self setLocalPush];
        
    }];
}

- (void)setLocalPush {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self cancelLocalPush:_typeID];
        
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-M-d HH:mm"];
        [dateFormat setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_TW"]];
        remindTime = [NSString stringWithFormat:@"%@:%@", hours, mins];
        
        NSDate *date = [dateFormat dateFromString:[NSString stringWithFormat:@"2016-%@-%@ %@:%@", month, day, hours, mins]];
        
        // code here
        if ([_oneDaySwitch isOn]) {
            UILocalNotification *notifyAlarm = [[UILocalNotification alloc] init];
            notifyAlarm.fireDate = [NSDate dateWithTimeInterval:-24 * 3600 sinceDate:date];
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;
            notifyAlarm.alertBody = [NSString stringWithFormat:@"%@將於24小時後開始", _scheduleName];
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:_typeID forKey:@"typeID"];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notifyAlarm];
        }
        
        if ([_twoHourSwitch isOn]) {
            UILocalNotification *notifyAlarm = [[UILocalNotification alloc] init];
            notifyAlarm.fireDate = [NSDate dateWithTimeInterval:-2 * 3600 sinceDate:date];
            notifyAlarm.timeZone = [NSTimeZone defaultTimeZone];
            notifyAlarm.repeatInterval = 0;
            notifyAlarm.alertBody = [NSString stringWithFormat:@"%@將於2小時後開始", _scheduleName];
            notifyAlarm.userInfo = [NSDictionary dictionaryWithObject:_typeID forKey:@"typeID"];
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notifyAlarm];
        }
        
        if ([_thirtyMinSwitch isOn]) {
            UILocalNotification *notifyAlarm = [[UILocalNotification alloc] init];
            notifyAlarm.fireDate = [NSDate dateWithTimeInterval:-30 * 60 sinceDate:date];
            notifyAlarm.timeZone = [NSTimeZone systemTimeZone];
            notifyAlarm.repeatInterval = 0;
            notifyAlarm.alertBody = [NSString stringWithFormat:@"%@將於30分鐘後開始", _scheduleName];
            notifyAlarm.userInfo = @{
                                     @"typeID": _typeID,
                                     @"type": _type
                                     };
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notifyAlarm];
        }
        
        [self saveInfo];
        [self.navigationController popViewControllerAnimated:YES];
    });
}

- (void)saveInfo {
    
    NSDictionary *newReminder = @{
                                  @"Name": _scheduleName,
                                  @"type": _type,
                                  @"typeID": _typeID,
                                  @"eventID": eventID,
                                  @"date": _dateString,
                                  @"eventTime": _timeString,
                                  @"remindHour": hours,
                                  @"remindMin": mins,
                                  @"aHour": aHour,
                                  @"ampm": ampm,
                                  @"month": month,
                                  @"day": day,
                                  @"oneDay": [NSNumber numberWithBool:[_oneDaySwitch isOn]],
                                  @"twoHour": [NSNumber numberWithBool:[_twoHourSwitch isOn]],
                                  @"thirtyMin": [NSNumber numberWithBool:[_thirtyMinSwitch isOn]],
                                  @"addCalendar": [NSNumber numberWithBool:[_addCalendarSwitch isOn]]
                                  };
    
    if ([self checkCalendar]) {
        [self replaceReminder:newReminder];
    }
    else {
        NSMutableArray *reminders = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
        
        [reminders addObject: newReminder];
        [[NSUserDefaults standardUserDefaults] setObject:reminders forKey:@"Reminder"];
    }
}

- (NSDate *) dateByAddingHours: (NSInteger) dHours
{
    NSTimeInterval aTimeInterval = [NSDate timeIntervalSinceReferenceDate] + 5 * dHours;
    NSDate *newDate = [NSDate dateWithTimeIntervalSinceReferenceDate:aTimeInterval];
    return newDate;
}

- (void)removeEventFromCalendar:(NSString *)removeEventID {
    EKEventStore *store = [EKEventStore new];
    
    EKEvent *event = [store eventWithIdentifier:removeEventID];
    
    if (event != nil) {
        NSError *error = nil;
        [store removeEvent:event span:EKSpanThisEvent commit:YES error:&error];
    }
}

- (void)replaceReminder:(NSDictionary *)newReminder {
    NSMutableArray *reminders = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"]];
    
    for (int i = 0; i < reminders.count; i++) {
        NSDictionary *reminder = [reminders objectAtIndex:i];
        
        if ([[reminder objectForKey:@"typeID"] isEqualToString:_typeID] && [[reminder objectForKey:@"type"] isEqualToString:_type]) {
            
            [reminders replaceObjectAtIndex:i withObject:newReminder];
            
            break;
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:reminders forKey:@"Reminder"];
}

- (void)cancelLocalPush:(NSString *)typeID {
    NSArray *localNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    for (UILocalNotification *local in localNotifications) {
        NSDictionary *userInfo = local.userInfo;
        
        if ([[userInfo objectForKey:@"typeID"] isEqualToString:typeID]) {
            [[UIApplication sharedApplication] cancelLocalNotification:local];
        }
    }
}

- (Boolean)checkCalendar {
    NSArray *reminders = [[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"];
    
    for (NSDictionary *reminder in reminders) {
        if ([[reminder objectForKey:@"typeID"] isEqualToString:_typeID] && [[reminder objectForKey:@"type"] isEqualToString:_type]) {
            currentInfo = [[NSDictionary alloc] initWithDictionary:reminder];
            return true;
        }
    }
    return false;
}
@end
