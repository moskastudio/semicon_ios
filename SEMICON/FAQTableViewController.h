//
//  FAQTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/24.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FAQTableViewController : UITableViewController

- (IBAction)indexBtn:(id)sender;
- (IBAction)backBtn:(id)sender;

@end
