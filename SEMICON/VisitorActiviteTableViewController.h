//
//  VisitorActiviteTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VisitorActiviteTableViewDelegate <NSObject>

- (void) showVisitorActiviteDetail:(NSInteger)index;

@end


@interface VisitorActiviteTableViewController : UITableViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property id<VisitorActiviteTableViewDelegate> delegate;

@property NSString *dataSource;
@property NSString *navBarTitle;
@property NSInteger index;

@end
