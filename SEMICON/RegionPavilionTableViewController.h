//
//  RegionPavilionTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegionPavilionTableDelegate <NSObject>

- (void) showRegionPavilionDetail:(NSInteger)index;

@end

@interface RegionPavilionTableViewController : UITableViewController

@property id <RegionPavilionTableDelegate> delegate;

@end
