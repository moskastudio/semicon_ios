//
//  ProgramSpeakerViewController.h
//  semicon
//
//  Created by aisoter on 2016/8/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramSpeakerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property NSString *string;
@property NSString *link;

@end
