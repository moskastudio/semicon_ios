//
//  ExhibitorListTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/8.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitorProductListTableViewController : UITableViewController
- (IBAction)indexBtn:(id)sender;
- (IBAction)backBtn:(id)sender;
@property NSString *navBarTitle;
@property NSString *categoryID;
@property NSMutableArray *exhibitorArray;
@property NSMutableDictionary *exhibitorDictionary;
@end
