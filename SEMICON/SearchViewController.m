//
//  SearchViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/30.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "SearchViewController.h"
#import "Service.h"
#import "ExhibitorIntroTableViewController.h"
#import "ProductIntroTableViewController.h"
#import "ProgramDetailsViewController.h"

@interface SearchViewController ()<UISearchBarDelegate>
@property UIView *coverView;
@property NSArray *tableTestArray;
@property NSArray *searchResult;
@property NSMutableArray *exhibitors;
@property NSMutableArray *products;
@property NSMutableArray *forums;

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = true;
    self.coverView = [[UIView alloc] initWithFrame:
                      CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    self.coverView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UIView *departureTimePopUpView = [[UIView alloc]
                                      initWithFrame:CGRectMake(self.view.frame.size.width/15, (self.view.frame.size.height-64)/15*2, self.view.frame.size.width/15*13, (self.view.frame.size.height-64)/15*13)];
    departureTimePopUpView.backgroundColor = [UIColor whiteColor];
    departureTimePopUpView.layer.cornerRadius = 10.0;
    departureTimePopUpView.clipsToBounds = YES;
    [self.coverView addSubview:departureTimePopUpView];
    
    // Change searchbar text color
    for (UIView *subView in self.searchBar.subviews)
    {
        for (UIView *secondLevelSubview in subView.subviews){
            if ([secondLevelSubview isKindOfClass:[UITextField class]])
            {
                UITextField *searchBarTextField = (UITextField *)secondLevelSubview;
                
                //set font color here
                searchBarTextField.textColor = [UIColor whiteColor];
                
                break;
            }
        }
    }
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(departureTimePopUpView.frame.size.width/2-90, 10, 180, 40)];
    titleLabel.text = NSLocalizedString(@"Search Scope", "") ;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:17];
    titleLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [departureTimePopUpView addSubview:titleLabel];
    
    UIView *lineUp = [[UIView alloc] initWithFrame: CGRectMake(0, 60, departureTimePopUpView.frame.size.width, 1.5)];
    lineUp.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [departureTimePopUpView addSubview:lineUp];
    
    
    UILabel *content = [[UILabel alloc] initWithFrame:CGRectMake(36, 70, departureTimePopUpView.frame.size.width-72, departureTimePopUpView.frame.size.height-140)];
    NSString *contentString =NSLocalizedString(@"Booth No.\rExhibitor Name\rProduct Name\rProgram Name\rProgram Themes\rSpearker Name\rSpearker Company\rEvent Name","");
    content.font = [UIFont fontWithName:@"GothamBook" size:20];
    content.numberOfLines = 0;
    content.lineBreakMode = NSLineBreakByWordWrapping;
    content.textAlignment = NSTextAlignmentLeft;
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:15];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
    content.attributedText = attributedString ;
    content.adjustsFontSizeToFitWidth = YES;
    content.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
    [departureTimePopUpView addSubview:content];
    
    UIView *lineDown = [[UIView alloc] initWithFrame: CGRectMake(0, departureTimePopUpView.frame.size.height-60, departureTimePopUpView.frame.size.width, 1.5)];
    lineDown.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [departureTimePopUpView addSubview:lineDown];
    
    UIButton *backBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    backBtn.frame = CGRectMake(departureTimePopUpView.frame.size.width/2-100, departureTimePopUpView.frame.size.height-50, 200, 40);
    [backBtn setTitle:NSLocalizedString(@"OK","") forState:UIControlStateNormal];
    backBtn.tintColor = [UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    backBtn.titleLabel.font = [UIFont fontWithName:@"GothamMedium" size:17];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [departureTimePopUpView addSubview:backBtn];
    [self.view addSubview:self.coverView];
    self.coverView.hidden = YES;

    _tableTestArray = [[NSArray alloc ] initWithObjects:@"Booth No.",@"Exhibitor Name",@"Product Name",@"Program Name",@"Program Themes",@"Spearker Name",@"Spearker Company",@"Event Name", nil];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationController.navigationBarHidden = true;
}

- (void)viewDidDisappear:(BOOL)animated {
    self.navigationController.navigationBarHidden = false;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_searchResult count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [[_searchResult objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CompanyCell" forIndexPath:indexPath];
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
                else if ([view isKindOfClass:[UIView class]]) {
                    [view removeFromSuperview];
                }
            }

            NSDictionary *exhibitor = [_exhibitors objectAtIndex:indexPath.row];
            
            UILabel *companyLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 24, self.view.frame.size.width-40, 20)];
            companyLabel.text = [exhibitor objectForKey:@"CompanyName"];
            [companyLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
            companyLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            companyLabel.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:companyLabel];
            
            NSString *boothNumber = [exhibitor objectForKey:@"BoothNumber"];
            
            UILabel *boothNoLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 50, 120, 20)];
            boothNoLabel.text = [NSString stringWithFormat:@"Booth : %@", boothNumber];
            [boothNoLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
            boothNoLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
            boothNoLabel.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:boothNoLabel];
            
            // This is how you change the background color
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor clearColor];
            [cell setSelectedBackgroundView:bgColorView];
            
            return cell;
        }
            break;
            
        case 1: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
                else if ([view isKindOfClass:[UIView class]]) {
                    [view removeFromSuperview];
                }
            }
            
            NSDictionary *product = [_products objectAtIndex:indexPath.row];
            
            UILabel *productLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 12, self.view.frame.size.width-40, 20)];
            productLabel.text = [product objectForKey:@"Name"];
            [productLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
            productLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            productLabel.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:productLabel];
            
            // This is how you change the background color
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor clearColor];
            [cell setSelectedBackgroundView:bgColorView];
            
            return cell;
        }
            break;
            
        case 2: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ForumCell" forIndexPath:indexPath];
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
                else if ([view isKindOfClass:[UIView class]]) {
                    [view removeFromSuperview];
                }
            }
            
            NSDictionary *forum = [_forums objectAtIndex:indexPath.row];
            
            UILabel *forumLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 12, self.view.frame.size.width-40, 20)];
            forumLabel.text = [forum objectForKey:@"title"];
            [forumLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
            forumLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
            forumLabel.textAlignment = NSTextAlignmentLeft;
            [cell addSubview:forumLabel];
            
            // This is how you change the background color
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor clearColor];
            [cell setSelectedBackgroundView:bgColorView];
            
            return cell;
        }
            break;
            
        default: {
            static NSString *simpleTableIdentifier = @"SearchCell";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
            for (UIView *view in cell.subviews) {
                if ([view isKindOfClass:[UILabel class]]) {
                    [view removeFromSuperview];
                }
                else if ([view isKindOfClass:[UIView class]]) {
                    [view removeFromSuperview];
                }
            }
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
            }
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor clearColor];
            [cell setSelectedBackgroundView:bgColorView];
            return cell;
        }
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(36, 0, tableView.frame.size.width-72, 44)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(36, 10, sectionHeaderView.frame.size.width, 24)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentLeft;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    
    switch (section) {
        case 0:
            headerLabel.text = @"Exhibitor";
            break;
        
        case 1:
            headerLabel.text = @"Product";
            break;
            
        case 2:
            headerLabel.text = @"Program";
            break;
        default:
            break;
    }
    [sectionHeaderView addSubview:headerLabel];
    
    return sectionHeaderView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([[_searchResult objectAtIndex:section] count] == 0) {
        return 0;
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0: {
            ExhibitorIntroTableViewController *exhibitorIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ExhibitorIntroTableViewController"];
            exhibitorIntroTableViewController.navBarTitle = NSLocalizedString(@"ExhibitorCompany", "") ;
            [exhibitorIntroTableViewController setBoothNumber:[[[_exhibitors objectAtIndex:indexPath.row] objectForKey:@"BoothNumber"] description]];
            
            [self.navigationController pushViewController:exhibitorIntroTableViewController animated:true];

        }
            break;
        case 1: {
            ProductIntroTableViewController *productIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductIntroTableViewController"];
            productIntroTableViewController.navBarTitle = [[_products objectAtIndex:indexPath.row] objectForKey:@"Name"];
            productIntroTableViewController.productID = [[_products objectAtIndex:indexPath.row] objectForKey:@"productId"];
            
            [self.navigationController pushViewController:productIntroTableViewController animated:true];
        }
            break;
        case 2: {
            ProgramDetailsViewController *programDetailsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProgramDetailsViewController"];
            programDetailsViewController.navBarTitle = [[_forums objectAtIndex:indexPath.row] objectForKey:@"title"];
            programDetailsViewController.node = [[_forums objectAtIndex:indexPath.row] objectForKey:@"node"];
            [self.navigationController pushViewController:programDetailsViewController animated:true];
        }
            break;
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 88;
    }
    return 60;
}
//- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope{
//    NSPredicate *resultPredicate = [NSPredicate
//                                    predicateWithFormat:@"SELF contains[cd] %@",
//                                    searchText];
//    
//    _searchResultArray = [_tableTestArray filteredArrayUsingPredicate:resultPredicate];
//}

#pragma mark - UISearchDisplayController delegate methods
//-(BOOL)searchDisplayController:(UISearchController *)controller shouldReloadTableForSearchString:(NSString *)searchString
//{
//    UISearchController *searchController = [[UISearchController alloc] initWithSearchResultsController:self];
//    [self filterContentForSearchText:searchString
//                               scope:[[searchController.searchBar scopeButtonTitles]
//                                      objectAtIndex:[searchController.searchBar
//                                                     selectedScopeButtonIndex]]];
    
    
//    return YES;
//}

-(void) backAction:(id)sender {
    self.coverView.hidden = YES;
    self.navigationItem.leftBarButtonItem.customView.hidden=NO;
}

- (IBAction)infoBtn:(id)sender {
    self.coverView.hidden = NO;
}

- (IBAction)cancelBtn:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
    [[Service sharedInstance] searchWithKeyword:searchBar.text Complete:^(BOOL success, NSDictionary *responseDict) {
//        NSLog(@"%@", responseDict);
        _exhibitors = [[NSMutableArray alloc] initWithArray:[responseDict objectForKey:@"exhibitors"]];
        
        _products = [[NSMutableArray alloc] initWithArray:[responseDict objectForKey:@"products"]];
        
        _forums = [[NSMutableArray alloc] initWithArray:[responseDict objectForKey:@"forums"]];
        
        _searchResult = [[NSArray alloc] initWithObjects:_exhibitors, _products, _forums, nil];
        [self.tableView reloadData];
    }];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = true;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [searchBar resignFirstResponder];
}

@end
