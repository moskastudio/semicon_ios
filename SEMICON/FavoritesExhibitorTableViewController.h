//
//  FavoritesExhibitorTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoritesExhibitorTableViewControllerDelegate <NSObject>

- (void)showExhibitor:(NSString *)boothNumber;

@end

@interface FavoritesExhibitorTableViewController : UITableViewController
@property id<FavoritesExhibitorTableViewControllerDelegate> delegate;
@end
