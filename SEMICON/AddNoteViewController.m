//
//  AddNoteViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/24.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "AddNoteViewController.h"

@interface AddNoteViewController ()<UITextFieldDelegate> {
    NSDictionary *noteInfo;
}

@end

@implementation AddNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([self checkNote]) {
        _noteTitle.text = [noteInfo objectForKey:@"title"];
        _noteContent.text = [noteInfo objectForKey:@"content"];
    }
    
    _noteTitle.delegate = self;
    _noteContent.delegate = self;
    
    _companyNameLabel.text = _companyName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveBtn:(id)sender {
    
    NSMutableArray *notes = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Note"]];
    
    if (![_noteTitle.text isEqualToString:@""] && ![_noteContent.text isEqualToString:@""]) {
        NSDictionary *newNote = @{
                                   @"imageUrl": _imgUrl,
                                   @"boothNumber": [NSString stringWithFormat:@"%@",_boothNumber],
                                   @"title": _noteTitle.text,
                                   @"content": _noteContent.text,
                                   @"companyName": _companyName
                                   };
        
        BOOL isExist = false;
        for (int i = 0; i < notes.count; i++) {
            NSDictionary *note = [notes objectAtIndex:i];
            
            if ([[note objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
                
                [notes replaceObjectAtIndex:i withObject:newNote];
                isExist = true;
                break;
            }
        }
        
        if (!isExist) {
            [notes addObject:newNote];
        }
    }
    else {
        for (int i = 0; i < notes.count; i++) {
            if ([[[notes objectAtIndex:i] objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
                [notes removeObjectAtIndex:i];
                break;
            }
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:notes forKey:@"Note"];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (Boolean)checkNote {
    NSArray *notes = [[NSUserDefaults standardUserDefaults] objectForKey:@"Note"];
    
    for (NSDictionary *note in notes) {
        if ([[note objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
            
            noteInfo = [[NSDictionary alloc] initWithDictionary:note];
            return true;
        }
    }
    return false;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}
@end
