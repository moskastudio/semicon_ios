//
//  PavilionIntroductionViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/27.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PavilionIntroductionViewController : UIViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property NSString *navBarTitle;
@property NSString *contentString;

@end
