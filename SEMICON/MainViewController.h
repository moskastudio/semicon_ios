//
//  ViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "FloorViewController.h"
#import "ScheduleViewController.h"
#import "PersonalViewController.h"

@interface MainViewController : UIViewController <UIActionSheetDelegate,UIAlertViewDelegate,UINavigationControllerDelegate>

@property(nonatomic, strong) HomeViewController *controllerHome;
@property(nonatomic, strong) FloorViewController *controllerFloor;
@property(nonatomic, strong) ScheduleViewController *controllerSchedule;


@end

