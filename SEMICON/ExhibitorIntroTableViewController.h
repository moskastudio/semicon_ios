//
//  ExhibitorIntroTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/8.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface ExhibitorIntroTableViewController : UITableViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property NSString *navBarTitle;
@property NSDictionary *exhibitor;
@property NSString *boothNumber;

@end
