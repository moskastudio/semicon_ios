//
//  HotelsDetailViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/20.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "HotelsDetailViewController.h"

@interface HotelsDetailViewController ()

@end

@implementation HotelsDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (_dataSource == 0) {
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://taipei.grand.hyatt.com/en/hotel/home.html"]];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.autoresizesSubviews = YES;
        [_webView.scrollView setScrollEnabled:YES];
        [_webView loadRequest:request];
        [self.view addSubview:_webView];
    }
    else if (_dataSource == 1) {
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.humblehousehotels.com/en/"]];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.autoresizesSubviews = YES;
        [_webView.scrollView setScrollEnabled:YES];
        [_webView loadRequest:request];
        [self.view addSubview:_webView];
    }
    else if (_dataSource == 2) {
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.fushin-hotel.com.tw/taipei/en/news.php?cid=1"]];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.autoresizesSubviews = YES;
        [_webView.scrollView setScrollEnabled:YES];
        [_webView loadRequest:request];
        [self.view addSubview:_webView];
    }
    else if (_dataSource == 3) {
        NSURLRequest *request =[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.semicontaiwan.org/en/semicon-taiwan-partnered-hotels"]];
        _webView.backgroundColor = [UIColor clearColor];
        _webView.scalesPageToFit = YES;
        _webView.autoresizesSubviews = YES;
        [_webView.scrollView setScrollEnabled:YES];
        [_webView loadRequest:request];
        [self.view addSubview:_webView];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
