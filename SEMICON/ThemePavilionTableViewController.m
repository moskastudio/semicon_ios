//
//  ThemePavilionTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/25.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ThemePavilionTableViewController.h"
#import "PavilionDetailsViewController.h"

@interface ThemePavilionTableViewController ()

@property NSArray *ThemePavilionsArray;
@property NSString *selectString;
@end

@implementation ThemePavilionTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _ThemePavilionsArray = [[NSArray alloc] initWithObjects:NSLocalizedString(@"AOI Pavilion", "") ,NSLocalizedString(@"CMP Pavilion", ""),NSLocalizedString(@"High-Tech Facility Pavilion", ""),NSLocalizedString(@"Materials Pavilion", ""),NSLocalizedString(@"Precision Machinery Pavilion", ""),NSLocalizedString(@"Secondary Market Pavilion", ""),NSLocalizedString(@"Smart Manufacturing Pavilion", ""),NSLocalizedString(@"Taiwan Localization Pavilion", ""), nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _ThemePavilionsArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ThemePavilionCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *PavilionsLabel=[[UILabel alloc]initWithFrame:CGRectMake(0, 30, self.view.frame.size.width, 34)];
    [PavilionsLabel setFont:[UIFont fontWithName:@"GothamMedium" size:16.5]];
    PavilionsLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
    PavilionsLabel.text = [_ThemePavilionsArray objectAtIndex:indexPath.row];
    PavilionsLabel.textAlignment = NSTextAlignmentCenter;
    [cell addSubview:PavilionsLabel];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 92.5, self.view.frame.size.width, 1.5)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 94;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.row:%ld",indexPath.row);
    self.selectString = [_ThemePavilionsArray objectAtIndex:indexPath.row];
    
    if ([_delegate respondsToSelector:@selector(showThemePavilionDetail:)]) {
        [_delegate showThemePavilionDetail:indexPath.row];
    }
}

//#pragma mark - Navigation
//
//// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    
//    if ([[segue identifier] isEqualToString:@"showPavilionDetails"]) {
//        // Get reference to the destination view controller
//        PavilionDetailsViewController *pavilionDetailsViewController = [segue destinationViewController];
////        pavilionDetailsViewController.getIndex = self.selectString;
//        // Pass any objects to the view controller here, like...
//    }
//    
//}

@end
