//
//  FavoritesEventsTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FavoritesEventsTableViewControllerDelegate <NSObject>

- (void)showEvent:(NSDictionary *)eventInfo dataSource:(NSString *)dataSource;

@end

@interface FavoritesEventsTableViewController : UITableViewController
@property id<FavoritesEventsTableViewControllerDelegate> delegate;
@end
