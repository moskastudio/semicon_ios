//
//  ExhibitionNewsDetailViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/17.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExhibitionNewsDetailViewController : UIViewController<UIWebViewDelegate>

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property NSString *newsTitle;
@property NSString *navBartitle;
@property NSString *partnerWebsite;

@end
