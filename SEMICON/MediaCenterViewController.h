//
//  MediaCenterViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/6.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MediaCenterViewController : UIViewController<UINavigationBarDelegate,UINavigationControllerDelegate>
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@end
