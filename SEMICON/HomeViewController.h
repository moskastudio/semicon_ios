//
//  HomeViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/5.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCLScrollViewSlider.h"

@protocol HomeViewControllerDelegate <NSObject>

- (void)showViewWithStoryBoardID:(NSString *) storyBoardID;

@end

@interface HomeViewController : UIViewController <UIGestureRecognizerDelegate>

@property id <HomeViewControllerDelegate> delegate;

@end
