//
//  SignUpViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "SignUpViewController.h"
#import "Service.h"

@interface SignUpViewController ()<UITextFieldDelegate>

@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    _textField1.delegate = self;
    _textField2.delegate = self;
    _textField3.delegate = self;
    _textField4.delegate = self;
    _textField5.delegate = self;
    
    if (_isFB) {
        _textField3.secureTextEntry = false;
        _textField4.hidden = true;
        _textField4.text = @"123";
        _textField5.hidden = true;
        _textField5.text = @"123";
        
        _line1.hidden = true;
        _line2.hidden = true;
        
        _imageView2.hidden = true;
        _imageView3.hidden = true;
        
        _imageView1.image = [UIImage imageNamed:@"img_phone.png"];
        
        _textField1.text = _name;
        _textField2.text = _email;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelSignBtn:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signUp:(id)sender {

    if ([_textField1.text isEqualToString:@""] || [_textField2.text isEqualToString:@""] || [_textField3.text isEqualToString:@""] || [_textField4.text isEqualToString:@""] || [_textField5.text isEqualToString:@""]) {
        
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Please fill in all the information" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [ac addAction:cancel];
        [self presentViewController:ac animated:true completion:nil];
    }
    else if (!_isFB) {
        if (![_textField3.text isEqualToString:_textField4.text]) {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Password information are not the same" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
        }
        else {
            [[Service sharedInstance] signUpWithName:_textField1.text email:_textField2.text password:_textField3.text mobile:_textField5.text complete:^(BOOL success, NSDictionary *responseDict) {
                
                if ([[[responseDict objectForKey:@"ERR_CODE"] description] isEqualToString:@"5"]) {
                    [[NSUserDefaults standardUserDefaults] setBool:true forKey:kUserLogin];
                    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
                }
                else {
                    UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Registration failed" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        
                    }];
                    [ac addAction:cancel];
                    [self presentViewController:ac animated:true completion:nil];
                }
            }];
        }
    }
    else {
        [[Service sharedInstance] signUpWithFB:_fbID token:_fbToken email:_textField2.text name:_textField1.text mobile:_textField3.text complete:^(BOOL success, NSDictionary *responseDict) {
            
            if ([[[responseDict objectForKey:@"ERR_CODE"] description] isEqualToString:@"5"]) {
                [[NSUserDefaults standardUserDefaults] setBool:true forKey:kUserLogin];
                [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
            }
            else {
                UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"Error" message:@"Registration failed" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                [ac addAction:cancel];
                [self presentViewController:ac animated:true completion:nil];
            }
        }];
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

-(void)animateTextField:(UITextField *)textField up:(BOOL)up
{
    const int movementDistance = 90;
    const float movementDuration = 0.3f;
    int movement = (up?-movementDistance:movementDistance);
    [UIView beginAnimations:@"anim" context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
