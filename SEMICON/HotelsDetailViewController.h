//
//  HotelsDetailViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/6/20.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotelsDetailViewController : UIViewController
- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property NSInteger dataSource;
@end
