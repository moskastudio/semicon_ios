//
//  MyNoteTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/21.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "MyNoteTableViewController.h"
#import "MyNoteDetailViewController.h"
#import "AddNoteViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyNoteTableViewController () {
    BOOL isNeedToRefresh;
}

@property NSArray *titleArray;
@property NSArray *imageArray;
@property NSArray *companyNameArray;
@property NSMutableArray *notes;

@end

@implementation MyNoteTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = NSLocalizedString(@"My Notes", "") ;
    
    _notes = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Note"]];
    
//    self.titleArray = [[NSArray alloc] initWithObjects:@"3dIncites logo web",@"AZONano-logo-web",@"ChipScaleReview-logo-web",@"compound-semiconductor-logo-web",@"GlobalSMT-logo-web",@"compound-semiconductor-logo-web",@"GlobalSMT-logo-web", nil];
//    self.imageArray = [[NSArray alloc] initWithObjects:@"3dIncites-logo-web.jpg",@"AZONano-logo-web.jpg",@"ChipScaleReview-logo-web.gif",@"compound-semiconductor-logo-web.png",@"GlobalSMT-logo-web.jpg",@"compound-semiconductor-logo-web.png",@"GlobalSMT-logo-web.jpg", nil];
//    self.companyNameArray = [[NSArray alloc] initWithObjects:@"3dincites.com",@"azonetwork.com",@"ChipScaleReview",@"compoundsemiconductor",@"Globalsmt",@"compoundsemiconductor",@"Globalsmt", nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    if (isNeedToRefresh) {
        _notes = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"Note"]];
        [self.tableView reloadData];
        
        isNeedToRefresh = false;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _notes.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyNoteCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    UILabel *title=[[UILabel alloc] initWithFrame:CGRectMake(142, 20, cell.frame.size.width-195, 50)];
    [title setFont:[UIFont fontWithName:@"GothamMedium" size:16]];
    title.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
    title.lineBreakMode = NSLineBreakByWordWrapping;
    title.numberOfLines = 0;
    title.textAlignment = NSTextAlignmentLeft;
    NSString *titleString = [[_notes objectAtIndex:indexPath.row] objectForKey:@"title"];
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleString];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [titleString length])];
    title.attributedText = attributedString ;
    title.adjustsFontSizeToFitWidth = NO;
    title.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:title];
    
    UILabel *companyName=[[UILabel alloc] initWithFrame:CGRectMake(142, 75, cell.frame.size.width-195, 12)];
    [companyName setFont:[UIFont fontWithName:@"GothamBook" size:12]];
    companyName.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1];
    companyName.lineBreakMode = NSLineBreakByWordWrapping;
    companyName.numberOfLines = 0;
    companyName.textAlignment = NSTextAlignmentLeft;
    NSString *companyNameString = [[_notes objectAtIndex:indexPath.row] objectForKey:@"companyName"];
    attributedString = [[NSMutableAttributedString alloc] initWithString:companyNameString];
    [paragraphStyle setLineSpacing:6];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [companyNameString length])];
    companyName.attributedText = attributedString ;
    companyName.adjustsFontSizeToFitWidth = NO;
    companyName.lineBreakMode = NSLineBreakByTruncatingTail;
    [cell addSubview:companyName];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(26,32,90,60)];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[[_notes objectAtIndex:indexPath.row] objectForKey:@"imageUrl"]]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    [cell addSubview:imageView];
    
    UIButton *moreBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    moreBtn.frame = CGRectMake(cell.frame.size.width-40, 0, 40, 125);
    UIImage *btnImage = [UIImage imageNamed:@"btn_mynote_more_grey.png"];
    [moreBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
    [moreBtn addTarget:self action:@selector(moreBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell addSubview:moreBtn];
    
    UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 123.5, cell.frame.size.width, 1.5)];
    line.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [cell addSubview:line];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 125;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)moreBtnAction:(id)sender {
    NSLog(@"moreBtnAction");
    
    CGPoint touchPoint = [sender convertPoint:CGPointZero toView:self.tableView]; // maintable --> replace your tableview name
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:touchPoint];
    
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", "") 
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* EditBtn = [UIAlertAction
                                        actionWithTitle:NSLocalizedString(@"Edit", "")
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            AddNoteViewController *addNoteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNoteViewController"];
                                            
                                            addNoteViewController.imgUrl = [[_notes objectAtIndex:indexPath.row] objectForKey:@"imageUrl"];
                                            addNoteViewController.boothNumber = [[_notes objectAtIndex:indexPath.row] objectForKey:@"boothNumber"];;
                                            addNoteViewController.companyName = [[_notes objectAtIndex:indexPath.row] objectForKey:@"companyName"];
                                            [self.navigationController pushViewController:addNoteViewController animated:YES];
                                            
                                            isNeedToRefresh = true;
                                        }];
    
    UIAlertAction* deleteBtn = [UIAlertAction
                                actionWithTitle:NSLocalizedString(@"Delete", "")
                                style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * action)
                                {
                                    [_notes removeObjectAtIndex:indexPath.row];
                                    [[NSUserDefaults standardUserDefaults] setObject:_notes forKey:@"Note"];
                                    
                                    [self.tableView beginUpdates];
                                    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                    [self.tableView endUpdates];
//                                    [self performSegueWithIdentifier:@"SignInView" sender:self];
                                }];
    
    [alert addAction:Cancel];
    [alert addAction:EditBtn];
    [alert addAction:deleteBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showNoteDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        MyNoteDetailViewController *myNoteDetailViewController = segue.destinationViewController;
        myNoteDetailViewController.selectNote = [[_notes objectAtIndex:indexPath.row] objectForKey:@"title"];
        myNoteDetailViewController.note = [_notes objectAtIndex:indexPath.row];
    }
}
- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
