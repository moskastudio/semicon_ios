//
//  ForgotPasswordViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/12.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ForgotPasswordViewController.h"
#import "Service.h"

@interface ForgotPasswordViewController ()<UITextFieldDelegate>

@end

@implementation ForgotPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelSignBtn:(id)sender {
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)getPassword:(id)sender {
    [[Service sharedInstance] getPasswordWithEmail:_emailTextField.text complete:^(BOOL success, NSDictionary *responseDict) {
        if (success) {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"" message:@"密碼已寄至此信箱" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                [self dismissViewControllerAnimated:true completion:nil];
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
        }
        else {
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"請確認電子郵件是否正確" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
        }
    }];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    
    return true;
}
@end
