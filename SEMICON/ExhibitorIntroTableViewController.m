//
//  ExhibitorIntroTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/6/8.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "ExhibitorIntroTableViewController.h"
#import "ProductIntroTableViewController.h"
#import "AddCalendarViewController.h"
#import "AddNoteViewController.h"
#import "Service.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MessageUI/MessageUI.h>
#import "SignInViewController.h"
#import "WebViewController.h"

#define MORETEXT_COLOR [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]
#define LABEL_CONTENT_COLOR [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0]

@interface ExhibitorIntroTableViewController ()<MFMailComposeViewControllerDelegate> {
    Boolean isNeedToRefresh;
}

@property NSInteger companyNameHeight;
//@property NSMutableDictionary *dict;
@property NSMutableArray *productArray;
@property NSString *companyCategory;

@property UILabel *introLabel;
@property UILabel *categoryLabel;
@property UILabel *productLabel;

@property UIButton *visitedBtn;

@property NSInteger introTouch;
@property NSInteger categoryTouch;

@end

@implementation ExhibitorIntroTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.title = _navBarTitle;

    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.introTouch = 0;
    self.categoryTouch = 0;
    self.exhibitor = [[NSDictionary alloc] init];
    
    // Ray
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onFetchExhibitorByBoothNumberSuccessNotification:) name:FetchExhibitorByBoothNumberSuccessNotification object:nil];
    
    [[Service sharedInstance] fetchExhibitorByBoothNumber:self.boothNumber];
}


- (void)viewWillAppear:(BOOL)animated{
    if (isNeedToRefresh) {
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:4 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
        
        isNeedToRefresh = false;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onFetchExhibitorByBoothNumberSuccessNotification:(NSNotification*) notify {
    NSLog(@"onFetchExhibitorByBoothNumberSuccessNotification:%@", notify.object);
    
    self.exhibitor = notify.object;
    
    self.productArray = [[NSMutableArray alloc] initWithArray:[self.exhibitor objectForKey:@"products"]];
    
    NSString *allCategoryText = @"";
    
    NSMutableDictionary *productCategoriesDict = [self.exhibitor objectForKey:@"ProductCategories"];
    NSArray *categoryArray = [productCategoriesDict objectForKey:@"Category"];
    for (NSDictionary *category in categoryArray) {
        NSString *categoryText = [category objectForKey:@"Text"];
        allCategoryText = [NSString stringWithFormat:@"%@%@\r\n", allCategoryText, categoryText];
        NSArray *subCategoryArray = [category objectForKey:@"SubCategory"];
        for (NSDictionary *subCategory in subCategoryArray) {
            NSString *subCategoryText = [subCategory objectForKey:@"Text"];
            allCategoryText = [NSString stringWithFormat:@"%@%@\r\n", allCategoryText, subCategoryText];
        }
    }
    self.companyCategory = allCategoryText;
    
    [self.tableView reloadData];
}

- (void)listSubviewsOfView:(UIView *)view {
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    
    for (UIView *subview in subviews) {
        
        // Do what you want to do with the subview
        NSLog(@"%@", subview);
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *tempLabel = (UILabel*)subview;
            [tempLabel setFrame:CGRectMake(tempLabel.frame.origin.x, tempLabel.frame.origin.y, tempLabel.frame.size.width, tempLabel.frame.size.height+5)];
        }
        
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.exhibitor.count == 0) {
        return 0;
    }
    return 11;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExhibitorIntroCell" forIndexPath:indexPath];
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (indexPath.row == 0) {
        UIImageView *backgroundImage = [[UIImageView alloc] initWithFrame: CGRectMake(0,106,cell.frame.size.width,36)];
        UIImage *bgImage = [UIImage imageNamed:@"img_gradient_purple.png"];
        backgroundImage.image = bgImage;
        [cell addSubview:backgroundImage];
        
        UIImageView *exhibitorImage = [[UIImageView alloc] initWithFrame: CGRectMake(self.view.frame.size.width/2-71,34,142,142)];
        [exhibitorImage sd_setImageWithURL:[NSURL URLWithString:[self.exhibitor objectForKey:@"CompanyLogo"]] placeholderImage:nil];
        exhibitorImage.contentMode = UIViewContentModeScaleAspectFit;
        exhibitorImage.clipsToBounds = YES;
        exhibitorImage.backgroundColor = [UIColor whiteColor];
        exhibitorImage.layer.borderColor = [[UIColor colorWithRed:0.341 green:0.118 blue:0.549 alpha:1.0] CGColor];
        exhibitorImage.layer.borderWidth = 1.0;
        [cell addSubview:exhibitorImage];
    }
    else if (indexPath.row == 1){
        cell.backgroundColor = [UIColor whiteColor];
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(36,0,cell.frame.size.width-72,25)];
        NSString *contentString = [self.exhibitor objectForKey:@"CompanyName"];
        titleLabel.numberOfLines = 0;
        titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        titleLabel.textAlignment = NSTextAlignmentLeft;
        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineSpacing:10];
        [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
        titleLabel.attributedText = attributedString ;
        titleLabel.adjustsFontSizeToFitWidth = NO;
        titleLabel.textColor = [UIColor blackColor];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        titleLabel.font = textFont;
        [titleLabel setNumberOfLines:0];
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        titleLabel.frame = CGRectMake(36, 0, actualSize.width,actualSize.height*1.5+10);
        [cell addSubview:titleLabel];
    }
    else if (indexPath.row == 2){
        UILabel *locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 10, self.view.frame.size.width-72, 15)];
        NSString *location = [NSString stringWithFormat:@"%@ %@", [self.exhibitor objectForKey:@"ContactCity"], [self.exhibitor objectForKey:@"ContactCountry"]];
        locationLabel.text = location;
        locationLabel.numberOfLines =1 ;
        [locationLabel setFont: [UIFont fontWithName:@"GothamBook" size:13.5]];
        locationLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        [cell addSubview:locationLabel];
        
        UILabel *boothLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 30, self.view.frame.size.width-72, 15)];
        NSString *booth = [self.exhibitor objectForKey:@"BoothNumber"];
        boothLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Booth : %@" , "") , booth];
        boothLabel.numberOfLines =1 ;
        [boothLabel setFont: [UIFont fontWithName:@"GothamBook" size:13.5]];
        boothLabel.textColor = [UIColor colorWithRed:0.502 green:0.51 blue:0.522 alpha:1.0];
        [cell addSubview:boothLabel];
    }
    else if (indexPath.row ==3){
        UIButton *websiteBtn = [[UIButton alloc]initWithFrame:CGRectMake(cell.frame.size.width-55, 14, 40, 40)];
        UIImage *websiteBtnImage = [UIImage imageNamed:@"btn_website_dark.png"];
        [websiteBtn setImage:websiteBtnImage forState:UIControlStateNormal];
        [websiteBtn addTarget:self action:@selector(websiteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:websiteBtn];
        
        // Ray
        if ([[self.exhibitor objectForKey:@"WebSiteURL"] isEqualToString:@""]) {
            websiteBtn.enabled = false;
        }

//        UIButton *fbBtn = [[UIButton alloc]initWithFrame:CGRectMake(cell.frame.size.width-105, 13, 40, 40)];
//        UIImage *fbBtnImage = [UIImage imageNamed:@"btn_fb_dark.png"];
//        [fbBtn setImage:fbBtnImage forState:UIControlStateNormal];
//        [fbBtn addTarget:self action:@selector(fbBtnAction:) forControlEvents:UIControlEventTouchUpInside];
//        [cell addSubview:fbBtn];
//        
//        // Ray
//        if ([[self.exhibitor objectForKey:@"Facebook"] isEqualToString:@""]) {
//            fbBtn.enabled = false;
//        }
        
        UIButton *callBtn = [[UIButton alloc]initWithFrame:CGRectMake(cell.frame.size.width-105, 13, 40, 40)];
        UIImage *callBtnImage = [UIImage imageNamed:@"btn_call_phone_dark.png"];
        [callBtn setImage:callBtnImage forState:UIControlStateNormal];
        [callBtn addTarget:self action:@selector(callBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:callBtn];
        
        // Ray
        if ([[self.exhibitor objectForKey:@"ContactPhone"] isEqualToString:@""]) {
            callBtn.enabled = false;
        }
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(21, 53, cell.frame.size.width-42, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    else if (indexPath.row == 4){
        //6個按鈕
        UIButton *calendarBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/6-25, 15, 50, 50)];
        UIImage *btnImage = [UIImage imageNamed:@"btn_add_calendar.png"];
        [calendarBtn setImage:btnImage forState:UIControlStateNormal];
        UIImage *selectedBtnImage = [UIImage imageNamed:@"btn_my_calendar.png"];
        [calendarBtn setImage:selectedBtnImage forState:UIControlStateSelected];
        [calendarBtn addTarget:self action:@selector(calendarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:calendarBtn];
        
        if ([self checkCalendar]) {
            calendarBtn.selected = true;
        }
        
        UILabel *calendarLabel = [[UILabel alloc] initWithFrame:
                                  CGRectMake(18,65,(cell.frame.size.width-36)/3,20)];
        calendarLabel.text = NSLocalizedString(@"Add Calendar","");
        calendarLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        calendarLabel.textAlignment = NSTextAlignmentCenter;
        calendarLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:calendarLabel];
        
        UIButton *mapitBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/2-25, 15, 50, 50)];
        UIImage *mapitBtnImage = [UIImage imageNamed:@"btn_map_it.png"];
        [mapitBtn setImage:mapitBtnImage forState:UIControlStateNormal];
        [mapitBtn addTarget:self action:@selector(mapitBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:mapitBtn];
        
        UILabel *mapitLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20)];
        mapitLabel.text = NSLocalizedString(@"Map It","");
        mapitLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        mapitLabel.textAlignment = NSTextAlignmentCenter;
        mapitLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:mapitLabel];
        
        UIButton *favoriteBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+5*(cell.frame.size.width-36)/6-25, 15, 50, 50)];
        UIImage *favoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_normal.png"];
        [favoriteBtn setImage:favoriteBtnImage forState:UIControlStateNormal];
        UIImage *selectedFavoriteBtnImage = [UIImage imageNamed:@"btn_add_favorite_press.png"];
        [favoriteBtn setImage:selectedFavoriteBtnImage forState:UIControlStateSelected];
        [favoriteBtn addTarget:self action:@selector(favoriteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:favoriteBtn];
        
        if ([self checkExhibitorFarvorites]) {
            favoriteBtn.selected = true;
        }
        
        UILabel *favoriteLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+2*(cell.frame.size.width-36)/3,65,(cell.frame.size.width-36)/3,20)];
        favoriteLabel.text = NSLocalizedString(@"Add Favorite","");
        favoriteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        favoriteLabel.textAlignment = NSTextAlignmentCenter;
        favoriteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:favoriteLabel];
        
        UIButton *mailBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/6-25, 120, 50, 50)];
        UIImage *mailBtnImage = [UIImage imageNamed:@"btn_mail_to_exhibitor.png"];
        [mailBtn setImage:mailBtnImage forState:UIControlStateNormal];
        [mailBtn addTarget:self action:@selector(mailBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:mailBtn];
        
        // Ray
        if ([[_exhibitor objectForKey:@"ContactEmail"] isEqualToString:@""]) {
            mailBtn.enabled = false;
        }
        
        UILabel *mailLabel = [[UILabel alloc] initWithFrame:
                                  CGRectMake(18,170,(cell.frame.size.width-36)/3,20)];
        mailLabel.text = NSLocalizedString(@"Mail","");
        mailLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        mailLabel.textAlignment = NSTextAlignmentCenter;
        mailLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:mailLabel];
        
        UIButton *noteBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+(cell.frame.size.width-36)/2-25, 120, 50, 50)];
        UIImage *noteBtnImage = [UIImage imageNamed:@"btn_notes.png"];
        [noteBtn setImage:noteBtnImage forState:UIControlStateNormal];
        UIImage *selectedNoteBtnImage = [UIImage imageNamed:@"btn_my_notes.png"];
        [noteBtn setImage:selectedNoteBtnImage forState:UIControlStateSelected];
        [noteBtn addTarget:self action:@selector(noteBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:noteBtn];
        
        if ([self checkNote]) {
            noteBtn.selected = true;
        }
        
        UILabel *noteLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+(cell.frame.size.width-36)/3,170,(cell.frame.size.width-36)/3,20)];
        noteLabel.text = NSLocalizedString(@"Note","");
        noteLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        noteLabel.textAlignment = NSTextAlignmentCenter;
        noteLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:noteLabel];
        
        _visitedBtn = [[UIButton alloc]initWithFrame:CGRectMake(18+5*(cell.frame.size.width-36)/6-25, 120, 50, 50)];
        UIImage *unvisitedBtnImage = [UIImage imageNamed:@"btn_visited_normal.png"];
        [_visitedBtn setImage:unvisitedBtnImage forState:UIControlStateNormal];
        UIImage *visitedBtnImage = [UIImage imageNamed:@"btn_visited_press.png"];
        [_visitedBtn setImage:visitedBtnImage forState:UIControlStateSelected];
        [_visitedBtn addTarget:self action:@selector(visitedBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:_visitedBtn];
        
        if ([self checkVisited]) {
            [_visitedBtn setSelected:true];
        }
        
        UILabel *visitedLabel = [[UILabel alloc] initWithFrame: CGRectMake(18+2*(cell.frame.size.width-36)/3,170,(cell.frame.size.width-36)/3,20)];
        visitedLabel.text = NSLocalizedString(@"Booth Visited","");
        visitedLabel.font = [UIFont fontWithName:@"GothamMedium" size:13];
        visitedLabel.textAlignment = NSTextAlignmentCenter;
        visitedLabel.textColor = [UIColor colorWithRed:0.306 green:0.349 blue:0.467 alpha:1.0];
        [cell addSubview:visitedLabel];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 208, self.view.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    else if (indexPath.row == 5){
        UILabel *introLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,30,cell.frame.size.width-72,26+10)];
        introLabel.text = NSLocalizedString(@"Introduction_exhibitor","");
        introLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        introLabel.textColor = [UIColor blackColor];
        [introLabel sizeToFit];
        [cell addSubview:introLabel];
    }
    else if (indexPath.row == 6){
        if (self.introTouch == 0) {
            _introLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 100)];
            NSString *contentString = [self.exhibitor objectForKey:@"CompanyIntroduction"];
            _introLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _introLabel.numberOfLines = 0;
            _introLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _introLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _introLabel.attributedText = attributedString ;
            _introLabel.adjustsFontSizeToFitWidth = NO;
            _introLabel.textColor = LABEL_CONTENT_COLOR;
            [cell addSubview:_introLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 63)];
            line.backgroundColor = [UIColor whiteColor];
            [cell addSubview:line];
            
            UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(36, 114, self.view.frame.size.width-72, 15)];
            more.text = NSLocalizedString(@"More","");
            more.font = [UIFont fontWithName:@"GothamMedium" size:15];
            more.textAlignment = NSTextAlignmentCenter;
            more.textColor = MORETEXT_COLOR;
            [cell addSubview:more];
        }
        else{
            _introLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 50)];
            NSString *contentString = [self.exhibitor objectForKey:@"CompanyIntroduction"];
            _introLabel.numberOfLines = 0;
            _introLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _introLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _introLabel.attributedText = attributedString ;
            _introLabel.adjustsFontSizeToFitWidth = NO;
            _introLabel.textColor = LABEL_CONTENT_COLOR;
            UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
            _introLabel.font = textFont;
            [_introLabel setNumberOfLines:0];
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            _introLabel.frame = CGRectMake(36, 5, actualSize.width,actualSize.height*1.8);
            [cell addSubview:_introLabel];
        }
    }
    else if (indexPath.row == 7){
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *categoryLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,32,cell.frame.size.width-72,26)];
        categoryLabel.text = NSLocalizedString(@"Category","");
        categoryLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        categoryLabel.textColor = [UIColor blackColor];
        [categoryLabel sizeToFit];
        [cell addSubview:categoryLabel];
    }
    else if (indexPath.row == 8){
        if (self.categoryTouch == 0) {
            _categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 100)];
            NSString *contentString = self.companyCategory;
            _categoryLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
            _categoryLabel.numberOfLines = 0;
            _categoryLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _categoryLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _categoryLabel.attributedText = attributedString ;
            _categoryLabel.adjustsFontSizeToFitWidth = NO;
            _categoryLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            [cell addSubview:_categoryLabel];
            
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 90, self.view.frame.size.width, 63)];
            line.backgroundColor = [UIColor whiteColor];
            [cell addSubview:line];
            
            UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(36, 114, self.view.frame.size.width-72, 15)];
            more.text = NSLocalizedString(@"More","");
            more.font = [UIFont fontWithName:@"GothamMedium" size:15];
            more.textAlignment = NSTextAlignmentCenter;
            more.textColor = MORETEXT_COLOR;
            [cell addSubview:more];
        }
        else{
            _categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(36, 5, self.view.frame.size.width-72, 50)];
            NSString *contentString = self.companyCategory;
            _categoryLabel.numberOfLines = 0;
            _categoryLabel.lineBreakMode = NSLineBreakByWordWrapping;
            _categoryLabel.textAlignment = NSTextAlignmentLeft;
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
            [paragraphStyle setLineSpacing:10];
            [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
            _categoryLabel.attributedText = attributedString ;
            _categoryLabel.adjustsFontSizeToFitWidth = NO;
            _categoryLabel.textColor = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1.0];
            UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
            _categoryLabel.font = textFont;
            [_categoryLabel setNumberOfLines:0];
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            _categoryLabel.frame = CGRectMake(36, 5, actualSize.width,actualSize.height*1.8);
            [cell addSubview:_categoryLabel];
        }
    }
    else if (indexPath.row == 9){
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1.5)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
        
        UILabel *productLabel = [[UILabel alloc]initWithFrame:CGRectMake(36,32,cell.frame.size.width-72,26)];
        productLabel.text = NSLocalizedString(@"Product","");
        productLabel.font = [UIFont fontWithName:@"GothamMedium" size:25];
        productLabel.textColor = [UIColor blackColor];
        [productLabel sizeToFit];
        [cell addSubview:productLabel];
    }
    else if (indexPath.row == 10){
        if (self.productArray.count > 0) {
            
            for (int i = 0; i < self.productArray.count; i++) {
                UIView *background = [[UIView alloc] initWithFrame:CGRectMake(12, 10 * (i + 1) + (i * 350), self.view.frame.size.width-24, 350)];
                background.backgroundColor = [UIColor whiteColor];
                background.layer.cornerRadius = 6.0;
                background.layer.shadowColor = [UIColor blackColor].CGColor;
                background.layer.shadowOpacity = 0.35;
                background.layer.shadowRadius = 6.0;
                background.layer.shadowOffset = CGSizeMake(0, 2);
                [cell addSubview:background];
                
                UIImageView *productImage = [[UIImageView alloc] initWithFrame: CGRectMake(0,0,background.frame.size.width,160)];
                
                [productImage sd_setImageWithURL:[NSURL URLWithString:[[self.productArray objectAtIndex:i] objectForKey:@"LargeImageURL"]] placeholderImage:nil];
                productImage.contentMode = UIViewContentModeScaleAspectFill;
                productImage.clipsToBounds = YES;
                UIBezierPath *maskPath;
                maskPath = [UIBezierPath bezierPathWithRoundedRect:productImage.bounds
                                                 byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight)
                                                       cornerRadii:CGSizeMake(6.0, 6.0)];
                
                CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
                maskLayer.frame = productImage.bounds;
                maskLayer.path = maskPath.CGPath;
                productImage.layer.mask = maskLayer;
                [background addSubview:productImage];
                
                UILabel *productNameLabel = [[UILabel alloc]initWithFrame:CGRectMake(24,178,background.frame.size.width-48,15)];
                productNameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
                productNameLabel.text = [[self.productArray objectAtIndex:i] objectForKey:@"Name"];
                productNameLabel.font = [UIFont fontWithName:@"GothamBold" size:14];
                productNameLabel.textColor = [UIColor blackColor];
                [productNameLabel sizeToFit];
                [background addSubview:productNameLabel];
                
                UILabel *productDetailLabel = [[UILabel alloc] initWithFrame:CGRectMake(24, 198, background.frame.size.width-48, 100)];
                NSString *contentString = [[self.productArray objectAtIndex:i] objectForKey:@"Description"];
                productDetailLabel.font = [UIFont fontWithName:@"GothamBook" size:14];
                productDetailLabel.numberOfLines = 0;
                productDetailLabel.lineBreakMode = NSLineBreakByWordWrapping;
                productDetailLabel.textAlignment = NSTextAlignmentLeft;
                NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:contentString];
                NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
                [paragraphStyle setLineSpacing:10];
                [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, [contentString length])];
                productDetailLabel.attributedText = attributedString ;
                productDetailLabel.adjustsFontSizeToFitWidth = NO;
                productDetailLabel.textColor = LABEL_CONTENT_COLOR;
                [background addSubview:productDetailLabel];
                
                UIView *line = [[UIView alloc] initWithFrame:CGRectMake(6, 282, background.frame.size.width-18, 57)];
                line.backgroundColor = [UIColor whiteColor];
                [background addSubview:line];
                
                UILabel *more = [[UILabel alloc] initWithFrame:CGRectMake(6, 306, background.frame.size.width-12, 15)];
                more.text = NSLocalizedString(@"Detail","");
                more.font = [UIFont fontWithName:@"GothamMedium" size:15];
                more.textAlignment = NSTextAlignmentCenter;
                more.textColor = MORETEXT_COLOR;
                [background addSubview:more];
                
                UIButton *detail = [UIButton buttonWithType:UIButtonTypeCustom];
                detail.frame = CGRectMake(0, 0, background.frame.size.width, background.frame.size.height);
                detail.tag = i;
                [detail addTarget:self action:@selector(showProductDetail:) forControlEvents:UIControlEventTouchUpInside];
                [background addSubview:detail];
            }
        }
    }
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    [self listSubviewsOfView:cell];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 6) {
        if (self.introTouch == 0) {
            self.introTouch = 1;
        }
        else{
            self.introTouch = 0;
        }
        [self.introLabel removeFromSuperview];
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:6 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
        
    }
    if (indexPath.row == 8) {
        if (self.categoryTouch == 0) {
            self.categoryTouch = 1;
        }
        else{
            self.categoryTouch = 0;
        }
        [self.categoryLabel removeFromSuperview];
        
        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:8 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 186;
    }
    else if (indexPath.row == 1){
        NSString *contentString = [self.exhibitor objectForKey:@"CompanyName"];
        UIFont *textFont = [UIFont fontWithName:@"GothamBold" size:28] ;
        CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
        CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
        return actualSize.height*1.5;
    }
    else if (indexPath.row == 2){
        return 45;
    }
    else if (indexPath.row == 3){
        return 55;
    }
    else if (indexPath.row == 4){
        //Calendar Map Favorite
        return 210;
    }
    else if (indexPath.row == 5 ){
        return 60;
    }
    else if (indexPath.row == 6){
        if (self.introTouch == 0) {
            return 153;
        }
        else{
            NSString *contentString = [self.exhibitor objectForKey:@"CompanyIntroduction"];
            UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.8+20;
        }
    }
    else if (indexPath.row == 7 ){
        return 62;
    }
    else if (indexPath.row == 8){
        if (self.categoryTouch == 0) {
            return 153;
        }
        else{
            NSString *contentString = self.companyCategory;
            UIFont *textFont = [UIFont fontWithName:@"GothamBook" size:14] ;
            CGSize size = CGSizeMake(self.view.frame.size.width-72, 1000);
            NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:textFont,NSFontAttributeName, nil];
            CGSize actualSize = [contentString boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
            return actualSize.height*1.8+20;
        }
    }
    else if (indexPath.row == 9 ){
        return 62;
    }
    else if (indexPath.row == 10){
        return 10 * (self.productArray.count + 1) + (self.productArray.count * 350) + 5;
    }
    return 50;
}

- (void)showProductDetail:(id) sender {
    isNeedToRefresh = true;
    NSDictionary *product = [self.productArray objectAtIndex:[sender tag]];
    ProductIntroTableViewController *productIntroTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductIntroTableViewController"];
    productIntroTableViewController.navBarTitle = [product objectForKey:@"Name"];
    productIntroTableViewController.productID = [product objectForKey:@"productId"];
    
    [self.navigationController pushViewController:productIntroTableViewController animated:true];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void) websiteBtnAction:(id)sender {
    NSLog(@"websiteBtnAction");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.exhibitor objectForKey:@"WebSiteURL"]]];
}
-(void) fbBtnAction:(id)sender{
    NSLog(@"fbBtnAction");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[self.exhibitor objectForKey:@"Facebook"]]];
}
-(void) callBtnAction:(id)sender{
    NSLog(@"callBtnAction");
    NSString *phoneNumber = [@"tel://" stringByAppendingString:[self.exhibitor objectForKey:@"ContactPhone"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}
-(void) calendarBtnAction:(id)sender {
    NSLog(@"calendarBtnAction");
    if ([self checkLogin]) {
        isNeedToRefresh = true;
        AddCalendarViewController *addCalendarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCalendarViewController"];
        addCalendarViewController.scheduleName = [self.exhibitor objectForKey:@"CompanyName"];
        addCalendarViewController.type = @"Exhibitor";
        addCalendarViewController.typeID = [NSString stringWithFormat:@"%@",_boothNumber];
        [self.navigationController pushViewController:addCalendarViewController animated:YES];
    }
}

-(void) mapitBtnAction:(id)sender {
    NSLog(@"mapitBtnAction");
    NSString *langID = @"1";
    if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
        langID = @"2";
    }
    
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://expo.semi.org/taiwan2016/Public/EventMap.aspx?LangID=%@&MapItBooth=%@", langID, _boothNumber]]];
    
    WebViewController *wvc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    wvc.urlString = [NSString stringWithFormat:@"http://expo.semi.org/taiwan2016/Public/EventMap.aspx?LangID=%@&MapItBooth=%@", langID, _boothNumber];
    wvc.navTitle = NSLocalizedString(@"Map", @"");
    [self.navigationController pushViewController:wvc animated:true];
}

-(void) favoriteBtnAction:(id)sender {
    NSLog(@"favoriteBtnAction");
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        NSMutableArray *exhibitorFarvorites = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"]];
        if ([sender isSelected]) {
            [exhibitorFarvorites addObject:@{
                                             @"companyName": [_exhibitor objectForKey:@"CompanyName"],
                                             @"boothNumber": [NSString stringWithFormat:@"%@",_boothNumber]}];
        }
        else {
            for (int i = 0; i < exhibitorFarvorites.count; i++) {
                if ([[[exhibitorFarvorites objectAtIndex:i] objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
                    [exhibitorFarvorites removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:exhibitorFarvorites forKey:@"ExhibitorFarvorites"];
    }
}

-(void) mailBtnAction:(id)sender {
    NSLog(@"mailBtnAction");
    
    if ([MFMessageComposeViewController canSendText]) {
        MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        
        if (controller != nil) {
            [controller setToRecipients:[NSArray arrayWithObjects:[_exhibitor objectForKey:@"ContactEmail"], nil]];
            
            [self presentViewController:controller animated:YES completion:^{
                
            }];
        }
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"此裝置不支援電子郵件功能" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [ac addAction:cancel];
        [self presentViewController:ac animated:true completion:nil];
    }
}

-(void) noteBtnAction:(id)sender {
    NSLog(@"calendarBtnAction");
    if ([self checkLogin]) {
        isNeedToRefresh = true;
        
        AddNoteViewController *addNoteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AddNoteViewController"];
        
        addNoteViewController.imgUrl = [self.exhibitor objectForKey:@"CompanyLogo"];
        addNoteViewController.boothNumber = [NSString stringWithFormat:@"%@", _boothNumber];
        addNoteViewController.companyName = [self.exhibitor objectForKey:@"CompanyName"];
        [self.navigationController pushViewController:addNoteViewController animated:YES];
    }
}

-(void) visitedBtnAction:(id)sender {
    if ([self checkLogin]) {
        [sender setSelected:![sender isSelected]];
        
        NSMutableArray *visitedExhibitor = [[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] objectForKey:@"visited"]];
        if ([sender isSelected]) {
            [visitedExhibitor addObject:[NSString stringWithFormat:@"%@", _boothNumber]];
        }
        else {
            for (int i = 0; i < visitedExhibitor.count; i++) {
                if ([[visitedExhibitor objectAtIndex:i] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
                    [visitedExhibitor removeObjectAtIndex:i];
                    break;
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setObject:visitedExhibitor forKey:@"visited"];
    }
    NSLog(@"visitedBtnAction");
}

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (Boolean)checkVisited {
    NSArray *visitedExhibitor = [[NSUserDefaults standardUserDefaults] objectForKey:@"visited"];
    
    for (NSString *visitedBoothNumber in visitedExhibitor) {
        if ([visitedBoothNumber isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkExhibitorFarvorites {
    NSArray *exhibitorFarvorites = [[NSUserDefaults standardUserDefaults] objectForKey:@"ExhibitorFarvorites"];
    
    for (NSDictionary *exhibitorFarvorite in exhibitorFarvorites) {
        if ([[exhibitorFarvorite objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
            return true;
        }
    }
    return false;
}

- (Boolean)checkNote {
    NSArray *notes = [[NSUserDefaults standardUserDefaults] objectForKey:@"Note"];
    
    for (NSDictionary *note in notes) {
        if ([[note objectForKey:@"boothNumber"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]]) {
            
            return true;
        }
    }
    return false;
}

- (Boolean)checkCalendar {
    NSArray *reminders = [[NSUserDefaults standardUserDefaults] objectForKey:@"Reminder"];
    
    for (NSDictionary *reminder in reminders) {
        if ([[reminder objectForKey:@"typeID"] isEqualToString:[NSString stringWithFormat:@"%@",_boothNumber]] && [[reminder objectForKey:@"type"] isEqualToString:@"Exhibitor"]) {
            
            return true;
        }
    }
    return false;
}

#pragma mark - MFMailComposeViewController Delegate

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultSent:{
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"" message:@"發送成功" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
        }
            break;
            
        case MFMailComposeResultFailed:{
            UIAlertController *ac = [UIAlertController alertControllerWithTitle:@"錯誤" message:@"發送失敗，請至網路順暢的地方再重新嘗試" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"確認" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                
            }];
            [ac addAction:cancel];
            [self presentViewController:ac animated:true completion:nil];
        }
            break;
            
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (Boolean)checkLogin {
    BOOL isLogin = [[NSUserDefaults standardUserDefaults] boolForKey:kUserLogin];
    if (isLogin) {
        return YES;
    }
    else {
        UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Login Required", @"")  message:NSLocalizedString(@"You have to login to use personal features", @"") preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:NSLocalizedString(@"Cancel", "") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        
        UIAlertAction *login = [UIAlertAction actionWithTitle:NSLocalizedString(@"Login", "") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            SignInViewController *sivn = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self presentViewController:sivn animated:true completion:^{
                nil;
            }];
        }];
        
        [ac addAction:cancel];
        [ac addAction:login];
        [self presentViewController:ac animated:true completion:nil];
        return NO;
    }
}
@end
