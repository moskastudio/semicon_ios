//
//  EvevtActiviteIntroTableViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/31.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EvevtActiviteIntroTableViewController : UITableViewController

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property NSString *navBarTitle;
@property NSString *dataSource;
@property NSInteger index;
@property NSString *infoID;
@property NSString *dateString;

@end
