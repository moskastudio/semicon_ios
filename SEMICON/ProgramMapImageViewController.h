//
//  ProgramMapImageViewController.h
//  semicon
//
//  Created by MuRay Lin on 2016/7/7.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgramMapImageViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property NSString *navTitle;
- (IBAction) backBtn:(id)sender;
- (IBAction) indexBtn:(id)sender;
@end
