//
//  ASDetailsViewController.h
//  SEMICON
//
//  Created by aisoter on 2016/5/19.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ASDetailsViewController : UIViewController<UIDocumentInteractionControllerDelegate>

@property NSString *source;
@property NSInteger indexPath;

- (IBAction)backBtn:(id)sender;
- (IBAction)indexBtn:(id)sender;

@property (strong, nonatomic) IBOutlet UIWebView *webView;

//@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end
