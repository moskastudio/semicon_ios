//
//  FoodBeverageTableViewController.m
//  SEMICON
//
//  Created by aisoter on 2016/5/13.
//  Copyright © 2016年 YiTing Wang. All rights reserved.
//

#import "FoodBeverageTableViewController.h"
#import "FoodDetailsTableViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>

@interface FoodBeverageTableViewController ()
@property NSMutableArray *restaurantNameArray;
@property NSMutableArray *foodTypeArray;
@property NSMutableArray *restaurantTelArray;
@property NSMutableArray *restaurantLongitudeArray;
@property NSMutableArray *restaurantLatitudeArray;

@property NSString *telNumber;

@end

@implementation FoodBeverageTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[NSLocale preferredLanguages][0]  isEqual: @"zh-TW"]) {
        NSArray *nangangArray = [[NSArray alloc] initWithObjects:@"摩斯漢堡",@"輕鬆食(素食)",@"伯朗咖啡館", @"更多", nil];
        NSArray *surroundingArray = [[NSArray alloc] initWithObjects:@"A.S. Cafe Lounge 中研咖啡廳",@"Lizard Kitchen",@"Magic 廚房", @"更多", nil];
        NSArray *financeArray = [[NSArray alloc] initWithObjects:@"鶴越烏龍麵",@"朱記",@"斑鳩的窩",@"更多", nil];
        self.restaurantNameArray = [[NSMutableArray alloc] initWithObjects:nangangArray, surroundingArray,financeArray, nil];
        
        NSArray *nangangTypeArray = [[NSArray alloc] initWithObjects:@"速食",@"速食",@"咖啡簡餐",@"", nil];
        NSArray *surroundingTypeArray = [[NSArray alloc] initWithObjects:@"飲品、會議室",@"咖哩飯",@"中式簡餐、麵類",@"", nil];
        NSArray *financeTypeArray = [[NSArray alloc] initWithObjects:@"烏龍麵",@"小吃、餡餅粥",@"日式炸物",@"", nil];
        self.foodTypeArray = [[NSMutableArray alloc] initWithObjects:nangangTypeArray,surroundingTypeArray,financeTypeArray, nil];
    }
    else{
        NSArray *nangangArray = [[NSArray alloc] initWithObjects:@"MOS BURGER",@"Easy Going (Vagetarian)",@"Mr. Brown Coffe", @"更多", nil];
        NSArray *surroundingArray = [[NSArray alloc] initWithObjects:@"A.S. Cafe Lounge 中研咖啡廳",@"Lizard Kitchen",@"Magic 廚房", @"更多", nil];
        NSArray *financeArray = [[NSArray alloc] initWithObjects:@"鶴越烏龍麵",@"朱記",@"斑鳩的窩",@"更多", nil];
        self.restaurantNameArray = [[NSMutableArray alloc] initWithObjects:nangangArray, surroundingArray,financeArray, nil];
        
        NSArray *nangangTypeArray = [[NSArray alloc] initWithObjects:@"Fast Food",@"Fast Food",@"Coffee & Meal",@"", nil];
        NSArray *surroundingTypeArray = [[NSArray alloc] initWithObjects:@"飲品、會議室",@"咖哩飯",@"中式簡餐、麵類",@"", nil];
        NSArray *financeTypeArray = [[NSArray alloc] initWithObjects:@"烏龍麵",@"小吃、餡餅粥",@"日式炸物",@"", nil];
        self.foodTypeArray = [[NSMutableArray alloc] initWithObjects:nangangTypeArray,surroundingTypeArray,financeTypeArray, nil];
    }
    
    NSArray *nangangTelArray = [[NSArray alloc] initWithObjects:@"+886-2-2783-4138",@"+886-2-2651-9394",@"+886-2-2783-6963",@"", nil];
    NSArray *surroundingTelArray = [[NSArray alloc] initWithObjects:@"+886-2-27852712#1219",@"+886-2-26552418",@"+886-2-26550029",@"", nil];
    NSArray *financeTelArray = [[NSArray alloc] initWithObjects:@"+886-2-6613-0967",@"+886-2-2653-2800",@"+886-2-2652-2259",@"", nil];
    self.restaurantTelArray = [[NSMutableArray alloc] initWithObjects:nangangTelArray,surroundingTelArray,financeTelArray, nil];
    
    NSArray *nangangLongitude = [[NSArray alloc] initWithObjects:@"",@"",@"",@"", nil];
    NSArray *surroundingLongitude = [[NSArray alloc] initWithObjects:@"25.042683,121.6119792",@"25.0565859,121.6114842",@"25.0820309,121.3518469",@"", nil];
    NSArray *financeLongitude = [[NSArray alloc] initWithObjects:@"25.0596987,121.6130575",@"25.0596987,121.6130575",@"25.0596987,121.6130575",@"", nil];
    self.restaurantLongitudeArray = [[NSMutableArray alloc] initWithObjects:nangangLongitude,surroundingLongitude,financeLongitude, nil];
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.restaurantNameArray count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.restaurantNameArray objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FoodBeverageCell" forIndexPath:indexPath];
    
    for (UIView *view in cell.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
        }
        else if ([view isKindOfClass:[UIView class]]) {
            [view removeFromSuperview];
        }
    }
    
    if (indexPath.row == 3) {
        UILabel *moreLabel=[[UILabel alloc] initWithFrame:
                            CGRectMake(30, 45, 200, 15)];
        [moreLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        moreLabel.textColor = [UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0];
        moreLabel.text = NSLocalizedString( @"More", "");
        moreLabel.frame = CGRectMake(self.view.frame.size.width/2-50, 8, 100, 30);
        moreLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:moreLabel];
        
        UIView *line = [[UIView alloc] initWithFrame: CGRectMake(0, 48, cell.frame.size.width, 2)];
        line.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [cell addSubview:line];
    }
    else{
        UILabel *restaurantNameLabel=[[UILabel alloc] initWithFrame:
                                      CGRectMake(30, 22, 212, 18)];
        [restaurantNameLabel setFont:[UIFont fontWithName:@"GothamBook" size:16]];
        restaurantNameLabel.textColor = [UIColor colorWithRed:0.302 green:0.302 blue:0.302 alpha:1.0];
        restaurantNameLabel.text = [[self.restaurantNameArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        restaurantNameLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:restaurantNameLabel];
        
        UILabel *foodTypeLabel=[[UILabel alloc] initWithFrame:
                                CGRectMake(30, 45, 200, 15)];
        [foodTypeLabel setFont:[UIFont fontWithName:@"GothamBook" size:14]];
        foodTypeLabel.textColor = [UIColor colorWithRed:0.655 green:0.663 blue:0.675 alpha:1.0];
        foodTypeLabel.text = [[self.foodTypeArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        foodTypeLabel.textAlignment = NSTextAlignmentLeft;
        [cell addSubview:foodTypeLabel];
        
        UIButton *telBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
        telBtn.frame = CGRectMake(self.view.frame.size.width-50, 24, 40, 40);
        UIImage *btnImage = [UIImage imageNamed:@"btn_call_phone.png"];
        [telBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
        [telBtn addTarget:self action:@selector(telBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [cell addSubview:telBtn];
        
        if (indexPath.section != 0 ) {
            UIButton *mapBtn = [UIButton buttonWithType: UIButtonTypeRoundedRect];
            mapBtn.frame = CGRectMake(self.view.frame.size.width-100, 24, 40, 40);
            UIImage *mapbtnImage = [UIImage imageNamed:@"btn_store_location.png"];
            [mapBtn setBackgroundImage:mapbtnImage forState:UIControlStateNormal];
            [mapBtn addTarget:self action:@selector(mapBtnAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:mapBtn];
        }
    }
    
    // This is how you change the background color
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor clearColor];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"indexPath.section:%ld,indexPath.row:%ld",indexPath.section,indexPath.row);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 0, tableView.frame.size.width, 50)];
    sectionHeaderView.backgroundColor = [UIColor whiteColor];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(0, 10, sectionHeaderView.frame.size.width, 30)];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor =[UIColor colorWithRed:0.663 green:0 blue:0.514 alpha:1.0];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    [headerLabel setFont:[UIFont fontWithName:@"GothamMedium" size:14.0]];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = NSLocalizedString(@"At Nangang Exhibition Hall Food & Beverage", "");
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = NSLocalizedString(@"Restaurant Near by Nangang Exhibition Hall ", "");
            return sectionHeaderView;
            break;
        case 2:
            headerLabel.text = NSLocalizedString(@"CTBC Financial Park", "");
            return sectionHeaderView;
            break;
        default:
            break;
    }
    return sectionHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 3) {
        return 50;
    }
    else
        return 88;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFoodDetails"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        FoodDetailsTableViewController *foodDetailsTableViewController = segue.destinationViewController;
        foodDetailsTableViewController.foodRegion = indexPath.section;
    }
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)indexBtn:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(NSIndexPath*)GetIndexPathFromSender:(id)sender{
    if(!sender) {
        return nil;
    }
    if([sender isKindOfClass:[UITableViewCell class]])
    {
        UITableViewCell *cell = sender;
        return [self.tableView indexPathForCell:cell];
    }
    return [self GetIndexPathFromSender:((UIView*)[sender superview])];
}

-(void) telBtnAction:(id)sender {
    NSIndexPath *indexPath = [self GetIndexPathFromSender:sender];
    UIAlertController* alert = [UIAlertController
                                alertControllerWithTitle:nil
                                message:nil
                                preferredStyle:UIAlertControllerStyleActionSheet];
    [alert.view setTintColor:[UIColor colorWithRed:0.404 green:0.231 blue:0.722 alpha:1.0]];
    
    UIAlertAction* Cancel = [UIAlertAction
                             actionWithTitle:NSLocalizedString(@"Cancel", "")
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction * action)
                             {
                                 //  UIAlertController will automatically dismiss the view
                             }];
    
    UIAlertAction* CallBtn = [UIAlertAction
                              actionWithTitle:NSLocalizedString(@"Call Restaurant", "")
                              style:UIAlertActionStyleDefault
                              handler:^(UIAlertAction * action)
                              {
                                  self.telNumber = [[self.restaurantTelArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
                                  NSString *phoneNumber = [@"tel://" stringByAppendingString:self.telNumber];
                                  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                              }];
    
    [alert addAction:Cancel];
    [alert addAction:CallBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

-(void) mapBtnAction:(id)sender {
    NSIndexPath *indexPath = [self GetIndexPathFromSender:sender];
    NSString *restaurantAddress =[[self.restaurantLongitudeArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    NSString* stringURL = [NSString stringWithFormat:@"comgooglemapsurl://?q=%@&zoom=10&views=traffic", restaurantAddress];
    if ([[UIApplication sharedApplication] canOpenURL:
         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:stringURL]];
    }
    else {
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.google.com/maps?q=%@&zoom=10&views=traffic",restaurantAddress]];
        [[UIApplication sharedApplication] openURL: url];
    }
    
}
@end
