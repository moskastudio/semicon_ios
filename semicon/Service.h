//
//  Service.h
//  PicMemory
//
//  Created by Hungchi on 7/15/15.
//  Copyright (c) 2015 Hungchi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

extern NSString* FetchAllExhibitorSuccessNotification;
extern NSString* FetchAllStructuredCategorySuccessNotification;
extern NSString* FetchExhibitorByCategoryIDSuccessNotification;
extern NSString* FetchExhibitorByBoothNumberSuccessNotification;

extern NSString* kUserLogin;

@interface Service : NSObject

typedef void (^CallbackDictWhenComplete)(BOOL success, NSDictionary* responseDict);
typedef void (^CallbackArrayWhenComplete)(BOOL success, NSArray* responseAry);

+ (id) sharedInstance;

// Fetch
- (void)fetchAllExhibitor;
- (void)fetchAllStructuredCategory;
- (void)fetchExhibitorByCategoryID:(NSString*) categoryID;
- (void)fetchExhibitorByBoothNumber:(NSString*) boothNumber;
- (void)fetchExhibitorFromNumber:(NSString*) fromNumber toNumber:(NSString *) toNumber complete:(CallbackArrayWhenComplete) callBack;
- (void)fetchAllProductWithComplete:(CallbackArrayWhenComplete) callBack;
- (void)fetchProductByID:(NSString*) productID withComplete:(CallbackDictWhenComplete) callBack;
- (void)fetchAllForumWithComplete:(CallbackArrayWhenComplete) callBack;
- (void)fetchForumByID:(NSString*) nodeID withComplete:(CallbackDictWhenComplete) callBack;
- (void)fetchPavilionWithAlias:(NSString *)alias complete:(CallbackArrayWhenComplete) callBack;
- (void)fetchScheduleWithComplete:(CallbackArrayWhenComplete) callBack;
- (void)fetchAllADWithComplete:(CallbackDictWhenComplete) callBack;
- (void)fetchTechXPOTWithTitle:(NSString *)title Complete:(CallbackArrayWhenComplete) callBack;
- (void)fetchActivitiesWithTitle:(NSString *)title Complete:(CallbackArrayWhenComplete) callBack;
- (void)searchWithKeyword:(NSString *)keyword Complete:(CallbackDictWhenComplete) callBack;
- (void)fetchTechXpotWithid:(NSString *)techXpotID Complete:(CallbackDictWhenComplete) callBack;
- (void)fetchExhibitorPR:(NSString*)title Complete:(CallbackDictWhenComplete) callBack;
- (void)fetchPressReleases:(NSString*)title Complete:(CallbackDictWhenComplete) callBack;
-(void)fetchSpeaker:(NSString*)link Complete:(CallbackDictWhenComplete) callBack;

- (void)signUpWithName:(NSString *)name email:(NSString *)email password:(NSString *)password mobile:(NSString *)mobile complete:(CallbackDictWhenComplete) callBack;
- (void)signUpWithFB:(NSString *)fbid token:(NSString *) accessToken email:(NSString *) email name:(NSString *) name mobile:(NSString *) mobile complete:(CallbackDictWhenComplete) callBack;
- (void)signInWithFB:(NSString *)fbid complete:(CallbackDictWhenComplete) callBack;
- (void)signInWithEmail:(NSString *)email password:(NSString *)password complete:(CallbackDictWhenComplete) callBack;
- (void)getPasswordWithEmail:(NSString *)email complete:(CallbackDictWhenComplete) callBack;

@end
