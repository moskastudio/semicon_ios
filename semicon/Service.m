//
//  Service.m
//  Picgo
//
//  Created by Hungchi on 7/15/15.
//  Copyright (c) 2015 Hungchi. All rights reserved.
//

#import "Service.h"
#import "JSONKit.h"
#import "AFHTTPSessionManager.h"

NSString* FetchAllExhibitorSuccessNotification = @"FetchAllExhibitorSuccessNotification";
NSString* FetchAllStructuredCategorySuccessNotification = @"FetchAllStructuredCategorySuccessNotification";
NSString* FetchExhibitorByCategoryIDSuccessNotification = @"FetchExhibitorByCategoryIDSuccessNotification";
NSString* FetchExhibitorByBoothNumberSuccessNotification = @"FetchExhibitorByBoothNumberSuccessNotification";

NSString* kUserLogin = @"UserLogin";

static NSString *serverURL = @"http://semicon.moskastudio.tw/";

@interface Service()
@property NSString *language;
@end


@implementation Service

+ (id) sharedInstance
{
    static Service* sharedMyInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyInstance = [[self alloc] initService];
    });
    return sharedMyInstance;
}

- (id) initService
{
    self = [super init];
    if (self)
    {
        NSLog(@"%@", [NSLocale preferredLanguages][0]);
        if ([[NSLocale preferredLanguages][0] hasPrefix: @"zh"]) {
            _language = @"?language=zh";
        }
        else {
            _language = @"?language=en";
        }
    }
    return self;
}

- (void)fetchAllExhibitor {
    NSString *URLString = [NSString stringWithFormat:@"%@exhibitor/get_all%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);

    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        //NSLog(@"jsonDict:%@", jsonDict);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:FetchAllExhibitorSuccessNotification object:jsonDict];

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchAllStructuredCategory {
    NSString *URLString = [NSString stringWithFormat:@"%@structuredcategory/get_all%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        [[NSNotificationCenter defaultCenter] postNotificationName:FetchAllStructuredCategorySuccessNotification object:[jsonDict objectForKey:@"result"]];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchExhibitorByCategoryID:(NSString*) categoryID{
    NSString *URLString = [NSString stringWithFormat:@"%@exhibitor/get_by_category%@&categoryId=%@", serverURL, _language, categoryID];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        [[NSNotificationCenter defaultCenter] postNotificationName:FetchExhibitorByCategoryIDSuccessNotification object:[jsonDict objectForKey:@"result"]];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchExhibitorByBoothNumber:(NSString*) boothNumber{
    NSString *URLString = [NSString stringWithFormat:@"%@exhibitor/get_by_booth%@&BoothNumber=%@", serverURL, _language, boothNumber];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        [[NSNotificationCenter defaultCenter] postNotificationName:FetchExhibitorByBoothNumberSuccessNotification object:[[jsonDict objectForKey:@"result"] firstObject]];
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void)fetchExhibitorFromNumber:(NSString*) fromNumber toNumber:(NSString *) toNumber complete:(CallbackArrayWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@exhibitor/get_all%@&sort=booth&fromNumber=%@&toNumber=%@", serverURL, _language, fromNumber, toNumber];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSArray* jsonArray = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        callBack(true, jsonArray);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        callBack(false, nil);
    }];
}

- (void)fetchAllProductWithComplete:(CallbackArrayWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@product/get_all%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchProductByID:(NSString*) productID withComplete:(CallbackDictWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@product/get_by_id%@&productId=%@", serverURL, _language, productID];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchAllForumWithComplete:(CallbackArrayWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@forum/get_list%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchForumByID:(NSString*) nodeID withComplete:(CallbackDictWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@forum/get_forum%@&node=%@", serverURL, _language, nodeID];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

-(void) fetchPressReleases:(NSString *)title Complete:(CallbackDictWhenComplete)callBack{
    NSString *URLString = [NSString stringWithFormat:@"%@press/get_by_alias%@&alias=press-releases", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

-(void)fetchSpeaker:(NSString*)link Complete:(CallbackDictWhenComplete) callBack{
    NSString *URLString = [NSString stringWithFormat:@"%@speaker/get_speaker?url=%@", serverURL, link];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

-(void) fetchExhibitorPR:(NSString *)title Complete:(CallbackDictWhenComplete)callBack{
    NSString *URLString = [NSString stringWithFormat:@"%@press/get_by_alias%@&alias=exhibitor-press-release", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}
- (void)fetchPavilionWithAlias:(NSString *)alias complete:(CallbackArrayWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@pavilion/get_exhibitor%@&alias=%@", serverURL, _language, alias];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}


- (void)fetchScheduleWithComplete:(CallbackArrayWhenComplete) callBack {
    NSString *URLString = [NSString stringWithFormat:@"%@activity/get_activity_by_alias%@&alias=event-and-activities", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [[jsonDict objectForKey:@"result"] objectForKey:@"activities"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchAllADWithComplete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@ad/get_ad%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchTechXPOTWithTitle:(NSString *)title Complete:(CallbackArrayWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@techxpot/get_techxpot%@&alias=%@", serverURL, _language, title];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [[jsonDict objectForKey:@"result"] objectForKey:@"activities"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchActivitiesWithTitle:(NSString *)title Complete:(CallbackArrayWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@activity/get_activity_by_alias%@&alias=%@", serverURL, _language, title];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [[jsonDict objectForKey:@"result"] objectForKey:@"activities"]);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)searchWithKeyword:(NSString *)keyword Complete:(CallbackDictWhenComplete) callBack {
    NSDictionary *params = @{
                             @"query": keyword};
    NSString *URLString = [NSString stringWithFormat:@"%@web/search%@", serverURL, _language];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    
    [manager GET:URLString parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        NSLog(@"%@", jsonDict);
        callBack(true, jsonDict);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)signUpWithName:(NSString *)name email:(NSString *)email password:(NSString *)password mobile:(NSString *)mobile complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@user/register?name=%@&email=%@&password=%@&mobile=%@", serverURL, name, email, password, mobile];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"json: %@", jsonDict);
        callBack(true, jsonDict);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)signUpWithFB:(NSString *)fbid token:(NSString *) accessToken email:(NSString *) email name:(NSString *) name mobile:(NSString *) mobile complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@user/fb_register?fbId=%@&accessToken=%@&email=%@&name=%@&mobile=%@", serverURL, fbid, accessToken, email, name, mobile];
    
    NSString *encoded = [URLString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    [URLString cStringUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:encoded parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"json: %@", jsonDict);
        callBack(true, jsonDict);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)signInWithFB:(NSString *)fbid complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@user/fb_login?fbId=%@", serverURL, fbid];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"json: %@", jsonDict);
        callBack(true, jsonDict);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)signInWithEmail:(NSString *)email password:(NSString *)password complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@user/login?email=%@&password=%@", serverURL, email, password];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"json: %@", jsonDict);
        callBack(true, jsonDict);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)getPasswordWithEmail:(NSString *)email complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@user/forget?email=%@", serverURL, email];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager POST:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        NSLog(@"json: %@", jsonDict);
        callBack(true, jsonDict);
        
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

- (void)fetchTechXpotWithid:(NSString *)techXpotID Complete:(CallbackDictWhenComplete) callBack {
    
    NSString *URLString = [NSString stringWithFormat:@"%@techxpot/get_event%@&id=%@", serverURL, _language, techXpotID];
    NSLog(@"URLString:%@", URLString);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:URLString parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        NSString* responseStr = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSDictionary* jsonDict = [responseStr objectFromJSONStringWithParseOptions:JKParseOptionPermitTextAfterValidJSON];
        
        callBack(true, [jsonDict objectForKey:@"result"]);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"Error: %@", error);
        callBack(false, nil);
    }];
}

@end
